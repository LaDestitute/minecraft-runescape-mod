package com.ladestitute.runicages.capability.runecrafting;

import com.ladestitute.runicages.network.ClientRunecraftingSkillPacket;
import com.ladestitute.runicages.network.SimpleNetworkHandler;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RunicAgesRunecraftingCapability {

    private int maxRunecraftingLevel = 99;
    private int currentRunecraftingLevel = 1;
    private int currentRunecraftingXP = 0;
    private int NextLevelRunecraftingXP = 83;

    public void addRunecraftingLevel(Player player, int amount) {
        if (this.currentRunecraftingLevel < this.maxRunecraftingLevel) {
            this.currentRunecraftingLevel += amount;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.currentRunecraftingLevel = this.maxRunecraftingLevel;
        levelClientUpdate(player);
    }

    public void addRunecraftingXP(Player player, int amount) {
        this.currentRunecraftingXP += amount;
        levelClientUpdate(player);
    }

    public void setRunecraftingLevel(int amount) {
        this.currentRunecraftingLevel = amount;
    }

    public void setRunecraftingXP(int amount) {
        this.currentRunecraftingXP = amount;
    }

    public void setNextRunecraftingXP(int amount) {
        this.NextLevelRunecraftingXP = amount;
    }

    public int getRunecraftingLevel() {
        return currentRunecraftingLevel;
    }

    public int getRunecraftingXP() {
        return currentRunecraftingXP;
    }

    public int getNextRunecraftingXP() {
        return NextLevelRunecraftingXP;
    }

    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentrunecraftinglevel", this.currentRunecraftingLevel);
        tag.putInt("runecraftingxp", this.currentRunecraftingXP);
        tag.putInt("nextrunecraftingxp", this.NextLevelRunecraftingXP);
        return tag;
    }

    public void deserializeNBT(CompoundTag tag) {
        this.currentRunecraftingLevel = tag.getInt("currentrunecraftinglevel");
        this.NextLevelRunecraftingXP = tag.getInt("nextrunecraftingxp");
        this.currentRunecraftingXP = tag.getInt("runecraftingxp");
    }

    //A method for sending a packet to the client to sync information for updating GUis/etc
    //This is done in the capability so this method is
    //included in any methods that add/subtract/set mana to make syncing easier
    public static void levelClientUpdate(Player player) {
        if (!player.level().isClientSide()) {
            ServerPlayer serverPlayer = (ServerPlayer) player;
            player.getCapability(RunicAgesRunecraftingCapability.Provider.RUNECRAFTING_LEVEL).ifPresent(cap ->
                    SimpleNetworkHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> serverPlayer),
                            new ClientRunecraftingSkillPacket(cap.currentRunecraftingLevel, cap.currentRunecraftingXP, cap.NextLevelRunecraftingXP)));
        }
    }

    public static Tag writeNBT(Capability<RunicAgesRunecraftingCapability> capability, RunicAgesRunecraftingCapability instance, Direction side) {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentrunecraftinglevel", instance.getRunecraftingLevel());
        tag.putInt("runecraftingxp", instance.getRunecraftingXP());
        tag.putInt("nextrunecraftingxp", instance.getNextRunecraftingXP());
        return tag;
    }

    public static void readNBT(Capability<RunicAgesRunecraftingCapability> capability, RunicAgesRunecraftingCapability instance, Direction side, Tag nbt) {
        instance.setRunecraftingLevel(((CompoundTag) nbt).getInt("currentrunecraftinglevel"));
        instance.setRunecraftingXP(((CompoundTag) nbt).getInt("runecraftingxp"));
        instance.setNextRunecraftingXP(((CompoundTag) nbt).getInt("nextrunecraftingxp"));
    }

    public static class Provider implements ICapabilitySerializable<Tag> {
        public static Capability<RunicAgesRunecraftingCapability> RUNECRAFTING_LEVEL = CapabilityManager.get(new CapabilityToken<>(){});

        @Nonnull
        private final RunicAgesRunecraftingCapability instance;

        private final LazyOptional<RunicAgesRunecraftingCapability> handler;

        public Provider() {
            instance = new RunicAgesRunecraftingCapability();
            handler = LazyOptional.of(this::getInstance);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return RUNECRAFTING_LEVEL.orEmpty(cap, handler);
        }

        public static LazyOptional<RunicAgesRunecraftingCapability> getFrom(Player player) {
            return player.getCapability(RUNECRAFTING_LEVEL);
        }

        public void invalidate() {
            handler.invalidate();
        }

        public RunicAgesRunecraftingCapability getInstance() {
            return instance;
        }

        @Override
        public Tag serializeNBT() {
            return RunicAgesRunecraftingCapability.writeNBT(RUNECRAFTING_LEVEL, instance, null);
        }

        @Override
        public void deserializeNBT(Tag nbt) {
            RunicAgesRunecraftingCapability.readNBT(RUNECRAFTING_LEVEL, instance, null, nbt);
        }
    }

}
