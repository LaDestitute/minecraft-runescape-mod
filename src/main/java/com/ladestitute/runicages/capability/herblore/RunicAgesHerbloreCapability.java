package com.ladestitute.runicages.capability.herblore;
import com.ladestitute.runicages.network.ClientHerbloreSkillPacket;
import com.ladestitute.runicages.network.SimpleNetworkHandler;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RunicAgesHerbloreCapability {

    private int maxHerbloreLevel = 99;
    private int currentHerbloreLevel = 1;
    private int currentHerbloreXP = 0;
    private int NextLevelHerbloreXP = 83;

    public void addHerbloreLevel(Player player, int amount) {
        if (this.currentHerbloreLevel < this.maxHerbloreLevel) {
            this.currentHerbloreLevel += amount;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.currentHerbloreLevel = this.maxHerbloreLevel;
        levelClientUpdate(player);
    }

    public void addHerbloreXP(Player player, int amount) {
        this.currentHerbloreXP += amount;
        levelClientUpdate(player);
    }

    public void setHerbloreLevel(int amount) {
        this.currentHerbloreLevel = amount;
    }

    public void setHerbloreXP(int amount) {
        this.currentHerbloreXP = amount;
    }

    public void setNextHerbloreXP(int amount) {
        this.NextLevelHerbloreXP = amount;
    }

    public int getHerbloreLevel() {
        return currentHerbloreLevel;
    }

    public int getHerbloreXP() {
        return currentHerbloreXP;
    }

    public int getNextHerbloreXP() {
        return NextLevelHerbloreXP;
    }

    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentherblorelevel", this.currentHerbloreLevel);
        tag.putInt("herblorexp", this.currentHerbloreXP);
        tag.putInt("nextherblorexp", this.NextLevelHerbloreXP);
        return tag;
    }

    public void deserializeNBT(CompoundTag tag) {
        this.currentHerbloreLevel = tag.getInt("currentherblorelevel");
        this.NextLevelHerbloreXP = tag.getInt("nextherblorexp");
        this.currentHerbloreXP = tag.getInt("herblorexp");
    }

    //A method for sending a packet to the client to sync information for updating GUis/etc
    //This is done in the capability so this method is
    //included in any methods that add/subtract/set mana to make syncing easier
    public static void levelClientUpdate(Player player) {
        if (!player.level().isClientSide()) {
            ServerPlayer serverPlayer = (ServerPlayer) player;
            player.getCapability(RunicAgesHerbloreCapability.Provider.HERBLORE_LEVEL).ifPresent(cap ->
                    SimpleNetworkHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> serverPlayer),
                            new ClientHerbloreSkillPacket(cap.currentHerbloreLevel, cap.currentHerbloreXP, cap.NextLevelHerbloreXP)));
        }
    }

    public static Tag writeNBT(Capability<RunicAgesHerbloreCapability> capability, RunicAgesHerbloreCapability instance, Direction side) {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentherblorelevel", instance.getHerbloreLevel());
        tag.putInt("herblorexp", instance.getHerbloreXP());
        tag.putInt("nextherblorexp", instance.getNextHerbloreXP());
        return tag;
    }

    public static void readNBT(Capability<RunicAgesHerbloreCapability> capability, RunicAgesHerbloreCapability instance, Direction side, Tag nbt) {
        instance.setHerbloreLevel(((CompoundTag) nbt).getInt("currentherblorelevel"));
        instance.setHerbloreXP(((CompoundTag) nbt).getInt("herblorexp"));
        instance.setNextHerbloreXP(((CompoundTag) nbt).getInt("nextherblorexp"));
    }

    public static class Provider implements ICapabilitySerializable<Tag> {
        public static Capability<RunicAgesHerbloreCapability> HERBLORE_LEVEL = CapabilityManager.get(new CapabilityToken<>(){});

        @Nonnull
        private final RunicAgesHerbloreCapability instance;

        private final LazyOptional<RunicAgesHerbloreCapability> handler;

        public Provider() {
            instance = new RunicAgesHerbloreCapability();
            handler = LazyOptional.of(this::getInstance);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return HERBLORE_LEVEL.orEmpty(cap, handler);
        }

        public static LazyOptional<RunicAgesHerbloreCapability> getFrom(Player player) {
            return player.getCapability(HERBLORE_LEVEL);
        }

        public void invalidate() {
            handler.invalidate();
        }

        public RunicAgesHerbloreCapability getInstance() {
            return instance;
        }

        @Override
        public Tag serializeNBT() {
            return RunicAgesHerbloreCapability.writeNBT(HERBLORE_LEVEL, instance, null);
        }

        @Override
        public void deserializeNBT(Tag nbt) {
            RunicAgesHerbloreCapability.readNBT(HERBLORE_LEVEL, instance, null, nbt);
        }
    }

}
