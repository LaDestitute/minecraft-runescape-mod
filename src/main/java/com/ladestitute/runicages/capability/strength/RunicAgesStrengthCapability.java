package com.ladestitute.runicages.capability.strength;

import com.ladestitute.runicages.network.ClientStrengthUpdatePacket;
import com.ladestitute.runicages.network.SimpleNetworkHandler;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RunicAgesStrengthCapability {

    private int maxStrengthLevel = 99;
    private int currentStrengthLevel = 1;
    private int currentStrengthXP = 0;
    private int NextLevelStrengthXP = 83;

    public void addStrengthLevel(Player player, int amount) {
        if (this.currentStrengthLevel < this.maxStrengthLevel) {
            this.currentStrengthLevel += amount;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.currentStrengthLevel = this.maxStrengthLevel;
        levelClientUpdate(player);
    }

    public void addStrengthXP(Player player, int amount) {
        this.currentStrengthXP += amount;
        levelClientUpdate(player);
    }

    public void setStrengthLevel(int amount) {
        this.currentStrengthLevel = amount;
    }

    public void setStrengthXP(int amount) {
        this.currentStrengthXP = amount;
    }

    public void setNextStrengthXP(int amount) {
        this.NextLevelStrengthXP = amount;
    }

    public int getStrengthLevel() {
        return currentStrengthLevel;
    }

    public int getStrengthXP() {
        return currentStrengthXP;
    }

    public int getNextStrengthXP() {
        return NextLevelStrengthXP;
    }

    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentstrengthlevel", this.currentStrengthLevel);
        tag.putInt("strengthxp", this.currentStrengthXP);
        tag.putInt("nextstrengthxp", this.NextLevelStrengthXP);
        return tag;
    }

    public void deserializeNBT(CompoundTag tag) {
        this.currentStrengthLevel = tag.getInt("currentstrengthlevel");
        this.NextLevelStrengthXP = tag.getInt("nextstrengthxp");
        this.currentStrengthXP = tag.getInt("strengthxp");
    }

    //A method for sending a packet to the client to sync information for updating GUis/etc
    //This is done in the capability so this method is
    //included in any methods that add/subtract/set mana to make syncing easier
    public static void levelClientUpdate(Player player) {
        if (!player.level().isClientSide()) {
            ServerPlayer serverPlayer = (ServerPlayer) player;
            player.getCapability(RunicAgesStrengthCapability.Provider.STRENGTH_LEVEL).ifPresent(cap ->
                    SimpleNetworkHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> serverPlayer),
                            new ClientStrengthUpdatePacket(cap.currentStrengthLevel, cap.currentStrengthXP, cap.NextLevelStrengthXP)));
        }
    }

    public static Tag writeNBT(Capability<RunicAgesStrengthCapability> capability, RunicAgesStrengthCapability instance, Direction side) {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentstrengthlevel", instance.getStrengthLevel());
        tag.putInt("strengthxp", instance.getStrengthXP());
        tag.putInt("nextstrengthxp", instance.getNextStrengthXP());
        return tag;
    }

    public static void readNBT(Capability<RunicAgesStrengthCapability> capability, RunicAgesStrengthCapability instance, Direction side, Tag nbt) {
        instance.setStrengthLevel(((CompoundTag) nbt).getInt("currentstrengthlevel"));
        instance.setStrengthXP(((CompoundTag) nbt).getInt("strengthxp"));
        instance.setNextStrengthXP(((CompoundTag) nbt).getInt("nextstrengthxp"));
    }

    public static class Provider implements ICapabilitySerializable<Tag> {
        public static Capability<RunicAgesStrengthCapability> STRENGTH_LEVEL = CapabilityManager.get(new CapabilityToken<>(){});

        @Nonnull
        private final RunicAgesStrengthCapability instance;

        private final LazyOptional<RunicAgesStrengthCapability> handler;

        public Provider() {
            instance = new RunicAgesStrengthCapability();
            handler = LazyOptional.of(this::getInstance);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return STRENGTH_LEVEL.orEmpty(cap, handler);
        }

        public static LazyOptional<RunicAgesStrengthCapability> getFrom(Player player) {
            return player.getCapability(STRENGTH_LEVEL);
        }

        public void invalidate() {
            handler.invalidate();
        }

        public RunicAgesStrengthCapability getInstance() {
            return instance;
        }

        @Override
        public Tag serializeNBT() {
            return RunicAgesStrengthCapability.writeNBT(STRENGTH_LEVEL, instance, null);
        }

        @Override
        public void deserializeNBT(Tag nbt) {
            RunicAgesStrengthCapability.readNBT(STRENGTH_LEVEL, instance, null, nbt);
        }
    }

}

