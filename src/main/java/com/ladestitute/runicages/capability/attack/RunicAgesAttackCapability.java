package com.ladestitute.runicages.capability.attack;

import com.ladestitute.runicages.network.ClientAttackSkillPacket;
import com.ladestitute.runicages.network.SimpleNetworkHandler;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RunicAgesAttackCapability {

    private int maxAttackLevel = 99;
    private int currentAttackLevel = 1;
    private int currentAttackXP = 0;
    private int NextLevelAttackXP = 83;
    private int attackboost = 0;
    private int attackboostdraintimer = 0;
    private int invisibleattackboost = 0;

    public void addAttackLevel(Player player, int amount) {
        if (this.currentAttackLevel < this.maxAttackLevel) {
            this.currentAttackLevel += amount;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.currentAttackLevel = this.maxAttackLevel;
        levelClientUpdate(player);
    }

    public void addAttackXP(Player player, int amount) {
        this.currentAttackXP += amount;
        levelClientUpdate(player);
    }

    public void setAttackLevel(int amount) {
        this.currentAttackLevel = amount;
    }

    public void setAttackXP(int amount) {
        this.currentAttackXP = amount;
    }

    public void setNextAttackXP(int amount) {
        this.NextLevelAttackXP = amount;
    }

    public int getAttackLevel() {
        return currentAttackLevel;
    }

    public int getAttackXP() {
        return currentAttackXP;
    }

    public int getNextAttackXP() {
        return NextLevelAttackXP;
    }

    //Visible boost stuff

    public void subAttackBoost(Player player, int amount) {
        this.attackboost -= amount;
        levelClientUpdate(player);
    }

    public void setAttackBoost(int amount) {
        this.attackboost = amount;
    }

    public int getAttackBoost() {
        return attackboost;
    }

    // Visible boost timers

    public void incrementattackboostdraintimer(Player player, int amount) {
        this.attackboostdraintimer += amount;
        levelClientUpdate(player);
    }

    public void setattackboostdraintimer(int amount) {
        this.attackboostdraintimer = amount;
    }

    public int getAttackBoostTimer() {
        return attackboostdraintimer;
    }

    //
    //Invisible boost stuff

    public void subInvisibleAttackBoost(Player player, int amount) {
        this.invisibleattackboost -= amount;
        levelClientUpdate(player);
    }

    public void setInvisibleAttackBoost(int amount) {
        this.invisibleattackboost = amount;
    }

    public int getInvisibleAttackBoost() {
        return invisibleattackboost;
    }

    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentattacklevel", this.currentAttackLevel);
        tag.putInt("attackxp", this.currentAttackXP);
        tag.putInt("nextattackxp", this.NextLevelAttackXP);
        return tag;
    }

    public void deserializeNBT(CompoundTag tag) {
        this.currentAttackLevel = tag.getInt("currentattacklevel");
        this.NextLevelAttackXP = tag.getInt("nextattackxp");
        this.currentAttackXP = tag.getInt("attackxp");
    }

    //A method for sending a packet to the client to sync information for updating GUis/etc
    //This is done in the capability so this method is
    //included in any methods that add/subtract/set mana to make syncing easier
    public static void levelClientUpdate(Player player) {
        if (!player.level().isClientSide()) {
            ServerPlayer serverPlayer = (ServerPlayer) player;
            player.getCapability(RunicAgesAttackCapability.Provider.ATTACK_LEVEL).ifPresent(cap ->
                    SimpleNetworkHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> serverPlayer),
                            new ClientAttackSkillPacket(cap.currentAttackLevel, cap.currentAttackXP, cap.NextLevelAttackXP, cap.attackboost, cap.attackboostdraintimer, cap.invisibleattackboost)));
        }
    }

    public static Tag writeNBT(Capability<RunicAgesAttackCapability> capability, RunicAgesAttackCapability instance, Direction side) {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentattacklevel", instance.getAttackLevel());
        tag.putInt("attackxp", instance.getAttackXP());
        tag.putInt("nextattackxp", instance.getNextAttackXP());
        tag.putInt("attackboost", instance.getAttackBoost());
        tag.putInt("attackboostdraintimer", instance.getAttackBoostTimer());
        tag.putInt("invisibleattackboost", instance.getInvisibleAttackBoost());
        return tag;
    }

    public static void readNBT(Capability<RunicAgesAttackCapability> capability, RunicAgesAttackCapability instance, Direction side, Tag nbt) {
        instance.setAttackLevel(((CompoundTag) nbt).getInt("currentattacklevel"));
        instance.setAttackXP(((CompoundTag) nbt).getInt("attackxp"));
        instance.setNextAttackXP(((CompoundTag) nbt).getInt("nextattackxp"));
        instance.setAttackBoost(((CompoundTag) nbt).getInt("attackboost"));
        instance.setattackboostdraintimer(((CompoundTag) nbt).getInt("attackboostdraintimer"));
        instance.setInvisibleAttackBoost(((CompoundTag) nbt).getInt("invisibleattackboost"));
    }

    public static class Provider implements ICapabilitySerializable<Tag> {
        public static Capability<RunicAgesAttackCapability> ATTACK_LEVEL = CapabilityManager.get(new CapabilityToken<>(){});

        @Nonnull
        private final RunicAgesAttackCapability instance;

        private final LazyOptional<RunicAgesAttackCapability> handler;

        public Provider() {
            instance = new RunicAgesAttackCapability();
            handler = LazyOptional.of(this::getInstance);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return ATTACK_LEVEL.orEmpty(cap, handler);
        }

        public static LazyOptional<RunicAgesAttackCapability> getFrom(Player player) {
            return player.getCapability(ATTACK_LEVEL);
        }

        public void invalidate() {
            handler.invalidate();
        }

        public RunicAgesAttackCapability getInstance() {
            return instance;
        }

        @Override
        public Tag serializeNBT() {
            return RunicAgesAttackCapability.writeNBT(ATTACK_LEVEL, instance, null);
        }

        @Override
        public void deserializeNBT(Tag nbt) {
            RunicAgesAttackCapability.readNBT(ATTACK_LEVEL, instance, null, nbt);
        }
    }

}


