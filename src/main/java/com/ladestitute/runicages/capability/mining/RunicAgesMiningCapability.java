package com.ladestitute.runicages.capability.mining;

import com.ladestitute.runicages.network.ClientMiningSkillPacket;
import com.ladestitute.runicages.network.SimpleNetworkHandler;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RunicAgesMiningCapability {

    private int maxMiningLevel = 99;
    private int currentMiningLevel = 1;
    private int currentMiningXP = 0;
    private int NextLevelMiningXP = 83;
    private int starterkit = 0;

    public void addMiningLevel(Player player, int amount) {
        if (this.currentMiningLevel < this.maxMiningLevel) {
            this.currentMiningLevel += amount;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.currentMiningLevel = this.maxMiningLevel;
        levelClientUpdate(player);
    }

    public void setstarterkitobtained(int amount) {
            this.starterkit = amount;
    }

    public void addMiningXP(Player player, int amount) {
            this.currentMiningXP += amount;
        levelClientUpdate(player);
    }

    public void setMiningLevel(int amount) {
        this.currentMiningLevel = amount;
    }

    public void setMiningXP(int amount) {
        this.currentMiningXP = amount;
    }

    public void setNextMiningXP(int amount) {
        this.NextLevelMiningXP = amount;
    }

    public int getMiningLevel() {
        return currentMiningLevel;
    }

    public int getstarterkit() {
        return starterkit;
    }

    public int getMiningXP() {
        return currentMiningXP;
    }

    public int getNextMiningXP() {
        return NextLevelMiningXP;
    }

    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentmininglevel", this.currentMiningLevel);
        tag.putInt("miningxp", this.currentMiningXP);
        tag.putInt("nextminingxp", this.NextLevelMiningXP);
        return tag;
    }

    public void deserializeNBT(CompoundTag tag) {
        this.currentMiningLevel = tag.getInt("currentmininglevel");
        this.NextLevelMiningXP = tag.getInt("nextminingxp");
        this.currentMiningXP = tag.getInt("miningxp");
    }


    //A method for sending a packet to the client to sync information for updating GUis/etc
    //This is done in the capability so this method is
    //included in any methods that add/subtract/set mana to make syncing easier
    public static void levelClientUpdate(Player player) {
        if (!player.level().isClientSide()) {
            ServerPlayer serverPlayer = (ServerPlayer) player;
            player.getCapability(Provider.MINING_LEVEL).ifPresent(cap ->
                    SimpleNetworkHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> serverPlayer),
                            new ClientMiningSkillPacket(cap.currentMiningLevel, cap.currentMiningXP, cap.NextLevelMiningXP, cap.starterkit)));
        }
    }

    public static Tag writeNBT(Capability<RunicAgesMiningCapability> capability, RunicAgesMiningCapability instance, Direction side) {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentmininglevel", instance.getMiningLevel());
        tag.putInt("miningxp", instance.getMiningXP());
        tag.putInt("nextminingxp", instance.getNextMiningXP());
        tag.putInt("starterkit", instance.getstarterkit());
        return tag;
    }

    public static void readNBT(Capability<RunicAgesMiningCapability> capability, RunicAgesMiningCapability instance, Direction side, Tag nbt) {
        instance.setMiningLevel(((CompoundTag) nbt).getInt("currentmininglevel"));
        instance.setMiningXP(((CompoundTag) nbt).getInt("miningxp"));
        instance.setNextMiningXP(((CompoundTag) nbt).getInt("nextminingxp"));
        instance.setstarterkitobtained(((CompoundTag) nbt).getInt("starterkit"));
    }

    public static class Provider implements ICapabilitySerializable<Tag> {
        public static Capability<RunicAgesMiningCapability> MINING_LEVEL = CapabilityManager.get(new CapabilityToken<>(){});

        @Nonnull
        private final RunicAgesMiningCapability instance;

        private final LazyOptional<RunicAgesMiningCapability> handler;

        public Provider() {
            instance = new RunicAgesMiningCapability();
            handler = LazyOptional.of(this::getInstance);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return MINING_LEVEL.orEmpty(cap, handler);
        }

        public static LazyOptional<RunicAgesMiningCapability> getFrom(Player player) {
            return player.getCapability(MINING_LEVEL);
        }

        public void invalidate() {
            handler.invalidate();
        }

        public RunicAgesMiningCapability getInstance() {
            return instance;
        }

        @Override
        public Tag serializeNBT() {
            return RunicAgesMiningCapability.writeNBT(MINING_LEVEL, instance, null);
        }

        @Override
        public void deserializeNBT(Tag nbt) {
            RunicAgesMiningCapability.readNBT(MINING_LEVEL, instance, null, nbt);
        }
    }

}
