package com.ladestitute.runicages.capability.agility;

import com.ladestitute.runicages.network.ClientAgilitySkillPacket;
import com.ladestitute.runicages.network.SimpleNetworkHandler;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RunicAgesAgilityCapability {

    private int maxAgilityLevel = 99;
    private int currentAgilityLevel = 1;
    private int currentAgilityXP = 0;
    private int NextLevelAgilityXP = 83;
    private int agilityboost = 0;
    private int agilityboostdraintimer = 0;
    private int invisibleagilityboost = 0;

    public void addAgilityLevel(Player player, int amount) {
        if (this.currentAgilityLevel < this.maxAgilityLevel) {
            this.currentAgilityLevel += amount;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.currentAgilityLevel = this.maxAgilityLevel;
        levelClientUpdate(player);
    }

    public void addAgilityXP(Player player, int amount) {
        this.currentAgilityXP += amount;
        levelClientUpdate(player);
    }

    public void setAgilityLevel(int amount) {
        this.currentAgilityLevel = amount;
    }

    public void setAgilityXP(int amount) {
        this.currentAgilityXP = amount;
    }

    public void setNextAgilityXP(int amount) {
        this.NextLevelAgilityXP = amount;
    }

    public int getAgilityLevel() {
        return currentAgilityLevel;
    }

    public int getAgilityXP() {
        return currentAgilityXP;
    }

    public int getNextAgilityXP() {
        return NextLevelAgilityXP;
    }

    //Visible boost stuff

    public void subAgilityBoost(Player player, int amount) {
        this.agilityboost -= amount;
        levelClientUpdate(player);
    }

    public void setAgilityBoost(int amount) {
        this.agilityboost = amount;
    }

    public int getAgilityBoost() {
        return agilityboost;
    }

    // Visible boost timers

    public void incrementagilityboostdraintimer(Player player, int amount) {
        this.agilityboostdraintimer += amount;
        levelClientUpdate(player);
    }

    public void setagilityboostdraintimer(int amount) {
        this.agilityboostdraintimer = amount;
    }

    public int getAgilityBoostTimer() {
        return agilityboostdraintimer;
    }

    //
    //Invisible boost stuff

    public void subInvisibleAgilityBoost(Player player, int amount) {
        this.invisibleagilityboost -= amount;
        levelClientUpdate(player);
    }

    public void setInvisibleAgilityBoost(int amount) {
        this.invisibleagilityboost = amount;
    }

    public int getInvisibleAgilityBoost() {
        return invisibleagilityboost;
    }

    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentagilitylevel", this.currentAgilityLevel);
        tag.putInt("agilityxp", this.currentAgilityXP);
        tag.putInt("nextagilityxp", this.NextLevelAgilityXP);
        return tag;
    }

    public void deserializeNBT(CompoundTag tag) {
        this.currentAgilityLevel = tag.getInt("currentagilitylevel");
        this.NextLevelAgilityXP = tag.getInt("nextagilityxp");
        this.currentAgilityXP = tag.getInt("agilityxp");
    }

    //A method for sending a packet to the client to sync information for updating GUis/etc
    //This is done in the capability so this method is
    //included in any methods that add/subtract/set mana to make syncing easier
    public static void levelClientUpdate(Player player) {
        if (!player.level().isClientSide()) {
            ServerPlayer serverPlayer = (ServerPlayer) player;
            player.getCapability(RunicAgesAgilityCapability.Provider.AGILITY_LEVEL).ifPresent(cap ->
                    SimpleNetworkHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> serverPlayer),
                            new ClientAgilitySkillPacket(cap.currentAgilityLevel, cap.currentAgilityXP, cap.NextLevelAgilityXP, cap.agilityboost, cap.agilityboostdraintimer, cap.invisibleagilityboost)));
        }
    }

    public static Tag writeNBT(Capability<RunicAgesAgilityCapability> capability, RunicAgesAgilityCapability instance, Direction side) {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentagilitylevel", instance.getAgilityLevel());
        tag.putInt("agilityxp", instance.getAgilityXP());
        tag.putInt("nextagilityxp", instance.getNextAgilityXP());
        tag.putInt("agilityboost", instance.getAgilityBoost());
        tag.putInt("agilityboostdraintimer", instance.getAgilityBoostTimer());
        tag.putInt("invisibleagilityboost", instance.getInvisibleAgilityBoost());
        return tag;
    }

    public static void readNBT(Capability<RunicAgesAgilityCapability> capability, RunicAgesAgilityCapability instance, Direction side, Tag nbt) {
        instance.setAgilityLevel(((CompoundTag) nbt).getInt("currentagilitylevel"));
        instance.setAgilityXP(((CompoundTag) nbt).getInt("agilityxp"));
        instance.setNextAgilityXP(((CompoundTag) nbt).getInt("nextagilityxp"));
        instance.setAgilityBoost(((CompoundTag) nbt).getInt("agilityboost"));
        instance.setagilityboostdraintimer(((CompoundTag) nbt).getInt("agilityboostdraintimer"));
        instance.setInvisibleAgilityBoost(((CompoundTag) nbt).getInt("invisibleagilityboost"));
    }

    public static class Provider implements ICapabilitySerializable<Tag> {
        public static Capability<RunicAgesAgilityCapability> AGILITY_LEVEL = CapabilityManager.get(new CapabilityToken<>(){});

        @Nonnull
        private final RunicAgesAgilityCapability instance;

        private final LazyOptional<RunicAgesAgilityCapability> handler;

        public Provider() {
            instance = new RunicAgesAgilityCapability();
            handler = LazyOptional.of(this::getInstance);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return AGILITY_LEVEL.orEmpty(cap, handler);
        }

        public static LazyOptional<RunicAgesAgilityCapability> getFrom(Player player) {
            return player.getCapability(AGILITY_LEVEL);
        }

        public void invalidate() {
            handler.invalidate();
        }

        public RunicAgesAgilityCapability getInstance() {
            return instance;
        }

        @Override
        public Tag serializeNBT() {
            return RunicAgesAgilityCapability.writeNBT(AGILITY_LEVEL, instance, null);
        }

        @Override
        public void deserializeNBT(Tag nbt) {
            RunicAgesAgilityCapability.readNBT(AGILITY_LEVEL, instance, null, nbt);
        }
    }

}
