package com.ladestitute.runicages.capability.woodcutting;

import com.ladestitute.runicages.network.ClientWoodcuttingSkillPacket;
import com.ladestitute.runicages.network.SimpleNetworkHandler;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RunicAgesWoodcuttingCapability {

    private int maxWoodcuttingLevel = 99;
    private int currentWoodcuttingLevel = 1;
    private int currentWoodcuttingXP = 0;
    private int NextLevelWoodcuttingXP = 83;

    public void addWoodcuttingLevel(Player player, int amount) {
        if (this.currentWoodcuttingLevel < this.maxWoodcuttingLevel) {
            this.currentWoodcuttingLevel += amount;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.currentWoodcuttingLevel = this.maxWoodcuttingLevel;
        levelClientUpdate(player);
    }

    public void addWoodcuttingXP(Player player, int amount) {
        this.currentWoodcuttingXP += amount;
        levelClientUpdate(player);
    }

    public void setWoodcuttingLevel(int amount) {
        this.currentWoodcuttingLevel = amount;
    }

    public void setWoodcuttingXP(int amount) {
        this.currentWoodcuttingXP = amount;
    }

    public void setNextWoodcuttingXP(int amount) {
        this.NextLevelWoodcuttingXP = amount;
    }

    public int getWoodcuttingLevel() {
        return currentWoodcuttingLevel;
    }

    public int getWoodcuttingXP() {
        return currentWoodcuttingXP;
    }

    public int getNextWoodcuttingXP() {
        return NextLevelWoodcuttingXP;
    }

    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentwoodcuttinglevel", this.currentWoodcuttingLevel);
        tag.putInt("woodcuttingxp", this.currentWoodcuttingXP);
        tag.putInt("nextwoodcuttingxp", this.NextLevelWoodcuttingXP);
        return tag;
    }

    public void deserializeNBT(CompoundTag tag) {
        this.currentWoodcuttingLevel = tag.getInt("currentwoodcuttinglevel");
        this.NextLevelWoodcuttingXP = tag.getInt("nextwoodcuttingxp");
        this.currentWoodcuttingXP = tag.getInt("woodcuttingxp");
    }

    //A method for sending a packet to the client to sync information for updating GUis/etc
    //This is done in the capability so this method is
    //included in any methods that add/subtract/set mana to make syncing easier
    public static void levelClientUpdate(Player player) {
        if (!player.level().isClientSide()) {
            ServerPlayer serverPlayer = (ServerPlayer) player;
            player.getCapability(RunicAgesWoodcuttingCapability.Provider.WOODCUTTING_LEVEL).ifPresent(cap ->
                    SimpleNetworkHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> serverPlayer),
                            new ClientWoodcuttingSkillPacket(cap.currentWoodcuttingLevel, cap.currentWoodcuttingXP, cap.NextLevelWoodcuttingXP)));
        }
    }

    public static Tag writeNBT(Capability<RunicAgesWoodcuttingCapability> capability, RunicAgesWoodcuttingCapability instance, Direction side) {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentwoodcuttinglevel", instance.getWoodcuttingLevel());
        tag.putInt("woodcuttingxp", instance.getWoodcuttingXP());
        tag.putInt("nextwoodcuttingxp", instance.getNextWoodcuttingXP());
        return tag;
    }

    public static void readNBT(Capability<RunicAgesWoodcuttingCapability> capability, RunicAgesWoodcuttingCapability instance, Direction side, Tag nbt) {
        instance.setWoodcuttingLevel(((CompoundTag) nbt).getInt("currentwoodcuttinglevel"));
        instance.setWoodcuttingXP(((CompoundTag) nbt).getInt("woodcuttingxp"));
        instance.setNextWoodcuttingXP(((CompoundTag) nbt).getInt("nextwoodcuttingxp"));
    }

    public static class Provider implements ICapabilitySerializable<Tag> {
        public static Capability<RunicAgesWoodcuttingCapability> WOODCUTTING_LEVEL = CapabilityManager.get(new CapabilityToken<>(){});

        @Nonnull
        private final RunicAgesWoodcuttingCapability instance;

        private final LazyOptional<RunicAgesWoodcuttingCapability> handler;

        public Provider() {
            instance = new RunicAgesWoodcuttingCapability();
            handler = LazyOptional.of(this::getInstance);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return WOODCUTTING_LEVEL.orEmpty(cap, handler);
        }

        public static LazyOptional<RunicAgesWoodcuttingCapability> getFrom(Player player) {
            return player.getCapability(WOODCUTTING_LEVEL);
        }

        public void invalidate() {
            handler.invalidate();
        }

        public RunicAgesWoodcuttingCapability getInstance() {
            return instance;
        }

        @Override
        public Tag serializeNBT() {
            return RunicAgesWoodcuttingCapability.writeNBT(WOODCUTTING_LEVEL, instance, null);
        }

        @Override
        public void deserializeNBT(Tag nbt) {
            RunicAgesWoodcuttingCapability.readNBT(WOODCUTTING_LEVEL, instance, null, nbt);
        }
    }

}

