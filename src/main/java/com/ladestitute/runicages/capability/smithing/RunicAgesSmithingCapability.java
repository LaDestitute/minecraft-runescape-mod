package com.ladestitute.runicages.capability.smithing;

import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.network.ClientSmithingSkillPacket;
import com.ladestitute.runicages.network.SimpleNetworkHandler;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RunicAgesSmithingCapability {

    //Using a float makes the meter behave strangely, use ints instead if you copy the example
    //As expected with ints, you'll have to use whole numbers, 1 for half-sphere, 2 for a whole
    private int maxSmithingLevel = 99;
    private int currentSmithingLevel = 1;
    private int currentSmithingXP = 0;
    private int nextLevelSmithingXP = 83;
    private int usingfurnace = 0;
    private int smelting = 0;
    private int bartype = 0;
    private int smeltcount = 0;
    private int smeltticks = 0;
    private int smeltstage = 0;
    private int makecount = 1;
    private boolean makingall = false;

    public void addSmithingLevel(Player player, int amount) {
        if (this.currentSmithingLevel < this.maxSmithingLevel) {
            this.currentSmithingLevel += amount;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.currentSmithingLevel = this.maxSmithingLevel;
        levelClientUpdate(player);
    }

    public void addSmithingXP(Player player, int amount) {
        this.currentSmithingXP += amount;
        levelClientUpdate(player);
    }

    //This is the only method where you may need to manually sync like the example commented out in the eventhandler
    public void setSmithingLevel(int amount) {
        this.currentSmithingLevel = amount;
    }
    public void setSmithingXP(int amount) {
        this.currentSmithingXP = amount;
    }
    public void setNextSmithingXP(int amount) {
        this.nextLevelSmithingXP = amount;
    }

    public int getSmithingLevel() {
        return currentSmithingLevel;
    }

    public int getSmithingXP() {
        return currentSmithingXP;
    }

    public int getNextSmithingXP() {
        return nextLevelSmithingXP;
    }

    public int getmaxSmithingLevel() {
        return maxSmithingLevel;
    }

    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentsmithinglevel", this.currentSmithingLevel);
        tag.putInt("smithingxp", this.currentSmithingXP);
        tag.putInt("nextsmithingxp", this.nextLevelSmithingXP);
        return tag;
    }

    public void deserializeNBT(CompoundTag tag) {
        this.currentSmithingLevel = tag.getInt("currentsmithinglevel");
        this.nextLevelSmithingXP = tag.getInt("nextsmithingxp");
        this.currentSmithingXP = tag.getInt("smithingxp");
    }

    //A method for sending a packet to the client to sync information for updating GUis/etc
    //This is done in the capability so this method is
    //included in any methods that add/subtract/set mana to make syncing easier
    public static void levelClientUpdate(Player player) {
        if (!player.level().isClientSide()) {
            ServerPlayer serverPlayer = (ServerPlayer) player;
            player.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(cap ->
                    SimpleNetworkHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> serverPlayer),
                            new ClientSmithingSkillPacket(cap.currentSmithingLevel, cap.currentSmithingXP, cap.nextLevelSmithingXP, cap.usingfurnace, cap.smelting,
                                    cap.bartype, cap.smeltcount, cap.smeltticks, cap.smeltstage, cap.makecount, cap.makingall)));
        }
    }

    //Furnace stuff
    public int getUsingFurnace() {
        return usingfurnace;
    }
    public int getIsSmelting() {
        return smelting;
    }
    public int getBarType() {
        return bartype;
    }
    public int getSmeltCount() {
        return smeltcount;
    }
    public int getSmeltTicks() {
        return smeltticks;
    }
    public int getSmeltStage() {
        return smeltstage;
    }
    public int getMakeCount() {
        return makecount;
    }
    public boolean getMakingAll() {
        return makingall;
    }

    public void setUsingFurnace(int amount) {
        this.usingfurnace = amount;
    }
    public void setSmelting(int amount) {
        this.smelting = amount;
    }
    public void setBarType(int amount) {
        this.bartype = amount;
    }
    public void setSmeltCount(int amount) {
        this.smeltcount = amount;
    }
    public void setSmeltTicks(int amount) {
        this.smeltticks = amount;
    }
    public void setSmeltStage(int amount) {
        this.smeltstage = amount;
    }
    public void setMakeCount(int amount) {
        this.makecount = amount;
    }
    public void setMakingAll(boolean amount) {
        this.makingall = amount;
    }

    public void addSmeltCount(int amount) {
        this.smeltcount += amount;
    }
    public void addSmeltTicks(int amount) {
        this.smeltticks += amount;
    }


//     player.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(s ->
//    {
//        if(s.getUsingFurnace() == 1) {
//            s.setSmelting(1);
//            s.setBarType(1);
//            levelClientUpdate(player);
//        }
//    });

    public static void smeltBronzeBar(Player player)
    {
        player.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(s ->
        {
            s.setSmelting(1);
            s.setBarType(1);
            levelClientUpdate(player);
        });
    }

    public static Tag writeNBT(Capability<RunicAgesSmithingCapability> capability, RunicAgesSmithingCapability instance, Direction side) {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentsmithinglevel", instance.getSmithingLevel());
        tag.putInt("smithingxp", instance.getSmithingXP());
        tag.putInt("nextsmithingxp", instance.getNextSmithingXP());
        tag.putInt("usingfurnace", instance.getUsingFurnace());
        tag.putInt("smelting", instance.getIsSmelting());
        tag.putInt("bartype", instance.getBarType());
        tag.putInt("smeltcount", instance.getSmeltCount());
        tag.putInt("smeltticks", instance.getSmeltTicks());
        tag.putInt("smeltstage", instance.getSmeltStage());
        tag.putInt("makecount", instance.getMakeCount());
        tag.putBoolean("makingall", instance.getMakingAll());
        return tag;
    }

    public static void readNBT(Capability<RunicAgesSmithingCapability> capability, RunicAgesSmithingCapability instance, Direction side, Tag nbt) {
        instance.setSmithingLevel(((CompoundTag) nbt).getInt("currentsmithinglevel"));
        instance.setSmithingXP(((CompoundTag) nbt).getInt("smithingxp"));
        instance.setNextSmithingXP(((CompoundTag) nbt).getInt("nextsmithingxp"));
        instance.setUsingFurnace(((CompoundTag) nbt).getInt("usingfurnace"));
        instance.setSmelting(((CompoundTag) nbt).getInt("smelting"));
        instance.setBarType(((CompoundTag) nbt).getInt("bartype"));
        instance.setSmeltCount(((CompoundTag) nbt).getInt("smeltcount"));
        instance.setSmeltTicks(((CompoundTag) nbt).getInt("smeltticks"));
        instance.setSmeltStage(((CompoundTag) nbt).getInt("smeltstage"));
        instance.setMakeCount(((CompoundTag) nbt).getInt("makecount"));
        instance.setMakingAll(((CompoundTag) nbt).getBoolean("makingall"));
    }

    public static class Provider implements ICapabilitySerializable<Tag> {
        public static Capability<RunicAgesSmithingCapability> SMITHING_LEVEL = CapabilityManager.get(new CapabilityToken<>(){});

        @Nonnull
        private final RunicAgesSmithingCapability instance;

        private final LazyOptional<RunicAgesSmithingCapability> handler;

        public Provider() {
            instance = new RunicAgesSmithingCapability();
            handler = LazyOptional.of(this::getInstance);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return SMITHING_LEVEL.orEmpty(cap, handler);
        }

        public static LazyOptional<RunicAgesSmithingCapability> getFrom(Player player) {
            return player.getCapability(SMITHING_LEVEL);
        }

        public void invalidate() {
            handler.invalidate();
        }


        public RunicAgesSmithingCapability getInstance() {
            return instance;
        }

        @Override
        public Tag serializeNBT() {
            return RunicAgesSmithingCapability.writeNBT(SMITHING_LEVEL, instance, null);
        }

        @Override
        public void deserializeNBT(Tag nbt) {
            RunicAgesSmithingCapability.readNBT(SMITHING_LEVEL, instance, null, nbt);
        }
    }

}

