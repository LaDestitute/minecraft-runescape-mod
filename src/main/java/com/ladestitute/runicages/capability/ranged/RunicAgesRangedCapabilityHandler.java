package com.ladestitute.runicages.capability.ranged;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.attack.RunicAgesAttackCapability;
import com.ladestitute.runicages.capability.magic.RunicAgesMagicCapability;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = RunicAgesMain.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class RunicAgesRangedCapabilityHandler {

    //Make sure this isn't invalid, if it, it will cause a crash
    //We provide a mod ID so it can be recognized with a specified path for the factory class
    public static final ResourceLocation RANGED_CAP = new ResourceLocation(RunicAgesMain.MODID, "ranged");

    //As explained below, this event is responsible for attaching and also removing the capability if the entity/item/etc no longer exists
    @SubscribeEvent
    public static void attachCapability(AttachCapabilitiesEvent<Entity> event) {
        //Do nothing if the entity is not the player
        if (event.getObject() instanceof Player && event.getObject().isAlive()) {
            //We attach our capability here to the player, another entity, a chunk, blocks, items, etc
            RunicAgesRangedCapability.Provider provider = new RunicAgesRangedCapability.Provider();
            event.addCapability(RANGED_CAP, new RunicAgesRangedCapability.Provider());
            event.addListener(provider::invalidate);
            //  System.out.println("CAPABILITY ATTACHED TO PLAYER");
            //The invalidate listener is a listener that removes the capability when the entity/itemstack/etc is destroyed, so we can clear our lazyoptional
            //This is important, if something else gets our cap and stores the value, it becomes invalid and is not usable for safety reasons
            //Otherwise, this creates a dangling reference, which is a reference to an object that no longer exists
            //As a result, make sure not to have the invalidate line missing when attaching a capability
            //You are responsible for handling your own mod's proper lazy-val cleanup
        }
    }

    @SubscribeEvent
    public void levelup(TickEvent.PlayerTickEvent event) {
        event.player.getCapability(RunicAgesRangedCapability.Provider.RANGED_LEVEL).ifPresent(h ->
        {
            if (!event.player.level().isClientSide && h.getRangedXP() > 0 && h.getRangedXP() >= h.getNextRangedXP()) {
                h.addRangedLevel(event.player, 1);
                h.setNextRangedXP(getnextxp);
                RunicAgesRangedCapability.levelClientUpdate(event.player);
                event.player.sendSystemMessage(Component.literal("You have hit Ranged level " + h.getRangedLevel()));
                event.player.sendSystemMessage(Component.literal("Your next Ranged level is at: " + h.getNextRangedXP() + " XP"));
            }
        });
    }

    public int getnextxp;

    @SubscribeEvent
    public void getnextxp(TickEvent.PlayerTickEvent event) {
        event.player.getCapability(RunicAgesRangedCapability.Provider.RANGED_LEVEL).ifPresent(h ->
        {
            //helper 2d array to help set next-xp reqs for each level
            int[][] nextxp = {
                    {0, 1}, {1, 174}, {2, 276}, {3, 388}, {4, 512},
                    {5, 650}, {6, 801}, {7, 969}, {8, 1154}, {9, 1358},
                    {10, 1584}, {11, 1833}, {12, 2107}, {13, 2411}, {14, 2746},
                    {15, 3115}, {16, 3523}, {17, 3973}, {18, 4470}, {19, 5018},
                    {20, 5624}, {21, 6291}, {22, 7028}, {23, 7842}, {24, 8740},
                    {25, 9730}, {26, 10824}, {27, 12031}, {28, 13363}, {29, 14833},
                    {30, 16456}, {31, 18247}, {32, 20224}, {33, 22406}, {34, 24815},
                    {35, 27473}, {36, 30408}, {37, 33648}, {38, 37224}, {39, 41171},
                    {40, 45529}, {41, 50339}, {42, 55649}, {43, 61512}, {44, 67983},
                    {45, 75127}, {46, 83014}, {47, 91721}, {48, 101333}, {49, 111945},
                    {50, 123660}, {51, 136594}, {52, 150872}, {53, 166636}, {54, 184040},
                    {55, 203254}, {56, 224466}, {57, 247886}, {58, 273742}, {59, 302288},
                    {60, 333804}, {61, 368599}, {62, 407015}, {63, 449428}, {64, 496254},
                    {65, 547953}, {66, 605032}, {67, 668051}, {68, 737627}, {69, 814445},
                    {70, 899257}, {71, 992895}, {72, 1096278}, {73, 1210421}, {74, 1336443},
                    {75, 1475581}, {76, 1629200}, {77, 1798808}, {78, 1986068}, {79, 2192818},
                    {80, 2421087}, {81, 2673114}, {82, 2951373}, {83, 3528594}, {84, 3597792},
                    {85, 3972294}, {86, 4385776}, {87, 4842295}, {88, 5346332}, {89, 5902831},
                    {90, 6517253}, {91, 7195629}, {92, 7944614}, {93, 8771558}, {94, 9684577},
                    {95, 10692629}, {96, 11805606}, {97, 13034431}
            };
            if(h.getRangedLevel() < 99)
            {
                getnextxp = nextxp[h.getRangedLevel()][1];
            }
        });
    }

}


