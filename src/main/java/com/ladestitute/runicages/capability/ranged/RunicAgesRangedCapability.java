package com.ladestitute.runicages.capability.ranged;

import com.ladestitute.runicages.network.ClientRangedSkillPacket;
import com.ladestitute.runicages.network.SimpleNetworkHandler;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RunicAgesRangedCapability {

    private int maxRangedLevel = 99;
    private int currentRangedLevel = 1;
    private int currentRangedXP = 0;
    private int NextLevelRangedXP = 83;
    private int rangedboost = 0;
    private int rangedboostdraintimer = 0;
    private int invisiblerangedboost = 0;

    public void addRangedLevel(Player player, int amount) {
        if (this.currentRangedLevel < this.maxRangedLevel) {
            this.currentRangedLevel += amount;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.currentRangedLevel = this.maxRangedLevel;
        levelClientUpdate(player);
    }

    public void addRangedXP(Player player, int amount) {
        this.currentRangedXP += amount;
        levelClientUpdate(player);
    }

    public void setRangedLevel(int amount) {
        this.currentRangedLevel = amount;
    }

    public void setRangedXP(int amount) {
        this.currentRangedXP = amount;
    }

    public void setNextRangedXP(int amount) {
        this.NextLevelRangedXP = amount;
    }

    public int getRangedLevel() {
        return currentRangedLevel;
    }

    public int getRangedXP() {
        return currentRangedXP;
    }

    public int getNextRangedXP() {
        return NextLevelRangedXP;
    }

    //Visible boost stuff

    public void subRangedBoost(Player player, int amount) {
        this.rangedboost -= amount;
        levelClientUpdate(player);
    }

    public void setRangedBoost(int amount) {
        this.rangedboost = amount;
    }

    public int getRangedBoost() {
        return rangedboost;
    }

    // Visible boost timers

    public void incrementrangedboostdraintimer(Player player, int amount) {
        this.rangedboostdraintimer += amount;
        levelClientUpdate(player);
    }

    public void setrangedboostdraintimer(int amount) {
        this.rangedboostdraintimer = amount;
    }

    public int getRangedBoostTimer() {
        return rangedboostdraintimer;
    }

    //
    //Invisible boost stuff

    public void subInvisibleRangedBoost(Player player, int amount) {
        this.invisiblerangedboost -= amount;
        levelClientUpdate(player);
    }

    public void setInvisibleRangedBoost(int amount) {
        this.invisiblerangedboost = amount;
    }

    public int getInvisibleRangedBoost() {
        return invisiblerangedboost;
    }

    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentrangedlevel", this.currentRangedLevel);
        tag.putInt("rangedxp", this.currentRangedXP);
        tag.putInt("nextrangedxp", this.NextLevelRangedXP);
        return tag;
    }

    public void deserializeNBT(CompoundTag tag) {
        this.currentRangedLevel = tag.getInt("currentrangedlevel");
        this.NextLevelRangedXP = tag.getInt("nextrangedxp");
        this.currentRangedXP = tag.getInt("rangedxp");
    }

    //A method for sending a packet to the client to sync information for updating GUis/etc
    //This is done in the capability so this method is
    //included in any methods that add/subtract/set mana to make syncing easier
    public static void levelClientUpdate(Player player) {
        if (!player.level().isClientSide()) {
            ServerPlayer serverPlayer = (ServerPlayer) player;
            player.getCapability(RunicAgesRangedCapability.Provider.RANGED_LEVEL).ifPresent(cap ->
                    SimpleNetworkHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> serverPlayer),
                            new ClientRangedSkillPacket(cap.currentRangedLevel, cap.currentRangedXP, cap.NextLevelRangedXP,
                                    cap.rangedboost, cap.rangedboostdraintimer, cap.invisiblerangedboost)));
        }
    }

    public static Tag writeNBT(Capability<RunicAgesRangedCapability> capability, RunicAgesRangedCapability instance, Direction side) {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentrangedlevel", instance.getRangedLevel());
        tag.putInt("rangedxp", instance.getRangedXP());
        tag.putInt("nextrangedxp", instance.getNextRangedXP());
        tag.putInt("rangedboost", instance.getRangedBoost());
        tag.putInt("rangedboostdraintimer", instance.getRangedBoostTimer());
        tag.putInt("invisiblerangedboost", instance.getInvisibleRangedBoost());
        return tag;
    }

    public static void readNBT(Capability<RunicAgesRangedCapability> capability, RunicAgesRangedCapability instance, Direction side, Tag nbt) {
        instance.setRangedLevel(((CompoundTag) nbt).getInt("currentrangedlevel"));
        instance.setRangedXP(((CompoundTag) nbt).getInt("rangedxp"));
        instance.setNextRangedXP(((CompoundTag) nbt).getInt("nextrangedxp"));
        instance.setRangedBoost(((CompoundTag) nbt).getInt("rangedboost"));
        instance.setrangedboostdraintimer(((CompoundTag) nbt).getInt("rangedboostdraintimer"));
        instance.setInvisibleRangedBoost(((CompoundTag) nbt).getInt("invisiblerangedboost"));
    }

    public static class Provider implements ICapabilitySerializable<Tag> {
        public static Capability<RunicAgesRangedCapability> RANGED_LEVEL = CapabilityManager.get(new CapabilityToken<>(){});

        @Nonnull
        private final RunicAgesRangedCapability instance;

        private final LazyOptional<RunicAgesRangedCapability> handler;

        public Provider() {
            instance = new RunicAgesRangedCapability();
            handler = LazyOptional.of(this::getInstance);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return RANGED_LEVEL.orEmpty(cap, handler);
        }

        public static LazyOptional<RunicAgesRangedCapability> getFrom(Player player) {
            return player.getCapability(RANGED_LEVEL);
        }

        public void invalidate() {
            handler.invalidate();
        }

        public RunicAgesRangedCapability getInstance() {
            return instance;
        }

        @Override
        public Tag serializeNBT() {
            return RunicAgesRangedCapability.writeNBT(RANGED_LEVEL, instance, null);
        }

        @Override
        public void deserializeNBT(Tag nbt) {
            RunicAgesRangedCapability.readNBT(RANGED_LEVEL, instance, null, nbt);
        }
    }

}

