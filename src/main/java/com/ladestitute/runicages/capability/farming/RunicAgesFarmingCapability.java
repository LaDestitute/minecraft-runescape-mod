package com.ladestitute.runicages.capability.farming;

import com.ladestitute.runicages.network.ClientFarmingSkillPacket;
import com.ladestitute.runicages.network.SimpleNetworkHandler;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RunicAgesFarmingCapability {

    private int maxFarmingLevel = 99;
    private int currentFarmingLevel = 1;
    private int currentFarmingXP = 0;
    private int NextLevelFarmingXP = 83;
    private int farmingboost = 0;
    private int farmingboostdraintimer = 0;
    private int invisiblefarmingboost = 0;

    public void addFarmingLevel(Player player, int amount) {
        if (this.currentFarmingLevel < this.maxFarmingLevel) {
            this.currentFarmingLevel += amount;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.currentFarmingLevel = this.maxFarmingLevel;
        levelClientUpdate(player);
    }

    public void addFarmingXP(Player player, int amount) {
        this.currentFarmingXP += amount;
        levelClientUpdate(player);
    }

    public void setFarmingLevel(int amount) {
        this.currentFarmingLevel = amount;
    }

    public void setFarmingXP(int amount) {
        this.currentFarmingXP = amount;
    }

    public void setNextFarmingXP(int amount) {
        this.NextLevelFarmingXP = amount;
    }

    public int getFarmingLevel() {
        return currentFarmingLevel;
    }

    public int getFarmingXP() {
        return currentFarmingXP;
    }

    public int getNextFarmingXP() {
        return NextLevelFarmingXP;
    }

    //Visible boost stuff

    public void subFarmingBoost(Player player, int amount) {
        this.farmingboost -= amount;
        levelClientUpdate(player);
    }

    public void setFarmingBoost(int amount) {
        this.farmingboost = amount;
    }

    public int getFarmingBoost() {
        return farmingboost;
    }

    // Visible boost timers

    public void incrementfarmingboostdraintimer(Player player, int amount) {
        this.farmingboostdraintimer += amount;
        levelClientUpdate(player);
    }

    public void setfarmingboostdraintimer(int amount) {
        this.farmingboostdraintimer = amount;
    }

    public int getFarmingBoostTimer() {
        return farmingboostdraintimer;
    }

    //
    //Invisible boost stuff

    public void subInvisibleFarmingBoost(Player player, int amount) {
        this.invisiblefarmingboost -= amount;
        levelClientUpdate(player);
    }

    public void setInvisibleFarmingBoost(int amount) {
        this.invisiblefarmingboost = amount;
    }

    public int getInvisibleFarmingBoost() {
        return invisiblefarmingboost;
    }

    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentfarminglevel", this.currentFarmingLevel);
        tag.putInt("farmingxp", this.currentFarmingXP);
        tag.putInt("nextfarmingxp", this.NextLevelFarmingXP);
        return tag;
    }

    public void deserializeNBT(CompoundTag tag) {
        this.currentFarmingLevel = tag.getInt("currentfarminglevel");
        this.NextLevelFarmingXP = tag.getInt("nextfarmingxp");
        this.currentFarmingXP = tag.getInt("farmingxp");
    }

    //A method for sending a packet to the client to sync information for updating GUis/etc
    //This is done in the capability so this method is
    //included in any methods that add/subtract/set mana to make syncing easier
    public static void levelClientUpdate(Player player) {
        if (!player.level().isClientSide()) {
            ServerPlayer serverPlayer = (ServerPlayer) player;
            player.getCapability(RunicAgesFarmingCapability.Provider.FARMING_LEVEL).ifPresent(cap ->
                    SimpleNetworkHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> serverPlayer),
                            new ClientFarmingSkillPacket(cap.currentFarmingLevel, cap.currentFarmingXP, cap.NextLevelFarmingXP, cap.farmingboost, cap.farmingboostdraintimer, cap.invisiblefarmingboost)));
        }
    }

    public static Tag writeNBT(Capability<RunicAgesFarmingCapability> capability, RunicAgesFarmingCapability instance, Direction side) {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentfarminglevel", instance.getFarmingLevel());
        tag.putInt("farmingxp", instance.getFarmingXP());
        tag.putInt("nextfarmingxp", instance.getNextFarmingXP());
        tag.putInt("farmingboost", instance.getFarmingBoost());
        tag.putInt("farmingboostdraintimer", instance.getFarmingBoostTimer());
        tag.putInt("invisiblefarmingboost", instance.getInvisibleFarmingBoost());
        return tag;
    }

    public static void readNBT(Capability<RunicAgesFarmingCapability> capability, RunicAgesFarmingCapability instance, Direction side, Tag nbt) {
        instance.setFarmingLevel(((CompoundTag) nbt).getInt("currentfarminglevel"));
        instance.setFarmingXP(((CompoundTag) nbt).getInt("farmingxp"));
        instance.setNextFarmingXP(((CompoundTag) nbt).getInt("nextfarmingxp"));
        instance.setFarmingBoost(((CompoundTag) nbt).getInt("farmingboost"));
        instance.setfarmingboostdraintimer(((CompoundTag) nbt).getInt("farmingboostdraintimer"));
        instance.setInvisibleFarmingBoost(((CompoundTag) nbt).getInt("invisiblefarmingboost"));
    }

    public static class Provider implements ICapabilitySerializable<Tag> {
        public static Capability<RunicAgesFarmingCapability> FARMING_LEVEL = CapabilityManager.get(new CapabilityToken<>(){});

        @Nonnull
        private final RunicAgesFarmingCapability instance;

        private final LazyOptional<RunicAgesFarmingCapability> handler;

        public Provider() {
            instance = new RunicAgesFarmingCapability();
            handler = LazyOptional.of(this::getInstance);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return FARMING_LEVEL.orEmpty(cap, handler);
        }

        public static LazyOptional<RunicAgesFarmingCapability> getFrom(Player player) {
            return player.getCapability(FARMING_LEVEL);
        }

        public void invalidate() {
            handler.invalidate();
        }

        public RunicAgesFarmingCapability getInstance() {
            return instance;
        }

        @Override
        public Tag serializeNBT() {
            return RunicAgesFarmingCapability.writeNBT(FARMING_LEVEL, instance, null);
        }

        @Override
        public void deserializeNBT(Tag nbt) {
            RunicAgesFarmingCapability.readNBT(FARMING_LEVEL, instance, null, nbt);
        }
    }

}
