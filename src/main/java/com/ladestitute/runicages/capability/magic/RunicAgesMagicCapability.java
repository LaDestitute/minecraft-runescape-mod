package com.ladestitute.runicages.capability.magic;

import com.ladestitute.runicages.network.ClientMagicSkillPacket;
import com.ladestitute.runicages.network.SimpleNetworkHandler;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RunicAgesMagicCapability {

    private int maxMagicLevel = 99;
    private int currentMagicLevel = 1;
    private int currentMagicXP = 0;
    private int NextLevelMagicXP = 83;
    private int magicbookopen = 0;
    private int mainhandspell = 0;
    private int offhandspell = 0;
    private int guibuttonpressed = 0;
    private int magicboost = 0;
    private int magicboostdraintimer = 0;
    private int invisiblemagicboost = 0;
    private int magicequipboost = 0;

    public void addMagicLevel(Player player, int amount) {
        if (this.currentMagicLevel < this.maxMagicLevel) {
            this.currentMagicLevel += amount;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.currentMagicLevel = this.maxMagicLevel;
        levelClientUpdate(player);
    }

    public void addMagicXP(Player player, int amount) {
        this.currentMagicXP += amount;
        levelClientUpdate(player);
    }

    public void setMainhandSpell(int amount) {
        this.mainhandspell = amount;
    }

    public void setOffhandSpell(int amount) {
        this.offhandspell = amount;
    }

    public void setmagicbookopen(int amount) {
        this.magicbookopen = amount;
    }

    public void setMagicLevel(int amount) {
        this.currentMagicLevel = amount;
    }

    public void setMagicXP(int amount) {
        this.currentMagicXP = amount;
    }

    public void setNextMagicXP(int amount) {
        this.NextLevelMagicXP = amount;
    }

    public void setGuiButtonPressed(int amount) {
        this.guibuttonpressed = amount;
    }
    public int getGuiButtonPressed() {
        return guibuttonpressed;
    }

    public int getMagicLevel() {
        return currentMagicLevel;
    }

    public int getMagicXP() {
        return currentMagicXP;
    }

    public int getNextMagicXP() {
        return NextLevelMagicXP;
    }

    public int getMainhandSpell() {
        return mainhandspell;
    }

    public int getOffhandSpell() {
        return offhandspell;
    }

    public int getIsMagicBookOpen() {
        return magicbookopen;
    }

    //Visible boost stuff

    public void subMagicBoost(Player player, int amount) {
        this.magicboost -= amount;
        levelClientUpdate(player);
    }

    public void setMagicEquipBoost(int amount) {
        this.magicequipboost = amount;
    }

    public int getMagicEquipBoost() {
        return magicequipboost;
    }

    public void setMagicBoost(int amount) {
        this.magicboost = amount;
    }

    public int getMagicBoost() {
        return magicboost;
    }

    // Visible boost timers

    public void incrementmagicboostdraintimer(Player player, int amount) {
        this.magicboostdraintimer += amount;
        levelClientUpdate(player);
    }

    public void setmagicboostdraintimer(int amount) {
        this.magicboostdraintimer = amount;
    }

    public int getMagicBoostTimer() {
        return magicboostdraintimer;
    }

    //
    //Invisible boost stuff

    public void subInvisibleMagicBoost(Player player, int amount) {
        this.invisiblemagicboost -= amount;
        levelClientUpdate(player);
    }

    public void setInvisibleMagicBoost(int amount) {
        this.invisiblemagicboost = amount;
    }

    public int getInvisibleMagicBoost() {
        return invisiblemagicboost;
    }

    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentmagiclevel", this.currentMagicLevel);
        tag.putInt("magicxp", this.currentMagicXP);
        tag.putInt("nextmagicxp", this.NextLevelMagicXP);
        tag.putInt("magicbook", this.magicbookopen);
        tag.putInt("mainhandspell", this.mainhandspell);
        tag.putInt("offhandspell", this.offhandspell);
        tag.putInt("guibuttonpressed", this.guibuttonpressed);
        return tag;
    }

    public void deserializeNBT(CompoundTag tag) {
        this.currentMagicLevel = tag.getInt("currentmagiclevel");
        this.NextLevelMagicXP = tag.getInt("nextmagicxp");
        this.currentMagicXP = tag.getInt("magicxp");
        this.magicbookopen = tag.getInt("magicbook");
        this.mainhandspell = tag.getInt("mainhandspell");
        this.offhandspell = tag.getInt("offhandspell");
        this.guibuttonpressed = tag.getInt("guibuttonpressed");
    }

    //A method for sending a packet to the client to sync information for updating GUis/etc
    //This is done in the capability so this method is
    //included in any methods that add/subtract/set mana to make syncing easier
    public static void levelClientUpdate(Player player) {
        if (!player.level().isClientSide()) {
            ServerPlayer serverPlayer = (ServerPlayer) player;
            player.getCapability(Provider.MAGIC_LEVEL).ifPresent(cap ->
                    SimpleNetworkHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> serverPlayer),
                            new ClientMagicSkillPacket(cap.currentMagicLevel, cap.currentMagicXP, cap.NextLevelMagicXP, cap.magicbookopen, cap.mainhandspell, cap.offhandspell, cap.guibuttonpressed,
                                    cap.magicboost, cap.magicequipboost, cap.magicboostdraintimer, cap.invisiblemagicboost)));
        }
    }

    public static Tag writeNBT(Capability<RunicAgesMagicCapability> capability, RunicAgesMagicCapability instance, Direction side) {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentmagiclevel", instance.getMagicLevel());
        tag.putInt("magicxp", instance.getMagicXP());
        tag.putInt("nextmagicxp", instance.getNextMagicXP());
        tag.putInt("magicbook", instance.getIsMagicBookOpen());
        tag.putInt("mainhandspell", instance.getMainhandSpell());
        tag.putInt("offhandspell", instance.getOffhandSpell());
        tag.putInt("guibuttonpressed", instance.getGuiButtonPressed());
        tag.putInt("magicboost", instance.getMagicBoost());
        tag.putInt("magicequipboost", instance.getMagicEquipBoost());
        tag.putInt("magicboostdraintimer", instance.getMagicBoostTimer());
        tag.putInt("invisiblemagicboost", instance.getInvisibleMagicBoost());
        return tag;
    }

    public static void readNBT(Capability<RunicAgesMagicCapability> capability, RunicAgesMagicCapability instance, Direction side, Tag nbt) {
        instance.setMagicLevel(((CompoundTag) nbt).getInt("currentmagiclevel"));
        instance.setMagicXP(((CompoundTag) nbt).getInt("magicxp"));
        instance.setNextMagicXP(((CompoundTag) nbt).getInt("nextmagicxp"));
        instance.setmagicbookopen(((CompoundTag) nbt).getInt("magicbook"));
        instance.setMainhandSpell(((CompoundTag) nbt).getInt("mainhandspell"));
        instance.setOffhandSpell(((CompoundTag) nbt).getInt("offhandspell"));
        instance.setGuiButtonPressed(((CompoundTag) nbt).getInt("guibuttonpressed"));
        instance.setMagicBoost(((CompoundTag) nbt).getInt("magicboost"));
        instance.setMagicEquipBoost(((CompoundTag) nbt).getInt("magicequipboost"));
        instance.setmagicboostdraintimer(((CompoundTag) nbt).getInt("magicboostdraintimer"));
        instance.setInvisibleMagicBoost(((CompoundTag) nbt).getInt("invisiblemagicboost"));
    }

    public static class Provider implements ICapabilitySerializable<Tag> {
        public static Capability<RunicAgesMagicCapability> MAGIC_LEVEL = CapabilityManager.get(new CapabilityToken<>(){});

        @Nonnull
        private final RunicAgesMagicCapability instance;

        private final LazyOptional<RunicAgesMagicCapability> handler;

        public Provider() {
            instance = new RunicAgesMagicCapability();
            handler = LazyOptional.of(this::getInstance);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return MAGIC_LEVEL.orEmpty(cap, handler);
        }

        public static LazyOptional<RunicAgesMagicCapability> getFrom(Player player) {
            return player.getCapability(MAGIC_LEVEL);
        }

        public void invalidate() {
            handler.invalidate();
        }

        public RunicAgesMagicCapability getInstance() {
            return instance;
        }

        @Override
        public Tag serializeNBT() {
            return RunicAgesMagicCapability.writeNBT(MAGIC_LEVEL, instance, null);
        }

        @Override
        public void deserializeNBT(Tag nbt) {
            RunicAgesMagicCapability.readNBT(MAGIC_LEVEL, instance, null, nbt);
        }
    }

}