package com.ladestitute.runicages.capability.defense;

import com.ladestitute.runicages.network.ClientDefenseSkillPacket;
import com.ladestitute.runicages.network.SimpleNetworkHandler;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RunicAgesDefenseCapability {

    private int maxDefenseLevel = 99;
    private int currentDefenseLevel = 1;
    private int currentDefenseXP = 0;
    private int NextLevelDefenseXP = 83;
    private int defenseboost = 0;
    private int defenseboostdraintimer = 0;
    private int invisibledefenseboost = 0;

    public void addDefenseLevel(Player player, int amount) {
        if (this.currentDefenseLevel < this.maxDefenseLevel) {
            this.currentDefenseLevel += amount;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.currentDefenseLevel = this.maxDefenseLevel;
        levelClientUpdate(player);
    }

    public void addDefenseXP(Player player, int amount) {
        this.currentDefenseXP += amount;
        levelClientUpdate(player);
    }

    public void setDefenseLevel(int amount) {
        this.currentDefenseLevel = amount;
    }

    public void setDefenseXP(int amount) {
        this.currentDefenseXP = amount;
    }

    public void setNextDefenseXP(int amount) {
        this.NextLevelDefenseXP = amount;
    }

    public int getDefenseLevel() {
        return currentDefenseLevel;
    }

    public int getDefenseXP() {
        return currentDefenseXP;
    }

    public int getNextDefenseXP() {
        return NextLevelDefenseXP;
    }

    //Visible boost stuff

    public void subDefenseBoost(Player player, int amount) {
        this.defenseboost -= amount;
        levelClientUpdate(player);
    }

    public void setDefenseBoost(int amount) {
        this.defenseboost = amount;
    }

    public int getDefenseBoost() {
        return defenseboost;
    }

    // Visible boost timers

    public void incrementdefenseboostdraintimer(Player player, int amount) {
        this.defenseboostdraintimer += amount;
        levelClientUpdate(player);
    }

    public void setdefenseboostdraintimer(int amount) {
        this.defenseboostdraintimer = amount;
    }

    public int getDefenseBoostTimer() {
        return defenseboostdraintimer;
    }

    //
    //Invisible boost stuff

    public void subInvisibleDefenseBoost(Player player, int amount) {
        this.invisibledefenseboost -= amount;
        levelClientUpdate(player);
    }

    public void setInvisibleDefenseBoost(int amount) {
        this.invisibledefenseboost = amount;
    }

    public int getInvisibleDefenseBoost() {
        return invisibledefenseboost;
    }

    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentdefenselevel", this.currentDefenseLevel);
        tag.putInt("defensexp", this.currentDefenseXP);
        tag.putInt("nextdefensexp", this.NextLevelDefenseXP);
        return tag;
    }

    public void deserializeNBT(CompoundTag tag) {
        this.currentDefenseLevel = tag.getInt("currentdefenselevel");
        this.NextLevelDefenseXP = tag.getInt("nextdefensexp");
        this.currentDefenseXP = tag.getInt("defensexp");
    }

    //A method for sending a packet to the client to sync information for updating GUis/etc
    //This is done in the capability so this method is
    //included in any methods that add/subtract/set mana to make syncing easier
    public static void levelClientUpdate(Player player) {
        if (!player.level().isClientSide()) {
            ServerPlayer serverPlayer = (ServerPlayer) player;
            player.getCapability(RunicAgesDefenseCapability.Provider.DEFENSE_LEVEL).ifPresent(cap ->
                    SimpleNetworkHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> serverPlayer),
                            new ClientDefenseSkillPacket(cap.currentDefenseLevel, cap.currentDefenseXP, cap.NextLevelDefenseXP,
                                    cap.defenseboost, cap.defenseboostdraintimer, cap.invisibledefenseboost)));
        }
    }

    public static Tag writeNBT(Capability<RunicAgesDefenseCapability> capability, RunicAgesDefenseCapability instance, Direction side) {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentdefenselevel", instance.getDefenseLevel());
        tag.putInt("defensexp", instance.getDefenseXP());
        tag.putInt("nextdefensexp", instance.getNextDefenseXP());
        tag.putInt("defenseboost", instance.getDefenseBoost());
        tag.putInt("defenseboostdraintimer", instance.getDefenseBoostTimer());
        tag.putInt("invisibledefenseboost", instance.getInvisibleDefenseBoost());
        return tag;
    }

    public static void readNBT(Capability<RunicAgesDefenseCapability> capability, RunicAgesDefenseCapability instance, Direction side, Tag nbt) {
        instance.setDefenseLevel(((CompoundTag) nbt).getInt("currentdefenselevel"));
        instance.setDefenseXP(((CompoundTag) nbt).getInt("defensexp"));
        instance.setNextDefenseXP(((CompoundTag) nbt).getInt("nextdefensexp"));
        instance.setDefenseBoost(((CompoundTag) nbt).getInt("defenseboost"));
        instance.setdefenseboostdraintimer(((CompoundTag) nbt).getInt("defenseboostdraintimer"));
        instance.setInvisibleDefenseBoost(((CompoundTag) nbt).getInt("invisibledefenseboost"));
    }

    public static class Provider implements ICapabilitySerializable<Tag> {
        public static Capability<RunicAgesDefenseCapability> DEFENSE_LEVEL = CapabilityManager.get(new CapabilityToken<>(){});

        @Nonnull
        private final RunicAgesDefenseCapability instance;

        private final LazyOptional<RunicAgesDefenseCapability> handler;

        public Provider() {
            instance = new RunicAgesDefenseCapability();
            handler = LazyOptional.of(this::getInstance);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return DEFENSE_LEVEL.orEmpty(cap, handler);
        }

        public static LazyOptional<RunicAgesDefenseCapability> getFrom(Player player) {
            return player.getCapability(DEFENSE_LEVEL);
        }

        public void invalidate() {
            handler.invalidate();
        }

        public RunicAgesDefenseCapability getInstance() {
            return instance;
        }

        @Override
        public Tag serializeNBT() {
            return RunicAgesDefenseCapability.writeNBT(DEFENSE_LEVEL, instance, null);
        }

        @Override
        public void deserializeNBT(Tag nbt) {
            RunicAgesDefenseCapability.readNBT(DEFENSE_LEVEL, instance, null, nbt);
        }
    }

}

