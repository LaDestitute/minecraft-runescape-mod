package com.ladestitute.runicages.capability.crafting;

import com.ladestitute.runicages.network.ClientCraftingSkillPacket;
import com.ladestitute.runicages.network.SimpleNetworkHandler;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RunicAgesCraftingCapability {

    private int maxCraftingLevel = 99;
    private int currentCraftingLevel = 1;
    private int currentCraftingXP = 0;
    private int NextLevelCraftingXP = 83;

    public void addCraftingLevel(Player player, int amount) {
        if (this.currentCraftingLevel < this.maxCraftingLevel) {
            this.currentCraftingLevel += amount;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.currentCraftingLevel = this.maxCraftingLevel;
        levelClientUpdate(player);
    }

    public void addCraftingXP(Player player, int amount) {
        this.currentCraftingXP += amount;
        levelClientUpdate(player);
    }

    public void setCraftingLevel(int amount) {
        this.currentCraftingLevel = amount;
    }

    public void setCraftingXP(int amount) {
        this.currentCraftingXP = amount;
    }

    public void setNextCraftingXP(int amount) {
        this.NextLevelCraftingXP = amount;
    }

    public int getCraftingLevel() {
        return currentCraftingLevel;
    }

    public int getCraftingXP() {
        return currentCraftingXP;
    }

    public int getNextCraftingXP() {
        return NextLevelCraftingXP;
    }

    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentcraftinglevel", this.currentCraftingLevel);
        tag.putInt("craftingxp", this.currentCraftingXP);
        tag.putInt("nextcraftingxp", this.NextLevelCraftingXP);
        return tag;
    }

    public void deserializeNBT(CompoundTag tag) {
        this.currentCraftingLevel = tag.getInt("currentcraftinglevel");
        this.NextLevelCraftingXP = tag.getInt("nextcraftingxp");
        this.currentCraftingXP = tag.getInt("craftingxp");
    }

    //A method for sending a packet to the client to sync information for updating GUis/etc
    //This is done in the capability so this method is
    //included in any methods that add/subtract/set mana to make syncing easier
    public static void levelClientUpdate(Player player) {
        if (!player.level().isClientSide()) {
            ServerPlayer serverPlayer = (ServerPlayer) player;
            player.getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(cap ->
                    SimpleNetworkHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> serverPlayer),
                            new ClientCraftingSkillPacket(cap.currentCraftingLevel, cap.currentCraftingXP, cap.NextLevelCraftingXP)));
        }
    }

    public static Tag writeNBT(Capability<RunicAgesCraftingCapability> capability, RunicAgesCraftingCapability instance, Direction side) {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentcraftinglevel", instance.getCraftingLevel());
        tag.putInt("craftingxp", instance.getCraftingXP());
        tag.putInt("nextcraftingxp", instance.getNextCraftingXP());
        return tag;
    }

    public static void readNBT(Capability<RunicAgesCraftingCapability> capability, RunicAgesCraftingCapability instance, Direction side, Tag nbt) {
        instance.setCraftingLevel(((CompoundTag) nbt).getInt("currentcraftinglevel"));
        instance.setCraftingXP(((CompoundTag) nbt).getInt("craftingxp"));
        instance.setNextCraftingXP(((CompoundTag) nbt).getInt("nextcraftingxp"));
    }

    public static class Provider implements ICapabilitySerializable<Tag> {
        public static Capability<RunicAgesCraftingCapability> CRAFTING_LEVEL = CapabilityManager.get(new CapabilityToken<>(){});

        @Nonnull
        private final RunicAgesCraftingCapability instance;

        private final LazyOptional<RunicAgesCraftingCapability> handler;

        public Provider() {
            instance = new RunicAgesCraftingCapability();
            handler = LazyOptional.of(this::getInstance);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return CRAFTING_LEVEL.orEmpty(cap, handler);
        }

        public static LazyOptional<RunicAgesCraftingCapability> getFrom(Player player) {
            return player.getCapability(CRAFTING_LEVEL);
        }

        public void invalidate() {
            handler.invalidate();
        }

        public RunicAgesCraftingCapability getInstance() {
            return instance;
        }

        @Override
        public Tag serializeNBT() {
            return RunicAgesCraftingCapability.writeNBT(CRAFTING_LEVEL, instance, null);
        }

        @Override
        public void deserializeNBT(Tag nbt) {
            RunicAgesCraftingCapability.readNBT(CRAFTING_LEVEL, instance, null, nbt);
        }
    }

}

