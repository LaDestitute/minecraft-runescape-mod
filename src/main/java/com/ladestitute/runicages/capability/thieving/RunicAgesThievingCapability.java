package com.ladestitute.runicages.capability.thieving;

import com.ladestitute.runicages.network.ClientThievingSkillPacket;
import com.ladestitute.runicages.network.SimpleNetworkHandler;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RunicAgesThievingCapability {

    private int maxThievingLevel = 99;
    private int currentThievingLevel = 1;
    private int currentThievingXP = 0;
    private int NextLevelThievingXP = 83;
    private int thievingboost = 0;
    private int thievingboostdraintimer = 0;
    private int invisiblethievingboost = 0;

    public void addThievingLevel(Player player, int amount) {
        if (this.currentThievingLevel < this.maxThievingLevel) {
            this.currentThievingLevel += amount;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.currentThievingLevel = this.maxThievingLevel;
        levelClientUpdate(player);
    }

    public void addThievingXP(Player player, int amount) {
        this.currentThievingXP += amount;
        levelClientUpdate(player);
    }

    public void setThievingLevel(int amount) {
        this.currentThievingLevel = amount;
    }

    public void setThievingXP(int amount) {
        this.currentThievingXP = amount;
    }

    public void setNextThievingXP(int amount) {
        this.NextLevelThievingXP = amount;
    }

    public int getThievingLevel() {
        return currentThievingLevel;
    }

    public int getThievingXP() {
        return currentThievingXP;
    }

    public int getNextThievingXP() {
        return NextLevelThievingXP;
    }

    //Visible boost stuff

    public void subThievingBoost(Player player, int amount) {
        this.thievingboost -= amount;
        levelClientUpdate(player);
    }

    public void setThievingBoost(int amount) {
        this.thievingboost = amount;
    }

    public int getThievingBoost() {
        return thievingboost;
    }

    // Visible boost timers

    public void incrementthievingboostdraintimer(Player player, int amount) {
        this.thievingboostdraintimer += amount;
        levelClientUpdate(player);
    }

    public void setthievingboostdraintimer(int amount) {
        this.thievingboostdraintimer = amount;
    }

    public int getThievingBoostTimer() {
        return thievingboostdraintimer;
    }

    //
    //Invisible boost stuff

    public void subInvisibleThievingBoost(Player player, int amount) {
        this.invisiblethievingboost -= amount;
        levelClientUpdate(player);
    }

    public void setInvisibleThievingBoost(int amount) {
        this.invisiblethievingboost = amount;
    }

    public int getInvisibleThievingBoost() {
        return invisiblethievingboost;
    }

    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentthievinglevel", this.currentThievingLevel);
        tag.putInt("thievingxp", this.currentThievingXP);
        tag.putInt("nextthievingxp", this.NextLevelThievingXP);
        return tag;
    }

    public void deserializeNBT(CompoundTag tag) {
        this.currentThievingLevel = tag.getInt("currentthievinglevel");
        this.NextLevelThievingXP = tag.getInt("nextthievingxp");
        this.currentThievingXP = tag.getInt("thievingxp");
    }

    //A method for sending a packet to the client to sync information for updating GUis/etc
    //This is done in the capability so this method is
    //included in any methods that add/subtract/set mana to make syncing easier
    public static void levelClientUpdate(Player player) {
        if (!player.level().isClientSide()) {
            ServerPlayer serverPlayer = (ServerPlayer) player;
            player.getCapability(RunicAgesThievingCapability.Provider.THIEVING_LEVEL).ifPresent(cap ->
                    SimpleNetworkHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> serverPlayer),
                            new ClientThievingSkillPacket(cap.currentThievingLevel, cap.currentThievingXP, cap.NextLevelThievingXP, cap.thievingboost, cap.thievingboostdraintimer, cap.invisiblethievingboost)));
        }
    }

    public static Tag writeNBT(Capability<RunicAgesThievingCapability> capability, RunicAgesThievingCapability instance, Direction side) {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentthievinglevel", instance.getThievingLevel());
        tag.putInt("thievingxp", instance.getThievingXP());
        tag.putInt("nextthievingxp", instance.getNextThievingXP());
        tag.putInt("thievingboost", instance.getThievingBoost());
        tag.putInt("thievingboostdraintimer", instance.getThievingBoostTimer());
        tag.putInt("invisiblethievingboost", instance.getInvisibleThievingBoost());
        return tag;
    }

    public static void readNBT(Capability<RunicAgesThievingCapability> capability, RunicAgesThievingCapability instance, Direction side, Tag nbt) {
        instance.setThievingLevel(((CompoundTag) nbt).getInt("currentthievinglevel"));
        instance.setThievingXP(((CompoundTag) nbt).getInt("thievingxp"));
        instance.setNextThievingXP(((CompoundTag) nbt).getInt("nextthievingxp"));
        instance.setThievingBoost(((CompoundTag) nbt).getInt("thievingboost"));
        instance.setthievingboostdraintimer(((CompoundTag) nbt).getInt("thievingboostdraintimer"));
        instance.setInvisibleThievingBoost(((CompoundTag) nbt).getInt("invisiblethievingboost"));
    }

    public static class Provider implements ICapabilitySerializable<Tag> {
        public static Capability<RunicAgesThievingCapability> THIEVING_LEVEL = CapabilityManager.get(new CapabilityToken<>(){});

        @Nonnull
        private final RunicAgesThievingCapability instance;

        private final LazyOptional<RunicAgesThievingCapability> handler;

        public Provider() {
            instance = new RunicAgesThievingCapability();
            handler = LazyOptional.of(this::getInstance);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return THIEVING_LEVEL.orEmpty(cap, handler);
        }

        public static LazyOptional<RunicAgesThievingCapability> getFrom(Player player) {
            return player.getCapability(THIEVING_LEVEL);
        }

        public void invalidate() {
            handler.invalidate();
        }

        public RunicAgesThievingCapability getInstance() {
            return instance;
        }

        @Override
        public Tag serializeNBT() {
            return RunicAgesThievingCapability.writeNBT(THIEVING_LEVEL, instance, null);
        }

        @Override
        public void deserializeNBT(Tag nbt) {
            RunicAgesThievingCapability.readNBT(THIEVING_LEVEL, instance, null, nbt);
        }
    }

}
