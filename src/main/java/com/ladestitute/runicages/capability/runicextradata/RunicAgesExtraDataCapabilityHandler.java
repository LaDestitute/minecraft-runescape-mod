package com.ladestitute.runicages.capability.runicextradata;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.runecrafting.RunicAgesRunecraftingCapability;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = RunicAgesMain.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class RunicAgesExtraDataCapabilityHandler {

    //Make sure this isn't invalid, if it, it will cause a crash
    //We provide a mod ID so it can be recognized with a specified path for the factory class
    public static final ResourceLocation RA_EXTRADATA_CAP = new ResourceLocation(RunicAgesMain.MODID, "ra_extradata");

    //As explained below, this event is responsible for attaching and also removing the capability if the entity/item/etc no longer exists
    @SubscribeEvent
    public static void attachCapability(AttachCapabilitiesEvent<Entity> event) {
        //Do nothing if the entity is not the player
        if (event.getObject() instanceof Player && event.getObject().isAlive()) {
            //We attach our capability here to the player, another entity, a chunk, blocks, items, etc
            RunicAgesExtraDataCapability.Provider provider = new RunicAgesExtraDataCapability.Provider();
            event.addCapability(RA_EXTRADATA_CAP, new RunicAgesExtraDataCapability.Provider());
            event.addListener(provider::invalidate);
            //  System.out.println("CAPABILITY ATTACHED TO PLAYER");
            //The invalidate listener is a listener that removes the capability when the entity/itemstack/etc is destroyed, so we can clear our lazyoptional
            //This is important, if something else gets our cap and stores the value, it becomes invalid and is not usable for safety reasons
            //Otherwise, this creates a dangling reference, which is a reference to an object that no longer exists
            //As a result, make sure not to have the invalidate line missing when attaching a capability
            //You are responsible for handling your own mod's proper lazy-val cleanup
        }
    }

}

