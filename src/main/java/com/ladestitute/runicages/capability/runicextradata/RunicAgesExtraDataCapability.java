package com.ladestitute.runicages.capability.runicextradata;

import com.ladestitute.runicages.network.ClientExtraDataPacket;
import com.ladestitute.runicages.network.SimpleNetworkHandler;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RunicAgesExtraDataCapability {

    private int homeset = 0;
    private float posx = 0;
    private float posy = 0;
    private float posz = 0;
    private int hastoolbeltchisel = 0;
    private int claybraceletcharges = 36;
    private int recoilringcharges = 80;
    private int totalxp = 0;
    private int receiveattackxp = 1;
    private int receivestrengthxp = 1;
    private int receivedefensexp = 1;
    private int receiverangedxp = 1;
    private int receivemagicxp = 1;
    private int hastoolbeltpestleandmortar = 0;
    private int hastoolbeltneedle = 0;
    private int bountycharges = 10;

    public void sethashome(int amount) {
        this.homeset = amount;
    }

        public int getHasHome() {
        return homeset;
    }

    //

    public void setreceiveattackxp(int amount) {
        if(receiveattackxp == 1)
        {
            this.receiveattackxp = 0;
        }
        else this.receiveattackxp = 1;
    }

    public int getreceiveattackxp() {
        return receiveattackxp;
    }

    public void receiveattackxp(int amount) {
        this.receiveattackxp = amount;
    }

    //

    public void setreceivestrengthxp(int amount) {
        if(receivestrengthxp == 1)
        {
            this.receivestrengthxp = 0;
        }
        else this.receivestrengthxp = 1;
    }


    public int getreceivestrengthxp() {
        return receivestrengthxp;
    }

    public void receivestrengthxp(int amount) {
        this.receivestrengthxp = amount;
    }

    //
    public void setreceivedefensexp(int amount) {
        if(receivedefensexp > 1||receivedefensexp >= 2)
        {
            this.receivedefensexp = 0;
        }
        else this.receivedefensexp += amount;

    }

    public void receivedefensexp(int amount) {
        this.receivedefensexp = amount;
    }

    public int getreceivedefensexp() {
        return receivedefensexp;
    }

    //

    public void setreceiverangedxp(Player player, int amount) {
        if(receiverangedxp >= 2)
        {
            this.receiverangedxp = 0;
        }
        this.receiverangedxp += amount;
        RunicAgesExtraDataCapability.levelClientUpdate(player);
    }

    public void receiverangedxp(int amount) {
        this.receiverangedxp = amount;
    }

    public int getreceiverangedxp() {
        return receiverangedxp;
    }
    //

    public void setreceivemagicxp(int amount) {
        if(receivemagicxp == 1)
        {
            this.receivemagicxp = 0;
        }
        else this.receivemagicxp = 1;
    }

    public void receivemagicxp(int amount) {
        this.receivemagicxp = amount;
    }

    public int getreceivemagicxp() {
        return receivemagicxp;
    }


    public void resetclaybraceletcharges(int amount) {
        this.claybraceletcharges = amount;
    }

    public void decreaseclaybraceletcharges(int amount) {
        if(claybraceletcharges <= 0) {
            this.claybraceletcharges = 0;
        }
        else this.claybraceletcharges -= amount;
    }

    public int getClayBraceletCharges() {
        return claybraceletcharges;
    }

    public void setrecoilringcharges(int amount) {
        this.recoilringcharges = amount;
    }

    public void decreaserecoilringcharges(int amount) {
        if(recoilringcharges <= 0) {
            this.recoilringcharges = 0;
        }
        else this.recoilringcharges -= amount;
    }

    public int getRecoilRingCharges() {
        return recoilringcharges;
    }

    public void setbountycharges(int amount) {
        this.bountycharges = amount;
    }

    public void decreasebountycharges(int amount) {
        if(bountycharges <= 0) {
            this.bountycharges = 0;
        }
        else this.bountycharges -= amount;
    }

    public int getBountyCharges() {
        return bountycharges;
    }

    public int getTotalXP() {
        return totalxp;
    }

    public void setx(float amount) {
        this.posx = Math.round(amount);
    }

    public float getX() {
        return posx;
    }

    public void sety(float amount) {
        this.posy = Math.round(amount);
    }

    public float getY() {
        return posy;
    }

    public void setz(float amount) {
        this.posz = Math.round(amount);
    }

    public float getZ() {
        return posz;
    }

    public void addxptotalxp(int amount) {
        this.totalxp += amount;
    }

    public void settotalxp(int amount) {
        this.totalxp = amount;
    }

    public void sethastoolbeltchisel(int amount) {
        this.hastoolbeltchisel = amount;
    }

    public int getHasToolbeltChisel() {
        return hastoolbeltchisel;
    }

    public void sethastoolbeltpestleandmortar(int amount) {
        this.hastoolbeltpestleandmortar = amount;
    }

    public int getHasToolbeltPestleAndMortar() {
        return hastoolbeltpestleandmortar;
    }

    public void sethastoolbeltneedle(int amount) {
        this.hastoolbeltneedle = amount;
    }

    public int getHasToolbeltNeedle() {
        return hastoolbeltneedle;
    }

    public CompoundTag serializeNBT() {
        CompoundTag tag = new CompoundTag();
        tag.putInt("homeset", this.homeset);
        tag.putFloat("posx", this.posy);
        tag.putFloat("posy", this.posy);
        tag.putFloat("posz", this.posz);
        tag.putInt("claybraceletcharges", this.claybraceletcharges);
        tag.putInt("recoilringcharges", this.recoilringcharges);
        tag.putInt("hastoolbeltchisel", this.hastoolbeltchisel);
        tag.putInt("totalxp", this.totalxp);
        tag.putInt("receiveattackxp", this.receiveattackxp);
        tag.putInt("receivestrengthxp", this.receivestrengthxp);
        tag.putInt("receivedefensexp", this.receivedefensexp);
        tag.putInt("receiverangedxp", this.receiverangedxp);
        tag.putInt("receivemagicxp", this.receivemagicxp);
        tag.putInt("hastoolbeltpestleandmortar", this.hastoolbeltpestleandmortar);
        tag.putInt("bountycharges", this.bountycharges);
        tag.putInt("hastoolbeltneedle", this.hastoolbeltneedle);
        return tag;
    }

    public void deserializeNBT(CompoundTag tag) {
        this.homeset = tag.getInt("homeset");
        this.posx = tag.getFloat("posx");
        this.posy = tag.getFloat("posy");
        this.posz = tag.getFloat("posz");
        this.claybraceletcharges = tag.getInt("claybraceletcharges");
        this.recoilringcharges = tag.getInt("recoilringcharges");
        this.hastoolbeltchisel = tag.getInt("hastoolbeltchisel");
        this.totalxp = tag.getInt("totalxp");
        this.receiveattackxp = tag.getInt("receiveattackxp");
        this.receivestrengthxp = tag.getInt("receivestrengthxp");
        this.receivedefensexp = tag.getInt("receivedefensexp");
        this.receiverangedxp = tag.getInt("receiverangedxp");
        this.receivemagicxp = tag.getInt("receivemagicxp");
        this.hastoolbeltpestleandmortar = tag.getInt("hastoolbeltpestleandmortar");
        this.hastoolbeltneedle = tag.getInt("hastoolbeltneedle");
        this.bountycharges = tag.getInt("bountycharges");
    }

    public static void levelClientUpdate(Player player) {
        if (!player.level().isClientSide()) {
            ServerPlayer serverPlayer = (ServerPlayer) player;
            player.getCapability(Provider.RA_EXTRADATA).ifPresent(ed ->
                    SimpleNetworkHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> serverPlayer),
                            new ClientExtraDataPacket(ed.getHasHome(), ed.getX(), ed.getY(), ed.getZ(),
                                            ed.getHasToolbeltChisel(), ed.getClayBraceletCharges(), ed.getRecoilRingCharges(), ed.getTotalXP(),
                                    ed.getreceiveattackxp(), ed.getreceivestrengthxp(), ed.getreceivedefensexp(),
                                    ed.getreceiverangedxp(), ed.getreceivemagicxp(), ed.getHasToolbeltPestleAndMortar(), ed.getBountyCharges(), ed.getHasToolbeltNeedle())));
        }
    }

    public static Tag writeNBT(Capability<RunicAgesExtraDataCapability> capability, RunicAgesExtraDataCapability instance, Direction side) {
        CompoundTag tag = new CompoundTag();
        tag.putInt("homeset", instance.homeset);
        tag.putFloat("posx", instance.posx);
        tag.putFloat("posy", instance.posy);
        tag.putFloat("posz", instance.posz);
        tag.putInt("claybraceletcharges", instance.getClayBraceletCharges());
        tag.putInt("recoilringcharges", instance.getRecoilRingCharges());
        tag.putInt("hastoolbeltchisel", instance.hastoolbeltchisel);
        tag.putInt("totalxp", instance.totalxp);
        tag.putInt("receiveattackxp", instance.receiveattackxp);
        tag.putInt("receivestrengthxp", instance.receivestrengthxp);
        tag.putInt("receivedefensexp", instance.receivedefensexp);
        tag.putInt("receiverangedxp", instance.receiverangedxp);
        tag.putInt("receivemagicxp", instance.receivemagicxp);
        tag.putInt("hastoolbeltpestleandmortar", instance.hastoolbeltpestleandmortar);
        tag.putInt("hastoolbeltneedle", instance.hastoolbeltneedle);
        tag.putInt("bountycharges", instance.getBountyCharges());
        return tag;
    }

    public static void readNBT(Capability<RunicAgesExtraDataCapability> capability, RunicAgesExtraDataCapability instance, Direction side, Tag nbt) {
        instance.sethashome(((CompoundTag) nbt).getInt("homeset"));
        instance.setx(((CompoundTag) nbt).getFloat("posx"));
        instance.sety(((CompoundTag) nbt).getFloat("posy"));
        instance.setz(((CompoundTag) nbt).getFloat("posz"));
        instance.resetclaybraceletcharges(((CompoundTag) nbt).getInt("claybraceletcharges"));
        instance.setrecoilringcharges(((CompoundTag) nbt).getInt("recoilringcharges"));
        instance.sethastoolbeltchisel(((CompoundTag) nbt).getInt("hastoolbeltchisel"));
        instance.settotalxp(((CompoundTag) nbt).getInt("totalxp"));
        instance.receiveattackxp(((CompoundTag) nbt).getInt("receiveattackxp"));
        instance.receivestrengthxp(((CompoundTag) nbt).getInt("receivestrengthxp"));
        instance.receivedefensexp(((CompoundTag) nbt).getInt("receivedefensexp"));
        instance.receiverangedxp(((CompoundTag) nbt).getInt("receiverangedxp"));
        instance.receivemagicxp(((CompoundTag) nbt).getInt("receivemagicxp"));
        instance.sethastoolbeltpestleandmortar(((CompoundTag) nbt).getInt("hastoolbeltpestleandmortar"));
        instance.sethastoolbeltneedle(((CompoundTag) nbt).getInt("hastoolbeltneedle"));
        instance.setbountycharges(((CompoundTag) nbt).getInt("bountycharges"));
    }

    public static class Provider implements ICapabilitySerializable<Tag> {
        public static Capability<RunicAgesExtraDataCapability> RA_EXTRADATA = CapabilityManager.get(new CapabilityToken<>(){});

        @Nonnull
        private final RunicAgesExtraDataCapability instance;

        private final LazyOptional<RunicAgesExtraDataCapability> handler;

        public Provider() {
            instance = new RunicAgesExtraDataCapability();
            handler = LazyOptional.of(this::getInstance);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return RA_EXTRADATA.orEmpty(cap, handler);
        }

        public static LazyOptional<RunicAgesExtraDataCapability> getFrom(Player player) {
            return player.getCapability(RA_EXTRADATA);
        }

        public void invalidate() {
            handler.invalidate();
        }

        public RunicAgesExtraDataCapability getInstance() {
            return instance;
        }

        @Override
        public Tag serializeNBT() {
            return RunicAgesExtraDataCapability.writeNBT(RA_EXTRADATA, instance, null);
        }

        @Override
        public void deserializeNBT(Tag nbt) {
            RunicAgesExtraDataCapability.readNBT(RA_EXTRADATA, instance, null, nbt);
        }
    }

}