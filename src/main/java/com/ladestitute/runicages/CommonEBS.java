package com.ladestitute.runicages;

import com.ladestitute.runicages.capability.agility.RunicAgesAgilityCapability;
import com.ladestitute.runicages.capability.agility.RunicAgesAgilityCapabilityHandler;
import com.ladestitute.runicages.capability.attack.RunicAgesAttackCapability;
import com.ladestitute.runicages.capability.attack.RunicAgesAttackCapabilityHandler;
import com.ladestitute.runicages.capability.crafting.RunicAgesCraftingCapability;
import com.ladestitute.runicages.capability.crafting.RunicAgesCraftingCapabilityHandler;
import com.ladestitute.runicages.capability.defense.RunicAgesDefenseCapability;
import com.ladestitute.runicages.capability.defense.RunicAgesDefenseCapabilityHandler;
import com.ladestitute.runicages.capability.farming.RunicAgesFarmingCapability;
import com.ladestitute.runicages.capability.farming.RunicAgesFarmingCapabilityHandler;
import com.ladestitute.runicages.capability.herblore.RunicAgesHerbloreCapability;
import com.ladestitute.runicages.capability.herblore.RunicAgesHerbloreCapabilityHandler;
import com.ladestitute.runicages.capability.magic.RunicAgesMagicCapability;
import com.ladestitute.runicages.capability.magic.RunicAgesMagicCapabilityHandler;
import com.ladestitute.runicages.capability.mining.RunicAgesMiningCapability;
import com.ladestitute.runicages.capability.mining.RunicAgesMiningCapabilityHandler;
import com.ladestitute.runicages.capability.ranged.RunicAgesRangedCapability;
import com.ladestitute.runicages.capability.ranged.RunicAgesRangedCapabilityHandler;
import com.ladestitute.runicages.capability.runecrafting.RunicAgesRunecraftingCapability;
import com.ladestitute.runicages.capability.runecrafting.RunicAgesRunecraftingCapabilityHandler;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapabilityHandler;
import com.ladestitute.runicages.capability.smithing.RunicAgesSmithingCapability;
import com.ladestitute.runicages.capability.smithing.RunicAgesSmithingCapabilityHandler;
import com.ladestitute.runicages.capability.strength.RunicAgesStrengthCapability;
import com.ladestitute.runicages.capability.strength.RunicAgesStrengthCapabilityHandler;
import com.ladestitute.runicages.capability.thieving.RunicAgesThievingCapability;
import com.ladestitute.runicages.capability.thieving.RunicAgesThievingCapabilityHandler;
import com.ladestitute.runicages.capability.woodcutting.RunicAgesWoodcuttingCapability;
import com.ladestitute.runicages.capability.woodcutting.RunicAgesWoodcuttingCapabilityHandler;
import com.ladestitute.runicages.entities.mobs.*;
import com.ladestitute.runicages.registry.EntityTypeInit;
import net.minecraft.world.entity.SpawnPlacements;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.RegisterCapabilitiesEvent;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;

@Mod.EventBusSubscriber(modid = RunicAgesMain.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class CommonEBS {

    @SubscribeEvent
    public static void onStaticCommonSetup(FMLCommonSetupEvent event) {
        MinecraftForge.EVENT_BUS.register(new RunicAgesExtraDataCapabilityHandler());
        MinecraftForge.EVENT_BUS.register(new RunicAgesMiningCapabilityHandler());
        MinecraftForge.EVENT_BUS.register(new RunicAgesSmithingCapabilityHandler());
        MinecraftForge.EVENT_BUS.register(new RunicAgesWoodcuttingCapabilityHandler());
        MinecraftForge.EVENT_BUS.register(new RunicAgesRunecraftingCapabilityHandler());
        MinecraftForge.EVENT_BUS.register(new RunicAgesMagicCapabilityHandler());
        MinecraftForge.EVENT_BUS.register(new RunicAgesCraftingCapabilityHandler());
        MinecraftForge.EVENT_BUS.register(new RunicAgesAttackCapabilityHandler());
        MinecraftForge.EVENT_BUS.register(new RunicAgesStrengthCapabilityHandler());
        MinecraftForge.EVENT_BUS.register(new RunicAgesDefenseCapabilityHandler());
        MinecraftForge.EVENT_BUS.register(new RunicAgesRangedCapabilityHandler());
        MinecraftForge.EVENT_BUS.register(new RunicAgesHerbloreCapabilityHandler());
        MinecraftForge.EVENT_BUS.register(new RunicAgesFarmingCapabilityHandler());
        MinecraftForge.EVENT_BUS.register(new RunicAgesThievingCapabilityHandler());
        MinecraftForge.EVENT_BUS.register(new RunicAgesAgilityCapabilityHandler());
        SpawnPlacements.register(EntityTypeInit.IMP.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, ImpEntity::checkSpawnRules);
        SpawnPlacements.register(EntityTypeInit.GRIZZLY_BEAR.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, GrizzlyBearEntity::checkGrizzlyBearSpawnRules);
        SpawnPlacements.register(EntityTypeInit.BLACK_BEAR.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, BlackBearEntity::checkBlackBearSpawnRules);
        SpawnPlacements.register(EntityTypeInit.UNICORN.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, UnicornEntity::checkSpawnRules);
        SpawnPlacements.register(EntityTypeInit.DEADLY_RED_SPIDER.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, DeadlyRedSpiderEntity::checkMonsterSpawnRules);
    }

    //The proper way to register capabilities in 1.18 onward
    @SubscribeEvent
    public void registerCaps(RegisterCapabilitiesEvent event) {
        event.register(RunicAgesExtraDataCapability.class);
        event.register(RunicAgesMiningCapability.class);
        event.register(RunicAgesSmithingCapability.class);
        event.register(RunicAgesWoodcuttingCapability.class);
        event.register(RunicAgesRunecraftingCapability.class);
        event.register(RunicAgesMagicCapability.class);
        event.register(RunicAgesCraftingCapability.class);
        event.register(RunicAgesAttackCapability.class);
        event.register(RunicAgesStrengthCapability.class);
        event.register(RunicAgesDefenseCapability.class);
        event.register(RunicAgesRangedCapability.class);
        event.register(RunicAgesHerbloreCapability.class);
        event.register(RunicAgesFarmingCapability.class);
        event.register(RunicAgesThievingCapability.class);
        event.register(RunicAgesAgilityCapability.class);
    }

    @SubscribeEvent
    public static void registerAttributes(EntityAttributeCreationEvent event) {
        event.put(EntityTypeInit.IMP.get(), ImpEntity.createAttributes().build());
        event.put(EntityTypeInit.GRIZZLY_BEAR.get(), GrizzlyBearEntity.createAttributes().build());
        event.put(EntityTypeInit.BLACK_BEAR.get(), BlackBearEntity.createAttributes().build());
        event.put(EntityTypeInit.UNICORN.get(), UnicornEntity.createAttributes().build());
        event.put(EntityTypeInit.DEADLY_RED_SPIDER.get(), DeadlyRedSpiderEntity.createAttributes().build());
    }


}

