package com.ladestitute.runicages.entities;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.registry.EntityTypeInit;
import com.ladestitute.runicages.registry.ItemInit;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundAddEntityPacket;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.projectile.ThrowableItemProjectile;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;

@SuppressWarnings("EntityConstructor")
public class EarthStrikeEntity extends ThrowableItemProjectile {

    // Three constructors, also make sure not to miss this line when altering it for copy-pasting
    public EarthStrikeEntity(EntityType<EarthStrikeEntity> type, Level world) {
        super(type, world);
        this.setNoGravity(true);
    }

    public EarthStrikeEntity(LivingEntity entity, Level world) {
        super(EntityTypeInit.EARTH_STRIKE.get(), entity, world);
    }

    public EarthStrikeEntity(double x, double y, double z, Level world) {
        super(EntityTypeInit.EARTH_STRIKE.get(), x, y, z, world);
    }

    // Get the item that the projectile is thrown from, blocks require ".asItem()" as well
    @Override
    protected Item getDefaultItem() {
        return ItemInit.EARTH_STRIKE_SPELL.get().asItem();
    }

    // Spawns the entity, just as important as the above method
    @Override
    public Packet<ClientGamePacketListener> getAddEntityPacket() {
        return new ClientboundAddEntityPacket(this);
    }


    // A method to do things on entity or block-hit
    @Override
    protected void onHit(HitResult result) {
        //This line is checking the type of RayTraceResult, in this case
        //it will be when it hits and entity
        if (result.getType() == HitResult.Type.ENTITY) {
            //This is a variable that we have set, it gets the entity from the RayTraceResult.
            //We cast it to EntityRayTraceResult, just to ensure that it is infact an entity.
            Entity entity = ((EntityHitResult) result).getEntity();
            //This integer is the damage value that it gives to the entity when it is hit
            //I haven't initialized it here as I will do that below.
            float damage = 7F;

            TagKey<EntityType<?>> earth_weakness = TagKey.create(Registries.ENTITY_TYPE,
                    new ResourceLocation(RunicAgesMain.MODID, "earth_weakness"));

            if(earth_weakness.equals(entity.getType()) && !this.level().isClientSide())
            {
                entity.hurt(damageSources().thrown(this, this.getOwner()), (float) (damage*1.5));
            }
            else entity.hurt(damageSources().thrown(this, this.getOwner()), damage);

            //ItemStack stack1 = new ItemStack(ItemInit.GLASS_SHARD.get());
            // ItemEntity rock = new ItemEntity(this.getCommandSenderWorld(), this.getX(), this.getY() + 1, this.getZ(), stack1);
            //level.addFreshEntity(rock);
            if (!level().isClientSide) {
                this.discard();
            }
        }

        //Just like before this checks the result and if it hits a block this code will run
        if (result.getType() == HitResult.Type.BLOCK) {
            // ItemStack stack1 = new ItemStack(ItemInit.GLASS_SHARD.get());
            // ItemEntity rock = new ItemEntity(this.getCommandSenderWorld(), this.getX(), this.getY() + 1, this.getZ(), stack1);
            // level.addFreshEntity(rock);
            this.discard();
            //Now we get the BlockRayTraceResult from the result
            //Casting it to the BlockRayTraceResult.
            BlockHitResult blockRTR = (BlockHitResult) result;

            //I have checked to see if it hits the top of the block

            //    if (blockRTR.getFace() == Direction.UP) {
            //Then I have added a small check here to only allow something to happen when it
            //Hits a grass block
            //  if (world.getBlockState(blockRTR.getPos()) == Blocks.GRASS_BLOCK.getDefaultState()) {
            //This gets the world, and then sets the blockstate of the position of the entity
            //and the blockstate
            //  world.setBlockState(this.getOnPosition(), Blocks.STONE.getDefaultState());
            //     }

            //And just incase non of these are true, I am removing it from the world.
            if (!level().isClientSide) {
                this.discard();
            }
        }
    }
}



