package com.ladestitute.runicages.entities.projectiles.arrows;

import com.ladestitute.runicages.registry.EntityTypeInit;
import com.ladestitute.runicages.registry.ItemInit;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.projectile.AbstractArrow;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraftforge.network.NetworkHooks;
import net.minecraftforge.network.PlayMessages;

//The EntityConstructor suppress warning is required for them to function in +1.16
@SuppressWarnings("EntityConstructor")
public class BronzeArrowEntity extends AbstractArrow {

    //2.0D is the damage of a vanilla arrow
    public static double baseDamage = 2.0D;

    public BronzeArrowEntity(EntityType<? extends BronzeArrowEntity> entity, Level world) {
        super(entity, world);
        this.setBaseDamage(baseDamage);
    }

    public BronzeArrowEntity(Level world, LivingEntity shooter) {
        super(EntityTypeInit.BRONZE_ARROW.get(), shooter, world);
        this.setBaseDamage(baseDamage);
    }

    public BronzeArrowEntity(PlayMessages.SpawnEntity packet, Level world) {
        super(EntityTypeInit.BRONZE_ARROW.get(), world);
        this.setBaseDamage(baseDamage);
    }

    @Override
    protected ItemStack getPickupItem() {
        return new ItemStack(ItemInit.BRONZE_ARROW.get());
    }

    @Override
    public Packet<ClientGamePacketListener> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }
}
