package com.ladestitute.runicages.effects;

import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectCategory;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.LivingEntity;

public class PoisonImmunityEffect extends MobEffect

{
    public PoisonImmunityEffect(MobEffectCategory mobEffectCategory, int color){
        super(mobEffectCategory, color);
    }

    @Override
    public void applyEffectTick (LivingEntity pLivingEntity, int pAmplifier){
        if(pLivingEntity.hasEffect(MobEffects.POISON))
        {
            pLivingEntity.removeEffect(MobEffects.POISON);
        }
        super.applyEffectTick(pLivingEntity, pAmplifier);
    }

    @Override
    public boolean isDurationEffectTick ( int pDuration, int pAmplifier){
        return true;
    }
}