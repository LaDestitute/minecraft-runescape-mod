package com.ladestitute.runicages.registry;

import com.google.common.collect.ImmutableSet;
import com.ladestitute.runicages.RunicAgesMain;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.entity.ai.village.poi.PoiType;
import net.minecraft.world.entity.npc.VillagerProfession;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.util.ObfuscationReflectionHelper;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.lang.reflect.InvocationTargetException;

public class VillagerInit {
    public static final DeferredRegister<PoiType> POI_TYPES =
            DeferredRegister.create(ForgeRegistries.POI_TYPES, RunicAgesMain.MODID);
    public static final DeferredRegister<VillagerProfession> VILLAGER_PROFESSIONS =
            DeferredRegister.create(ForgeRegistries.VILLAGER_PROFESSIONS, RunicAgesMain.MODID);

    public static final RegistryObject<PoiType> WIZARD_POI = POI_TYPES.register("wizard_poi",
            () -> new PoiType(ImmutableSet.copyOf(Blocks.ENCHANTING_TABLE.getStateDefinition().getPossibleStates()),
                    1, 1));

    public static final RegistryObject<PoiType> CRAFTSMAN_POI = POI_TYPES.register("craftsman_poi",
            () -> new PoiType(ImmutableSet.copyOf(Blocks.CRAFTING_TABLE.getStateDefinition().getPossibleStates()),
                    1, 1));

    public static final RegistryObject<PoiType> DRUID_POI = POI_TYPES.register("druid_poi",
            () -> new PoiType(ImmutableSet.copyOf(Blocks.MOSSY_COBBLESTONE_SLAB.getStateDefinition().getPossibleStates()),
                    1, 1));

    public static final RegistryObject<PoiType> BANKER_POI = POI_TYPES.register("banker_poi",
            () -> new PoiType(ImmutableSet.copyOf(Blocks.GOLD_BLOCK.getStateDefinition().getPossibleStates()),
                    1, 1));

    public static final RegistryObject<VillagerProfession> WIZARD = VILLAGER_PROFESSIONS.register("wizard",
            () -> new VillagerProfession("wizard", x -> x.get() == WIZARD_POI.get(),
                    x -> x.get() == WIZARD_POI.get(), ImmutableSet.of(), ImmutableSet.of(),
                    SoundEvents.VILLAGER_WORK_CLERIC));

    public static final RegistryObject<VillagerProfession> CRAFTSMAN = VILLAGER_PROFESSIONS.register("craftsman",
            () -> new VillagerProfession("craftsman", x -> x.get() == CRAFTSMAN_POI.get(),
                    x -> x.get() == CRAFTSMAN_POI.get(), ImmutableSet.of(), ImmutableSet.of(),
                    SoundEvents.VILLAGER_WORK_CLERIC));

    public static final RegistryObject<VillagerProfession> DRUID = VILLAGER_PROFESSIONS.register("druid",
            () -> new VillagerProfession("druid", x -> x.get() == DRUID_POI.get(),
                    x -> x.get() == DRUID_POI.get(), ImmutableSet.of(), ImmutableSet.of(),
                    SoundEvents.VILLAGER_WORK_CLERIC));

    public static final RegistryObject<VillagerProfession> BANKER = VILLAGER_PROFESSIONS.register("banker",
            () -> new VillagerProfession("banker", x -> x.get() == BANKER_POI.get(),
                    x -> x.get() == BANKER_POI.get(), ImmutableSet.of(), ImmutableSet.of(),
                    SoundEvents.VILLAGER_WORK_CLERIC));

    public static void register(IEventBus eventBus) {
        POI_TYPES.register(eventBus);
        VILLAGER_PROFESSIONS.register(eventBus);
    }
}
