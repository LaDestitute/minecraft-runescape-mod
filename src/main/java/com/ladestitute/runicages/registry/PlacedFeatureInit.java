package com.ladestitute.runicages.registry;

import com.ladestitute.runicages.RunicAgesMain;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderGetter;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.data.worldgen.placement.VegetationPlacements;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.placement.*;

import java.util.List;

//
//import com.ladestitute.runicages.RunicAgesMain;
//import com.ladestitute.runicages.world.OrePlacement;
//import net.minecraft.core.Holder;
//import net.minecraft.core.Registry;
//import net.minecraft.data.worldgen.features.VegetationFeatures;
//import net.minecraft.data.worldgen.placement.PlacementUtils;
//import net.minecraft.data.worldgen.placement.VegetationPlacements;
//import net.minecraft.world.level.levelgen.VerticalAnchor;
//import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
//import net.minecraft.world.level.levelgen.placement.*;
//import net.minecraftforge.eventbus.api.IEventBus;
//import net.minecraftforge.registries.DeferredRegister;
//import net.minecraftforge.registries.RegistryObject;
//
//import java.util.List;
//
//@SuppressWarnings("ALL")
public class PlacedFeatureInit {

    public static final ResourceKey<PlacedFeature> NORMAL_PLACED_KEY = createKey("normal_placed");
    public static final ResourceKey<PlacedFeature> COPPER_PLACED_SWAMPS_KEY = createKey("copper_placed_swamps");
    public static final ResourceKey<PlacedFeature> COPPER_PLACED_SANDY_KEY = createKey("copper_placed_sandy");
    public static final ResourceKey<PlacedFeature> COPPER_PLACED_SLOPE_KEY = createKey("copper_placed_slope");
    public static final ResourceKey<PlacedFeature> TIN_PLACED_SWAMPS_KEY = createKey("tin_placed_swamps");
    public static final ResourceKey<PlacedFeature> TIN_PLACED_SANDY_KEY = createKey("tin_placed_sandy");
    public static final ResourceKey<PlacedFeature> TIN_PLACED_SLOPE_KEY = createKey("tin_placed_slope");
    public static final ResourceKey<PlacedFeature> CLAY_ROCK_PLACED_KEY = createKey("clay_rock_placed");
    public static final ResourceKey<PlacedFeature> BLURITE_ROCK_PLACED_KEY = createKey("blurite_rock_placed");
    public static final ResourceKey<PlacedFeature> IRON_ROCK_PLACED_KEY = createKey("iron_rock_placed");
    public static final ResourceKey<PlacedFeature> IRON_ROCK_PLACED_SANDY_KEY = createKey("iron_rock_placed_sandy");
    public static final ResourceKey<PlacedFeature> IRON_ROCK_PLACED_RIVER_KEY = createKey("iron_rock_placed_river");
    public static final ResourceKey<PlacedFeature> IRON_ROCK_PLACED_MOUNTAIN_KEY = createKey("iron_rock_placed_mountain");
    public static final ResourceKey<PlacedFeature> IRON_ROCK_PLACED_FOREST_KEY = createKey("iron_rock_placed_forest");
    public static final ResourceKey<PlacedFeature> IRON_ROCK_PLACED_JUNGLE_KEY = createKey("iron_rock_placed_jungle");
    public static final ResourceKey<PlacedFeature> IRON_ROCK_PLACED_CONIFER_KEY = createKey("iron_rock_placed_conifer");
    public static final ResourceKey<PlacedFeature> SILVER_ROCK_PLACED_KEY = createKey("silver_rock_placed");
    public static final ResourceKey<PlacedFeature> MITHRIL_ROCK_PLACED_KEY = createKey("mithril_rock_placed");
    public static final ResourceKey<PlacedFeature> ADAMANTITE_ROCK_PLACED_KEY = createKey("adamantite_rock_placed");
    public static final ResourceKey<PlacedFeature> LUMINITE_ROCK_PLACED_KEY = createKey("luminite_rock_placed");
    public static final ResourceKey<PlacedFeature> LIGHT_JUNGLE_PLACED_KEY = createKey("light_jungle_placed");
    public static final ResourceKey<PlacedFeature> FLAX_PLACED_KEY = createKey("flax_placed");
    public static final ResourceKey<PlacedFeature> TIN_ORE_PLACED_KEY = createKey("tin_ore_placed");
    public static final ResourceKey<PlacedFeature> BLURITE_ORE_PLACED_KEY = createKey("blurite_ore_placed");
    public static final ResourceKey<PlacedFeature> RUNE_ORE_PLACED_KEY = createKey("rune_ore_placed");
    public static final ResourceKey<PlacedFeature> SILVER_ORE_PLACED_KEY = createKey("silver_ore_placed");
    public static final ResourceKey<PlacedFeature> MITHRIL_ORE_PLACED_KEY = createKey("mithril_ore_placed");
    public static final ResourceKey<PlacedFeature> ADAMANTITE_ORE_PLACED_KEY = createKey("adamantite_ore_placed");
    public static final ResourceKey<PlacedFeature> LUMINITE_ORE_PLACED_KEY = createKey("luminite_ore_placed");

    public static void bootstrap(BootstapContext<PlacedFeature> context) {
        HolderGetter<ConfiguredFeature<?, ?>> configuredFeatures = context.lookup(Registries.CONFIGURED_FEATURE);

        register(context, NORMAL_PLACED_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.NORMAL_TREE),
                VegetationPlacements.treePlacement(PlacementUtils.countExtra(0, 0.2f, 1),
                        Blocks.OAK_SAPLING));

        register(context, COPPER_PLACED_SWAMPS_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.COPPER),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));
        register(context, COPPER_PLACED_SANDY_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.COPPER),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));
        register(context, COPPER_PLACED_SLOPE_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.COPPER),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));

        register(context, TIN_PLACED_SWAMPS_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.TIN),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));
        register(context, TIN_PLACED_SANDY_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.TIN),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));
        register(context, TIN_PLACED_SLOPE_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.TIN),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));

        register(context, CLAY_ROCK_PLACED_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.CLAY),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));

        register(context, BLURITE_ROCK_PLACED_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.BLURITE),
                List.of(RarityFilter.onAverageOnceEvery(4),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));

        register(context, IRON_ROCK_PLACED_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.IRON),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));
        register(context, IRON_ROCK_PLACED_SANDY_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.IRON),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));
        register(context, IRON_ROCK_PLACED_RIVER_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.IRON),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));
        register(context, IRON_ROCK_PLACED_MOUNTAIN_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.IRON),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));
        register(context, IRON_ROCK_PLACED_FOREST_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.IRON),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));
        register(context, IRON_ROCK_PLACED_JUNGLE_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.IRON),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));
        register(context, IRON_ROCK_PLACED_CONIFER_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.IRON),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));
        register(context, SILVER_ROCK_PLACED_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.SILVER),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));
        register(context, MITHRIL_ROCK_PLACED_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.MITHRIL),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));
        register(context, ADAMANTITE_ROCK_PLACED_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.ADAMANTITE),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));
        register(context, LUMINITE_ROCK_PLACED_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.LUMINITE),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));
        register(context, LIGHT_JUNGLE_PLACED_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.LIGHT_JUNGLE),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));
        register(context, FLAX_PLACED_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.FLAX),
                List.of(RarityFilter.onAverageOnceEvery(16),
                        InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome()));

        register(context, TIN_ORE_PLACED_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.OVERWORLD_TIN_ORES_KEY),
                commonOrePlacement(9, // VeinsPerChunk
                        HeightRangePlacement.triangle(VerticalAnchor.aboveBottom(-15), VerticalAnchor.aboveBottom(111))));

        register(context, BLURITE_ORE_PLACED_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.OVERWORLD_BLURITE_ORES_KEY),
                commonOrePlacement(5, // VeinsPerChunk
                        HeightRangePlacement.triangle(VerticalAnchor.aboveBottom(-15), VerticalAnchor.aboveBottom(111))));
        register(context, RUNE_ORE_PLACED_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.OVERWORLD_RUNE_ESSENCE_ORES_KEY),
                commonOrePlacement(5, // VeinsPerChunk
                        HeightRangePlacement.triangle(VerticalAnchor.aboveBottom(-15), VerticalAnchor.aboveBottom(111))));
        register(context, SILVER_ORE_PLACED_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.OVERWORLD_SILVER_ORES_KEY),
                commonOrePlacement(5, // VeinsPerChunk
                        HeightRangePlacement.triangle(VerticalAnchor.aboveBottom(-64), VerticalAnchor.aboveBottom(32))));
        register(context, MITHRIL_ORE_PLACED_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.OVERWORLD_MITHRIL_ORES_KEY),
                commonOrePlacement(10, // VeinsPerChunk
                        HeightRangePlacement.triangle(VerticalAnchor.aboveBottom(-24), VerticalAnchor.aboveBottom(56))));
        register(context, ADAMANTITE_ORE_PLACED_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.OVERWORLD_ADAMANTITE_ORES_KEY),
                commonOrePlacement(10, // VeinsPerChunk
                        HeightRangePlacement.triangle(VerticalAnchor.aboveBottom(-24), VerticalAnchor.aboveBottom(56))));
        register(context, LUMINITE_ORE_PLACED_KEY, configuredFeatures.getOrThrow(ConfiguredFeatureInit.OVERWORLD_LUMINITE_ORES_KEY),
                commonOrePlacement(12, // VeinsPerChunk
                        HeightRangePlacement.triangle(VerticalAnchor.aboveBottom(-16), VerticalAnchor.aboveBottom(112))));

    }

    public static List<PlacementModifier> orePlacement(PlacementModifier p_195347_, PlacementModifier p_195348_) {
        return List.of(p_195347_, InSquarePlacement.spread(), p_195348_, BiomeFilter.biome());
    }

    public static List<PlacementModifier> commonOrePlacement(int p_195344_, PlacementModifier p_195345_) {
        return orePlacement(CountPlacement.of(p_195344_), p_195345_);
    }

    private static void register(BootstapContext<PlacedFeature> context, ResourceKey<PlacedFeature> key, Holder<ConfiguredFeature<?, ?>> configuration,
                                 List<PlacementModifier> modifiers) {
        context.register(key, new PlacedFeature(configuration, List.copyOf(modifiers)));
    }

    private static void register(BootstapContext<PlacedFeature> context, ResourceKey<PlacedFeature> key, Holder<ConfiguredFeature<?, ?>> configuration,
                                 PlacementModifier... modifiers) {
        register(context, key, configuration, List.of(modifiers));
    }

    private static ResourceKey<PlacedFeature> createKey(String name) {
        return ResourceKey.create(Registries.PLACED_FEATURE, new ResourceLocation(RunicAgesMain.MODID, name));
    }
//    public static final DeferredRegister<PlacedFeature> PLACED_FEATURES =
//            DeferredRegister.create(Registry.PLACED_FEATURE_REGISTRY, RunicAgesMain.MODID);
//
//    public static final RegistryObject<PlacedFeature> NORMAL_PLACED = PLACED_FEATURES.register("normal_placed",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.NORMAL_TREE, VegetationPlacements.treePlacement(
//                    PlacementUtils.countExtra(0, 0.2f, 1))));
//    //Fiddling around with the above countExtra values occasionally causes crashes, investigating further later
//
//    public static final RegistryObject<PlacedFeature> COPPER_PLACED_SWAMPS = PLACED_FEATURES.register("copper_placed_swamps",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.COPPER, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//    public static final RegistryObject<PlacedFeature> COPPER_PLACED_SANDY = PLACED_FEATURES.register("copper_placed_sandy",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.COPPER, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//    public static final RegistryObject<PlacedFeature> COPPER_PLACED_SLOPE = PLACED_FEATURES.register("copper_placed_slope",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.COPPER, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//
//    public static final RegistryObject<PlacedFeature> TIN_PLACED_SWAMPS = PLACED_FEATURES.register("tin_placed_swamps",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.TIN, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//    public static final RegistryObject<PlacedFeature> TIN_PLACED_SANDY = PLACED_FEATURES.register("tin_placed_sandy",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.TIN, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//    public static final RegistryObject<PlacedFeature> TIN_PLACED_SLOPE = PLACED_FEATURES.register("tin_placed_slope",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.TIN, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//
//
//    public static final RegistryObject<PlacedFeature> CLAY_ROCK_PLACED = PLACED_FEATURES.register("clay_rock_placed",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.CLAY, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//
//    public static final RegistryObject<PlacedFeature> BLURITE_ROCK_PLACED = PLACED_FEATURES.register("blurite_rock_placed",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.BLURITE, List.of(RarityFilter.onAverageOnceEvery(4),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//
//    public static final RegistryObject<PlacedFeature> IRON_ROCK_PLACED = PLACED_FEATURES.register("iron_rock_placed",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.IRON, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//
//    public static final RegistryObject<PlacedFeature> IRON_ROCK_PLACED_SANDY = PLACED_FEATURES.register("iron_rock_placed_sandy",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.IRON, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//
//    public static final RegistryObject<PlacedFeature> IRON_ROCK_PLACED_RIVER = PLACED_FEATURES.register("iron_rock_placed_river",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.IRON, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//
//    public static final RegistryObject<PlacedFeature> IRON_ROCK_PLACED_MOUNTAIN = PLACED_FEATURES.register("iron_rock_placed_mountain",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.IRON, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//
//    public static final RegistryObject<PlacedFeature> IRON_ROCK_PLACED_FOREST = PLACED_FEATURES.register("iron_rock_placed_forest",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.IRON, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//
//    public static final RegistryObject<PlacedFeature> IRON_ROCK_PLACED_JUNGLE = PLACED_FEATURES.register("iron_rock_placed_jungle",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.IRON, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//
//    public static final RegistryObject<PlacedFeature> IRON_ROCK_PLACED_CONIFER = PLACED_FEATURES.register("iron_rock_placed_conifer",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.IRON, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//
//    public static final RegistryObject<PlacedFeature> SILVER_ROCK_PLACED = PLACED_FEATURES.register("silver_rock_placed",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.SILVER, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//
//    public static final RegistryObject<PlacedFeature> MITHRIL_ROCK_PLACED = PLACED_FEATURES.register("mithril_rock_placed",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.MITHRIL, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//
//    public static final RegistryObject<PlacedFeature> ADAMANTITE_ROCK_PLACED = PLACED_FEATURES.register("adamantite_rock_placed",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.ADAMANTITE, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//
//    public static final RegistryObject<PlacedFeature> LUMINITE_ROCK_PLACED = PLACED_FEATURES.register("luminite_rock_placed",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.LUMINITE, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//
//    public static final RegistryObject<PlacedFeature> LIGHT_JUNGLE_PLACED = PLACED_FEATURES.register("light_jungle_placed",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.LIGHT_JUNGLE, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//
//    public static final RegistryObject<PlacedFeature> FLAX_PLACED = PLACED_FEATURES.register("flax_placed",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.FLAX, List.of(RarityFilter.onAverageOnceEvery(16),
//                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));
//
//    // Ore
//
//    public static final RegistryObject<PlacedFeature> TIN_ORE_PLACED = PLACED_FEATURES.register("tin_ore_placed",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.TIN_ORE, OrePlacement.commonOrePlacement(
//                    9, // VeinsPerChunk
//                    HeightRangePlacement.triangle(VerticalAnchor.aboveBottom(-15), VerticalAnchor.aboveBottom(111)))));
//
//    public static final RegistryObject<PlacedFeature> BLURITE_ORE_PLACED = PLACED_FEATURES.register("blurite_ore_placed",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.BLURITE_ORE, OrePlacement.commonOrePlacement(
//                    5, // VeinsPerChunk
//                    HeightRangePlacement.triangle(VerticalAnchor.aboveBottom(-15), VerticalAnchor.aboveBottom(111)))));
//
//    public static final RegistryObject<PlacedFeature> RUNE_ORE_PLACED = PLACED_FEATURES.register("rune_ore_placed",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.RUNE_ESSENCE_ORE, OrePlacement.commonOrePlacement(
//                    5, // VeinsPerChunk
//                    HeightRangePlacement.triangle(VerticalAnchor.aboveBottom(-15), VerticalAnchor.aboveBottom(111)))));
//
//    public static final RegistryObject<PlacedFeature> SILVER_ORE_PLACED = PLACED_FEATURES.register("silver_ore_placed",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.SILVER_ORE, OrePlacement.commonOrePlacement(
//                    5, // VeinsPerChunk
//                    HeightRangePlacement.triangle(VerticalAnchor.aboveBottom(-64), VerticalAnchor.aboveBottom(32)))));
//
//    public static final RegistryObject<PlacedFeature> MITHRIL_ORE_PLACED = PLACED_FEATURES.register("mithril_ore_placed",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.MITHRIL_ORE, OrePlacement.commonOrePlacement(
//                    10, HeightRangePlacement.triangle(VerticalAnchor.absolute(-24), VerticalAnchor.absolute(56)))));
//
//    public static final RegistryObject<PlacedFeature> ADAMANTITE_ORE_PLACED = PLACED_FEATURES.register("adamantite_ore_placed",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.ADAMANTITE_ORE, OrePlacement.commonOrePlacement(
//                    10, HeightRangePlacement.triangle(VerticalAnchor.absolute(-24), VerticalAnchor.absolute(56)))));
//
//    public static final RegistryObject<PlacedFeature> LUMINITE_ORE_PLACED = PLACED_FEATURES.register("luminite_ore_placed",
//            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
//                    ConfiguredFeatureInit.LUMINITE_ORE, OrePlacement.commonOrePlacement(
//                    12, HeightRangePlacement.triangle(VerticalAnchor.absolute(-16), VerticalAnchor.absolute(112)))));
//
//    public static void register(IEventBus eventBus) {
//        PLACED_FEATURES.register(eventBus);
//    }
}
