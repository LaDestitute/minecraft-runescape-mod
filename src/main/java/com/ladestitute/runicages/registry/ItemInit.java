package com.ladestitute.runicages.registry;

import com.ladestitute.runicages.RunicAgesCreativeTabs;
import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.items.BankCertificateItem;
import com.ladestitute.runicages.items.MagicNotepaperItem;
import com.ladestitute.runicages.items.cooking.*;
import com.ladestitute.runicages.items.crafting.ClayBowlItem;
import com.ladestitute.runicages.items.crafting.JewelryMoldPackItem;
import com.ladestitute.runicages.items.crafting.gems.CrushedGemItem;
import com.ladestitute.runicages.items.crafting.gems.cut.OpalItem;
import com.ladestitute.runicages.items.crafting.gems.cut.SapphireItem;
import com.ladestitute.runicages.items.crafting.gems.uncut.UncutOpalItem;
import com.ladestitute.runicages.items.crafting.gems.uncut.UncutSapphireItem;
import com.ladestitute.runicages.items.crafting.leather.CowhideItem;
import com.ladestitute.runicages.items.crafting.leather.HardLeatherItem;
import com.ladestitute.runicages.items.crafting.leather.SoftLeatherItem;
import com.ladestitute.runicages.items.crafting.material.ThreadItem;
import com.ladestitute.runicages.items.crafting.tools.ChiselItem;
import com.ladestitute.runicages.items.crafting.tools.NeedleItem;
import com.ladestitute.runicages.items.crafting.tools.PestleAndMortarItem;
import com.ladestitute.runicages.items.crafting.tools.SewingKitItem;
import com.ladestitute.runicages.items.farming.FishingBaitItem;
import com.ladestitute.runicages.items.farming.produce.BarleyItem;
import com.ladestitute.runicages.items.farming.produce.CabbageItem;
import com.ladestitute.runicages.items.farming.produce.OnionItem;
import com.ladestitute.runicages.items.farming.produce.RedberryItem;
import com.ladestitute.runicages.items.farming.seeds.*;
import com.ladestitute.runicages.items.herblore.VialOfWaterItem;
import com.ladestitute.runicages.items.herblore.VialOfWaterPackItem;
import com.ladestitute.runicages.items.herblore.herbs.clean.*;
import com.ladestitute.runicages.items.herblore.herbs.grimy.*;
import com.ladestitute.runicages.items.herblore.potions.*;
import com.ladestitute.runicages.items.herblore.secondaries.*;
import com.ladestitute.runicages.items.herblore.unfinished.GuamPotionUnfItem;
import com.ladestitute.runicages.items.herblore.unfinished.MarrentillPotionUnfItem;
import com.ladestitute.runicages.items.herblore.unfinished.TarrominPotionUnfItem;
import com.ladestitute.runicages.items.magic.armor.wizard.WizardBootsItem;
import com.ladestitute.runicages.items.magic.armor.wizard.WizardHatItem;
import com.ladestitute.runicages.items.magic.armor.wizard.WizardRobeSkirtItem;
import com.ladestitute.runicages.items.magic.armor.wizard.WizardRobeTopItem;
import com.ladestitute.runicages.items.magic.enchanted.lapis.RingOfLuckItem;
import com.ladestitute.runicages.items.magic.enchanted.opal.AmuletOfBountifulHarvestItem;
import com.ladestitute.runicages.items.magic.enchanted.sapphire.BraceletOfClayItem;
import com.ladestitute.runicages.items.magic.enchanted.sapphire.RingOfRecoilItem;
import com.ladestitute.runicages.items.magic.spells.*;
import com.ladestitute.runicages.items.magic.tablets.BonesToBananasTabletItem;
import com.ladestitute.runicages.items.magic.tablets.EnchantSapphireTabletItem;
import com.ladestitute.runicages.items.magic.tablets.HomeTeleportTabletItem;
import com.ladestitute.runicages.items.magic.weapons.StaffItem;
import com.ladestitute.runicages.items.magic.weapons.StaffOfAirItem;
import com.ladestitute.runicages.items.magic.weapons.WizardWandItem;
import com.ladestitute.runicages.items.mining.stonespirits.CopperStoneSpiritItem;
import com.ladestitute.runicages.items.mining.stonespirits.IronStoneSpiritItem;
import com.ladestitute.runicages.items.mining.stonespirits.SilverStoneSpiritItem;
import com.ladestitute.runicages.items.mining.stonespirits.TinStoneSpiritItem;
import com.ladestitute.runicages.items.misc.AshesItem;
import com.ladestitute.runicages.items.misc.BurntMeatItem;
import com.ladestitute.runicages.items.misc.ChefsHatItem;
import com.ladestitute.runicages.items.misc.FlierItem;
import com.ladestitute.runicages.items.ore.*;
import com.ladestitute.runicages.items.ranged.BronzeArrowItem;
import com.ladestitute.runicages.items.runecrafting.PureEssenceItem;
import com.ladestitute.runicages.items.runecrafting.RuneEssenceItem;
import com.ladestitute.runicages.items.runecrafting.runes.*;
import com.ladestitute.runicages.items.runecrafting.talismans.*;
import com.ladestitute.runicages.items.smithing.HammerItem;
import com.ladestitute.runicages.items.smithing.blurite.BluriteBarItem;
import com.ladestitute.runicages.items.smithing.blurite.BluriteSwordItem;
import com.ladestitute.runicages.items.smithing.bronze.*;
import com.ladestitute.runicages.items.smithing.bronze.salvage.TinyPlatedBronzeSalvageItem;
import com.ladestitute.runicages.items.smithing.bronze.salvage.TinySpikyIronSalvageItem;
import com.ladestitute.runicages.items.smithing.mithril.MithrilBarItem;
import com.ladestitute.runicages.items.smithing.silver.SilverBarItem;
import com.ladestitute.runicages.items.smithing.steel.SteelBarItem;
import com.ladestitute.runicages.items.smithing.unfinishedbars.UnfinishedBronzePickaxeItem;
import com.ladestitute.runicages.items.woodcutting.*;
import com.ladestitute.runicages.util.RunicAgesArmorMaterial;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.food.FoodProperties;
import net.minecraft.world.item.*;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.material.Fluids;
import net.minecraftforge.common.ForgeSpawnEggItem;
import net.minecraftforge.common.ForgeTier;
import net.minecraftforge.common.TierSortingRegistry;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.List;

public class ItemInit {
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, RunicAgesMain.MODID);

    //Combat
    public static final RegistryObject<Item> RING_OF_RECOIL = ITEMS.register("ring_of_recoil",
            () -> new RingOfRecoilItem(new Item.Properties()));
    public static final RegistryObject<Item> RING_OF_LUCK = ITEMS.register("ring_of_luck",
            () -> new RingOfLuckItem(new Item.Properties()));

    //Melee
    public static final RegistryObject<SwordItem> BRONZE_DAGGER = ITEMS.register("bronze_dagger",
            () -> new BronzeDaggerItem(ToolTiers.BRONZE, 2, 5.0f,
                    new Item.Properties()));
    public static final RegistryObject<SwordItem> BRONZE_MACE = ITEMS.register("bronze_mace",
            () -> new BronzeMaceItem(ToolTiers.BRONZE, 2, 5.0f,
                    new Item.Properties()));
    public static final RegistryObject<SwordItem> BRONZE_SWORD = ITEMS.register("bronze_sword",
            () -> new BronzeSwordItem(ToolTiers.BRONZE, 2, 5.0f,
                    new Item.Properties()));
    public static RegistryObject<ArmorItem> BRONZE_HELM = ITEMS.register("bronze_helm", () ->
            new BronzeHelmItem(ArmorTiers.BRONZE, ArmorItem.Type.HELMET, props()));
    public static RegistryObject<ArmorItem> BRONZE_CHAINBODY = ITEMS.register("bronze_chainbody", () ->
            new BronzeChainbodyItem(ArmorTiers.BRONZE_CHAINBODY, ArmorItem.Type.CHESTPLATE, props()));
    public static RegistryObject<ArmorItem> BRONZE_PLATEBODY = ITEMS.register("bronze_platebody", () ->
            new BronzePlatebodyItem(ArmorTiers.BRONZE, ArmorItem.Type.CHESTPLATE, props()));
    public static RegistryObject<ArmorItem> BRONZE_PLATELEGS = ITEMS.register("bronze_platelegs", () ->
            new BronzePlatelegsItem(ArmorTiers.BRONZE, ArmorItem.Type.LEGGINGS, props()));
    public static RegistryObject<ArmorItem> BRONZE_BOOTS = ITEMS.register("bronze_boots", () ->
            new BronzeBootsItem(ArmorTiers.BRONZE, ArmorItem.Type.BOOTS, props()));
    public static final RegistryObject<ShieldItem> BRONZE_SQ_SHIELD = ITEMS.register("bronze_sq_shield",
            () -> new BronzeSqShieldItem(new Item.Properties().durability(168)));
    public static final RegistryObject<SwordItem> IRON_DAGGER = ITEMS.register("iron_dagger",
            () -> new BronzeDaggerItem(Tiers.IRON, 2, 5.0f,
                    new Item.Properties()));
    public static final RegistryObject<SwordItem> BLURITE_SWORD = ITEMS.register("blurite_sword",
            () -> new BluriteSwordItem(ToolTiers.BLURITE, 3, 5.0f,
                    new Item.Properties()));

    //Mining
    public static final RegistryObject<PickaxeItem> BRONZE_PICKAXE = ITEMS.register("bronze_pickaxe",
            () -> new BronzePickaxeItem(ToolTiers.BRONZE, 2, 5.0f,
                    new Item.Properties()));
    public static final RegistryObject<Item> COPPER_ORE = ITEMS.register("copper_ore",
            () -> new CopperOreItem(new Item.Properties()));
    public static final RegistryObject<Item> TIN_ORE = ITEMS.register("tin_ore",
            () -> new TinOreItem(new Item.Properties()));
    public static final RegistryObject<Item> IRON_ORE = ITEMS.register("iron_ore_item",
            () -> new IronOreItem(new Item.Properties()));
    public static final RegistryObject<Item> BLURITE_ORE = ITEMS.register("blurite_ore",
            () -> new BluriteOreItem(new Item.Properties()));
    public static final RegistryObject<Item> SILVER_ORE = ITEMS.register("silver_ore",
            () -> new SilverOreItem(new Item.Properties()));
    public static final RegistryObject<Item> MITHRIL_ORE = ITEMS.register("mithril_ore",
            () -> new MithrilOreItem(new Item.Properties()));
    public static final RegistryObject<Item> ADAMANTITE_ORE = ITEMS.register("adamantite_ore",
            () -> new MithrilOreItem(new Item.Properties()));
    public static final RegistryObject<Item> LUMINITE = ITEMS.register("luminite",
            () -> new MithrilOreItem(new Item.Properties()));
    public static final RegistryObject<Item> HARD_CLAY = ITEMS.register("hard_clay",
            () -> new HardClayItem(new Item.Properties()));
    public static final RegistryObject<Item> RUNE_ESSENCE = ITEMS.register("rune_essence",
            () -> new RuneEssenceItem(new Item.Properties()));
    public static final RegistryObject<Item> PURE_ESSENCE = ITEMS.register("pure_essence",
            () -> new PureEssenceItem(new Item.Properties()));
    public static final RegistryObject<Item> COPPER_STONE_SPIRIT = ITEMS.register("copper_stone_spirit",
            () -> new CopperStoneSpiritItem(new Item.Properties()));
    public static final RegistryObject<Item> TIN_STONE_SPIRIT = ITEMS.register("tin_stone_spirit",
            () -> new TinStoneSpiritItem(new Item.Properties()));
    public static final RegistryObject<Item> IRON_STONE_SPIRIT = ITEMS.register("iron_stone_spirit",
            () -> new IronStoneSpiritItem(new Item.Properties()));
    public static final RegistryObject<Item> SILVER_STONE_SPIRIT = ITEMS.register("silver_stone_spirit",
            () -> new SilverStoneSpiritItem(new Item.Properties()));
    public static final RegistryObject<Item> BRACELET_OF_CLAY = ITEMS.register("bracelet_of_clay",
            () -> new BraceletOfClayItem(new Item.Properties()));

    //Agility

    //Smithing
    public static final RegistryObject<Item> HAMMER = ITEMS.register("hammer",
            () -> new HammerItem(new Item.Properties()));
    public static final RegistryObject<Item> UNFINISHED_BRONZE_PICKAXE = ITEMS.register("unfinished_bronze_pickaxe",
            () -> new UnfinishedBronzePickaxeItem(new Item.Properties()));
    public static final RegistryObject<Item> BRONZE_BAR = ITEMS.register("bronze_bar",
            () -> new BronzeBarItem(new Item.Properties()));
    public static final RegistryObject<Item> BLURITE_BAR = ITEMS.register("blurite_bar",
            () -> new BluriteBarItem(new Item.Properties()));
    public static final RegistryObject<Item> SILVER_BAR = ITEMS.register("silver_bar",
            () -> new SilverBarItem(new Item.Properties()));
    public static final RegistryObject<Item> STEEL_BAR = ITEMS.register("steel_bar",
            () -> new SteelBarItem(new Item.Properties()));
    public static final RegistryObject<Item> MITHRIL_BAR = ITEMS.register("mithril_bar",
            () -> new MithrilBarItem(new Item.Properties()));
    public static final RegistryObject<Item> ADAMANTITE_BAR = ITEMS.register("adamantite_bar",
            () -> new MithrilBarItem(new Item.Properties()));
    public static final RegistryObject<Item> SILVTHRIL_BAR = ITEMS.register("silvthril_bar",
            () -> new MithrilBarItem(new Item.Properties()));
    public static final RegistryObject<Item> BRONZE_NUGGET = ITEMS.register("bronze_nugget",
            () -> new Item(new Item.Properties()));
    public static final RegistryObject<Item> BRONZE_CHAIN = ITEMS.register("bronze_chain",
            () -> new Item(new Item.Properties()));
    public static final RegistryObject<Item> TINY_PLATED_BRONZE_SALVAGE = ITEMS.register("tiny_plated_bronze_salvage",
            () -> new TinyPlatedBronzeSalvageItem(new Item.Properties()));
    public static final RegistryObject<Item> TINY_SPIKY_IRON_SALVAGE = ITEMS.register("tiny_spiky_iron_salvage",
            () -> new TinySpikyIronSalvageItem(new Item.Properties()));

    //Herblore
    public static final RegistryObject<Item> VIAL_OF_WATER_PACK = ITEMS.register("vial_of_water_pack",
            () -> new VialOfWaterPackItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> VIAL_OF_WATER = ITEMS.register("vial_of_water",
            () -> new VialOfWaterItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> GRIMY_GUAM = ITEMS.register("grimy_guam",
            () -> new GrimyGuamItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> CLEAN_GUAM = ITEMS.register("clean_guam",
            () -> new CleanGuamItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> GRIMY_TARROMIN = ITEMS.register("grimy_tarromin",
            () -> new GrimyTarrominItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> CLEAN_TARROMIN = ITEMS.register("clean_tarromin",
            () -> new CleanTarrominItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> GRIMY_MARRENTILL = ITEMS.register("grimy_marrentill",
            () -> new GrimyMarrentillItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> CLEAN_MARRENTILL = ITEMS.register("clean_marrentill",
            () -> new CleanMarrentillItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> GRIMY_HARRALANDER = ITEMS.register("grimy_harralander",
            () -> new GrimyHarralanderItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> CLEAN_HARRALANDER = ITEMS.register("clean_harralander",
            () -> new CleanHarralanderItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> GRIMY_RANARR = ITEMS.register("grimy_ranarr",
            () -> new GrimyRanarrItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> CLEAN_RANARR = ITEMS.register("clean_ranarr",
            () -> new CleanRanarrItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> GRIMY_IRIT = ITEMS.register("grimy_irit",
            () -> new GrimyIritItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> CLEAN_IRIT = ITEMS.register("clean_irit",
            () -> new CleanIritItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> GRIMY_AVANTOE = ITEMS.register("grimy_avantoe",
            () -> new GrimyAvantoeItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> CLEAN_AVANTOE = ITEMS.register("clean_avantoe",
            () -> new CleanAvantoeItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> GRIMY_KWUARM = ITEMS.register("grimy_kwuarm",
            () -> new GrimyKwuarmItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> CLEAN_KWUARM = ITEMS.register("clean_kwuarm",
            () -> new CleanKwuarmItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> GRIMY_CADANTINE = ITEMS.register("grimy_cadantine",
            () -> new GrimyCadantineItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> CLEAN_CADANTINE = ITEMS.register("clean_cadantine",
            () -> new CleanCadantineItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> GRIMY_LANTADYME = ITEMS.register("grimy_lantadyme",
            () -> new GrimyLantadymeItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> CLEAN_LANTADYME = ITEMS.register("clean_lantadyme",
            () -> new CleanLantadymeItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> GRIMY_DWARF_WEED = ITEMS.register("grimy_dwarf_weed",
            () -> new GrimyDwarfWeedItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> CLEAN_DWARF_WEED = ITEMS.register("clean_dwarf_weed",
            () -> new CleanDwarfWeedItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> GUAM_POTION_UNF = ITEMS.register("guam_potion_unf",
            () -> new GuamPotionUnfItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> MARRENTILL_POTION_UNF = ITEMS.register("marrentill_potion_unf",
            () -> new MarrentillPotionUnfItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> TARROMIN_POTION_UNF = ITEMS.register("tarromin_potion_unf",
            () -> new TarrominPotionUnfItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> EYE_OF_NEWT = ITEMS.register("eye_of_newt",
            () -> new EyeOfNewtItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> WHITE_BEAD = ITEMS.register("white_bead",
            () -> new WhiteBeadItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> RED_BEAD = ITEMS.register("red_bead",
            () -> new RedBeadItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> YELLOW_BEAD = ITEMS.register("yellow_bead",
            () -> new YellowBeadItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> BLACK_BEAD = ITEMS.register("black_bead",
            () -> new BlackBeadItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> BEAR_FUR = ITEMS.register("bear_fur",
            () -> new BearFurItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> UNICORN_HORN = ITEMS.register("unicorn_horn",
            () -> new UnicornHornItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> UNICORN_HORN_DUST = ITEMS.register("unicorn_horn_dust",
            () -> new UnicornHornDustItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> RED_SPIDERS_EGGS = ITEMS.register("red_spiders_eggs",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> BOTANISTS_AMULET = ITEMS.register("botanists_amulet",
            () -> new Item(new Item.Properties().durability(5)));
    public static final RegistryObject<Item> AMULET_OF_CHEMISTRY = ITEMS.register("amulet_of_chemistry",
            () -> new Item(new Item.Properties().durability(5)));
    public static final RegistryObject<Item> ATTACK_POTION = ITEMS.register("attack_potion",
            () -> new AttackPotionItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> RANGING_POTION = ITEMS.register("ranging_potion",
            () -> new RangingPotionItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> MAGIC_POTION = ITEMS.register("magic_potion",
            () -> new MagicPotionItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> DEFENSE_POTION = ITEMS.register("defense_potion",
            () -> new DefensePotionItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> ANTIPOISON_POTION = ITEMS.register("antipoison_potion",
            () -> new AntipoisonPotionItem(new Item.Properties().stacksTo(1)));

    //Fishing
    public static final RegistryObject<Item> FISHING_BAIT = ITEMS.register("fishing_bait",
            () -> new FishingBaitItem(new Item.Properties().stacksTo(1)));

    //Ranged
    public static final RegistryObject<Item> BOWSTRING = ITEMS.register("bowstring",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> BRONZE_ARROW = ITEMS.register("bronze_arrow",
            () -> new BronzeArrowItem(new Item.Properties()));

    //Thieving

    //Cooking
    public static final RegistryObject<Item> SPIDER_CARCASS = ITEMS.register("spider_carcass",
            () -> new SpiderCarcassItem(new Item.Properties()));
    public static final RegistryObject<Item> RAW_SPIDER_ON_STICK = ITEMS.register("raw_spider_on_stick",
            () -> new RawSpiderOnStickItem(new Item.Properties()));
    public static final RegistryObject<Item> SPIDER_ON_STICK = ITEMS.register("spider_on_stick",
            () -> new SpiderOnStickItem(props().food(Foods.SPIDER_ON_STICK)));
    public static final RegistryObject<Item> BANANA = ITEMS.register("banana",
            () -> new BananaItem(props().food(Foods.BANANA)));
    public static final RegistryObject<Item> RAW_BEAR_MEAT = ITEMS.register("raw_bear_meat",
            () -> new RawBearMeatItem(new Item.Properties()));
    public static final RegistryObject<Item> COOKED_MEAT = ITEMS.register("cooked_meat",
            () -> new CookedMeatItem(props().food(Foods.COOKED_MEAT)));

    //Crafting
    public static final RegistryObject<Item> CHISEL = ITEMS.register("chisel",
            () -> new ChiselItem(new Item.Properties()));
    public static final RegistryObject<Item> PESTLE_AND_MORTAR = ITEMS.register("pestle_and_mortar",
            () -> new PestleAndMortarItem(new Item.Properties()));
    public static final RegistryObject<Item> COWHIDE = ITEMS.register("cowhide",
            () -> new CowhideItem(new Item.Properties()));
    public static final RegistryObject<Item> SOFT_LEATHER = ITEMS.register("soft_leather",
            () -> new SoftLeatherItem(new Item.Properties()));
    public static final RegistryObject<Item> HARD_LEATHER = ITEMS.register("hard_leather",
            () -> new HardLeatherItem(new Item.Properties()));
    public static final RegistryObject<Item> CRUSHED_GEM = ITEMS.register("crushed_gem",
            () -> new CrushedGemItem(new Item.Properties()));
    public static final RegistryObject<Item> RING_MOLD = ITEMS.register("ring_mold",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> TIARA_MOLD = ITEMS.register("tiara_mold",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> NECKLACE_MOLD = ITEMS.register("necklace_mold",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> BRACELET_MOLD = ITEMS.register("bracelet_mold",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> AMULET_MOLD = ITEMS.register("amulet_mold",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> FILLED_TIARA_MOLD = ITEMS.register("filled_tiara_mold",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> FILLED_SILVER_RING_MOLD = ITEMS.register("filled_silver_ring_mold",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> FILLED_GOLD_RING_MOLD = ITEMS.register("filled_gold_ring_mold",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> FILLED_GOLD_BRACELET_MOLD = ITEMS.register("filled_gold_bracelet_mold",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> FILLED_SAPPHIRE_BRACELET_MOLD = ITEMS.register("filled_sapphire_bracelet_mold",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> FILLED_GOLD_NECKLACE_MOLD = ITEMS.register("filled_gold_necklace_mold",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> FILLED_GOLD_AMULET_MOLD = ITEMS.register("filled_gold_amulet_mold",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> FILLED_SAPPHIRE_AMULET_MOLD = ITEMS.register("filled_sapphire_amulet_mold",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> FILLED_OPAL_AMULET_MOLD = ITEMS.register("filled_opal_amulet_mold",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> FILLED_OPAL_RING_MOLD = ITEMS.register("filled_opal_ring_mold",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> FILLED_LAPIS_LAZULI_RING_MOLD = ITEMS.register("filled_lapis_lazuli_ring_mold",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> FILLED_SAPPHIRE_RING_MOLD = ITEMS.register("filled_sapphire_ring_mold",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> SILVER_RING = ITEMS.register("silver_ring",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> GOLD_RING = ITEMS.register("gold_ring",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> LAPIS_LAZULI_RING = ITEMS.register("lapis_lazuli_ring",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> SAPPHIRE_RING = ITEMS.register("sapphire_ring",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> GOLD_NECKLACE = ITEMS.register("gold_necklace",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> GOLD_BRACELET = ITEMS.register("gold_bracelet",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> UNSTRUNG_GOLD_AMULET = ITEMS.register("unstrung_gold_amulet",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> GOLD_AMULET = ITEMS.register("gold_amulet",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> UNSTRUNG_SAPPHIRE_AMULET = ITEMS.register("unstrung_sapphire_amulet",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> SAPPHIRE_AMULET = ITEMS.register("sapphire_amulet",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> UNSTRUNG_OPAL_AMULET = ITEMS.register("unstrung_opal_amulet",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> OPAL_AMULET = ITEMS.register("opal_amulet",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> SAPPHIRE_BRACELET = ITEMS.register("sapphire_bracelet",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> OPAL_RING = ITEMS.register("opal_ring",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<ArmorItem> TIARA = ITEMS.register("tiara",
            () -> new ArmorItem(ArmorTiers.TIARA, ArmorItem.Type.HELMET, new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> BALL_OF_WOOL = ITEMS.register("ball_of_wool",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> STRIP_OF_CLOTH = ITEMS.register("strip_of_cloth",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> FLAX = ITEMS.register("flax",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> UNFIRED_POT = ITEMS.register("unfired_pot",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> CLAY_POT = ITEMS.register("clay_pot",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> SEWING_KIT = ITEMS.register("sewing_kit",
            () -> new SewingKitItem(new Item.Properties()));
    public static final RegistryObject<Item> NEEDLE = ITEMS.register("needle",
            () -> new NeedleItem(new Item.Properties()));
    public static final RegistryObject<Item> THREAD = ITEMS.register("thread",
            () -> new ThreadItem(new Item.Properties()));
    public static final RegistryObject<Item> UNFIRED_PIE_DISH = ITEMS.register("unfired_pie_dish",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> PIE_DISH = ITEMS.register("pie_dish",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> UNFIRED_BOWL = ITEMS.register("unfired_bowl",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> CLAY_BOWL = ITEMS.register("clay_bowl",
            () -> new ClayBowlItem(Fluids.EMPTY, new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> BOWL_OF_WATER = ITEMS.register("bowl_of_water",
            () -> new Item(new Item.Properties().stacksTo(1).craftRemainder(ItemInit.CLAY_BOWL.get())));
    public static final RegistryObject<Item> UNCUT_SAPPHIRE = ITEMS.register("uncut_sapphire",
            () -> new UncutSapphireItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> UNCUT_OPAL = ITEMS.register("uncut_opal",
            () -> new UncutOpalItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> SAPPHIRE = ITEMS.register("sapphire",
            () -> new SapphireItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> OPAL = ITEMS.register("opal",
            () -> new OpalItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> CUT_LAPIS_LAZULI = ITEMS.register("cut_lapis_lazuli",
            () -> new Item(new Item.Properties().stacksTo(1)));

    //Magic
    public static RegistryObject<ArmorItem> WIZARD_HAT = ITEMS.register("wizard_hat", () ->
            new WizardHatItem(ArmorTiers.WIZARD, ArmorItem.Type.HELMET, props()));
    public static RegistryObject<ArmorItem> WIZARD_ROBE_TOP = ITEMS.register("wizard_robe_top", () ->
            new WizardRobeTopItem(ArmorTiers.WIZARD, ArmorItem.Type.CHESTPLATE, props()));
    public static RegistryObject<ArmorItem> WIZARD_ROBE_SKIRT = ITEMS.register("wizard_robe_skirt", () ->
            new WizardRobeSkirtItem(ArmorTiers.WIZARD, ArmorItem.Type.LEGGINGS, props()));
    public static RegistryObject<ArmorItem> WIZARD_BOOTS = ITEMS.register("wizard_boots", () ->
            new WizardBootsItem(ArmorTiers.WIZARD, ArmorItem.Type.BOOTS, props()));
    public static final RegistryObject<Item> STAFF = ITEMS.register("staff",
            () -> new StaffItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> MAGIC_WAND = ITEMS.register("magic_wand",
            () -> new WizardWandItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> STAFF_OF_AIR = ITEMS.register("staff_of_air",
            () -> new StaffOfAirItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> AMULET_OF_MAGIC = ITEMS.register("amulet_of_magic",
            () -> new Item(new Item.Properties()));
    public static final RegistryObject<Item> UNENCHANTED_HOME_TELEPORT_TABLET = ITEMS.register("unenchanted_home_teleport_tablet",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> HOME_TELEPORT_TABLET = ITEMS.register("home_teleport_tablet",
            () -> new HomeTeleportTabletItem(new Item.Properties()));
    public static final RegistryObject<Item> UNENCHANTED_BONES_TO_BANANAS_TABLET = ITEMS.register("unenchanted_bones_to_bananas_tablet",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> BONES_TO_BANANAS_TABLET = ITEMS.register("bones_to_bananas_tablet",
            () -> new BonesToBananasTabletItem(new Item.Properties()));
    public static final RegistryObject<Item> UNENCHANTED_ENCHANT_SAPPHIRE_TABLET = ITEMS.register("unenchanted_enchant_sapphire_tablet",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> ENCHANT_SAPPHIRE_TABLET = ITEMS.register("enchant_sapphire_tablet",
            () -> new EnchantSapphireTabletItem(new Item.Properties()));
    public static final RegistryObject<Item> AIR_STRIKE_SPELL = ITEMS.register("air_strike_spell",
            () -> new AirStrikeSpellItem(new Item.Properties()));
    public static final RegistryObject<Item> WATER_STRIKE_SPELL = ITEMS.register("water_strike_spell",
            () -> new WaterStrikeSpellItem(new Item.Properties()));
    public static final RegistryObject<Item> EARTH_STRIKE_SPELL = ITEMS.register("earth_strike_spell",
            () -> new EarthStrikeSpellItem(new Item.Properties()));
    public static final RegistryObject<Item> FIRE_STRIKE_SPELL = ITEMS.register("fire_strike_spell",
            () -> new FireStrikeSpellItem(new Item.Properties()));
    public static final RegistryObject<Item> CONFUSE_SPELL = ITEMS.register("confuse_spell",
            () -> new ConfuseSpellItem(new Item.Properties()));
    public static final RegistryObject<Item> WEAKEN_SPELL = ITEMS.register("weaken_spell",
            () -> new WeakenSpellItem(new Item.Properties()));


    //Woodcutting
    public static final RegistryObject<AxeItem> BRONZE_HATCHET = ITEMS.register("bronze_hatchet",
            () -> new BronzeHatchetItem(ToolTiers.BRONZE, 2, 5.0f,
                    new Item.Properties()));
    public static final RegistryObject<Item> NORMAL_TREE_LOG = ITEMS.register("normal_log",
            () -> new NormalTreeLogItem(new Item.Properties()));
    public static final RegistryObject<Item> BIRCH_TREE_LOG = ITEMS.register("birch_log_item",
            () -> new BirchLogItem(new Item.Properties()));
    public static final RegistryObject<Item> STRUNG_RABBIT_FOOT = ITEMS.register("strung_rabbit_foot",
            () -> new StrungRabbitFootItem(new Item.Properties()));
    public static final RegistryObject<Item> BIRDS_NEST = ITEMS.register("birds_nest",
            () -> new BirdsNestItem(new Item.Properties()));
    public static final RegistryObject<Item> BIRDS_NEST_EGG = ITEMS.register("birds_nest_egg",
            () -> new BirdsNestEggItem(new Item.Properties()));
    public static final RegistryObject<Item> BIRDS_NEST_SEEDS = ITEMS.register("birds_nest_seeds",
            () -> new BirdsNestSeedsItem(new Item.Properties()));
    public static final RegistryObject<Item> LIGHT_THATCH_SPAR = ITEMS.register("light_thatch_spar",
            () -> new LightThatchSparItem(new Item.Properties()));
    public static final RegistryObject<Item> SKEWER_STICK = ITEMS.register("skewer_stick",
            () -> new SkewerStickItem(new Item.Properties()));

    //Runecrafting
    public static final RegistryObject<Item> AIR_TALISMAN = ITEMS.register("air_talisman",
            () -> new AirTalismanItem(new Item.Properties()));
    public static final RegistryObject<Item> MIND_TALISMAN = ITEMS.register("mind_talisman",
            () -> new MindTalismanItem(new Item.Properties()));
    public static final RegistryObject<Item> WATER_TALISMAN = ITEMS.register("water_talisman",
            () -> new WaterTalismanItem(new Item.Properties()));
    public static final RegistryObject<Item> EARTH_TALISMAN = ITEMS.register("earth_talisman",
            () -> new EarthTalismanItem(new Item.Properties()));
    public static final RegistryObject<Item> FIRE_TALISMAN = ITEMS.register("fire_talisman",
            () -> new FireTalismanItem(new Item.Properties()));
    public static final RegistryObject<Item> BODY_TALISMAN = ITEMS.register("body_talisman",
            () -> new BodyTalismanItem(new Item.Properties()));
    public static final RegistryObject<Item> AIR_RUNE = ITEMS.register("air_rune",
            () -> new AirRuneItem(new Item.Properties()));
    public static final RegistryObject<Item> MIND_RUNE = ITEMS.register("mind_rune",
            () -> new MindRuneItem(new Item.Properties()));
    public static final RegistryObject<Item> WATER_RUNE = ITEMS.register("water_rune",
            () -> new WaterRuneItem(new Item.Properties()));
    public static final RegistryObject<Item> EARTH_RUNE = ITEMS.register("earth_rune",
            () -> new EarthRuneItem(new Item.Properties()));
    public static final RegistryObject<Item> FIRE_RUNE = ITEMS.register("fire_rune",
            () -> new FireRuneItem(new Item.Properties()));
    public static final RegistryObject<Item> BODY_RUNE = ITEMS.register("body_rune",
            () -> new BodyRuneItem(new Item.Properties()));
    public static final RegistryObject<Item> COSMIC_RUNE = ITEMS.register("cosmic_rune",
            () -> new CosmicRuneItem(new Item.Properties()));
    public static final RegistryObject<Item> CHAOS_RUNE = ITEMS.register("chaos_rune",
            () -> new ChaosRuneItem(new Item.Properties()));
    public static final RegistryObject<Item> NATURE_RUNE = ITEMS.register("nature_rune",
            () -> new NatureRuneItem(new Item.Properties()));
    public static final RegistryObject<Item> LAW_RUNE = ITEMS.register("law_rune",
            () -> new LawRuneItem(new Item.Properties()));
    public static final RegistryObject<ArmorItem> AIR_TIARA = ITEMS.register("air_tiara",
            () -> new ArmorItem(ArmorTiers.AIR_TIARA, ArmorItem.Type.HELMET, new Item.Properties().stacksTo(1)));
    public static final RegistryObject<ArmorItem> WATER_TIARA = ITEMS.register("water_tiara",
            () -> new ArmorItem(ArmorTiers.WATER_TIARA, ArmorItem.Type.HELMET, new Item.Properties().stacksTo(1)));
    public static final RegistryObject<ArmorItem> EARTH_TIARA = ITEMS.register("earth_tiara",
            () -> new ArmorItem(ArmorTiers.EARTH_TIARA, ArmorItem.Type.HELMET, new Item.Properties().stacksTo(1)));
    public static final RegistryObject<ArmorItem> FIRE_TIARA = ITEMS.register("fire_tiara",
            () -> new ArmorItem(ArmorTiers.FIRE_TIARA, ArmorItem.Type.HELMET, new Item.Properties().stacksTo(1)));
    public static final RegistryObject<ArmorItem> MIND_TIARA = ITEMS.register("mind_tiara",
            () -> new ArmorItem(ArmorTiers.MIND_TIARA, ArmorItem.Type.HELMET, new Item.Properties().stacksTo(1)));
    public static final RegistryObject<ArmorItem> BODY_TIARA = ITEMS.register("body_tiara",
            () -> new ArmorItem(ArmorTiers.BODY_TIARA, ArmorItem.Type.HELMET, new Item.Properties().stacksTo(1)));

    //Farming
    public static final RegistryObject<Item> BARLEY_SEED
            = ITEMS.register("barley_seed", () ->
            new BarleySeedItem(new Item.Properties()));
    public static final RegistryObject<Item> BARLEY = ITEMS.register("barley",
            () -> new BarleyItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> ONION_SEED
            = ITEMS.register("onion_seed", () ->
            new OnionSeedItem(new Item.Properties()));
    public static final RegistryObject<Item> ONION = ITEMS.register("onion",
            () -> new OnionItem(new Item.Properties().stacksTo(1).food(Foods.ONION)));
    public static final RegistryObject<Item> CABBAGE_SEED
            = ITEMS.register("cabbage_seed", () ->
            new CabbageSeedItem(new Item.Properties()));
    public static final RegistryObject<Item> CABBAGE = ITEMS.register("cabbage",
            () -> new CabbageItem(new Item.Properties().stacksTo(1).food(Foods.ONION)));
    public static final RegistryObject<Item> GUAM_SEED
            = ITEMS.register("guam_seed", () ->
            new GuamSeedItem(new Item.Properties()));
    public static final RegistryObject<Item> REDBERRY_SEED
            = ITEMS.register("redberry_seed", () ->
            new RedberrySeedItem(new Item.Properties()));
    public static final RegistryObject<Item> REDBERRY = ITEMS.register("redberry",
            () -> new RedberryItem(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> AMULET_OF_BOUNTIFUL_HARVEST = ITEMS.register("amulet_of_bountiful_harvest",
            () -> new AmuletOfBountifulHarvestItem(new Item.Properties()));
    public static final RegistryObject<Item> AMULET_OF_BOUNTY = ITEMS.register("amulet_of_bounty",
            () -> new AmuletOfBountifulHarvestItem(new Item.Properties()));
    public static final RegistryObject<Item> JEWELRY_MOLD_PACK = ITEMS.register("jewelry_mold_pack",
            () -> new JewelryMoldPackItem(new Item.Properties()));

    //A shortcut for setting tabs
    private static Item.Properties props() {
        return new Item.Properties();
    }

    public static final RegistryObject<Item> BANK_CERTIFICATE = ITEMS.register("bank_certificate",
            () -> new BankCertificateItem(new Item.Properties()));
    public static final RegistryObject<Item> MAGIC_NOTEPAPER = ITEMS.register("magic_notepaper",
            () -> new MagicNotepaperItem(new Item.Properties()));
    public static final RegistryObject<Item> NOTED_AIR_TALISMAN = ITEMS.register("noted_air_talisman",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BIRCH_LOG = ITEMS.register("noted_birch_log",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BLURITE_BAR = ITEMS.register("noted_blurite_bar",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BLURITE_ORE = ITEMS.register("noted_blurite_ore",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BRONZE_BAR = ITEMS.register("noted_bronze_bar",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_EARTH_TALISMAN = ITEMS.register("noted_earth_talisman",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_FIRE_TALISMAN = ITEMS.register("noted_fire_talisman",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_HARD_CLAY = ITEMS.register("noted_hard_clay",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_MIND_TALISMAN = ITEMS.register("noted_mind_talisman",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_LOG = ITEMS.register("noted_log",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_PURE_ESSENCE = ITEMS.register("noted_pure_essence",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_RUNE_ESSENCE = ITEMS.register("noted_rune_essence",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_SPIDER_CARCASS = ITEMS.register("noted_spider_carcass",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_THATCH_SPAR = ITEMS.register("noted_thatch_spar",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_TIN_ORE = ITEMS.register("noted_tin_ore",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_WATER_TALISMAN = ITEMS.register("noted_water_talisman",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_VIAL_OF_WATER = ITEMS.register("noted_vial_of_water",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BODY_TALISMAN =
            ITEMS.register("noted_body_talisman",
            () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_AIR_TIARA =
            ITEMS.register("noted_air_tiara",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_MIND_TIARA =
            ITEMS.register("noted_mind_tiara",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_WATER_TIARA =
            ITEMS.register("noted_water_tiara",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_EARTH_TIARA =
            ITEMS.register("noted_earth_tiara",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_FIRE_TIARA =
            ITEMS.register("noted_fire_tiara",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BODY_TIARA =
            ITEMS.register("noted_body_tiara",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_SILVER_ORE =
            ITEMS.register("noted_silver_ore",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_MITHRIL_ORE =
            ITEMS.register("noted_mithril_ore",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_ADAMANTITE_ORE =
            ITEMS.register("noted_adamantite_ore",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_LUMINITE =
            ITEMS.register("noted_luminite",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_SILVER_BAR =
            ITEMS.register("noted_silver_bar",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_STEEL_BAR =
            ITEMS.register("noted_steel_bar",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_SILVTHRIL_BAR =
            ITEMS.register("noted_silvthril_bar",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BRONZE_PICKAXE =
            ITEMS.register("noted_bronze_pickaxe",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BRONZE_HATCHET =
            ITEMS.register("noted_bronze_hatchet",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BRONZE_DAGGER =
            ITEMS.register("noted_bronze_dagger",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BRONZE_MACE =
            ITEMS.register("noted_bronze_mace",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BRONZE_SWORD =
            ITEMS.register("noted_bronze_sword",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_STAFF =
            ITEMS.register("noted_staff",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_STAFF_OF_AIR =
            ITEMS.register("noted_staff_of_air",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_WIZARD_WAND =
            ITEMS.register("noted_wizard_wand",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BOW =
            ITEMS.register("noted_bow",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_CROSSBOW =
            ITEMS.register("noted_crossbow",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BRONZE_HELM =
            ITEMS.register("noted_bronze_helm",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BRONZE_CHAINBODY =
            ITEMS.register("noted_bronze_chainbody",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BRONZE_PLATEBODY =
            ITEMS.register("noted_bronze_platebody",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BRONZE_PLATELEGS =
            ITEMS.register("noted_bronze_platelegs",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BRONZE_BOOTS =
            ITEMS.register("noted_bronze_boots",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BRONZE_SQ_SHIELD =
            ITEMS.register("noted_bronze_sq_shield",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_WIZARD_HAT =
            ITEMS.register("noted_wizard_hat",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_WIZARD_ROBE_TOP =
            ITEMS.register("noted_wizard_robe_top",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_WIZARD_ROBE_SKIRT =
            ITEMS.register("noted_wizard_robe_skirt",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_WIZARD_BOOTS =
            ITEMS.register("noted_wizard_boots",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_LEATHER_CAP =
            ITEMS.register("noted_leather_cap",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_LEATHER_TUNIC =
            ITEMS.register("noted_leather_tunic",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_LEATHER_PANTS =
            ITEMS.register("noted_leather_pants",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_LEATHER_BOOTS =
            ITEMS.register("noted_leather_boots",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_GRIMY_GUAM =
            ITEMS.register("noted_grimy_guam",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_GRIMY_TARROMIN =
            ITEMS.register("noted_grimy_tarromin",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_GRIMY_MARRENTILL =
            ITEMS.register("noted_grimy_marrentill",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_CLEAN_GUAM =
            ITEMS.register("noted_clean_guam",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_CLEAN_TARROMIN =
            ITEMS.register("noted_clean_tarromin",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_CLEAN_MARRENTILL =
            ITEMS.register("noted_clean_marrentill",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_GUAM_POTION_UNF =
            ITEMS.register("noted_guam_potion_unf",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_TARROMIN_POTION_UNF =
            ITEMS.register("noted_tarromin_potion_unf",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_MARRENTILL_POTION_UNF =
            ITEMS.register("noted_marrentill_potion_unf",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_EYE_OF_NEWT =
            ITEMS.register("noted_eye_of_newt",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BEAR_FUR =
            ITEMS.register("noted_bear_fur",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_UNICORN_HORN =
            ITEMS.register("noted_unicorn_horn",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_UNICORN_HORN_DUST =
            ITEMS.register("noted_unicorn_horn_dust",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BLACK_BEAD =
            ITEMS.register("noted_black_bead",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_RED_BEAD =
            ITEMS.register("noted_red_bead",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_YELLOW_BEAD =
            ITEMS.register("noted_yellow_bead",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_WHITE_BEAD =
            ITEMS.register("noted_white_bead",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_ONION =
            ITEMS.register("noted_onion",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_CABBAGE =
            ITEMS.register("noted_cabbage",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BARLEY =
            ITEMS.register("noted_barley",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_REDBERRIES =
            ITEMS.register("noted_redberries",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_FLAX =
            ITEMS.register("noted_flax",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_CHISEL =
            ITEMS.register("noted_chisel",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_PESTLE_AND_MORTAR =
            ITEMS.register("noted_pestle_and_mortar",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_NEEDLE =
            ITEMS.register("noted_needle",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_RING_MOLD =
            ITEMS.register("noted_ring_mold",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_AMULET_MOLD =
            ITEMS.register("noted_amulet_mold",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_NECKLACE_MOLD =
            ITEMS.register("noted_necklace_mold",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BRACELET_MOLD =
            ITEMS.register("noted_bracelet_mold",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_TIARA_MOLD =
            ITEMS.register("noted_tiara_mold",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_COWHIDE =
            ITEMS.register("noted_cowhide",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_LEATHER =
            ITEMS.register("noted_leather",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_HARD_LEATHER =
            ITEMS.register("noted_hard_leather",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_UNCUT_OPAL =
            ITEMS.register("noted_uncut_opal",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_UNCUT_SAPPHIRE =
            ITEMS.register("noted_uncut_sapphire",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_CUT_LAPIS_LAZULI =
            ITEMS.register("noted_cut_lapis_lazuli",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_OPAL =
            ITEMS.register("noted_opal",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_SAPPHIRE =
            ITEMS.register("noted_sapphire",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BALL_OF_WOOL =
            ITEMS.register("noted_ball_of_wool",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_STRIP_OF_CLOTH =
            ITEMS.register("noted_strip_of_cloth",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BOWL_OF_WATER =
            ITEMS.register("noted_bowl_of_water",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BOWL =
            ITEMS.register("noted_bowl",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_UNFIRED_BOWL =
            ITEMS.register("noted_unfired_bowl",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_PIE_DISH =
            ITEMS.register("noted_pie_dish",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_UNFIRED_PIE_DISH =
            ITEMS.register("noted_unfired_pie_dish",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_POT =
            ITEMS.register("noted_pot",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_UNFIRED_POT =
            ITEMS.register("noted_unfired_pot",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BOWSTRING =
            ITEMS.register("noted_bowstring",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_FILLED_SILVER_RING_MOLD =
            ITEMS.register("noted_filled_silver_ring_mold",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_FILLED_TIARA_MOLD =
            ITEMS.register("noted_filled_tiara_mold",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_FILLED_OPAL_RING_MOLD =
            ITEMS.register("noted_filled_opal_ring_mold",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_FILLED_OPAL_AMULET_MOLD =
            ITEMS.register("noted_filled_opal_amulet_mold",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_FILLED_LAPIS_LAZULI_RING_MOLD =
            ITEMS.register("noted_filled_lapis_lazuli_ring_mold",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_FILLED_GOLD_RING_MOLD =
            ITEMS.register("noted_filled_gold_ring_mold",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_FILLED_GOLD_AMULET_MOLD =
            ITEMS.register("noted_filled_gold_amulet_mold",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_FILLED_GOLD_NECKLACE_MOLD =
            ITEMS.register("noted_filled_gold_necklace_mold",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_FILLED_GOLD_BRACELET_MOLD =
            ITEMS.register("noted_filled_gold_bracelet_mold",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_FILLED_SAPPHIRE_RING_MOLD =
            ITEMS.register("noted_filled_sapphire_ring_mold",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_FILLED_SAPPHIRE_AMULET_MOLD =
            ITEMS.register("noted_filled_sapphire_amulet_mold",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_FILLED_SAPPHIRE_BRACELET_MOLD =
            ITEMS.register("noted_filled_sapphire_bracelet_mold",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_SILVER_RING =
            ITEMS.register("noted_silver_ring",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_TIARA =
            ITEMS.register("noted_tiara",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_OPAL_RING =
            ITEMS.register("noted_opal_ring",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_UNSTRUNG_OPAL_AMULET =
            ITEMS.register("noted_unstrung_opal_amulet",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_OPAL_AMULET =
            ITEMS.register("noted_opal_amulet",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_LAPIS_LAZULI_RING =
            ITEMS.register("noted_lapis_lazuli_ring",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_GOLD_RING =
            ITEMS.register("noted_gold_ring",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_UNSTRUNG_GOLD_AMULET =
            ITEMS.register("noted_unstrung_gold_amulet",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_GOLD_AMULET =
            ITEMS.register("noted_gold_amulet",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_GOLD_NECKLACE =
            ITEMS.register("noted_gold_necklace",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_GOLD_BRACELET =
            ITEMS.register("noted_gold_bracelet",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_SAPPHIRE_RING =
            ITEMS.register("noted_sapphire_ring",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_UNSTRUNG_SAPPHIRE_AMULET =
            ITEMS.register("noted_unstrung_sapphire_amulet",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_SAPPHIRE_AMULET =
            ITEMS.register("noted_sapphire_amulet",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_SAPPHIRE_BRACELET =
            ITEMS.register("noted_sapphire_bracelet",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_AMULET_OF_BOUNTIFUL_HARVEST =
            ITEMS.register("noted_amulet_of_bountiful_harvest",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_AMULET_OF_BOUNTY =
            ITEMS.register("noted_amulet_of_bounty",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_RING_OF_LUCK =
            ITEMS.register("noted_ring_of_luck",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_RING_OF_RECOIL =
            ITEMS.register("noted_ring_of_recoil",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_AMULET_OF_MAGIC =
            ITEMS.register("noted_amulet_of_magic",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BRACELET_OF_CLAY =
            ITEMS.register("noted_bracelet_of_clay",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_IRON_PICKAXE =
            ITEMS.register("noted_iron_pickaxe",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_IRON_AXE =
            ITEMS.register("noted_iron_axe",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_IRON_DAGGER =
            ITEMS.register("noted_iron_dagger",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_RAW_BEAR_MEAT =
            ITEMS.register("noted_raw_bear_meat",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_COOKED_MEAT =
            ITEMS.register("noted_cooked_meat",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_RAW_SPIDER_ON_STICK =
            ITEMS.register("noted_raw_spider_on_stick",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_SPIDER_ON_STICK =
            ITEMS.register("noted_spider_on_stick",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BANANA =
            ITEMS.register("noted_banana",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BLURITE_SWORD =
            ITEMS.register("noted_blurite_sword",
                    () -> new Item(new Item.Properties().stacksTo(64)));

    public static final RegistryObject<Item> NOTED_STRUNG_RABBIT_FOOT =
            ITEMS.register("noted_strung_rabbit_foot",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_BIRD_NEST =
            ITEMS.register("noted_bird_nest",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_EGG_BIRD_NEST =
            ITEMS.register("noted_egg_bird_nest",
                    () -> new Item(new Item.Properties().stacksTo(64)));
    public static final RegistryObject<Item> NOTED_SEEDS_BIRD_NEST =
            ITEMS.register("noted_seeds_bird_nest",
                    () -> new Item(new Item.Properties().stacksTo(64)));

    //Projectiles
    public static final RegistryObject<Item> AIR_STRIKE_PROJECTILE = ITEMS.register("air_strike_projectile",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> WATER_STRIKE_PROJECTILE = ITEMS.register("water_strike_projectile",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> EARTH_STRIKE_PROJECTILE = ITEMS.register("earth_strike_projectile",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> FIRE_STRIKE_PROJECTILE = ITEMS.register("fire_strike_projectile",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> CONFUSE_PROJECTILE = ITEMS.register("confuse_projectile",
            () -> new Item(new Item.Properties().stacksTo(1)));
    public static final RegistryObject<Item> WEAKEN_PROJECTILE = ITEMS.register("weaken_projectile",
            () -> new Item(new Item.Properties().stacksTo(1)));

    //Spawn eggs
    public static final RegistryObject<Item> IMP_SPAWN_EGG = ITEMS.register("imp_spawn_egg",
            () -> new ForgeSpawnEggItem(EntityTypeInit.IMP, 0x77231F, 0x7F7E79,
                    new Item.Properties()));
    public static final RegistryObject<Item> GRIZZLY_BEAR_SPAWN_EGG = ITEMS.register("grizzly_bear_spawn_egg",
            () -> new ForgeSpawnEggItem(EntityTypeInit.GRIZZLY_BEAR, 0x6B5124, 0x473618,
                    new Item.Properties()));
    public static final RegistryObject<Item> BLACK_BEAR_SPAWN_EGG = ITEMS.register("black_bear_spawn_egg",
            () -> new ForgeSpawnEggItem(EntityTypeInit.BLACK_BEAR, 0x1F1B15, 0x15120E,
                    new Item.Properties()));
    public static final RegistryObject<Item> UNICORN_SPAWN_EGG = ITEMS.register("unicorn_spawn_egg",
            () -> new ForgeSpawnEggItem(EntityTypeInit.UNICORN, 0xDBDBDB, 0xE3C670,
                    new Item.Properties()));
    public static final RegistryObject<Item> DEADLY_RED_SPIDER_SPAWN_EGG = ITEMS.register("deadly_red_spider_spawn_egg",
            () -> new ForgeSpawnEggItem(EntityTypeInit.DEADLY_RED_SPIDER, 0x410406, 0x1E0607,
                    new Item.Properties()));

    //Misc items
    public static RegistryObject<ArmorItem> CHEFS_HAT = ITEMS.register("chefs_hat", () ->
            new ChefsHatItem(ArmorTiers.CHEF, ArmorItem.Type.HELMET, props()));
    public static final RegistryObject<Item> ASHES
            = ITEMS.register("ashes", () ->
            new AshesItem(new Item.Properties()));
    public static final RegistryObject<Item> BURNT_MEAT
            = ITEMS.register("burnt_meat", () ->
            new BurntMeatItem(new Item.Properties()));
    public static final RegistryObject<Item> FLIER
            = ITEMS.register("flier", () ->
            new FlierItem(new Item.Properties()));

    private static Item.Properties itemBuilder(boolean fireImmune) { if (fireImmune) { return new Item.Properties().fireResistant();} else { return new Item.Properties(); } }

    public static class Foods {
        public static final FoodProperties SPIDER_ON_STICK = new FoodProperties.Builder()
                .nutrition(6)
                .saturationMod(0.6f)
                .meat()
                .fast()
                .alwaysEat()
                .build();
        public static final FoodProperties BANANA = new FoodProperties.Builder()
                .nutrition(4)
                .saturationMod(0.3f)
                .fast()
                .alwaysEat()
                .build();
        public static final FoodProperties COOKED_MEAT = new FoodProperties.Builder()
                .nutrition(6)
                .saturationMod(0.6f)
                .fast()
                .alwaysEat()
                .build();
        public static final FoodProperties ONION = new FoodProperties.Builder()
                .nutrition(1)
                .saturationMod(0.6f)
                .fast()
                .alwaysEat()
                .build();

        public static final FoodProperties POTION = new FoodProperties.Builder()
                .nutrition(0)
                .saturationMod(0f)
                .fast()
                .alwaysEat()
                .build();
    }

    //Tool tiers are now a static class
    public static class ToolTiers {
        //We need to register our tier to TierSortingRegistry and like in BlockInit,
        // specify the blocks directory in our modid's tags
        public static final Tier BRONZE = TierSortingRegistry.registerTier(
                new ForgeTier(1, 188, 4.5f, 1.5f, 10, BlockInit.Tags.NEEDS_BRONZE_TOOL,
                        () -> Ingredient.of(ItemInit.BRONZE_BAR.get())),
                new ResourceLocation("runicages:blocks/needs_bronze_tool"),
                List.of(Tiers.STONE), List.of(Tiers.IRON));
        public static final Tier BLURITE = TierSortingRegistry.registerTier(
                new ForgeTier(2, 250, 6f, 2f, 14, BlockInit.Tags.NEEDS_BLURITE_TOOL,
                        () -> Ingredient.of(ItemInit.BLURITE_BAR.get())),
                new ResourceLocation("runicages:blocks/needs_blurite_tool"),
                List.of(Tiers.IRON), List.of());
    }

    //Forge no longer has a class for armor-tiers so we will have to create one via the java record class
    //See the ArmorMaterial class for more
    public static class ArmorTiers {
        public static final RunicAgesArmorMaterial BRONZE = new RunicAgesArmorMaterial(
                new int[] { 1, 3, 4, 1 },
                new int[] { 1, 4, 2, 1 },
                6,
                SoundEvents.ARMOR_EQUIP_IRON,
                () -> Ingredient.of(ItemInit.BRONZE_BAR::get),
                "bronze",
                0.0f,
                0.0f
        );
        public static final RunicAgesArmorMaterial BRONZE_CHAINBODY = new RunicAgesArmorMaterial(
                new int[] { 1, 2, 4, 1 },
                new int[] { 1, 2, 2, 1 },
                6,
                SoundEvents.ARMOR_EQUIP_IRON,
                () -> Ingredient.of(ItemInit.BRONZE_BAR::get),
                "bronze_chainbody",
                0.0f,
                0.0f
        );

        public static final RunicAgesArmorMaterial WIZARD = new RunicAgesArmorMaterial(
                new int[] { 1, 3, 4, 1 },
                new int[] { 1, 3, 2, 1 },
                6,
                SoundEvents.ARMOR_EQUIP_GENERIC,
                () -> Ingredient.of(ItemInit.STRIP_OF_CLOTH::get),
                "wizard",
                0.0f,
                0.0f
        );

        public static final RunicAgesArmorMaterial TIARA = new RunicAgesArmorMaterial(
                new int[] { 0, 0, 0, 0 },
                new int[] { 0, 0, 0, 0 },
                0,
                SoundEvents.ARMOR_EQUIP_GENERIC,
                () -> Ingredient.of(Items.AIR),
                "tiara",
                0.0f,
                0.0f
        );
        public static final RunicAgesArmorMaterial AIR_TIARA = new RunicAgesArmorMaterial(
                new int[] { 0, 0, 0, 0 },
                new int[] { 0, 0, 0, 0 },
                0,
                SoundEvents.ARMOR_EQUIP_GENERIC,
                () -> Ingredient.of(Items.AIR),
                "air_tiara",
                0.0f,
                0.0f
        );
        public static final RunicAgesArmorMaterial MIND_TIARA = new RunicAgesArmorMaterial(
                new int[] { 0, 0, 0, 0 },
                new int[] { 0, 0, 0, 0 },
                0,
                SoundEvents.ARMOR_EQUIP_GENERIC,
                () -> Ingredient.of(Items.AIR),
                "mind_tiara",
                0.0f,
                0.0f
        );
        public static final RunicAgesArmorMaterial WATER_TIARA = new RunicAgesArmorMaterial(
                new int[] { 0, 0, 0, 0 },
                new int[] { 0, 0, 0, 0 },
                0,
                SoundEvents.ARMOR_EQUIP_GENERIC,
                () -> Ingredient.of(Items.AIR),
                "water_tiara",
                0.0f,
                0.0f
        );
        public static final RunicAgesArmorMaterial EARTH_TIARA = new RunicAgesArmorMaterial(
                new int[] { 0, 0, 0, 0 },
                new int[] { 0, 0, 0, 0 },
                0,
                SoundEvents.ARMOR_EQUIP_GENERIC,
                () -> Ingredient.of(Items.AIR),
                "earth_tiara",
                0.0f,
                0.0f
        );
        public static final RunicAgesArmorMaterial FIRE_TIARA = new RunicAgesArmorMaterial(
                new int[] { 0, 0, 0, 0 },
                new int[] { 0, 0, 0, 0 },
                0,
                SoundEvents.ARMOR_EQUIP_GENERIC,
                () -> Ingredient.of(Items.AIR),
                "fire_tiara",
                0.0f,
                0.0f
        );
        public static final RunicAgesArmorMaterial BODY_TIARA = new RunicAgesArmorMaterial(
                new int[] { 0, 0, 0, 0 },
                new int[] { 0, 0, 0, 0 },
                0,
                SoundEvents.ARMOR_EQUIP_GENERIC,
                () -> Ingredient.of(Items.AIR),
                "body_tiara",
                0.0f,
                0.0f
        );

        public static final RunicAgesArmorMaterial CHEF = new RunicAgesArmorMaterial(
                new int[] { 1, 3, 4, 1 },
                new int[] { 1, 0, 1, 1 },
                6,
                SoundEvents.ARMOR_EQUIP_GENERIC,
                () -> Ingredient.of(Items.AIR),
                "chef",
                0.0f,
                0.0f
        );
    }
}
