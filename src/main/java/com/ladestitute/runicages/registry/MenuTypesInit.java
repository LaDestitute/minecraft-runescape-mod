package com.ladestitute.runicages.registry;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.client.menu.*;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.MenuType;
import net.minecraftforge.common.extensions.IForgeMenuType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.network.IContainerFactory;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class MenuTypesInit {
    public static final DeferredRegister<MenuType<?>> MENUS =
            DeferredRegister.create(ForgeRegistries.MENU_TYPES, RunicAgesMain.MODID);

    public static final RegistryObject<MenuType<AlloyFurnaceMenu>> ALLOY_FURNACE_MENU =
            registerMenuType(AlloyFurnaceMenu::new, "alloy_furnace_menu");

    public static final RegistryObject<MenuType<IronAlloyFurnaceMenu>> IRON_ALLOY_FURNACE_MENU =
            registerMenuType(IronAlloyFurnaceMenu::new, "iron_alloy_furnace_menu");

    public static final RegistryObject<MenuType<SmithingFurnaceMenu>> SMITHING_FURNACE_MENU =
            registerMenuType(SmithingFurnaceMenu::new, "smithing_furnace_menu");

    public static final RegistryObject<MenuType<MagicBookMenu>> MAGIC_BOOK_MENU =
            registerMenuType(MagicBookMenu::new, "magic_book_menu");

    public static final RegistryObject<MenuType<SteelAlloyFurnaceMenu>> STEEL_ALLOY_FURNACE_MENU =
            registerMenuType(SteelAlloyFurnaceMenu::new, "steel_alloy_furnace_menu");

    public static final RegistryObject<MenuType<SilverSmithingFurnaceMenu>> SILVER_SMITHING_FURNACE_MENU =
            registerMenuType(SilverSmithingFurnaceMenu::new, "silver_smithing_furnace_menu");

    public static final RegistryObject<MenuType<MithrilAlloyFurnaceMenu>> MITHRIL_ALLOY_FURNACE_MENU =
            registerMenuType(MithrilAlloyFurnaceMenu::new, "mithril_alloy_furnace_menu");

    public static final RegistryObject<MenuType<GoldSmithingFurnaceMenu>> GOLD_SMITHING_FURNACE_MENU =
            registerMenuType(GoldSmithingFurnaceMenu::new, "gold_smithing_furnace_menu");

    public static final RegistryObject<MenuType<AdamantiteAlloyFurnaceMenu>> ADAMANTITE_ALLOY_FURNACE_MENU =
            registerMenuType(AdamantiteAlloyFurnaceMenu::new, "adamantite_alloy_furnace_menu");

    public static final RegistryObject<MenuType<MoldPressMenu>> MOLD_PRESS_MENU =
            registerMenuType(MoldPressMenu::new, "mold_press_menu");

    public static final RegistryObject<MenuType<SpinningWheelMenu>> SPINNING_WHEEL_MENU =
            registerMenuType(SpinningWheelMenu::new, "spinning_wheel_menu");

    public static final RegistryObject<MenuType<SewingKitMenu>> SEWING_KIT_MENU =
            registerMenuType(SewingKitMenu::new, "sewing_kit_menu");

    public static final RegistryObject<MenuType<BankMenu>> BANK_MENU = MENUS.register("sb_container", () -> IForgeMenuType.create(BankMenu::fromNetwork));

    private static <T extends AbstractContainerMenu>RegistryObject<MenuType<T>> registerMenuType(IContainerFactory<T> factory,
                                                                                                 String name) {
        return MENUS.register(name, () -> IForgeMenuType.create(factory));
    }

    public static void register(IEventBus eventBus) {
        MENUS.register(eventBus);
    }
}
