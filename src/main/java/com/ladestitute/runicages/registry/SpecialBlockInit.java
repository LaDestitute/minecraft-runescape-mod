package com.ladestitute.runicages.registry;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.blocks.farming.crops.*;
import com.ladestitute.runicages.blocks.ore.depleted.*;
import com.ladestitute.runicages.blocks.plants.YoungFlaxBlock;
import com.ladestitute.runicages.blocks.woodcutting.CutLightJungleBlock;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SaplingBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.grower.OakTreeGrower;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.material.MapColor;
import net.minecraft.world.level.material.PushReaction;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Supplier;

public class SpecialBlockInit {
    //A class for blocks we don't want automatically registered into our creative tab and/or put into a different one
    public static final DeferredRegister<Block> SPECIAL_BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS,
            RunicAgesMain.MODID);

    public static final RegistryObject<Block> DEPLETED_COPPER_ROCK = SPECIAL_BLOCKS.register("depleted_copper_rock",
            () -> new DepletedCopperRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));
    public static final RegistryObject<Block> DEPLETED_TIN_ROCK = SPECIAL_BLOCKS.register("depleted_tin_rock",
            () -> new DepletedTinRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));
    public static final RegistryObject<Block> DEPLETED_IRON_ROCK = SPECIAL_BLOCKS.register("depleted_iron_rock",
            () -> new DepletedIronRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));
    public static final RegistryObject<Block> DEPLETED_BLURITE_ROCK = SPECIAL_BLOCKS.register("depleted_blurite_rock",
            () -> new DepletedBluriteRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));
    public static final RegistryObject<Block> DEPLETED_CLAY_ROCK = SPECIAL_BLOCKS.register("depleted_clay_rock",
            () -> new DepletedClayRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));
    public static final RegistryObject<Block> DEPLETED_SILVER_ROCK = SPECIAL_BLOCKS.register("depleted_silver_rock",
            () -> new DepletedSilverRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));
    public static final RegistryObject<Block> DEPLETED_MITHRIL_ROCK = SPECIAL_BLOCKS.register("depleted_mithril_rock",
            () -> new DepletedMithrilRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));
    public static final RegistryObject<Block> DEPLETED_ADAMANTITE_ROCK = SPECIAL_BLOCKS.register("depleted_adamantite_rock",
            () -> new DepletedAdamantiteRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));

    public static final RegistryObject<Block> DEPLETED_LUMINITE_ROCK = SPECIAL_BLOCKS.register("depleted_luminite_rock",
            () -> new DepletedLuminiteRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));

    public static final RegistryObject<Block> CUT_LIGHT_JUNGLE = SPECIAL_BLOCKS.register("cut_light_jungle",
            () -> new CutLightJungleBlock(PropertiesInit.LEVEL_ONE_TREE));
//    public static final RegistryObject<Block> NORMAL_SAPLING = SPECIAL_BLOCKS.register("normal_sapling",
//            () -> new NormalSaplingBlock(new NormalTreeGrower(), BlockBehaviour.Properties.copy(Blocks.OAK_SAPLING),
//                    () -> Blocks.DIRT));
//public static final RegistryObject<Block> NORMAL_SAPLING = SPECIAL_BLOCKS.register("normal_sapling",
//        () -> new SaplingBlock(new OakTreeGrower(), BlockBehaviour.Properties.of().mapColor(MapColor.PLANT)
//                .noCollission().randomTicks().instabreak().sound(SoundType.GRASS).pushReaction(PushReaction.DESTROY)));

    public static final RegistryObject<Block> YOUNG_FLAX = SPECIAL_BLOCKS.register("young_flax",
            () -> new YoungFlaxBlock(PropertiesInit.FLAX));

    //Farming
    public static final RegistryObject<Block> BARLEY_CROP =
            registerBlock("barley_crop", () -> new BarleyCropBlock(PropertiesInit.CROP));
    public static final RegistryObject<Block> ALLOTMENT_POTATO_CROP =
            registerBlock("potato_crop", () -> new AllotmentPotatoBlock(PropertiesInit.CROP));
    public static final RegistryObject<Block> ONION_CROP =
            registerBlock("onion_crop", () -> new OnionCropBlock(PropertiesInit.CROP));
    public static final RegistryObject<Block> CABBAGE_CROP =
            registerBlock("cabbage_crop", () -> new CabbageCropBlock(PropertiesInit.CROP));
    public static final RegistryObject<Block> GUAM_CROP =
            registerBlock("guam_crop", () -> new GuamCropBlock(PropertiesInit.CROP));
    public static final RegistryObject<Block> REDBERRY_CROP =
            registerBlock("redberry_crop", () -> new RedberryCropBlock(PropertiesInit.CROP));

    private static <T extends Block> RegistryObject<T> registerBlock(String name, Supplier<T> block) {
        RegistryObject<T> toReturn = SPECIAL_BLOCKS.register(name, block);
        registerBlockItem(name, toReturn);
        return toReturn;
    }

    private static <T extends Block> RegistryObject<Item> registerBlockItem(String name, RegistryObject<T> block) {
        return ItemInit.ITEMS.register(name, () -> new BlockItem(block.get(), new Item.Properties()));
    }
}
