package com.ladestitute.runicages.registry;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.effects.ConfuseEffect;
import com.ladestitute.runicages.effects.PoisonImmunityEffect;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectCategory;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class EffectInit {
    public static final DeferredRegister<MobEffect> MOB_EFFECTS
            = DeferredRegister.create(ForgeRegistries.MOB_EFFECTS, RunicAgesMain.MODID);

    public static final RegistryObject<MobEffect> CONFUSE = MOB_EFFECTS.register("confuse",
            () -> new ConfuseEffect(MobEffectCategory.HARMFUL, 11077357));

    public static final RegistryObject<MobEffect> POISON_IMMUNITY = MOB_EFFECTS.register("poison_immunity",
            () -> new PoisonImmunityEffect(MobEffectCategory.BENEFICIAL, 5186611));

    public static void register(IEventBus eventBus) {
        MOB_EFFECTS.register(eventBus);
    }
}
