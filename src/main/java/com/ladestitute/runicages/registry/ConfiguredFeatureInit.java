package com.ladestitute.runicages.registry;

import com.google.common.base.Suppliers;
import com.ladestitute.runicages.RunicAgesMain;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderGetter;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.data.worldgen.features.OreFeatures;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.valueproviders.BiasedToBottomInt;
import net.minecraft.util.valueproviders.ConstantInt;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.blockpredicates.BlockPredicate;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.WeightedPlacedFeature;
import net.minecraft.world.level.levelgen.feature.configurations.*;
import net.minecraft.world.level.levelgen.feature.featuresize.TwoLayersFeatureSize;
import net.minecraft.world.level.levelgen.feature.foliageplacers.AcaciaFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.feature.stateproviders.SimpleStateProvider;
import net.minecraft.world.level.levelgen.feature.trunkplacers.ForkingTrunkPlacer;
import net.minecraft.world.level.levelgen.placement.BlockPredicateFilter;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.structure.templatesystem.TagMatchTest;

import java.util.List;
import java.util.function.Supplier;

public class ConfiguredFeatureInit {
    //Features now use Holders, this is carried over from +1.18

    public static final ResourceKey<ConfiguredFeature<?, ?>> NORMAL_TREE = registerKey("normal");
    public static final ResourceKey<ConfiguredFeature<?, ?>> COPPER = registerKey("copper");
    public static final ResourceKey<ConfiguredFeature<?, ?>> TIN = registerKey("tin");
    public static final ResourceKey<ConfiguredFeature<?, ?>> CLAY = registerKey("clay_rock");
    public static final ResourceKey<ConfiguredFeature<?, ?>> BLURITE = registerKey("blurite_rock");
    public static final ResourceKey<ConfiguredFeature<?, ?>> IRON = registerKey("iron_rock");
    public static final ResourceKey<ConfiguredFeature<?, ?>> SILVER = registerKey("silver_rock");
    public static final ResourceKey<ConfiguredFeature<?, ?>> MITHRIL = registerKey("mithril_rock");
    public static final ResourceKey<ConfiguredFeature<?, ?>> ADAMANTITE = registerKey("adamantite_rock");
    public static final ResourceKey<ConfiguredFeature<?, ?>> LUMINITE = registerKey("luminite_rock");
    public static final ResourceKey<ConfiguredFeature<?, ?>> LIGHT_JUNGLE
            = registerKey("light_jungle");
    public static final ResourceKey<ConfiguredFeature<?, ?>> FLAX = registerKey("flax");
    public static final ResourceKey<ConfiguredFeature<?, ?>> OVERWORLD_TIN_ORES_KEY = registerKey("overworld_tin_ore");
    public static final ResourceKey<ConfiguredFeature<?, ?>> OVERWORLD_BLURITE_ORES_KEY = registerKey("overworld_blurite_ore");
    public static final ResourceKey<ConfiguredFeature<?, ?>> OVERWORLD_RUNE_ESSENCE_ORES_KEY = registerKey("overworld_rune_essence_ore");
    public static final ResourceKey<ConfiguredFeature<?, ?>> OVERWORLD_SILVER_ORES_KEY = registerKey("overworld_silver_ore");
    public static final ResourceKey<ConfiguredFeature<?, ?>> OVERWORLD_MITHRIL_ORES_KEY = registerKey("overworld_mithril_ore");
    public static final ResourceKey<ConfiguredFeature<?, ?>> OVERWORLD_ADAMANTITE_ORES_KEY = registerKey("overworld_adamantite_ore");
    public static final ResourceKey<ConfiguredFeature<?, ?>> OVERWORLD_LUMINITE_ORES_KEY = registerKey("overworld_luminite_ore");

    public static final Supplier<List<OreConfiguration.TargetBlockState>> OVERWORLD_TIN_ORES = Suppliers.memoize(() -> List.of(
            OreConfiguration.target(new TagMatchTest(BlockTags.STONE_ORE_REPLACEABLES), BlockInit.TIN_ORE.get().defaultBlockState()),
            OreConfiguration.target(new TagMatchTest(BlockTags.DEEPSLATE_ORE_REPLACEABLES), BlockInit.DEEPSLATE_TIN_ORE.get().defaultBlockState())));

    public static final Supplier<List<OreConfiguration.TargetBlockState>> OVERWORLD_BLURITE_ORES = Suppliers.memoize(() -> List.of(
            OreConfiguration.target(new TagMatchTest(BlockTags.STONE_ORE_REPLACEABLES), BlockInit.BLURITE_ORE.get().defaultBlockState()),
            OreConfiguration.target(new TagMatchTest(BlockTags.DEEPSLATE_ORE_REPLACEABLES), BlockInit.DEEPSLATE_BLURITE_ORE.get().defaultBlockState())));

    public static final Supplier<List<OreConfiguration.TargetBlockState>> OVERWORLD_RUNE_ESSENCE_ORES = Suppliers.memoize(() -> List.of(
            OreConfiguration.target(new TagMatchTest(BlockTags.STONE_ORE_REPLACEABLES), BlockInit.RUNE_ESSENCE_ORE.get().defaultBlockState()),
            OreConfiguration.target(new TagMatchTest(BlockTags.DEEPSLATE_ORE_REPLACEABLES), BlockInit.RUNE_ESSENCE_ORE.get().defaultBlockState())));

    public static final Supplier<List<OreConfiguration.TargetBlockState>> OVERWORLD_SILVER_ORES = Suppliers.memoize(() -> List.of(
            OreConfiguration.target(new TagMatchTest(BlockTags.STONE_ORE_REPLACEABLES), BlockInit.SILVER_ORE.get().defaultBlockState()),
            OreConfiguration.target(new TagMatchTest(BlockTags.DEEPSLATE_ORE_REPLACEABLES), BlockInit.DEEPSLATE_SILVER_ORE.get().defaultBlockState())));

    public static final Supplier<List<OreConfiguration.TargetBlockState>> OVERWORLD_MITHRIL_ORES = Suppliers.memoize(() -> List.of(
            OreConfiguration.target(new TagMatchTest(BlockTags.STONE_ORE_REPLACEABLES), BlockInit.MITHRIL_ORE.get().defaultBlockState()),
            OreConfiguration.target(new TagMatchTest(BlockTags.DEEPSLATE_ORE_REPLACEABLES), BlockInit.DEEPSLATE_MITHRIL_ORE.get().defaultBlockState())));

    public static final Supplier<List<OreConfiguration.TargetBlockState>> OVERWORLD_ADAMANTITE_ORES = Suppliers.memoize(() -> List.of(
            OreConfiguration.target(new TagMatchTest(BlockTags.STONE_ORE_REPLACEABLES), BlockInit.ADAMANTITE_ORE.get().defaultBlockState()),
            OreConfiguration.target(new TagMatchTest(BlockTags.DEEPSLATE_ORE_REPLACEABLES), BlockInit.DEEPSLATE_ADAMANTITE_ORE.get().defaultBlockState())));

    public static final Supplier<List<OreConfiguration.TargetBlockState>> OVERWORLD_LUMINITE_ORES = Suppliers.memoize(() -> List.of(
            OreConfiguration.target(new TagMatchTest(BlockTags.STONE_ORE_REPLACEABLES), BlockInit.LUMINITE_ORE.get().defaultBlockState()),
            OreConfiguration.target(new TagMatchTest(BlockTags.DEEPSLATE_ORE_REPLACEABLES), BlockInit.DEEPSLATE_LUMINITE_ORE.get().defaultBlockState())));

    public static void bootstrap(BootstapContext<ConfiguredFeature<?, ?>> context) {
        HolderGetter<PlacedFeature> placedFeatures = context.lookup(Registries.PLACED_FEATURE);

        register(context, NORMAL_TREE, Feature.TREE, new TreeConfiguration.TreeConfigurationBuilder(
                BlockStateProvider.simple(BlockInit.NORMAL_TREE_LOG.get()),
                new ForkingTrunkPlacer(3, 1, 1),
                BlockStateProvider.simple(BlockInit.NORMAL_TREE_LEAVES.get()),
                new AcaciaFoliagePlacer(ConstantInt.of(2), ConstantInt.of(0)),
                new TwoLayersFeatureSize(1, 0, 2))
                .dirt(BlockStateProvider.simple(BlockInit.NORMAL_TREE_LOG.get()))
                .forceDirt()
                .ignoreVines().build());

        register(context, COPPER,
                Feature.RANDOM_PATCH, randomPatch(BlockStateProvider.simple(BlockInit.COPPER_ROCK.get()), 31));

        register(context, TIN,
                Feature.RANDOM_PATCH, randomPatch(BlockStateProvider.simple(BlockInit.TIN_ROCK.get()), 31));

        register(context, CLAY,
                Feature.RANDOM_PATCH, randomPatch(BlockStateProvider.simple(BlockInit.CLAY_ROCK.get()), 31));

        register(context, BLURITE,
                Feature.RANDOM_PATCH, randomPatch(BlockStateProvider.simple(BlockInit.BLURITE_ROCK.get()), 8));

        register(context, IRON,
                Feature.RANDOM_PATCH, randomPatch(BlockStateProvider.simple(BlockInit.IRON_ROCK.get()), 31));

        register(context, SILVER,
                Feature.RANDOM_PATCH, randomPatch(BlockStateProvider.simple(BlockInit.SILVER_ROCK.get()), 31));

        register(context, MITHRIL,
                Feature.RANDOM_PATCH, randomPatch(BlockStateProvider.simple(BlockInit.MITHRIL_ROCK.get()), 31));

        register(context, ADAMANTITE,
                Feature.RANDOM_PATCH, randomPatch(BlockStateProvider.simple(BlockInit.ADAMANTITE_ROCK.get()), 31));

        register(context, LUMINITE,
                Feature.RANDOM_PATCH, randomPatch(BlockStateProvider.simple(BlockInit.LUMINITE_ROCK.get()), 31));

        register(context, LIGHT_JUNGLE,
                Feature.RANDOM_PATCH, randomPatch(BlockStateProvider.simple(BlockInit.LIGHT_JUNGLE.get()), 250));

        register(context, FLAX,
                Feature.RANDOM_PATCH, randomPatch(BlockStateProvider.simple(BlockInit.FLAX.get()), 32));

        register(context, OVERWORLD_TIN_ORES_KEY, Feature.ORE, new OreConfiguration(OVERWORLD_TIN_ORES.get(), 8));

        register(context, OVERWORLD_BLURITE_ORES_KEY, Feature.ORE, new OreConfiguration(OVERWORLD_BLURITE_ORES.get(), 4));

        register(context, OVERWORLD_RUNE_ESSENCE_ORES_KEY, Feature.ORE, new OreConfiguration(OVERWORLD_RUNE_ESSENCE_ORES.get(), 10));

        register(context, OVERWORLD_SILVER_ORES_KEY, Feature.ORE, new OreConfiguration(OVERWORLD_SILVER_ORES.get(), 10));

        register(context, OVERWORLD_MITHRIL_ORES_KEY, Feature.ORE, new OreConfiguration(OVERWORLD_MITHRIL_ORES.get(), 10));

        register(context, OVERWORLD_ADAMANTITE_ORES_KEY, Feature.ORE, new OreConfiguration(OVERWORLD_ADAMANTITE_ORES.get(), 10));

        register(context, OVERWORLD_LUMINITE_ORES_KEY, Feature.ORE, new OreConfiguration(OVERWORLD_LUMINITE_ORES.get(), 8));
    }

    private static RandomPatchConfiguration randomPatch(BlockStateProvider p_195203_, int p_195204_) {
        return FeatureUtils.simpleRandomPatchConfiguration(p_195204_, PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new SimpleBlockConfiguration(p_195203_)));
    }

    public static ResourceKey<ConfiguredFeature<?, ?>> registerKey(String name) {
        return ResourceKey.create(Registries.CONFIGURED_FEATURE, new ResourceLocation(RunicAgesMain.MODID, name));
    }

    private static <FC extends FeatureConfiguration, F extends Feature<FC>> void register(BootstapContext<ConfiguredFeature<?, ?>> context,
                                                                                          ResourceKey<ConfiguredFeature<?, ?>> key, F feature, FC configuration) {
        context.register(key, new ConfiguredFeature<>(feature, configuration));
    }



    //"Normal" level one tree for the woodcutting skill (as all other trees are locked by level requirements)


    // Or

//    public static final Holder<ConfiguredFeature<OreConfiguration, ?>> TIN_ORE = FeatureUtils.register("tin_ore",
//            Feature.ORE, new OreConfiguration(OVERWORLD_TIN_ORES, 8));
//
//    public static final Holder<ConfiguredFeature<OreConfiguration, ?>> BLURITE_ORE = FeatureUtils.register("blurite_ore",
//            Feature.ORE, new OreConfiguration(OVERWORLD_BLURITE_ORES, 4));
//
//    public static final Holder<ConfiguredFeature<OreConfiguration, ?>> RUNE_ESSENCE_ORE = FeatureUtils.register("rune_essence_ore",
//            Feature.ORE, new OreConfiguration(OW_RUNE_ESSENCE_ORE, 10));
//
//    public static final Holder<ConfiguredFeature<OreConfiguration, ?>> SILVER_ORE = FeatureUtils.register("silver_ore",
//            Feature.ORE, new OreConfiguration(OVERWORLD_SILVER_ORES, 10));
//
//    public static final Holder<ConfiguredFeature<OreConfiguration, ?>> MITHRIL_ORE = FeatureUtils.register("mithril_ore",
//            Feature.ORE, new OreConfiguration(OVERWORLD_MITHRIL_ORES, 10));
//
//    public static final Holder<ConfiguredFeature<OreConfiguration, ?>> ADAMANTITE_ORE = FeatureUtils.register("adamantite_ore",
//            Feature.ORE, new OreConfiguration(OVERWORLD_ADAMANTITE_ORES, 10));
//
//    public static final Holder<ConfiguredFeature<OreConfiguration, ?>> LUMINITE_ORE = FeatureUtils.register("luminite_ore",
//            Feature.ORE, new OreConfiguration(OVERWORLD_LUMINITE_ORES, 8));


}

