package com.ladestitute.runicages.registry;

import com.ladestitute.runicages.blocks.farming.crops.AllotmentPotatoBlock;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Supplier;

public class OverrideVanillaInit {
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, "minecraft");

    public static final RegistryObject<Block> POTATOES = registerBlockWithoutBlockItem("potatoes",
            () -> new AllotmentPotatoBlock(PropertiesInit.CROP));

    private static <T extends Block> RegistryObject<T> registerBlockWithoutBlockItem(String name, Supplier<T> block) {
        return BLOCKS.register(name, block);
    }

}
