package com.ladestitute.runicages.registry;

import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockBehaviour;

public class PropertiesInit {
    public static final BlockBehaviour.Properties LEVEL_ONE_ORE_BLOCK = BlockBehaviour.Properties.of()
            .strength(100f, 0f)
            .sound(SoundType.STONE)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties LEVEL_ONE_CAVE_ORE_BLOCK = BlockBehaviour.Properties.of()
            .strength(3f, 3f)
            .sound(SoundType.STONE)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties LEVEL_ONE_DEEPCAVE_ORE_BLOCK = BlockBehaviour.Properties.of()
            .strength(4.5f, 3f)
            .sound(SoundType.STONE)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties LEVEL_ONE_TREE = BlockBehaviour.Properties.of()
            .strength(2f, 2f)
            .sound(SoundType.WOOD)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties ALTAR_STONE = BlockBehaviour.Properties.of()
            .strength(20f, 10f)
            .sound(SoundType.STONE)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties RUNECRAFTING_ALTAR = BlockBehaviour.Properties.of()
            .strength(100f, 100f)
            .sound(SoundType.STONE)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties ALLOTMENT = BlockBehaviour.Properties.of()
            .strength(0.6f, 0.6f)
            .sound(SoundType.GRAVEL)
            .randomTicks()
            .noOcclusion()
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties CROP = BlockBehaviour.Properties.of()
            .strength(20f, 0.0f)
            .sound(SoundType.CROP)
            .randomTicks()
            .noOcclusion()
            .noCollission();

    public static final BlockBehaviour.Properties RUNE_ESSENCE = BlockBehaviour.Properties.of()
            .strength(3.5f, 3.5f)
            .sound(SoundType.GRAVEL)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties RUNE_ESSENCE_ORE = BlockBehaviour.Properties.of()
            .strength(0.6f, 0.6f)
            .sound(SoundType.GRAVEL)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties SMITHING_FURNACE = BlockBehaviour.Properties.of()
            .strength(3.5f, 3.5f)
            .sound(SoundType.STONE)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties SPINNING_WHEEL = BlockBehaviour.Properties.of()
            .strength(2f, 2f)
            .sound(SoundType.WOOD)
            .noOcclusion()
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties FLAX = BlockBehaviour.Properties.of()
            .strength(0.5f, 0.5f)
            .sound(SoundType.CROP)
            .noOcclusion()
            .noCollission()
            .requiresCorrectToolForDrops();


}
