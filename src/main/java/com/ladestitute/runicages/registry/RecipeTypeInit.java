package com.ladestitute.runicages.registry;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.recipes.*;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.SimpleCraftingRecipeSerializer;
import net.minecraft.world.item.crafting.SuspiciousStewRecipe;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class RecipeTypeInit {
    public static final DeferredRegister<RecipeSerializer<?>> SERIALIZERS =
            DeferredRegister.create(ForgeRegistries.RECIPE_SERIALIZERS, RunicAgesMain.MODID);

    public static final RegistryObject<RecipeSerializer<SmithingAnvilRecipe>> SMITHING_ANVIL_SERIALIZER =
            SERIALIZERS.register("smithing_anvil", () -> SmithingAnvilRecipe.Serializer.INSTANCE);

    public static final RegistryObject<RecipeSerializer<AlloyFurnaceRecipe>> ALLOY_FURNACE_SERIALIZER =
            SERIALIZERS.register("alloy_furnace", () -> AlloyFurnaceRecipe.Serializer.INSTANCE);

    public static final RegistryObject<RecipeSerializer<IronAlloyFurnaceRecipe>> IRON_ALLOY_FURNACE_SERIALIZER =
            SERIALIZERS.register("iron_alloy_furnace", () -> IronAlloyFurnaceRecipe.Serializer.INSTANCE);

    public static final RegistryObject<RecipeSerializer<SmithingFurnaceRecipe>> SMITHING_FURNACE_SERIALIZER =
            SERIALIZERS.register("smithing_furnace", () -> SmithingFurnaceRecipe.Serializer.INSTANCE);

    public static final RegistryObject<RecipeSerializer<SteelAlloyFurnaceRecipe>> STEEL_ALLOY_FURNACE_SERIALIZER =
            SERIALIZERS.register("steel_alloy_furnace", () -> SteelAlloyFurnaceRecipe.Serializer.INSTANCE);

    public static final RegistryObject<RecipeSerializer<SilverSmithingFurnaceRecipe>> SILVER_SMITHING_FURNACE_SERIALIZER =
            SERIALIZERS.register("silver_smithing_furnace", () -> SilverSmithingFurnaceRecipe.Serializer.INSTANCE);

    public static final RegistryObject<RecipeSerializer<MithrilAlloyFurnaceRecipe>> MITHRIL_ALLOY_FURNACE_SERIALIZER =
            SERIALIZERS.register("mithril_alloy_furnace", () -> MithrilAlloyFurnaceRecipe.Serializer.INSTANCE);

    public static final RegistryObject<RecipeSerializer<GoldSmithingFurnaceRecipe>> GOLD_SMITHING_FURNACE_SERIALIZER =
            SERIALIZERS.register("gold_smithing_furnace", () -> GoldSmithingFurnaceRecipe.Serializer.INSTANCE);

    public static final RegistryObject<RecipeSerializer<AdamantiteAlloyFurnaceRecipe>> ADAMANTITE_ALLOY_FURNACE_SERIALIZER =
            SERIALIZERS.register("adamantite_alloy_furnace", () -> AdamantiteAlloyFurnaceRecipe.Serializer.INSTANCE);

    public static final RegistryObject<RecipeSerializer<SpinningWheelRecipe>> SPINNING_WHEEL_SERIALIZER =
            SERIALIZERS.register("spinning_wheel", () -> SpinningWheelRecipe.Serializer.INSTANCE);

//    public static final RegistryObject<RecipeSerializer<ModdedSuspiciousStewRecipe>> SUS_STEW_SERIALIZER =
//            SERIALIZERS.register("modded_suspicious_stew", () -> ModdedSuspiciousStewRecipe.);
//
//    RecipeSerializer<ModdedSuspiciousStewRecipe> SUS_STEW_SERIALIZER = SERIALIZERS.register("modded_special_suspiciousstew", () -> ModdedSuspiciousStewRecipe::new);

    public static void register(IEventBus eventBus) {
        SERIALIZERS.register(eventBus);
    }
}
