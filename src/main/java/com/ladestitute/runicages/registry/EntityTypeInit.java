package com.ladestitute.runicages.registry;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.entities.*;
import com.ladestitute.runicages.entities.mobs.*;
import com.ladestitute.runicages.entities.projectiles.arrows.BronzeArrowEntity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class EntityTypeInit {
    public static final DeferredRegister<EntityType<?>> ENTITY_TYPES = DeferredRegister.create(ForgeRegistries.ENTITY_TYPES,
            RunicAgesMain.MODID);

    public static final RegistryObject<EntityType<AirStrikeEntity>> AIR_STRIKE = ENTITY_TYPES.register("air_strike",
            () -> EntityType.Builder.<AirStrikeEntity>of(AirStrikeEntity::new, MobCategory.MISC)
                    .sized(0.5F, 0.5F).build("air_strike"));

    public static final RegistryObject<EntityType<ConfuseEntity>> CONFUSE = ENTITY_TYPES.register("confuse",
            () -> EntityType.Builder.<ConfuseEntity>of(ConfuseEntity::new, MobCategory.MISC)
                    .sized(0.5F, 0.5F).build("confuse"));

    public static final RegistryObject<EntityType<WaterStrikeEntity>> WATER_STRIKE = ENTITY_TYPES.register("water_strike",
            () -> EntityType.Builder.<WaterStrikeEntity>of(WaterStrikeEntity::new, MobCategory.MISC)
                    .sized(0.5F, 0.5F).build("water_strike"));

    public static final RegistryObject<EntityType<EarthStrikeEntity>> EARTH_STRIKE = ENTITY_TYPES.register("earth_strike",
            () -> EntityType.Builder.<EarthStrikeEntity>of(EarthStrikeEntity::new, MobCategory.MISC)
                    .sized(0.5F, 0.5F).build("earth_strike"));

    public static final RegistryObject<EntityType<WeakenEntity>> WEAKEN = ENTITY_TYPES.register("weaken",
            () -> EntityType.Builder.<WeakenEntity>of(WeakenEntity::new, MobCategory.MISC)
                    .sized(0.5F, 0.5F).build("weaken"));

    public static final RegistryObject<EntityType<FireStrikeEntity>> FIRE_STRIKE = ENTITY_TYPES.register("fire_strike",
            () -> EntityType.Builder.<FireStrikeEntity>of(FireStrikeEntity::new, MobCategory.MISC)
                    .sized(0.5F, 0.5F).build("fire_strike"));

    public static final RegistryObject<EntityType<ImpEntity>> IMP = ENTITY_TYPES.register("imp",
            () -> EntityType.Builder.<ImpEntity>of(ImpEntity::new, MobCategory.CREATURE)
                    .sized(1F, 1F).build("imp"));

    public static final RegistryObject<EntityType<GrizzlyBearEntity>> GRIZZLY_BEAR = ENTITY_TYPES.register("grizzly_bear",
            () -> EntityType.Builder.<GrizzlyBearEntity>of(GrizzlyBearEntity::new, MobCategory.CREATURE)
                    .sized(1F, 1F).build("grizzly_bear"));

    public static final RegistryObject<EntityType<BlackBearEntity>> BLACK_BEAR = ENTITY_TYPES.register("black_bear",
            () -> EntityType.Builder.<BlackBearEntity>of(BlackBearEntity::new, MobCategory.CREATURE)
                    .sized(1F, 1F).build("black_bear"));

    public static final RegistryObject<EntityType<UnicornEntity>> UNICORN = ENTITY_TYPES.register("unicorn",
            () -> EntityType.Builder.<UnicornEntity>of(UnicornEntity::new, MobCategory.CREATURE)
                    .sized(1F, 1F).build("unicorn"));

    public static final RegistryObject<EntityType<DeadlyRedSpiderEntity>> DEADLY_RED_SPIDER = ENTITY_TYPES.register("deadly_red_spider",
            () -> EntityType.Builder.<DeadlyRedSpiderEntity>of(DeadlyRedSpiderEntity::new, MobCategory.MONSTER)
                    .sized(1F, 1F).build("deadly_red_spider"));

    public static final RegistryObject<EntityType<BronzeArrowEntity>> BRONZE_ARROW = ENTITY_TYPES.register("bronze_arrow",
            () -> EntityType.Builder.<BronzeArrowEntity>of(BronzeArrowEntity::new, MobCategory.MISC)
                    .sized(1F, 1F).build("bronze_arrow"));

    public static void register(IEventBus eventBus) {
        ENTITY_TYPES.register(eventBus);
    }

}
