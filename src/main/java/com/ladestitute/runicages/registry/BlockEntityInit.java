package com.ladestitute.runicages.registry;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.blocks.entities.crafting.MoldPressBlockEntity;
import com.ladestitute.runicages.blocks.entities.crafting.SewingHoopBlockEntity;
import com.ladestitute.runicages.blocks.entities.crafting.SpinningWheelBlockEntity;
import com.ladestitute.runicages.blocks.entities.smithing.*;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class BlockEntityInit {
    public static final DeferredRegister<BlockEntityType<?>> BLOCK_ENTITIES =
            DeferredRegister.create(ForgeRegistries.BLOCK_ENTITY_TYPES, RunicAgesMain.MODID);

    public static final RegistryObject<BlockEntityType<AlloyFurnaceBlockEntity>> ALLOY_FURNACE =
            BLOCK_ENTITIES.register("alloy_furnace", () ->
                    BlockEntityType.Builder.of(AlloyFurnaceBlockEntity::new,
                            BlockInit.ALLOY_FURNACE.get()).build(null));

    public static final RegistryObject<BlockEntityType<IronAlloyFurnaceBlockEntity>> IRON_ALLOY_FURNACE =
            BLOCK_ENTITIES.register("iron_alloy_furnace", () ->
                    BlockEntityType.Builder.of(IronAlloyFurnaceBlockEntity::new,
                            BlockInit.IRON_ALLOY_FURNACE.get()).build(null));

    public static final RegistryObject<BlockEntityType<SmithingFurnaceBlockEntity>> SMITHING_FURNACE =
            BLOCK_ENTITIES.register("smithing_furnace", () ->
                    BlockEntityType.Builder.of(SmithingFurnaceBlockEntity::new,
                            BlockInit.SMITHING_FURNACE.get()).build(null));

    public static final RegistryObject<BlockEntityType<SteelAlloyFurnaceBlockEntity>> STEEL_ALLOY_FURNACE =
            BLOCK_ENTITIES.register("steel_alloy_furnace", () ->
                    BlockEntityType.Builder.of(SteelAlloyFurnaceBlockEntity::new,
                            BlockInit.STEEL_ALLOY_FURNACE.get()).build(null));

    public static final RegistryObject<BlockEntityType<SilverSmithingFurnaceBlockEntity>> SILVER_SMITHING_FURNACE =
            BLOCK_ENTITIES.register("silver_smithing_furnace", () ->
                    BlockEntityType.Builder.of(SilverSmithingFurnaceBlockEntity::new,
                            BlockInit.SILVER_SMITHING_FURNACE.get()).build(null));

    public static final RegistryObject<BlockEntityType<MithrilAlloyFurnaceBlockEntity>> MITHRIL_ALLOY_FURNACE =
            BLOCK_ENTITIES.register("mithril_alloy_furnace", () ->
                    BlockEntityType.Builder.of(MithrilAlloyFurnaceBlockEntity::new,
                            BlockInit.MITHRIL_ALLOY_FURNACE.get()).build(null));

    public static final RegistryObject<BlockEntityType<GoldSmithingFurnaceBlockEntity>> GOLD_SMITHING_FURNACE =
            BLOCK_ENTITIES.register("gold_smithing_furnace", () ->
                    BlockEntityType.Builder.of(GoldSmithingFurnaceBlockEntity::new,
                            BlockInit.GOLD_SMITHING_FURNACE.get()).build(null));

    public static final RegistryObject<BlockEntityType<AdamantiteAlloyFurnaceBlockEntity>> ADAMANTITE_ALLOY_FURNACE =
            BLOCK_ENTITIES.register("adamantite_alloy_furnace", () ->
                    BlockEntityType.Builder.of(AdamantiteAlloyFurnaceBlockEntity::new,
                            BlockInit.ADAMANTITE_ALLOY_FURNACE.get()).build(null));

    public static final RegistryObject<BlockEntityType<MoldPressBlockEntity>> MOLD_PRESS =
            BLOCK_ENTITIES.register("mold_press", () ->
                    BlockEntityType.Builder.of(MoldPressBlockEntity::new,
                            BlockInit.MOLD_PRESS.get()).build(null));

    public static final RegistryObject<BlockEntityType<SpinningWheelBlockEntity>> SPINNING_WHEEL =
            BLOCK_ENTITIES.register("spinning_wheel", () ->
                    BlockEntityType.Builder.of(SpinningWheelBlockEntity::new,
                            BlockInit.SPINNING_WHEEL.get()).build(null));

    public static final RegistryObject<BlockEntityType<SewingHoopBlockEntity>> SEWING_KIT =
            BLOCK_ENTITIES.register("sewing_kit", () ->
                    BlockEntityType.Builder.of(SewingHoopBlockEntity::new,
                            BlockInit.SEWING_HOOP.get()).build(null));

    public static void register(IEventBus eventBus) {
        BLOCK_ENTITIES.register(eventBus);
    }
}
