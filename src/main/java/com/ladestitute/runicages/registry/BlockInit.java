package com.ladestitute.runicages.registry;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.blocks.RedSpidersEggsBlock;
import com.ladestitute.runicages.blocks.crafting.MoldPressBlock;
import com.ladestitute.runicages.blocks.crafting.SewingKitBlock;
import com.ladestitute.runicages.blocks.crafting.SpinningWheelBlock;
import com.ladestitute.runicages.blocks.farming.AllotmentBlock;
import com.ladestitute.runicages.blocks.farming.crops.BarleyCropBlock;
import com.ladestitute.runicages.blocks.ore.*;
import com.ladestitute.runicages.blocks.ore.cave.*;
import com.ladestitute.runicages.blocks.plants.FlaxBlock;
import com.ladestitute.runicages.blocks.runecrafting.*;
import com.ladestitute.runicages.blocks.runecrafting.altars.*;
import com.ladestitute.runicages.blocks.smithing.*;
import com.ladestitute.runicages.blocks.woodcutting.LightJungleBlock;
import com.ladestitute.runicages.blocks.woodcutting.NormalTreeLogBlock;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.*;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.function.Supplier;

public class BlockInit {
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, RunicAgesMain.MODID);

    //Rocks
    public static final RegistryObject<Block> COPPER_ROCK = registerBlock("copper_rock",
            () -> new CopperRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));
    public static final RegistryObject<Block> TIN_ROCK = registerBlock("tin_rock",
            () -> new TinRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));
    public static final RegistryObject<Block> IRON_ROCK = registerBlock("iron_rock",
            () -> new IronRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));
    public static final RegistryObject<Block> BLURITE_ROCK = registerBlock("blurite_rock",
            () -> new BluriteRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));
    public static final RegistryObject<Block> CLAY_ROCK = registerBlock("clay_rock",
            () -> new ClayRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));
    public static final RegistryObject<Block> SILVER_ROCK = registerBlock("silver_rock",
            () -> new SilverRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));
    public static final RegistryObject<Block> MITHRIL_ROCK = registerBlock("mithril_rock",
            () -> new MithrilRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));
    public static final RegistryObject<Block> ADAMANTITE_ROCK = registerBlock("adamantite_rock",
            () -> new AdamantiteRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));
    public static final RegistryObject<Block> LUMINITE_ROCK = registerBlock("luminite_rock",
            () -> new LuminiteRockBlock(PropertiesInit.LEVEL_ONE_ORE_BLOCK));

    //Cave ores
    public static final RegistryObject<Block> TIN_ORE = registerBlock("tin_ore_block",
            () -> new TinOreBlock(PropertiesInit.LEVEL_ONE_CAVE_ORE_BLOCK));
    public static final RegistryObject<Block> DEEPSLATE_TIN_ORE = registerBlock("deepslate_tin_ore_block",
            () -> new DeepslateTinOreBlock(PropertiesInit.LEVEL_ONE_DEEPCAVE_ORE_BLOCK));
    public static final RegistryObject<Block> BLURITE_ORE = registerBlock("blurite_ore_block",
            () -> new BluriteOreBlock(PropertiesInit.LEVEL_ONE_CAVE_ORE_BLOCK));
    public static final RegistryObject<Block> DEEPSLATE_BLURITE_ORE = registerBlock("deepslate_blurite_ore_block",
            () -> new DeepslateBluriteOreBlock(PropertiesInit.LEVEL_ONE_DEEPCAVE_ORE_BLOCK));
    public static final RegistryObject<Block> RUNE_ESSENCE = registerBlock("renewable_rune_essence",
            () -> new RuneEssenceBlock(PropertiesInit.RUNE_ESSENCE));
    public static final RegistryObject<Block> RUNE_ESSENCE_ORE = registerBlock("rune_essence_ore",
            () -> new RuneEssenceOreBlock(PropertiesInit.RUNE_ESSENCE_ORE));
    public static final RegistryObject<Block> SILVER_ORE = registerBlock("silver_ore_block",
            () -> new SilverOreBlock(PropertiesInit.LEVEL_ONE_CAVE_ORE_BLOCK));
    public static final RegistryObject<Block> DEEPSLATE_SILVER_ORE = registerBlock("deepslate_silver_ore_block",
            () -> new DeepslateSilverOreBlock(PropertiesInit.LEVEL_ONE_DEEPCAVE_ORE_BLOCK));
    public static final RegistryObject<Block> MITHRIL_ORE = registerBlock("mithril_ore_block",
            () -> new MithrilOreBlock(PropertiesInit.LEVEL_ONE_CAVE_ORE_BLOCK));
    public static final RegistryObject<Block> DEEPSLATE_MITHRIL_ORE = registerBlock("deepslate_mithril_ore_block",
            () -> new DeepslateMithrilOreBlock(PropertiesInit.LEVEL_ONE_DEEPCAVE_ORE_BLOCK));
    public static final RegistryObject<Block> ADAMANTITE_ORE = registerBlock("adamantite_ore_block",
            () -> new AdamantiteOreBlock(PropertiesInit.LEVEL_ONE_CAVE_ORE_BLOCK));
    public static final RegistryObject<Block> DEEPSLATE_ADAMANTITE_ORE = registerBlock("deepslate_adamantite_ore_block",
            () -> new DeepslateAdamantiteOreBlock(PropertiesInit.LEVEL_ONE_DEEPCAVE_ORE_BLOCK));
    public static final RegistryObject<Block> LUMINITE_ORE = registerBlock("luminite_ore_block",
            () -> new LuminiteOreBlock(PropertiesInit.LEVEL_ONE_CAVE_ORE_BLOCK));
    public static final RegistryObject<Block> DEEPSLATE_LUMINITE_ORE = registerBlock("deepslate_luminite_ore_block",
            () -> new DeepslateLuminiteOreBlock(PropertiesInit.LEVEL_ONE_DEEPCAVE_ORE_BLOCK));

    //Smithing
    public static final RegistryObject<Block> SMITHING_FURNACE = registerBlock("smithing_furnace",
            () -> new SmithingFurnaceBlock(PropertiesInit.SMITHING_FURNACE));
    public static final RegistryObject<Block> ALLOY_FURNACE = registerBlock("alloy_furnace",
            () -> new AlloyFurnaceBlock(PropertiesInit.SMITHING_FURNACE));
    public static final RegistryObject<Block> IRON_ALLOY_FURNACE = registerBlock("iron_alloy_furnace",
            () -> new IronAlloyFurnaceBlock(PropertiesInit.SMITHING_FURNACE));
    public static final RegistryObject<Block> STEEL_ALLOY_FURNACE = registerBlock("steel_alloy_furnace",
            () -> new SteelAlloyFurnaceBlock(PropertiesInit.SMITHING_FURNACE));
    public static final RegistryObject<Block> SILVER_SMITHING_FURNACE = registerBlock("silver_smithing_furnace",
            () -> new SilverSmithingFurnaceBlock(PropertiesInit.SMITHING_FURNACE));
    public static final RegistryObject<Block> MITHRIL_ALLOY_FURNACE = registerBlock("mithril_alloy_furnace",
            () -> new MithrilAlloyFurnaceBlock(PropertiesInit.SMITHING_FURNACE));
    public static final RegistryObject<Block> GOLD_SMITHING_FURNACE = registerBlock("gold_smithing_furnace",
            () -> new GoldSmithingFurnaceBlock(PropertiesInit.SMITHING_FURNACE));
    public static final RegistryObject<Block> ADAMANTITE_ALLOY_FURNACE = registerBlock("adamantite_alloy_furnace",
            () -> new AdamantiteAlloyFurnaceBlock(PropertiesInit.SMITHING_FURNACE));
    public static final RegistryObject<Block> MOLD_PRESS = registerBlock("mold_press",
            () -> new MoldPressBlock(PropertiesInit.SMITHING_FURNACE));
    public static final RegistryObject<Block> SPINNING_WHEEL = registerBlock("spinning_wheel",
            () -> new SpinningWheelBlock(PropertiesInit.SPINNING_WHEEL));

    //Crafting and misc
    public static final RegistryObject<Block> FLAX = registerBlock("flax_block",
            () -> new FlaxBlock(PropertiesInit.FLAX));
    public static final RegistryObject<Block> SEWING_HOOP = registerBlock("sewing_hoop",
            () -> new SewingKitBlock(PropertiesInit.SPINNING_WHEEL));

    //Runecrafting
    public static final RegistryObject<Block> AIR_ALTAR = registerBlock("air_altar",
            () -> new AirAltarBlock(PropertiesInit.RUNECRAFTING_ALTAR));
    public static final RegistryObject<Block> MIND_ALTAR = registerBlock("mind_altar",
            () -> new MindAltarBlock(PropertiesInit.RUNECRAFTING_ALTAR));
    public static final RegistryObject<Block> WATER_ALTAR = registerBlock("water_altar",
            () -> new WaterAltarBlock(PropertiesInit.RUNECRAFTING_ALTAR));
    public static final RegistryObject<Block> EARTH_ALTAR = registerBlock("earth_altar",
            () -> new EarthAltarBlock(PropertiesInit.RUNECRAFTING_ALTAR));
    public static final RegistryObject<Block> FIRE_ALTAR = registerBlock("fire_altar",
            () -> new FireAltarBlock(PropertiesInit.RUNECRAFTING_ALTAR));
    public static final RegistryObject<Block> BODY_ALTAR = registerBlock("body_altar",
            () -> new BodyAltarBlock(PropertiesInit.RUNECRAFTING_ALTAR));
    public static final RegistryObject<Block> AIR_ALTAR_CYCLIC_STONE = registerBlock("air_altar_cyclic_stone",
            () -> new AirAltarCyclicStoneBlock(PropertiesInit.ALTAR_STONE));
    public static final RegistryObject<Block> MIND_ALTAR_CYCLIC_STONE = registerBlock("mind_altar_cyclic_stone",
            () -> new MindAltarCyclicStoneBlock(PropertiesInit.ALTAR_STONE));
    public static final RegistryObject<Block> WATER_ALTAR_CYCLIC_STONE = registerBlock("water_altar_cyclic_stone",
            () -> new WaterAltarCyclicStoneBlock(PropertiesInit.ALTAR_STONE));
    public static final RegistryObject<Block> EARTH_ALTAR_CYCLIC_STONE = registerBlock("earth_altar_cyclic_stone",
            () -> new EarthAltarCyclicStoneBlock(PropertiesInit.ALTAR_STONE));
    public static final RegistryObject<Block> FIRE_ALTAR_CYCLIC_STONE = registerBlock("fire_altar_cyclic_stone",
            () -> new FireAltarCyclicStoneBlock(PropertiesInit.ALTAR_STONE));
    public static final RegistryObject<Block> BODY_ALTAR_CYCLIC_STONE = registerBlock("body_altar_cyclic_stone",
            () -> new BodyAltarCyclicStoneBlock(PropertiesInit.ALTAR_STONE));
    public static final RegistryObject<Block> ALTAR_BRICK_CENTER = registerBlock("altar_brick_center",
            () -> new AltarBrickCenterBlock(PropertiesInit.ALTAR_STONE));
    public static final RegistryObject<Block> ALTAR_BRICK_SIDE = registerBlock("altar_brick_side",
            () -> new AltarBrickSideBlock(PropertiesInit.ALTAR_STONE));
    public static final RegistryObject<Block> ALTAR_SLAB = registerBlock("altar_slab",
            () -> new AltarSlabBlock(PropertiesInit.ALTAR_STONE));

    //Woodcutting
    public static final RegistryObject<Block> NORMAL_TREE_LOG = registerBlock("normal_tree_log",
            () -> new NormalTreeLogBlock(PropertiesInit.LEVEL_ONE_TREE));
    public static final RegistryObject<Block> NORMAL_TREE_LEAVES = registerBlock("normal_tree_leaves",
            () -> new LeavesBlock(BlockBehaviour.Properties.copy(Blocks.OAK_LEAVES)) {
                @Override
                public boolean isFlammable(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
                    return true;
                }

                @Override
                public int getFlammability(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
                    return 60;
                }

                @Override
                public int getFireSpreadSpeed(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
                    return 30;
                }
            });
    public static final RegistryObject<Block> LIGHT_JUNGLE = registerBlock("light_jungle",
            () -> new LightJungleBlock(PropertiesInit.LEVEL_ONE_TREE));

    //Farming
    public static final RegistryObject<Block> ALLOTMENT = registerBlock("allotment",
            () -> new AllotmentBlock(PropertiesInit.ALLOTMENT));
    public static final RegistryObject<Block> HOPS_PATCH = registerBlock("hops_patch",
            () -> new AllotmentBlock(PropertiesInit.ALLOTMENT));
    public static final RegistryObject<Block> HERB_PATCH = registerBlock("herb_patch",
            () -> new AllotmentBlock(PropertiesInit.ALLOTMENT));
    public static final RegistryObject<Block> BUSH_PATCH = registerBlock("bush_patch",
            () -> new AllotmentBlock(PropertiesInit.ALLOTMENT));

    public static final RegistryObject<Block> RED_SPIDERS_EGG_BLOCK = registerBlock("red_spider_eggs_block",
            () -> new RedSpidersEggsBlock(PropertiesInit.FLAX));

    private static <T extends Block> RegistryObject<T> registerBlock(String name, Supplier<T> block) {
        RegistryObject<T> toReturn = BLOCKS.register(name, block);
        registerBlockItem(name, toReturn);
        return toReturn;
    }

    private static <T extends Block> RegistryObject<Item> registerBlockItem(String name, RegistryObject<T> block) {
        return ItemInit.ITEMS.register(name, () -> new BlockItem(block.get(), new Item.Properties()));
    }


    public static class Tags {
        //Pay attention here, we specify our modid and imply the data/modid/tags/blocks directory in our resource-location
        //all it needs though is the "blocks" part of the directory, it finds the rest of the directory automatically
        public static final TagKey<Block> NEEDS_BRONZE_TOOL = create("blocks/needs_bronze_tool");
        public static final TagKey<Block> NEEDS_BLURITE_TOOL = create("blocks/needs_blurite_tool");

        private static TagKey<Block> create(String location) {
            return BlockTags.create(new ResourceLocation(RunicAgesMain.MODID, location));
        }
    }

    public static void register(IEventBus eventBus) {
        BLOCKS.register(eventBus);
    }
}
