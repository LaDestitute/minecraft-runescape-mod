package com.ladestitute.runicages.registry;

import com.ladestitute.runicages.RunicAgesMain;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;


public class SoundInit {
    public static final DeferredRegister<SoundEvent> SOUNDS = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, RunicAgesMain.MODID);

//    public static final RegistryObject<SoundEvent> EXAMPLE_SOUND = SOUNDS.register("block.example_sound",
//            () -> new SoundEvent(new ResourceLocation(RunicAgesMain.MODID, "block.example_sound")));

}
