package com.ladestitute.runicages.network;

import com.ladestitute.runicages.capability.woodcutting.RunicAgesWoodcuttingCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientWoodcuttingSkillPacket implements INormalMessage {
    int WOODCUTTING_LEVEL;
    int WOODCUTTING_XP;
    int NEXT_WOODCUTTING_XP;

    public ClientWoodcuttingSkillPacket(int level, int xp, int next_xp) {
        this.WOODCUTTING_LEVEL = level;
        this.WOODCUTTING_XP = xp;
        this.NEXT_WOODCUTTING_XP = next_xp;
    }

    public ClientWoodcuttingSkillPacket(FriendlyByteBuf buf) {
        WOODCUTTING_LEVEL = buf.readInt();
        WOODCUTTING_XP = buf.readInt();
        NEXT_WOODCUTTING_XP = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(WOODCUTTING_LEVEL);
        buf.writeInt(WOODCUTTING_XP);
        buf.writeInt(NEXT_WOODCUTTING_XP);
    }

    @Override
    public void process(Supplier<NetworkEvent.Context> context) {
        //This method is for when information is received by the intended end (i.e, client in this case)
        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
        //Remember that client/server side rules apply here
        //Access client stuff only in client, otherwise you will crash MC
        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(RunicAgesWoodcuttingCapability.Provider.WOODCUTTING_LEVEL).
                    ifPresent(cap -> {
                        //do stuff with the info, such as mainly syncing info for the client-side gui
                        cap.setWoodcuttingLevel(WOODCUTTING_LEVEL);
                        cap.setWoodcuttingXP(WOODCUTTING_XP);
                        cap.setNextWoodcuttingXP(NEXT_WOODCUTTING_XP);
                    }));
        }
    }
}

