package com.ladestitute.runicages.network;

import com.ladestitute.runicages.capability.smithing.RunicAgesSmithingCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientSmithingSkillPacket implements INormalMessage {
    int SMITHING_LEVEL;
    int SMITHING_XP;
    int NEXT_SMITHING_XP;
    int USING_FURNACE;
    int SMELTING;
    int BAR_TYPE;
    int SMELT_COUNT;
    int SMELT_TICKS;
    int SMELT_STAGE;
    int MAKE_COUNT;
    boolean MAKING_ALL;

    public ClientSmithingSkillPacket(int level, int xp, int next_xp, int using_furnace, int smelting, int bar_type,
                                     int smelt_count, int smelt_ticks, int smelt_stage, int make_count, boolean making_all) {
        this.SMITHING_LEVEL = level;
        this.SMITHING_XP = xp;
        this.NEXT_SMITHING_XP = next_xp;
        this.USING_FURNACE = using_furnace;
        this.SMELTING = smelting;
        this.BAR_TYPE = bar_type;
        this.SMELT_COUNT = smelt_count;
        this.SMELT_TICKS = smelt_ticks;
        this.SMELT_STAGE = smelt_stage;
        this.MAKE_COUNT = make_count;
        this.MAKING_ALL = making_all;
    }

    public ClientSmithingSkillPacket(FriendlyByteBuf buf) {
        SMITHING_LEVEL = buf.readInt();
        SMITHING_XP = buf.readInt();
        NEXT_SMITHING_XP = buf.readInt();
        USING_FURNACE = buf.readInt();
        SMELTING = buf.readInt();
        BAR_TYPE = buf.readInt();
        SMELT_COUNT = buf.readInt();
        SMELT_TICKS = buf.readInt();
        SMELT_STAGE = buf.readInt();
        MAKE_COUNT = buf.readInt();
        MAKING_ALL = buf.readBoolean();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(SMITHING_LEVEL);
        buf.writeInt(SMITHING_XP);
        buf.writeInt(NEXT_SMITHING_XP);
        buf.writeInt(USING_FURNACE);
        buf.writeInt(SMELTING);
        buf.writeInt(BAR_TYPE);
        buf.writeInt(SMELT_COUNT);
        buf.writeInt(SMELT_TICKS);
        buf.writeInt(SMELT_STAGE);
        buf.writeInt(MAKE_COUNT);
        buf.writeBoolean(MAKING_ALL);
    }

    @Override
    public void process(Supplier<NetworkEvent.Context> context) {
        //This method is for when information is received by the intended end (i.e, client in this case)
        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
        //Remember that client/server side rules apply here
        //Access client stuff only in client, otherwise you will crash MC
        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).
                    ifPresent(cap -> {
                        //do stuff with the info, such as mainly syncing info for the client-side gui
                        cap.setSmithingLevel(SMITHING_LEVEL);
                        cap.setSmithingXP(SMITHING_XP);
                        cap.setNextSmithingXP(NEXT_SMITHING_XP);
                        cap.setUsingFurnace(USING_FURNACE);
                        cap.setSmelting(SMELTING);
                        cap.setBarType(BAR_TYPE);
                        cap.setSmeltCount(SMELT_COUNT);
                        cap.setSmeltTicks(SMELT_TICKS);
                        cap.setSmeltStage(SMELT_STAGE);
                        cap.setMakeCount(MAKE_COUNT);
                        cap.setMakingAll(MAKING_ALL);
                    }));
        }
    }
}
