package com.ladestitute.runicages.network;

import com.ladestitute.runicages.capability.attack.RunicAgesAttackCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientAttackSkillPacket implements INormalMessage {
    int ATTACK_LEVEL;
    int ATTACK_XP;
    int NEXT_ATTACK_XP;
    int ATTACK_BOOST;
    int ATTACK_BOOST_TIMER;
    int INVISIBLE_ATTACK_BOOST;

    public ClientAttackSkillPacket(int level, int xp, int next_xp, int attack_boost, int attack_boost_timer,
                                   int invisible_attack_boost) {
        this.ATTACK_LEVEL = level;
        this.ATTACK_XP = xp;
        this.NEXT_ATTACK_XP = next_xp;
        this.ATTACK_BOOST = attack_boost;
        this.ATTACK_BOOST_TIMER = attack_boost_timer;
        this.INVISIBLE_ATTACK_BOOST = invisible_attack_boost;
    }

    public ClientAttackSkillPacket(FriendlyByteBuf buf) {
        ATTACK_LEVEL = buf.readInt();
        ATTACK_XP = buf.readInt();
        NEXT_ATTACK_XP = buf.readInt();
        ATTACK_BOOST = buf.readInt();
        ATTACK_BOOST_TIMER = buf.readInt();
        INVISIBLE_ATTACK_BOOST = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(ATTACK_LEVEL);
        buf.writeInt(ATTACK_XP);
        buf.writeInt(NEXT_ATTACK_XP);
        buf.writeInt(ATTACK_BOOST);
        buf.writeInt(ATTACK_BOOST_TIMER);
        buf.writeInt(INVISIBLE_ATTACK_BOOST);
    }

    @Override
    public void process(Supplier<NetworkEvent.Context> context) {
        //This method is for when information is received by the intended end (i.e, client in this case)
        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
        //Remember that client/server side rules apply here
        //Access client stuff only in client, otherwise you will crash MC
        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(RunicAgesAttackCapability.Provider.ATTACK_LEVEL).
                    ifPresent(cap -> {
                        //do stuff with the info, such as mainly syncing info for the client-side gui
                        cap.setAttackLevel(ATTACK_LEVEL);
                        cap.setAttackXP(ATTACK_XP);
                        cap.setNextAttackXP(NEXT_ATTACK_XP);
                        cap.setAttackBoost(ATTACK_BOOST);
                        cap.setattackboostdraintimer(ATTACK_BOOST_TIMER);
                        cap.setAttackBoost(INVISIBLE_ATTACK_BOOST);
                    }));
        }
    }
}



