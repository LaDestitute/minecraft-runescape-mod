package com.ladestitute.runicages.network;

import com.ladestitute.runicages.capability.crafting.RunicAgesCraftingCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientCraftingSkillPacket implements INormalMessage {
    int CRAFTING_LEVEL;
    int CRAFTING_XP;
    int NEXT_CRAFTING_XP;

    public ClientCraftingSkillPacket(int level, int xp, int next_xp) {
        this.CRAFTING_LEVEL = level;
        this.CRAFTING_XP = xp;
        this.NEXT_CRAFTING_XP = next_xp;
    }

    public ClientCraftingSkillPacket(FriendlyByteBuf buf) {
        CRAFTING_LEVEL = buf.readInt();
        CRAFTING_XP = buf.readInt();
        NEXT_CRAFTING_XP = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(CRAFTING_LEVEL);
        buf.writeInt(CRAFTING_XP);
        buf.writeInt(NEXT_CRAFTING_XP);
    }

    @Override
    public void process(Supplier<NetworkEvent.Context> context) {
        //This method is for when information is received by the intended end (i.e, client in this case)
        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
        //Remember that client/server side rules apply here
        //Access client stuff only in client, otherwise you will crash MC
        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).
                    ifPresent(cap -> {
                        //do stuff with the info, such as mainly syncing info for the client-side gui
                        cap.setCraftingLevel(CRAFTING_LEVEL);
                        cap.setCraftingXP(CRAFTING_XP);
                        cap.setNextCraftingXP(NEXT_CRAFTING_XP);
                    }));
        }
    }
}

