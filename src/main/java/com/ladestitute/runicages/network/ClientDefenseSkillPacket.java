package com.ladestitute.runicages.network;

import com.ladestitute.runicages.capability.defense.RunicAgesDefenseCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientDefenseSkillPacket implements INormalMessage {
    int DEFENSE_LEVEL;
    int DEFENSE_XP;
    int NEXT_DEFENSE_XP;
    int DEFENSE_BOOST;
    int DEFENSE_BOOST_DRAIN_TIMER;
    int INVISIBLE_DEFENSE_BOOST;

    public ClientDefenseSkillPacket(int level, int xp, int next_xp, int defense_boost, int defense_timer, int invisible_defense_boost) {
        this.DEFENSE_LEVEL = level;
        this.DEFENSE_XP = xp;
        this.NEXT_DEFENSE_XP = next_xp;
        this.DEFENSE_BOOST = defense_boost;
        this.DEFENSE_BOOST_DRAIN_TIMER = defense_timer;
        this.INVISIBLE_DEFENSE_BOOST = invisible_defense_boost;
    }

    public ClientDefenseSkillPacket(FriendlyByteBuf buf) {
        DEFENSE_LEVEL = buf.readInt();
        DEFENSE_XP = buf.readInt();
        NEXT_DEFENSE_XP = buf.readInt();
        DEFENSE_BOOST = buf.readInt();
        DEFENSE_BOOST_DRAIN_TIMER = buf.readInt();
        INVISIBLE_DEFENSE_BOOST = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(DEFENSE_LEVEL);
        buf.writeInt(DEFENSE_XP);
        buf.writeInt(NEXT_DEFENSE_XP);
        buf.writeInt(DEFENSE_BOOST);
        buf.writeInt(DEFENSE_BOOST_DRAIN_TIMER);
        buf.writeInt(INVISIBLE_DEFENSE_BOOST);
    }

    @Override
    public void process(Supplier<NetworkEvent.Context> context) {
        //This method is for when information is received by the intended end (i.e, client in this case)
        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
        //Remember that client/server side rules apply here
        //Access client stuff only in client, otherwise you will crash MC
        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(RunicAgesDefenseCapability.Provider.DEFENSE_LEVEL).
                    ifPresent(cap -> {
                        //do stuff with the info, such as mainly syncing info for the client-side gui
                        cap.setDefenseLevel(DEFENSE_LEVEL);
                        cap.setDefenseXP(DEFENSE_XP);
                        cap.setNextDefenseXP(NEXT_DEFENSE_XP);
                        cap.setDefenseBoost(DEFENSE_BOOST);
                        cap.setdefenseboostdraintimer(DEFENSE_BOOST_DRAIN_TIMER);
                        cap.setInvisibleDefenseBoost(INVISIBLE_DEFENSE_BOOST);
                    }));
        }
    }
}



