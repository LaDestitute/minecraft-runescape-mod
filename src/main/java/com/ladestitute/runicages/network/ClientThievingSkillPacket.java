package com.ladestitute.runicages.network;

import com.ladestitute.runicages.capability.thieving.RunicAgesThievingCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientThievingSkillPacket implements INormalMessage {
    int THIEVING_LEVEL;
    int THIEVING_XP;
    int NEXT_THIEVING_XP;
    int THIEVING_BOOST;
    int THIEVING_BOOST_TIMER;
    int INVISIBLE_THIEVING_BOOST;

    public ClientThievingSkillPacket(int level, int xp, int next_xp, int thieving_boost, int thieving_boost_timer,
                                     int invisible_thieving_boost) {
        this.THIEVING_LEVEL = level;
        this.THIEVING_XP = xp;
        this.NEXT_THIEVING_XP = next_xp;
        this.THIEVING_BOOST = thieving_boost;
        this.THIEVING_BOOST_TIMER = thieving_boost_timer;
        this.INVISIBLE_THIEVING_BOOST = invisible_thieving_boost;
    }

    public ClientThievingSkillPacket(FriendlyByteBuf buf) {
        THIEVING_LEVEL = buf.readInt();
        THIEVING_XP = buf.readInt();
        NEXT_THIEVING_XP = buf.readInt();
        THIEVING_BOOST = buf.readInt();
        THIEVING_BOOST_TIMER = buf.readInt();
        INVISIBLE_THIEVING_BOOST = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(THIEVING_LEVEL);
        buf.writeInt(THIEVING_XP);
        buf.writeInt(NEXT_THIEVING_XP);
        buf.writeInt(THIEVING_BOOST);
        buf.writeInt(THIEVING_BOOST_TIMER);
        buf.writeInt(INVISIBLE_THIEVING_BOOST);
    }

    @Override
    public void process(Supplier<NetworkEvent.Context> context) {
        //This method is for when information is received by the intended end (i.e, client in this case)
        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
        //Remember that client/server side rules apply here
        //Access client stuff only in client, otherwise you will crash MC
        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(RunicAgesThievingCapability.Provider.THIEVING_LEVEL).
                    ifPresent(cap -> {
                        //do stuff with the info, such as mainly syncing info for the client-side gui
                        cap.setThievingLevel(THIEVING_LEVEL);
                        cap.setThievingXP(THIEVING_XP);
                        cap.setNextThievingXP(NEXT_THIEVING_XP);
                        cap.setThievingBoost(THIEVING_BOOST);
                        cap.setthievingboostdraintimer(THIEVING_BOOST_TIMER);
                        cap.setThievingBoost(INVISIBLE_THIEVING_BOOST);
                    }));
        }
    }
}

