package com.ladestitute.runicages.network;

import com.ladestitute.runicages.capability.ranged.RunicAgesRangedCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientRangedSkillPacket implements INormalMessage {
    int RANGED_LEVEL;
    int RANGED_XP;
    int NEXT_RANGED_XP;
    int RANGED_BOOST;
    int RANGED_BOOST_DRAIN_TIMER;
    int INVISIBLE_RANGED_BOOST;

    public ClientRangedSkillPacket(int level, int xp, int next_xp, int ranged_boost, int ranged_boost_drain_timer, int invisible_ranged_boost) {
        this.RANGED_LEVEL = level;
        this.RANGED_XP = xp;
        this.NEXT_RANGED_XP = next_xp;
        this.RANGED_BOOST = ranged_boost;
        this.RANGED_BOOST_DRAIN_TIMER = ranged_boost_drain_timer;
        this.INVISIBLE_RANGED_BOOST = invisible_ranged_boost;
    }

    public ClientRangedSkillPacket(FriendlyByteBuf buf) {
        RANGED_LEVEL = buf.readInt();
        RANGED_XP = buf.readInt();
        NEXT_RANGED_XP = buf.readInt();
        RANGED_BOOST = buf.readInt();
        RANGED_BOOST_DRAIN_TIMER = buf.readInt();
        INVISIBLE_RANGED_BOOST = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(RANGED_LEVEL);
        buf.writeInt(RANGED_XP);
        buf.writeInt(NEXT_RANGED_XP);
        buf.writeInt(RANGED_BOOST);
        buf.writeInt(RANGED_BOOST_DRAIN_TIMER);
        buf.writeInt(INVISIBLE_RANGED_BOOST);
    }

    @Override
    public void process(Supplier<NetworkEvent.Context> context) {
        //This method is for when information is received by the intended end (i.e, client in this case)
        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
        //Remember that client/server side rules apply here
        //Access client stuff only in client, otherwise you will crash MC
        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(RunicAgesRangedCapability.Provider.RANGED_LEVEL).
                    ifPresent(cap -> {
                        //do stuff with the info, such as mainly syncing info for the client-side gui
                        cap.setRangedLevel(RANGED_LEVEL);
                        cap.setRangedXP(RANGED_XP);
                        cap.setNextRangedXP(NEXT_RANGED_XP);
                        cap.setRangedBoost(RANGED_BOOST);
                        cap.setrangedboostdraintimer(RANGED_BOOST_DRAIN_TIMER);
                        cap.setInvisibleRangedBoost(INVISIBLE_RANGED_BOOST);
                    }));
        }
    }
}

