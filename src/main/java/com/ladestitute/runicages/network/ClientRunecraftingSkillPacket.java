package com.ladestitute.runicages.network;

import com.ladestitute.runicages.capability.runecrafting.RunicAgesRunecraftingCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientRunecraftingSkillPacket implements INormalMessage {
    int RUNECRAFTING_LEVEL;
    int RUNECRAFTING_XP;
    int NEXT_RUNECRAFTING_XP;

    public ClientRunecraftingSkillPacket(int level, int xp, int next_xp) {
        this.RUNECRAFTING_LEVEL = level;
        this.RUNECRAFTING_XP = xp;
        this.NEXT_RUNECRAFTING_XP = next_xp;
    }

    public ClientRunecraftingSkillPacket(FriendlyByteBuf buf) {
        RUNECRAFTING_LEVEL = buf.readInt();
        RUNECRAFTING_XP = buf.readInt();
        NEXT_RUNECRAFTING_XP = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(RUNECRAFTING_LEVEL);
        buf.writeInt(RUNECRAFTING_XP);
        buf.writeInt(NEXT_RUNECRAFTING_XP);
    }
//
//    @Override
//    public void process(Supplier<NetworkEvent.Context> context) {
//        //This method is for when information is received by the intended end (i.e, client in this case)
//        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
//        //Remember that client/server side rules apply here
//        //Access client stuff only in client, otherwise you will crash MC
//        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
//            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(RunicAgesRunecraftingCapability.Provider.RUNECRAFTING_LEVEL).
//                    ifPresent(cap -> {
//                        //do stuff with the info, such as mainly syncing info for the client-side gui
//                        cap.setRunecraftingLevel(RUNECRAFTING_LEVEL);
//                        cap.setRunecraftingXP(RUNECRAFTING_XP);
//                        cap.setNextRunecraftingXP(NEXT_RUNECRAFTING_XP);
//                    }));
//        }
//    }

    @Override
    public void process(Supplier<NetworkEvent.Context> context) {
        //This method is for when information is received by the intended end (i.e, client in this case)
        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
        //Remember that client/server side rules apply here
        //Access client stuff only in client, otherwise you will crash MC
        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(RunicAgesRunecraftingCapability.Provider.RUNECRAFTING_LEVEL).
                    ifPresent(cap -> {
                        //do stuff with the info, such as mainly syncing info for the client-side gui
                        cap.setRunecraftingLevel(RUNECRAFTING_LEVEL);
                        cap.setRunecraftingXP(RUNECRAFTING_XP);
                        cap.setNextRunecraftingXP(NEXT_RUNECRAFTING_XP);
                    }));
        }
    }
}


