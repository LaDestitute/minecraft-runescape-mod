package com.ladestitute.runicages.network;

import com.ladestitute.runicages.capability.farming.RunicAgesFarmingCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientFarmingSkillPacket implements INormalMessage {
    int FARMING_LEVEL;
    int FARMING_XP;
    int NEXT_FARMING_XP;
    int FARMING_BOOST;
    int FARMING_BOOST_TIMER;
    int INVISIBLE_FARMING_BOOST;

    public ClientFarmingSkillPacket(int level, int xp, int next_xp, int farming_boost, int farming_boost_timer,
                                    int invisible_farming_boost) {
        this.FARMING_LEVEL = level;
        this.FARMING_XP = xp;
        this.NEXT_FARMING_XP = next_xp;
        this.FARMING_BOOST = farming_boost;
        this.FARMING_BOOST_TIMER = farming_boost_timer;
        this.INVISIBLE_FARMING_BOOST = invisible_farming_boost;
    }

    public ClientFarmingSkillPacket(FriendlyByteBuf buf) {
        FARMING_LEVEL = buf.readInt();
        FARMING_XP = buf.readInt();
        NEXT_FARMING_XP = buf.readInt();
        FARMING_BOOST = buf.readInt();
        FARMING_BOOST_TIMER = buf.readInt();
        INVISIBLE_FARMING_BOOST = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(FARMING_LEVEL);
        buf.writeInt(FARMING_XP);
        buf.writeInt(NEXT_FARMING_XP);
        buf.writeInt(FARMING_BOOST);
        buf.writeInt(FARMING_BOOST_TIMER);
        buf.writeInt(INVISIBLE_FARMING_BOOST);
    }

    @Override
    public void process(Supplier<NetworkEvent.Context> context) {
        //This method is for when information is received by the intended end (i.e, client in this case)
        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
        //Remember that client/server side rules apply here
        //Access client stuff only in client, otherwise you will crash MC
        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(RunicAgesFarmingCapability.Provider.FARMING_LEVEL).
                    ifPresent(cap -> {
                        //do stuff with the info, such as mainly syncing info for the client-side gui
                        cap.setFarmingLevel(FARMING_LEVEL);
                        cap.setFarmingXP(FARMING_XP);
                        cap.setNextFarmingXP(NEXT_FARMING_XP);
                        cap.setFarmingBoost(FARMING_BOOST);
                        cap.setfarmingboostdraintimer(FARMING_BOOST_TIMER);
                        cap.setFarmingBoost(INVISIBLE_FARMING_BOOST);
                    }));
        }
    }
}
