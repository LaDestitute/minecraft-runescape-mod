package com.ladestitute.runicages.network;

import com.ladestitute.runicages.capability.herblore.RunicAgesHerbloreCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientHerbloreSkillPacket implements INormalMessage {
    int HERBLORE_LEVEL;
    int HERBLORE_XP;
    int NEXT_HERBLORE_XP;

    public ClientHerbloreSkillPacket(int level, int xp, int next_xp) {
        this.HERBLORE_LEVEL = level;
        this.HERBLORE_XP = xp;
        this.NEXT_HERBLORE_XP = next_xp;
    }

    public ClientHerbloreSkillPacket(FriendlyByteBuf buf) {
        HERBLORE_LEVEL = buf.readInt();
        HERBLORE_XP = buf.readInt();
        NEXT_HERBLORE_XP = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(HERBLORE_LEVEL);
        buf.writeInt(HERBLORE_XP);
        buf.writeInt(NEXT_HERBLORE_XP);
    }

    @Override
    public void process(Supplier<NetworkEvent.Context> context) {
        //This method is for when information is received by the intended end (i.e, client in this case)
        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
        //Remember that client/server side rules apply here
        //Access client stuff only in client, otherwise you will crash MC
        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(RunicAgesHerbloreCapability.Provider.HERBLORE_LEVEL).
                    ifPresent(cap -> {
                        //do stuff with the info, such as mainly syncing info for the client-side gui
                        cap.setHerbloreLevel(HERBLORE_LEVEL);
                        cap.setHerbloreXP(HERBLORE_XP);
                        cap.setNextHerbloreXP(NEXT_HERBLORE_XP);
                    }));
        }
    }
}



