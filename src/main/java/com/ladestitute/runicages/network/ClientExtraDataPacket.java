package com.ladestitute.runicages.network;

import com.ladestitute.runicages.capability.magic.RunicAgesMagicCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientExtraDataPacket implements INormalMessage {
    int SET_HOME;
    float POS_X;
    float POS_Y;
    float POS_Z;
    int HAS_TOOLBELT_CHISEL;
    int CLAY_CHARGES;
    int RECOIL_CHARGES;
    int TOTAL_XP;
    int RECEIVE_ATTACK_XP;

    int RECEIVE_STRENGTH_XP;
    int RECEIVE_DEFENSE_XP;
    int RECEIVE_RANGED_XP;
    int RECEIVE_MAGIC_XP;
    int HAS_TOOLBELT_PESTLE_AND_MORTAR;
    int BOUNTY_CHARGES;
    int HAS_TOOLBELT_NEEDLE;

    public ClientExtraDataPacket(int set_home, float pos_x, float pos_y, float pos_z, int has_toolbelt_chisel,
                                 int clay_charges, int recoil_charges, int total_xp, int receive_attack_xp,
                                 int receive_strength_xp, int receive_defense_xp,
                                 int receive_ranged_xp, int receive_magic_xp, int has_toolbelt_pestle_and_mortar, int bounty_charges, int has_toolbelt_needle) {
        this.SET_HOME = set_home;
        this.POS_X = pos_x;
        this.POS_Y = pos_y;
        this.POS_Z = pos_z;
        this.HAS_TOOLBELT_CHISEL = has_toolbelt_chisel;
        this.CLAY_CHARGES = clay_charges;
        this.RECOIL_CHARGES = recoil_charges;
        this.TOTAL_XP = total_xp;
        this.RECEIVE_ATTACK_XP = receive_attack_xp;
        this.RECEIVE_STRENGTH_XP = receive_strength_xp;
        this.RECEIVE_DEFENSE_XP = receive_defense_xp;
        this.RECEIVE_RANGED_XP = receive_ranged_xp;
        this.RECEIVE_MAGIC_XP = receive_magic_xp;
        this.HAS_TOOLBELT_PESTLE_AND_MORTAR = has_toolbelt_pestle_and_mortar;
        this.BOUNTY_CHARGES = bounty_charges;
        this.HAS_TOOLBELT_NEEDLE = has_toolbelt_needle;
    }

    public ClientExtraDataPacket(FriendlyByteBuf buf) {
        SET_HOME = buf.readInt();
        POS_X = buf.readInt();
        POS_Y = buf.readInt();
        POS_Z = buf.readInt();
        HAS_TOOLBELT_CHISEL = buf.readInt();
        CLAY_CHARGES = buf.readInt();
        RECOIL_CHARGES = buf.readInt();
        TOTAL_XP = buf.readInt();
        RECEIVE_ATTACK_XP = buf.readInt();
        RECEIVE_STRENGTH_XP = buf.readInt();
        RECEIVE_DEFENSE_XP = buf.readInt();
        RECEIVE_RANGED_XP = buf.readInt();
        RECEIVE_MAGIC_XP = buf.readInt();
        HAS_TOOLBELT_PESTLE_AND_MORTAR = buf.readInt();
        BOUNTY_CHARGES = buf.readInt();
        HAS_TOOLBELT_NEEDLE = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(SET_HOME);
        buf.writeFloat(POS_X);
        buf.writeFloat(POS_Y);
        buf.writeFloat(POS_Z);
        buf.writeInt(HAS_TOOLBELT_CHISEL);
        buf.writeInt(CLAY_CHARGES);
        buf.writeInt(RECOIL_CHARGES);
        buf.writeInt(TOTAL_XP);
        buf.writeInt(RECEIVE_ATTACK_XP);
        buf.writeInt(RECEIVE_STRENGTH_XP);
        buf.writeInt(RECEIVE_DEFENSE_XP);
        buf.writeInt(RECEIVE_RANGED_XP);
        buf.writeInt(RECEIVE_MAGIC_XP);
        buf.writeInt(HAS_TOOLBELT_PESTLE_AND_MORTAR);
        buf.writeInt(BOUNTY_CHARGES);
        buf.writeInt(HAS_TOOLBELT_NEEDLE);
    }

    @Override
    public void process(Supplier<NetworkEvent.Context> context) {
        //This method is for when information is received by the intended end (i.e, client in this case)
        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
        //Remember that client/server side rules apply here
        //Access client stuff only in client, otherwise you will crash MC
        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).
                    ifPresent(cap -> {
                        //do stuff with the info, such as mainly syncing info for the client-side gui
                        cap.sethashome(SET_HOME);
                        cap.setx(POS_X);
                        cap.sety(POS_Y);
                        cap.setz(POS_Z);
                        cap.sethastoolbeltchisel(HAS_TOOLBELT_CHISEL);
                        cap.resetclaybraceletcharges(CLAY_CHARGES);
                        cap.setrecoilringcharges(RECOIL_CHARGES);
                        cap.settotalxp(TOTAL_XP);
                        cap.receiveattackxp(RECEIVE_ATTACK_XP);
                        cap.receivestrengthxp(RECEIVE_STRENGTH_XP);
                        cap.receivedefensexp(RECEIVE_DEFENSE_XP);
                        cap.receiverangedxp(RECEIVE_RANGED_XP);
                        cap.receivemagicxp(RECEIVE_MAGIC_XP);
                        cap.sethastoolbeltpestleandmortar(HAS_TOOLBELT_PESTLE_AND_MORTAR);
                        cap.setbountycharges(BOUNTY_CHARGES);
                        cap.sethastoolbeltneedle(HAS_TOOLBELT_NEEDLE);
                    }));
        }
    }
}


