package com.ladestitute.runicages.network;

import com.ladestitute.runicages.capability.mining.RunicAgesMiningCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientMiningSkillPacket implements INormalMessage {
    int MINING_LEVEL;
    int MINING_XP;
    int NEXT_MINING_XP;
    int STARTER_KIT;

    public ClientMiningSkillPacket(int level, int mining_xp, int next_mining_xp, int starter_kit) {
        this.MINING_LEVEL = level;
        this.MINING_XP = mining_xp;
        this.NEXT_MINING_XP = next_mining_xp;
        this.STARTER_KIT = starter_kit;
    }

    public ClientMiningSkillPacket(FriendlyByteBuf buf) {
        MINING_LEVEL = buf.readInt();
        MINING_XP = buf.readInt();
                NEXT_MINING_XP = buf.readInt();
                STARTER_KIT = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(MINING_LEVEL);
        buf.writeInt(MINING_XP);
        buf.writeInt(NEXT_MINING_XP);
        buf.writeInt(STARTER_KIT);
    }

    @Override
    public void process(Supplier<NetworkEvent.Context> context) {
        //This method is for when information is received by the intended end (i.e, client in this case)
        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
        //Remember that client/server side rules apply here
        //Access client stuff only in client, otherwise you will crash MC
        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(RunicAgesMiningCapability.Provider.MINING_LEVEL).
                    ifPresent(cap -> {
                        //do stuff with the info, such as mainly syncing info for the client-side gui
                        cap.setMiningLevel(MINING_LEVEL);
                        cap.setMiningXP(MINING_XP);
                        cap.setNextMiningXP(NEXT_MINING_XP);
                        cap.setstarterkitobtained(STARTER_KIT);
                    }));
        }
    }
}
