package com.ladestitute.runicages.network;

import com.ladestitute.runicages.capability.agility.RunicAgesAgilityCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientAgilitySkillPacket implements INormalMessage {
    int AGILITY_LEVEL;
    int AGILITY_XP;
    int NEXT_AGILITY_XP;
    int AGILITY_BOOST;
    int AGILITY_BOOST_TIMER;
    int INVISIBLE_AGILITY_BOOST;

    public ClientAgilitySkillPacket(int level, int xp, int next_xp, int thieving_boost, int thieving_boost_timer,
                                     int invisible_thieving_boost) {
        this.AGILITY_LEVEL = level;
        this.AGILITY_XP = xp;
        this.NEXT_AGILITY_XP = next_xp;
        this.AGILITY_BOOST = thieving_boost;
        this.AGILITY_BOOST_TIMER = thieving_boost_timer;
        this.INVISIBLE_AGILITY_BOOST = invisible_thieving_boost;
    }

    public ClientAgilitySkillPacket(FriendlyByteBuf buf) {
        AGILITY_LEVEL = buf.readInt();
        AGILITY_XP = buf.readInt();
        NEXT_AGILITY_XP = buf.readInt();
        AGILITY_BOOST = buf.readInt();
        AGILITY_BOOST_TIMER = buf.readInt();
        INVISIBLE_AGILITY_BOOST = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(AGILITY_LEVEL);
        buf.writeInt(AGILITY_XP);
        buf.writeInt(NEXT_AGILITY_XP);
        buf.writeInt(AGILITY_BOOST);
        buf.writeInt(AGILITY_BOOST_TIMER);
        buf.writeInt(INVISIBLE_AGILITY_BOOST);
    }

    @Override
    public void process(Supplier<NetworkEvent.Context> context) {
        //This method is for when information is received by the intended end (i.e, client in this case)
        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
        //Remember that client/server side rules apply here
        //Access client stuff only in client, otherwise you will crash MC
        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(RunicAgesAgilityCapability.Provider.AGILITY_LEVEL).
                    ifPresent(cap -> {
                        //do stuff with the info, such as mainly syncing info for the client-side gui
                        cap.setAgilityLevel(AGILITY_LEVEL);
                        cap.setAgilityXP(AGILITY_XP);
                        cap.setNextAgilityXP(NEXT_AGILITY_XP);
                        cap.setAgilityBoost(AGILITY_BOOST);
                        cap.setagilityboostdraintimer(AGILITY_BOOST_TIMER);
                        cap.setAgilityBoost(INVISIBLE_AGILITY_BOOST);
                    }));
        }
    }
}

