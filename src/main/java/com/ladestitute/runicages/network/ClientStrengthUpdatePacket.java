package com.ladestitute.runicages.network;

import com.ladestitute.runicages.capability.strength.RunicAgesStrengthCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientStrengthUpdatePacket implements INormalMessage {
    int STRENGTH_LEVEL;
    int STRENGTH_XP;
    int NEXT_STRENGTH_XP;

    public ClientStrengthUpdatePacket(int level, int xp, int next_xp) {
        this.STRENGTH_LEVEL = level;
        this.STRENGTH_XP = xp;
        this.NEXT_STRENGTH_XP = next_xp;
    }

    public ClientStrengthUpdatePacket(FriendlyByteBuf buf) {
        STRENGTH_LEVEL = buf.readInt();
        STRENGTH_XP = buf.readInt();
        NEXT_STRENGTH_XP = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(STRENGTH_LEVEL);
        buf.writeInt(STRENGTH_XP);
        buf.writeInt(NEXT_STRENGTH_XP);
    }

    @Override
    public void process(Supplier<NetworkEvent.Context> context) {
        //This method is for when information is received by the intended end (i.e, client in this case)
        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
        //Remember that client/server side rules apply here
        //Access client stuff only in client, otherwise you will crash MC
        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(RunicAgesStrengthCapability.Provider.STRENGTH_LEVEL).
                    ifPresent(cap -> {
                        //do stuff with the info, such as mainly syncing info for the client-side gui
                        cap.setStrengthLevel(STRENGTH_LEVEL);
                        cap.setStrengthXP(STRENGTH_XP);
                        cap.setNextStrengthXP(NEXT_STRENGTH_XP);
                    }));
        }
    }
}



