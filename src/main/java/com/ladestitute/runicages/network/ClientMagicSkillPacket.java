package com.ladestitute.runicages.network;

import com.ladestitute.runicages.capability.magic.RunicAgesMagicCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientMagicSkillPacket implements INormalMessage {
    int MAGIC_LEVEL;
    int MAGIC_XP;
    int NEXT_MAGIC_XP;
    int MAGIC_BOOK;
    int MAINHAND_SPELL;
    int OFFHAND_SPELL;
    int GUI_BUTTON_PRESSED;
    int MAGIC_BOOST;
    int MAGIC_EQUIP_BOOST;
    int MAGIC_BOOST_DRAIN_TIMER;
    int INVISIBLE_MAGIC_BOOST;

    public ClientMagicSkillPacket(int level, int xp, int next_xp, int magic_book,
                                  int mainhand_spell, int offhand_spell, int gui_button_pressed,
                                  int magic_boost, int magic_equip_boost, int magic_timer, int invisible_boost) {
        this.MAGIC_LEVEL = level;
        this.MAGIC_XP = xp;
        this.NEXT_MAGIC_XP = next_xp;
        this.MAGIC_BOOK = magic_book;
        this.MAINHAND_SPELL = mainhand_spell;
        this.OFFHAND_SPELL = offhand_spell;
        this.GUI_BUTTON_PRESSED = gui_button_pressed;
        this.MAGIC_BOOST = magic_boost;
        this.MAGIC_EQUIP_BOOST = magic_equip_boost;
        this.MAGIC_BOOST_DRAIN_TIMER = magic_timer;
        this.INVISIBLE_MAGIC_BOOST = invisible_boost;
    }

    public ClientMagicSkillPacket(FriendlyByteBuf buf) {
        MAGIC_LEVEL = buf.readInt();
        MAGIC_XP = buf.readInt();
        NEXT_MAGIC_XP = buf.readInt();
        MAGIC_BOOK = buf.readInt();
        MAINHAND_SPELL = buf.readInt();
        OFFHAND_SPELL = buf.readInt();
        GUI_BUTTON_PRESSED = buf.readInt();
        this.MAGIC_BOOST = buf.readInt();
        this.MAGIC_EQUIP_BOOST = buf.readInt();
        this.MAGIC_BOOST_DRAIN_TIMER = buf.readInt();
        this.INVISIBLE_MAGIC_BOOST = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(MAGIC_LEVEL);
        buf.writeInt(MAGIC_XP);
        buf.writeInt(NEXT_MAGIC_XP);
        buf.writeInt(MAGIC_BOOK);
        buf.writeInt(MAINHAND_SPELL);
        buf.writeInt(OFFHAND_SPELL);
        buf.writeInt(GUI_BUTTON_PRESSED);
        buf.writeInt(MAGIC_BOOST);
        buf.writeInt(MAGIC_EQUIP_BOOST);
        buf.writeInt(MAGIC_BOOST_DRAIN_TIMER);
        buf.writeInt(INVISIBLE_MAGIC_BOOST);
    }

    @Override
    public void process(Supplier<NetworkEvent.Context> context) {
        //This method is for when information is received by the intended end (i.e, client in this case)
        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
        //Remember that client/server side rules apply here
        //Access client stuff only in client, otherwise you will crash MC
        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(RunicAgesMagicCapability.Provider.MAGIC_LEVEL).
                    ifPresent(cap -> {
                        //do stuff with the info, such as mainly syncing info for the client-side gui
                        cap.setMagicLevel(MAGIC_LEVEL);
                        cap.setMagicXP(MAGIC_XP);
                        cap.setNextMagicXP(NEXT_MAGIC_XP);
                        cap.setmagicbookopen(MAGIC_BOOK);
                        cap.setMainhandSpell(MAINHAND_SPELL);
                        cap.setOffhandSpell(OFFHAND_SPELL);
                        cap.setGuiButtonPressed(GUI_BUTTON_PRESSED);
                        cap.setMagicBoost(MAGIC_BOOST);
                        cap.setMagicEquipBoost(MAGIC_EQUIP_BOOST);
                        cap.setmagicboostdraintimer(MAGIC_BOOST_DRAIN_TIMER);
                        cap.setInvisibleMagicBoost(INVISIBLE_MAGIC_BOOST);
                    }));
        }
    }
}

