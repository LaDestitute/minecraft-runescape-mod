package com.ladestitute.runicages.network;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.network.NetworkRegistry;
import net.minecraftforge.network.simple.SimpleChannel;

import java.util.function.Function;

public final class SimpleNetworkHandler {
    //This should be 1.0
    private static final String PROTOCOL_VERSION = "1";
    public static final SimpleChannel INSTANCE = NetworkRegistry.newSimpleChannel(
            new ResourceLocation("runecraft", "main"),
            () -> PROTOCOL_VERSION,
            PROTOCOL_VERSION::equals,
            PROTOCOL_VERSION::equals
    );

    //An init where we register our packets
    public static void init() {
        int index = 0;
        //This type of packet is server to client
        registerMessage(index++, ClientExtraDataPacket.class,
                ClientExtraDataPacket::new);
        registerMessage(index++, ClientMiningSkillPacket.class,
                ClientMiningSkillPacket::new);
        registerMessage(index++, ClientSmithingSkillPacket.class,
                ClientSmithingSkillPacket::new);
        registerMessage(index++, ClientWoodcuttingSkillPacket.class,
                ClientWoodcuttingSkillPacket::new);
        registerMessage(index++, ClientRunecraftingSkillPacket.class,
                ClientRunecraftingSkillPacket::new);
        registerMessage(index++, ClientMagicSkillPacket.class,
                ClientMagicSkillPacket::new);
        registerMessage(index++, ClientCraftingSkillPacket.class,
                ClientCraftingSkillPacket::new);
        registerMessage(index++, ClientAttackSkillPacket.class,
                ClientAttackSkillPacket::new);
        registerMessage(index++, ClientStrengthUpdatePacket.class,
                ClientStrengthUpdatePacket::new);
        registerMessage(index++, ClientDefenseSkillPacket.class,
                ClientDefenseSkillPacket::new);
        registerMessage(index++, ClientRangedSkillPacket.class,
                ClientRangedSkillPacket::new);
        registerMessage(index++, ClientHerbloreSkillPacket.class,
                ClientHerbloreSkillPacket::new);
        registerMessage(index++, ClientFarmingSkillPacket.class,
                ClientFarmingSkillPacket::new);
        registerMessage(index++, ClientThievingSkillPacket.class,
                ClientThievingSkillPacket::new);
        registerMessage(index++, ClientAgilitySkillPacket.class,
                ClientAgilitySkillPacket::new);
    }

    private static <T extends INormalMessage> void registerMessage(int index, Class<T> messageType, Function<FriendlyByteBuf, T> decoder) {
        //Encoding is saving information to the byte buffer, decoding is the opposite of that (reading info)
        INSTANCE.registerMessage(index, messageType, INormalMessage::toBytes, decoder, (message, context) -> {
            message.process(context);
            context.get().setPacketHandled(true);
        });
    }
}

