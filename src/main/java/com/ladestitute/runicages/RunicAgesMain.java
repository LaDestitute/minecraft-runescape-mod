package com.ladestitute.runicages;

import com.google.common.base.Suppliers;
import com.ladestitute.runicages.client.ClientEventBusSubscriber;
import com.ladestitute.runicages.client.ShieldItemProperties;
import com.ladestitute.runicages.client.screen.*;
import com.ladestitute.runicages.event.*;
import com.ladestitute.runicages.event.FarmingEventHandler;
import com.ladestitute.runicages.event.combatxp.MagicXPEventHandler;
import com.ladestitute.runicages.event.combatxp.MeleeXPEventHandler;
import com.ladestitute.runicages.event.combatxp.RangedXPEventHandler;
import com.ladestitute.runicages.network.SimpleNetworkHandler;
import com.ladestitute.runicages.registry.*;
import com.ladestitute.runicages.util.RunicAgesConfig;
import com.ladestitute.runicages.util.datagen.glm.CowLootModifier;
import com.ladestitute.runicages.util.datagen.glm.RemoveSeedsModifier;
import com.ladestitute.runicages.util.datagen.glm.VillagerLootModifier;
import com.ladestitute.runicages.world.biomemod.BiomeModifiers;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import net.minecraft.client.gui.screens.MenuScreens;
import net.minecraft.core.Direction;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.loot.IGlobalLootModifier;
import net.minecraftforge.common.loot.LootModifier;
import net.minecraftforge.data.event.GatherDataEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.NotNull;

import java.util.function.Supplier;

@Mod(RunicAgesMain.MODID)
public class RunicAgesMain
{
    public static final String MODID = "runicages";

    public RunicAgesMain() {
        IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
        ItemInit.ITEMS.register(bus);
        BlockInit.register(bus);
        SpecialBlockInit.SPECIAL_BLOCKS.register(bus);
      //  OverrideVanillaInit.BLOCKS.register(bus);
        SoundInit.SOUNDS.register(bus);
        BlockEntityInit.register(bus);
        MenuTypesInit.register(bus);
        RecipeTypeInit.register(bus);
        EffectInit.register(bus);
        BiomeModifiers.register(bus);
        RunicAgesCreativeTabs.TABS.register(bus);
        //PlacedFeatureInit.register(bus);
        EntityTypeInit.register(bus);
        VillagerInit.register(bus);
        bus.addListener(this::commonSetup);
        bus.addListener(this::clientSetup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(ClientEventBusSubscriber::initRenders);
        MinecraftForge.EVENT_BUS.register(new MainEventHandler());
        MinecraftForge.EVENT_BUS.register(new RunicAgesSkillsHandler());
        MinecraftForge.EVENT_BUS.register(new MiningEventHandler());
        MinecraftForge.EVENT_BUS.register(new SmithingEventHandler());
        MinecraftForge.EVENT_BUS.register(new WoodcuttingEventHandler());
        MinecraftForge.EVENT_BUS.register(new MobDropsHandler());
        MinecraftForge.EVENT_BUS.register(new MagicEventHandler());
        MinecraftForge.EVENT_BUS.register(new MobEffectsHandler());
        MinecraftForge.EVENT_BUS.register(new ThievingEventHandler());
        MinecraftForge.EVENT_BUS.register(new CraftingEventHandler());
        MinecraftForge.EVENT_BUS.register(new MeleeSkillsEventHandler());
        MinecraftForge.EVENT_BUS.register(new RangedSkillEventHandler());
        MinecraftForge.EVENT_BUS.register(new MeleeXPEventHandler());
        MinecraftForge.EVENT_BUS.register(new MagicXPEventHandler());
        MinecraftForge.EVENT_BUS.register(new RangedXPEventHandler());
        MinecraftForge.EVENT_BUS.register(new HerbloreEventHandler());
        MinecraftForge.EVENT_BUS.register(new FarmingEventHandler());
        MinecraftForge.EVENT_BUS.register(new FoodEffectsHandler());
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON,
                RunicAgesConfig.SPEC, "runicages-config.toml");
      //  FarmingCropModifier.GLM.register(bus);
        GLM.register(bus);
    }

    private void clientSetup(final FMLClientSetupEvent event) {
        MenuScreens.register(MenuTypesInit.SMITHING_FURNACE_MENU.get(), SmithingFurnaceScreen::new);
        MenuScreens.register(MenuTypesInit.ALLOY_FURNACE_MENU.get(), AlloyFurnaceScreen::new);
        MenuScreens.register(MenuTypesInit.IRON_ALLOY_FURNACE_MENU.get(), IronAlloyFurnaceScreen::new);
        MenuScreens.register(MenuTypesInit.MAGIC_BOOK_MENU.get(), MagicBookScreen::new);
        MenuScreens.register(MenuTypesInit.STEEL_ALLOY_FURNACE_MENU.get(), SteelAlloyFurnaceScreen::new);
        MenuScreens.register(MenuTypesInit.SILVER_SMITHING_FURNACE_MENU.get(), SilverSmithingFurnaceScreen::new);
        MenuScreens.register(MenuTypesInit.MITHRIL_ALLOY_FURNACE_MENU.get(), MithrilAlloyFurnaceScreen::new);
        MenuScreens.register(MenuTypesInit.GOLD_SMITHING_FURNACE_MENU.get(), GoldSmithingFurnaceScreen::new);
        MenuScreens.register(MenuTypesInit.ADAMANTITE_ALLOY_FURNACE_MENU.get(), AdamantiteAlloyFurnaceScreen::new);
        MenuScreens.register(MenuTypesInit.MOLD_PRESS_MENU.get(), MoldPressScreen::new);
        MenuScreens.register(MenuTypesInit.SPINNING_WHEEL_MENU.get(), SpinningWheelScreen::new);
        MenuScreens.register(MenuTypesInit.SEWING_KIT_MENU.get(), SewingKitScreen::new);
        MenuScreens.register(MenuTypesInit.BANK_MENU.get(), BankScreen::new);
    }

    public void commonSetup(final FMLCommonSetupEvent event) {
        event.enqueueWork(SimpleNetworkHandler::init);
        event.enqueueWork(() -> {
            ShieldItemProperties.addCustomItemProperties();
        //    Sheets.addWoodType(RuneCraftWoodTypes.NORMAL_TREE);
        });
    }

    //In 1.19, we move GLM to the main class
    private static final DeferredRegister<Codec<? extends IGlobalLootModifier>> GLM = DeferredRegister.create(ForgeRegistries.Keys.GLOBAL_LOOT_MODIFIER_SERIALIZERS, MODID);

    public static VoxelShape calculateShapes(Direction to, VoxelShape shape) {
        final VoxelShape[] buffer = { shape, Shapes.empty() };

        final int times = (to.get2DDataValue() - Direction.NORTH.get2DDataValue() + 4) % 4;
        for (int i = 0; i < times; i++) {
            buffer[0].forAllBoxes((minX, minY, minZ, maxX, maxY, maxZ) -> buffer[1] = Shapes.or(buffer[1],
                    Shapes.create(1 - maxZ, minY, minX, 1 - minZ, maxY, maxX)));
            buffer[0] = buffer[1];
            buffer[1] = Shapes.empty();
        }

        return buffer[0];
    }

    //Example loot modifiers, the wheat seeds one for wheat crops operates on chance
    //The remove seeds modifier is its own class as an example
    private static final RegistryObject<Codec<WheatSeedsConverterModifier>> WHEATSEEDS = GLM.register("wheat_harvest", WheatSeedsConverterModifier.CODEC);
    public static final RegistryObject<Codec<RemoveSeedsModifier>> REMOVE_SEEDS = GLM.register("remove_seeds", () -> RemoveSeedsModifier.CODEC);
    public static final RegistryObject<Codec<CowLootModifier>> COW_LOOT = GLM.register("cow_loot", () -> CowLootModifier.CODEC);
    public static final RegistryObject<Codec<VillagerLootModifier>> VILLAGER_LOOT = GLM.register("villager_loot", () -> VillagerLootModifier.CODEC);

    @Mod.EventBusSubscriber(modid = MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
    public static class EventHandlers {
        @SubscribeEvent
        public static void runData(GatherDataEvent event)
        {
             //   event.getGenerator().addProvider(event.includeServer(), new DataProvider(event.getGenerator(), MODID));
        }
    }

    private static class WheatSeedsConverterModifier extends LootModifier {
        public static final Supplier<Codec<WheatSeedsConverterModifier>> CODEC = Suppliers.memoize(() -> RecordCodecBuilder.create(inst -> codecStart(inst).and(
                inst.group(
                        Codec.INT.fieldOf("numSeeds").forGetter(m -> m.numSeedsToConvert),
                        ForgeRegistries.ITEMS.getCodec().fieldOf("seedItem").forGetter(m -> m.itemToCheck),
                        ForgeRegistries.ITEMS.getCodec().fieldOf("replacement").forGetter(m -> m.itemReward)
                )).apply(inst, WheatSeedsConverterModifier::new)
        ));

        private final int numSeedsToConvert;
        private final Item itemToCheck;
        private final Item itemReward;
        public WheatSeedsConverterModifier(LootItemCondition[] conditionsIn, int numSeeds, Item itemCheck, Item reward) {
            super(conditionsIn);
            numSeedsToConvert = numSeeds;
            itemToCheck = itemCheck;
            itemReward = reward;
        }

        @NotNull
        @Override
        public ObjectArrayList<ItemStack> doApply(ObjectArrayList<ItemStack> generatedLoot, LootContext context) {
            //
            // Additional conditions can be checked, though as much as possible should be parameterized via JSON data.
            // It is better to write a new ILootCondition implementation than to do things here.
            //
            int numSeeds = 0;
            for(ItemStack stack : generatedLoot) {
                if(stack.getItem() == itemToCheck)
                    numSeeds+=stack.getCount();
            }
            if(numSeeds >= numSeedsToConvert) {
                generatedLoot.removeIf(x -> x.getItem() == itemToCheck);
                generatedLoot.add(new ItemStack(itemReward, (numSeeds/numSeedsToConvert)));
                numSeeds = numSeeds%numSeedsToConvert;
                if(numSeeds > 0)
                    generatedLoot.add(new ItemStack(itemToCheck, numSeeds));
            }
            return generatedLoot;
        }

        @Override
        public Codec<? extends IGlobalLootModifier> codec() {
            return CODEC.get();
        }
    }

    //End of GLM stuff

}
