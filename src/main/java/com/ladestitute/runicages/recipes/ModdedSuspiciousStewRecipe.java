//package com.ladestitute.runicages.recipes;
//
//import com.google.gson.JsonArray;
//import com.google.gson.JsonObject;
//import com.ladestitute.runicages.RunicAgesMain;
//import com.ladestitute.runicages.registry.ItemInit;
//import net.minecraft.core.NonNullList;
//import net.minecraft.core.RegistryAccess;
//import net.minecraft.network.FriendlyByteBuf;
//import net.minecraft.resources.ResourceLocation;
//import net.minecraft.tags.ItemTags;
//import net.minecraft.util.GsonHelper;
//import net.minecraft.world.inventory.CraftingContainer;
//import net.minecraft.world.item.ItemStack;
//import net.minecraft.world.item.Items;
//import net.minecraft.world.item.SuspiciousStewItem;
//import net.minecraft.world.item.crafting.*;
//import net.minecraft.world.level.Level;
//import net.minecraft.world.level.block.Blocks;
//import net.minecraft.world.level.block.SuspiciousEffectHolder;
//
//public class ModdedSuspiciousStewRecipe extends CustomRecipe {
//    public ModdedSuspiciousStewRecipe(ResourceLocation p_248870_, CraftingBookCategory p_250392_) {
//        super(p_248870_, p_250392_);
//    }
//
//    public boolean matches(CraftingContainer p_44499_, Level p_44500_) {
//        boolean flag = false;
//        boolean flag1 = false;
//        boolean flag2 = false;
//        boolean flag3 = false;
//
//        for(int i = 0; i < p_44499_.getContainerSize(); ++i) {
//            ItemStack itemstack = p_44499_.getItem(i);
//            if (!itemstack.isEmpty()) {
//                if (itemstack.is(Blocks.BROWN_MUSHROOM.asItem()) && !flag2) {
//                    flag2 = true;
//                } else if (itemstack.is(Blocks.RED_MUSHROOM.asItem()) && !flag1) {
//                    flag1 = true;
//                } else if (itemstack.is(ItemTags.SMALL_FLOWERS) && !flag) {
//                    flag = true;
//                } else {
//                    if (!itemstack.is(ItemInit.CLAY_BOWL.get()) || flag3) {
//                        return false;
//                    }
//
//                    flag3 = true;
//                }
//            }
//        }
//
//        return flag && flag2 && flag1 && flag3;
//    }
//
//    public ItemStack assemble(CraftingContainer p_44497_, RegistryAccess p_266871_) {
//        ItemStack itemstack = new ItemStack(Items.SUSPICIOUS_STEW, 1);
//
//        for(int i = 0; i < p_44497_.getContainerSize(); ++i) {
//            ItemStack itemstack1 = p_44497_.getItem(i);
//            if (!itemstack1.isEmpty()) {
//                SuspiciousEffectHolder suspiciouseffectholder = SuspiciousEffectHolder.tryGet(itemstack1.getItem());
//                if (suspiciouseffectholder != null) {
//                    SuspiciousStewItem.saveMobEffect(itemstack, suspiciouseffectholder.getSuspiciousEffect(), suspiciouseffectholder.getEffectDuration());
//                    break;
//                }
//            }
//        }
//
//        return itemstack;
//    }
//
//    public boolean canCraftInDimensions(int p_44489_, int p_44490_) {
//        return p_44489_ >= 2 && p_44490_ >= 2;
//    }
//
//    public static class Type implements RecipeType<ModdedSuspiciousStewRecipe> {
//        private Type() { }
//        public static final ModdedSuspiciousStewRecipe.Type INSTANCE = new ModdedSuspiciousStewRecipe.Type();
//        public static final String ID = "modded_suspicious_stew";
//    }
//
//    public static class Serializer implements RecipeSerializer<ModdedSuspiciousStewRecipe> {
//        public static final ModdedSuspiciousStewRecipe.Serializer INSTANCE = new ModdedSuspiciousStewRecipe.Serializer();
//        public static final ResourceLocation ID =
//                new ResourceLocation(RunicAgesMain.MODID,"modded_suspicious_stew_recipe");
//
//        @Override
//        public ModdedSuspiciousStewRecipe fromJson(ResourceLocation id, JsonObject json) {
//            ItemStack output = ShapedRecipe.itemStackFromJson(GsonHelper.getAsJsonObject(json, "output"));
//
//            JsonArray ingredients = GsonHelper.getAsJsonArray(json, "ingredients");
//            NonNullList<Ingredient> inputs = NonNullList.withSize(1, Ingredient.EMPTY);
//
//            for (int i = 0; i < inputs.size(); i++) {
//                inputs.set(i, Ingredient.fromJson(ingredients.get(i)));
//            }
//
//            return new ModdedSuspiciousStewRecipe(id, output, inputs);
//        }
//
//        @Override
//        public ModdedSuspiciousStewRecipe fromNetwork(ResourceLocation id, FriendlyByteBuf buf) {
//            NonNullList<Ingredient> inputs = NonNullList.withSize(buf.readInt(), Ingredient.EMPTY);
//
//            for (int i = 0; i < inputs.size(); i++) {
//                inputs.set(i, Ingredient.fromNetwork(buf));
//            }
//
//            ItemStack output = buf.readItem();
//            return new ModdedSuspiciousStewRecipe();
//        }
//
//        @Override
//        public void toNetwork(FriendlyByteBuf buf, ModdedSuspiciousStewRecipe recipe) {
//            buf.writeInt(recipe.getIngredients().size());
//            for (Ingredient ing : recipe.getIngredients()) {
//                ing.toNetwork(buf);
//            }
//            buf.writeItemStack(recipe, false);
//        }
//
//        @SuppressWarnings("unchecked") // Need this wrapper, because generics
//        private static <G> Class<G> castClass(Class<?> cls) {
//            return (Class<G>)cls;
//        }
//    }
//
//    @Override
//    public RecipeSerializer<?> getSerializer() {
//        return RecipeSerializer.SUSPICIOUS_STEW;
//    }
//
//    @Override
//    public RecipeType<?> getType() {
//        return ModdedSuspiciousStewRecipe.Type.INSTANCE;
//    }
//
//
//
//}
