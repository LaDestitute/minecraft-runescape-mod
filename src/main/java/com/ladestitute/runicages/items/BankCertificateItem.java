package com.ladestitute.runicages.items;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class BankCertificateItem extends Item {

    public BankCertificateItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("A notarized document used as an ID for a single 6x11 bank storage vault. Rename to keeping track of it easier. Don't lose it!"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}

