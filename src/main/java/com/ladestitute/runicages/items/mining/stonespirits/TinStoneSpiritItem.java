package com.ladestitute.runicages.items.mining.stonespirits;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class TinStoneSpiritItem extends Item {

    public TinStoneSpiritItem(Item.Properties properties) {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("A strange wisp-like creature. When mining tin ore, this is consumed and you will receive an additional tin ore."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
