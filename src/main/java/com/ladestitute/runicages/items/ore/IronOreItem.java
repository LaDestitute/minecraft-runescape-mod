package com.ladestitute.runicages.items.ore;

import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class IronOreItem extends Item {

    public IronOreItem(Item.Properties properties) {
        super(properties.stacksTo(16));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(RunicAgesConfig.modernrs.get()) {
            tooltip.add(Component.literal("Used in Smithing (10)."));
        }
        else tooltip.add(Component.literal("This needs refining."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}