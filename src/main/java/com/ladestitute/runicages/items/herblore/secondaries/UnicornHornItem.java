package com.ladestitute.runicages.items.herblore.secondaries;

import com.ladestitute.runicages.capability.crafting.RunicAgesCraftingCapability;
import com.ladestitute.runicages.capability.herblore.RunicAgesHerbloreCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;
import java.util.Random;

public class UnicornHornItem extends Item {

    public UnicornHornItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(RunicAgesConfig.modernrs.get())
        {
            tooltip.add(Component.literal("This horn has restorative properties. Used in Herblore (13)."));
        }
        else tooltip.add(Component.literal("This horn has restorative properties."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level p_41432_, Player p_41433_, InteractionHand p_41434_) {
        ItemStack pestle = ItemInit.PESTLE_AND_MORTAR.get().getDefaultInstance();
        p_41433_.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            p_41433_.getCapability(RunicAgesHerbloreCapability.Provider.HERBLORE_LEVEL).ifPresent(c ->
            {
                if (c.getHerbloreLevel() >= 1 && p_41433_.getInventory().contains(pestle) ||
                        c.getHerbloreLevel() >= 1 &&  ed.getHasToolbeltPestleAndMortar() == 1) {
                    ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.UNICORN_HORN_DUST.get().getDefaultInstance());
                    p_41433_.getMainHandItem().shrink(1);
                    if(RunicAgesConfig.modernrs.get()) {
                        c.addHerbloreXP(p_41433_, 1);
                    }
                }
            });
        });
        return super.use(p_41432_, p_41433_, p_41434_);
    }

}

