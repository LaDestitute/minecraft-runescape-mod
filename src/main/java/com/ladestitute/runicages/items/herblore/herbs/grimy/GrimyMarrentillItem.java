package com.ladestitute.runicages.items.herblore.herbs.grimy;

import com.ladestitute.runicages.capability.herblore.RunicAgesHerbloreCapability;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;

public class GrimyMarrentillItem extends Item {

    public GrimyMarrentillItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(RunicAgesConfig.modernrs.get())
            tooltip.add(Component.literal("I need to clean this herb before I can use it."));
        else tooltip.add(Component.literal("It needs cleaning."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }


    @Override
    public InteractionResultHolder<ItemStack> use(Level p_41432_, Player p_41433_, InteractionHand p_41434_) {
        p_41433_.getCapability(RunicAgesHerbloreCapability.Provider.HERBLORE_LEVEL).ifPresent(h ->
        {
            if(RunicAgesConfig.modernrs.get()) {
                if (h.getHerbloreLevel() >= 9) {
                    ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.CLEAN_MARRENTILL.get().getDefaultInstance());
                    p_41433_.getMainHandItem().shrink(1);
                    h.addHerbloreXP(p_41433_, 5);
                }
                else
                if(!p_41433_.level().isClientSide()) {
                    p_41433_.sendSystemMessage(Component.literal("Level 9 Herblore is required to clean this herb."));
                }
            }
            if(!RunicAgesConfig.modernrs.get()) {
                if (h.getHerbloreLevel() >= 5) {
                    ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.CLEAN_MARRENTILL.get().getDefaultInstance());
                    p_41433_.getMainHandItem().shrink(1);
                    h.addHerbloreXP(p_41433_, (int) Math.round(3.8));
                }
                else
                if(!p_41433_.level().isClientSide()) {
                    p_41433_.sendSystemMessage(Component.literal("Level 5 Herblore is required to clean this herb."));
                }
            }
        });
        return super.use(p_41432_, p_41433_, p_41434_);
    }
}