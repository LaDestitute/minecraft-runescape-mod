package com.ladestitute.runicages.items.herblore.secondaries;

import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class EyeOfNewtItem extends Item {

    public EyeOfNewtItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(RunicAgesConfig.modernrs.get())
        tooltip.add(Component.literal("It seems to be looking at me. Used in Herblore (3)."));
        else tooltip.add(Component.literal("It seems to be looking at me."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
