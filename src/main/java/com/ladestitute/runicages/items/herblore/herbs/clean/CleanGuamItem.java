package com.ladestitute.runicages.items.herblore.herbs.clean;

import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class CleanGuamItem extends Item {

    public CleanGuamItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(RunicAgesConfig.modernrs.get())
            tooltip.add(Component.literal("A fresh herb."));
        else tooltip.add(Component.literal("A bitter green herb."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
