package com.ladestitute.runicages.items.herblore;

import com.ladestitute.runicages.registry.ItemInit;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;

public class VialOfWaterPackItem extends Item {

    public VialOfWaterPackItem(Item.Properties properties) {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("A pack of 25 noted vials of water."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level p_41432_, Player p_41433_, InteractionHand p_41434_) {
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.NOTED_VIAL_OF_WATER.get().getDefaultInstance());
        p_41433_.getMainHandItem().shrink(1);
        return super.use(p_41432_, p_41433_, p_41434_);
    }
}
