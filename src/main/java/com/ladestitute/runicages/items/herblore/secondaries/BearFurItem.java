package com.ladestitute.runicages.items.herblore.secondaries;

import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class BearFurItem extends Item {

    public BearFurItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(RunicAgesConfig.modernrs.get())
            tooltip.add(Component.literal("Warm fur from a bear. Used in Herblore (9)."));
        else tooltip.add(Component.literal("This would make warm clothing."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}

