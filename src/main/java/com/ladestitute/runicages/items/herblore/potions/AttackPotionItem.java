package com.ladestitute.runicages.items.herblore.potions;

import com.ladestitute.runicages.capability.attack.RunicAgesAttackCapability;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.jetbrains.annotations.NotNull;
import top.theillusivec4.curios.api.CuriosApi;

import java.util.List;
import java.util.Random;

public class AttackPotionItem  extends Item {

    // credit goes to DePhoegon()

    // a double stored in nbt to decide what sprite the item will change to
    double customData;
    // a string referenced with an override predicate with the json
    // (specifically "custom_model_data")
    // the json item-model file with the override/predicate data is in:
    // resources/assets/runicages/models/item/attack_potion.json
    static final String customModelData = "CustomModelData";

    public AttackPotionItem(Properties pProperties) {
        super(pProperties.stacksTo(1));
    }

    // initialize the compoundtag data for the item
    public CompoundTag initializeNBT(String key, double string) {
        CompoundTag nbt = new CompoundTag();
        nbt.putDouble(key, string);
        return nbt;
    }

    // the helper-method referenced to update and/or set the NBT-data
    private void setNBT(@NotNull ItemStack itemStack) {
        // Will Refresh & remove Tags not in use
        CompoundTag tempTag = itemStack.getTag();
        CompoundTag display = null;
        if (tempTag != null) {
            if (tempTag.contains("display")) { display = tempTag.getCompound("display"); }
            if (tempTag.contains(customModelData)) { customData = tempTag.getDouble(customModelData); }
        }
        itemStack.setTag(initializeNBT(customModelData, customData));
        CompoundTag tag = itemStack.getTag();
        assert tag != null;
        tag.putDouble("CustomModelData", getCustomModelData(tag));
        if (display != null) { tag.put("display", display); }
        if (customData > 3) {
            itemStack.shrink(1);
        }
    }

    // get the custommodeldata (used in setNBT method)
    private double getCustomModelData(@NotNull CompoundTag value) {
        double out = 0;
        if (value.contains(customModelData)) { out = value.getDouble(customModelData)+1; }
        else { out = 1; }
        return out;
    }

    // use method, checks cooldown and if the item is an instance of the required item
    // updates and sets nbt after use, to make the item visually update
    public @NotNull InteractionResultHolder<ItemStack> use(@NotNull Level pLevel, @NotNull Player pPlayer, @NotNull InteractionHand pUsedHand) {
        ItemStack itemstack = pPlayer.getItemInHand(pUsedHand).getItem() instanceof AttackPotionItem ? pPlayer.getItemInHand(pUsedHand) : null;

        //Functionality code starts here
        if (!pPlayer.getCooldowns().isOnCooldown(ItemInit.ATTACK_POTION.get()) && itemstack != null) {
            pPlayer.getCapability(RunicAgesAttackCapability.Provider.ATTACK_LEVEL).ifPresent(h ->
            {
                if(RunicAgesConfig.modernrs.get()) {
                    if(h.getAttackLevel() >= 13) {
                        h.setAttackBoost((int) ((Math.round(h.getAttackLevel()*0.08))+1));
                    }
                    else h.setAttackBoost(1);
                }
                if(!RunicAgesConfig.modernrs.get()) {
                    h.setAttackBoost((int) ((Math.round(h.getAttackLevel()*0.1))+3));
                }
            });
            //Functionality code ends here
            setNBT(itemstack);
            pPlayer.getCooldowns().addCooldown(ItemInit.ATTACK_POTION.get(), 36);
        }
        return InteractionResultHolder.pass(pPlayer.getItemInHand(pUsedHand));
    }

    // Append hover text based on a config and the nbt data
    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(RunicAgesConfig.modernrs.get()) {
            if(stack.hasTag() && stack.getTag().getDouble("CustomModelData") == 1.0d)
            {
                tooltip.add(Component.literal("4 doses of attack potion."));
            }
            if(stack.hasTag() && stack.getTag().getDouble("CustomModelData") == 2.0d)
            {
                tooltip.add(Component.literal("3 doses of attack potion."));
            }
            if(stack.hasTag() && stack.getTag().getDouble("CustomModelData") == 3.0d)
            {
                tooltip.add(Component.literal("2 doses of attack potion."));
            }
            if(stack.hasTag() && stack.getTag().getDouble("CustomModelData") == 4.0d)
            {
                tooltip.add(Component.literal("1 dose of attack potion."));
            }
        }
        if(!RunicAgesConfig.modernrs.get()) {
            if(stack.hasTag() && stack.getTag().getDouble("CustomModelData") == 1.0d)
            {
                tooltip.add(Component.literal("4 doses of Attack potion."));
            }
            if(stack.hasTag() && stack.getTag().getDouble("CustomModelData") == 2.0d)
            {
                tooltip.add(Component.literal("3 doses of Attack potion."));
            }
            if(stack.hasTag() && stack.getTag().getDouble("CustomModelData") == 3.0d)
            {
                tooltip.add(Component.literal("2 doses of Attack potion."));
            }
            if(stack.hasTag() && stack.getTag().getDouble("CustomModelData") == 4.0d)
            {
                tooltip.add(Component.literal("1 dose of Attack potion."));
            }
        }
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    // set nbt data when crafted (example mechanic: special amulet that gives a 1/20 chance for a 4-dose potion)
  //  @Override
//    public void onCraftedBy(ItemStack itemStack, Level pLevel, Player pPlayer) {
//        Random rand = new Random();
//        int procchance = rand.nextInt(20);
//        ItemStack stack =
//                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.BOTANISTS_AMULET.get(), pPlayer).map(
//                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
//        if (!stack.isEmpty())
//        {
//            if (procchance == 0) {
//                customData = 0;
//                setNBT(itemStack);
//                stack.hurtAndBreak(1, pPlayer, (player2) -> {
//                    CuriosApi.getCuriosHelper().onBrokenCurio("necklace", 0, player2);
//                });
//            }
//            if (procchance >= 1) {
//                customData = 1;
//                setNBT(itemStack);
//            }
//        }
//        else
//            customData = 1;
//        setNBT(itemStack);
//    }

    // set the nbt data in creative tabs
 //   @Override
  //  public void fillItemCategory(CreativeModeTab tab, NonNullList<ItemStack> items) {
//        if(allowdedIn(group)) {
//            {
//                ItemStack stack = new ItemStack(this);
//                customData = 1;
//                setNBT(stack);
//                items.add(stack);
//            }
//        }
  //  }

    @Override
    public void onCraftedBy(ItemStack p_41447_, Level p_41448_, Player p_41449_) {
                Random rand = new Random();
        int procchance = rand.nextInt(20);
        ItemStack stack =
                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.BOTANISTS_AMULET.get(), p_41449_).map(
                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
        if (!stack.isEmpty())
        {
            if (procchance == 0) {
                customData = 0;
                setNBT(p_41447_);
                stack.hurtAndBreak(1, p_41449_, (player2) -> {
                    CuriosApi.getCuriosHelper().onBrokenCurio("necklace", 0, player2);
                });
            }
            if (procchance >= 1) {
                customData = 1;
                setNBT(p_41447_);
            }
        }
        else
            customData = 1;
        setNBT(p_41447_);
    }
}
