package com.ladestitute.runicages.items.crafting.tools;

import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.util.RunicAgesConfig;
import com.ladestitute.runicages.util.RunicAgesKeyboardUtil;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class ChiselItem extends Item {

    public ChiselItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Good for detailed crafting."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level p_41432_, Player p_41433_, InteractionHand p_41434_) {
        p_41433_.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            if(RunicAgesConfig.qualityoflifechanges.get()) {
                if (ed.getHasToolbeltChisel() == 0) {
                    if (!p_41433_.level().isClientSide()) {
                        p_41433_.displayClientMessage(Component.literal("Would you like to add this item to your toolbelt? Use this item again and hold Enter to confirm."), false);
                    }
                    if (RunicAgesKeyboardUtil.enter.isDown()) {
                        ed.sethastoolbeltchisel(1);
                        p_41433_.getMainHandItem().shrink(1);
                        if (!p_41433_.level().isClientSide()) {
                            p_41433_.displayClientMessage(Component.literal("You have added a chisel to your toolbelt."), false);
                        }
                    }
                }
            }
        });
        return super.use(p_41432_, p_41433_, p_41434_);
    }
}

