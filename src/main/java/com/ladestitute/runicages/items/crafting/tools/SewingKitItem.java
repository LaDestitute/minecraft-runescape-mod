package com.ladestitute.runicages.items.crafting.tools;

import com.ladestitute.runicages.client.menu.SewingKitMenu;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.network.NetworkHooks;

import java.util.List;

public class SewingKitItem extends Item {
    private static final Component TITLE = Component.literal("Sewing Hoop");

    public SewingKitItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("A kit used for sewing ranged and magic armor."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    protected ContainerData containerData;

    @Override
    public InteractionResultHolder<ItemStack> use(Level world, Player player, InteractionHand p_41434_) {
        if (!world.isClientSide() && player instanceof ServerPlayer) {
            NetworkHooks.openScreen((ServerPlayer) player,new SimpleMenuProvider((pContainerId, pPlayerInventory, pPlayer1) -> {
                return new SewingKitMenu(pContainerId,pPlayerInventory, (BlockEntity) ContainerLevelAccess.create(world, player.blockPosition()), containerData);
            }, Component.literal("")), friendlyByteBuf -> friendlyByteBuf.writeInt(100000));
        }

        return super.use(world, player, p_41434_);
    }

}

