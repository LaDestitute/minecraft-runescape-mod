package com.ladestitute.runicages.items.crafting.gems.uncut;

import com.ladestitute.runicages.capability.crafting.RunicAgesCraftingCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;
import java.util.Random;

public class UncutSapphireItem extends Item {

    public UncutSapphireItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("An uncut sapphire. Used in Crafting (20)."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level p_41432_, Player p_41433_, InteractionHand p_41434_) {
        ItemStack chisel = ItemInit.CHISEL.get().getDefaultInstance();
        p_41433_.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            p_41433_.getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(c ->
            {
                if (c.getCraftingLevel() >= 1 && p_41433_.getInventory().contains(chisel) ||
                        c.getCraftingLevel() >= 1 &&  ed.getHasToolbeltChisel() == 1 && RunicAgesConfig.qualityoflifechanges.get()) {
                    ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.SAPPHIRE.get().getDefaultInstance());
                    p_41433_.getMainHandItem().shrink(1);
                    c.addCraftingXP(p_41433_, 50);
                    ed.addxptotalxp(50);
                }
            });
        });
        return super.use(p_41432_, p_41433_, p_41434_);
    }
}