package com.ladestitute.runicages.items.crafting.gems.uncut;

import com.ladestitute.runicages.capability.crafting.RunicAgesCraftingCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;
import java.util.Random;

public class UncutOpalItem extends Item {

    public UncutOpalItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("An uncut opal. Used in Crafting (1)."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }


    @Override
    public InteractionResultHolder<ItemStack> use(Level p_41432_, Player p_41433_, InteractionHand p_41434_) {
        ItemStack chisel = ItemInit.CHISEL.get().getDefaultInstance();
        p_41433_.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            p_41433_.getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(c ->
            {
                if (c.getCraftingLevel() >= 1 && p_41433_.getInventory().contains(chisel) ||
                        c.getCraftingLevel() >= 1 &&  ed.getHasToolbeltChisel() == 1 && RunicAgesConfig.qualityoflifechanges.get()) {
                    Random rand = new Random();
                    int crushchance = rand.nextInt(101);
                    int bonus = (int) Math.round(c.getCraftingLevel() * 0.4897);
                    if(crushchance <= 51 - bonus)
                    {
                        if(!p_41433_.level().isClientSide()) {
                            p_41433_.displayClientMessage(Component.literal("You accidentally crush the opal."), false);
                        }
                        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.CRUSHED_GEM.get().getDefaultInstance());
                        p_41433_.getMainHandItem().shrink(1);
                        c.addCraftingXP(p_41433_, 4);
                    }
                    else ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.OPAL.get().getDefaultInstance());
                    p_41433_.getMainHandItem().shrink(1);
                    c.addCraftingXP(p_41433_, 15);
                    ed.addxptotalxp(15);
                }
            });
        });
        return super.use(p_41432_, p_41433_, p_41434_);
    }
}