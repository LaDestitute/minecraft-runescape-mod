package com.ladestitute.runicages.items.farming.seeds;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class RedberrySeedItem extends Item {

    public RedberrySeedItem(Item.Properties properties) {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("A redberry bush seed - plant in a bush patch."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}

