package com.ladestitute.runicages.items.farming.seeds;

import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class CabbageSeedItem extends Item {

    public CabbageSeedItem(Item.Properties properties) {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(RunicAgesConfig.modernrs.get())
        {
            tooltip.add(Component.literal("A cabbage seed - plant in an allotment. Used in Farming (7)."));
        }
        else tooltip.add(Component.literal("A cabbage seed - plant in an allotment."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
