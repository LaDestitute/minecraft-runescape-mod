package com.ladestitute.runicages.items.farming.seeds;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class OnionSeedItem extends Item {

    public OnionSeedItem(Item.Properties properties) {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("An onion seed - plant in an allotment."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}