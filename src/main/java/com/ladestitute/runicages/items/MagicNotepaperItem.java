package com.ladestitute.runicages.items;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class MagicNotepaperItem extends Item {

    public MagicNotepaperItem(Item.Properties properties) {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Use an item with this magical notepaper to make it noted and stackable."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}
