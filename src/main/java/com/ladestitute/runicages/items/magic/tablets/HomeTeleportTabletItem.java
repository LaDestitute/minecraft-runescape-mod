package com.ladestitute.runicages.items.magic.tablets;

import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.registry.ItemInit;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;

public class HomeTeleportTabletItem extends Item {

    public HomeTeleportTabletItem(Item.Properties properties) {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("A tablet containing a magic spell. Teleports players to their set home when broken."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level p_41432_, Player p_41433_, InteractionHand p_41434_) {
        if(!p_41433_.level().isClientSide())
        {
            p_41433_.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
            {
                p_41433_.teleportTo(ed.getX(), ed.getY(), ed.getZ());
                p_41433_.displayClientMessage(Component.literal("You teleported home."), false);
            });
            p_41433_.getMainHandItem().shrink(1);
        }
        return super.use(p_41432_, p_41433_, p_41434_);
    }
}

