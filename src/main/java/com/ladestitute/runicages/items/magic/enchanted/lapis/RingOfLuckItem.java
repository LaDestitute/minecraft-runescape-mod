package com.ladestitute.runicages.items.magic.enchanted.lapis;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.registry.ItemInit;
import net.minecraft.network.chat.Component;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.jetbrains.annotations.NotNull;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.List;

@Mod.EventBusSubscriber(modid = RunicAgesMain.MODID)
public class RingOfLuckItem extends Item implements ICurioItem {

    public RingOfLuckItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("An enchanted Lapis lazuli ring, this should make me more lucky."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @SubscribeEvent
    public static void ringofrecoil(TickEvent.PlayerTickEvent event)
    {
        event.player.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            ItemStack stack =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_LUCK.get(), event.player).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            if (!stack.isEmpty() ) {
               event.player.addEffect(new MobEffectInstance(MobEffects.LUCK, 600, 0));
            }
        });
    }

    @NotNull
    @Override
    public ICurio.DropRule getDropRule(SlotContext slotContext, DamageSource source, int lootingLevel, boolean recentlyHit, ItemStack stack) {
        return ICurio.DropRule.ALWAYS_DROP;
    }
}
