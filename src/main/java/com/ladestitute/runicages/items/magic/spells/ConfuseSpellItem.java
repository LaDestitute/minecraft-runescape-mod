package com.ladestitute.runicages.items.magic.spells;

import com.ladestitute.runicages.RunicAgesMain;
import net.minecraft.network.chat.Component;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.fml.common.Mod;
import org.jetbrains.annotations.NotNull;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.List;

@Mod.EventBusSubscriber(modid = RunicAgesMain.MODID)
public class ConfuseSpellItem extends Item implements ICurioItem {

    public ConfuseSpellItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }



    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("A basic level debuff spell. When cast on mobs, 10% chance for them to do zero damage to the target."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @NotNull
    @Override
    public ICurio.DropRule getDropRule(SlotContext slotContext, DamageSource source, int lootingLevel, boolean recentlyHit, ItemStack stack) {
        return ICurio.DropRule.ALWAYS_KEEP;
    }
}

