package com.ladestitute.runicages.items.magic.tablets;

import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;

public class EnchantSapphireTabletItem extends Item {

    public EnchantSapphireTabletItem(Item.Properties properties) {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("A tablet containing a magic spell. Enchants all unenchanted opal, lapis and sapphire jewelry when broken."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level p_41432_, Player p_41433_, InteractionHand p_41434_) {
        if(!p_41433_.level().isClientSide())
        {
            for (ItemStack enchant : p_41433_.getInventory().items) {
                if (enchant.getItem() == ItemInit.SAPPHIRE_RING.get()) {
                    enchant.shrink(1);
                    ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.RING_OF_RECOIL.get().getDefaultInstance());
                    break;
                }
                if (enchant.getItem() == ItemInit.SAPPHIRE_BRACELET.get()) {
                    enchant.shrink(1);
                    ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.BRACELET_OF_CLAY.get().getDefaultInstance());
                    break;
                }
                if (enchant.getItem() == ItemInit.SAPPHIRE_AMULET.get()) {
                    enchant.shrink(1);
                    ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.AMULET_OF_MAGIC.get().getDefaultInstance());
                    break;
                }
                if (enchant.getItem() == ItemInit.LAPIS_LAZULI_RING.get()) {
                    enchant.shrink(1);
                    ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.RING_OF_LUCK.get().getDefaultInstance());
                    break;
                }
                if (enchant.getItem() == ItemInit.OPAL_AMULET.get()) {
                    enchant.shrink(1);
                    if(RunicAgesConfig.modernrs.get()) {
                        ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.AMULET_OF_BOUNTIFUL_HARVEST.get().getDefaultInstance());
                    }
                    else ItemHandlerHelper.giveItemToPlayer(p_41433_, ItemInit.AMULET_OF_BOUNTY.get().getDefaultInstance());
                    break;
                }
            }
            p_41433_.getMainHandItem().shrink(1);
        }
        return super.use(p_41432_, p_41433_, p_41434_);
    }
}
