package com.ladestitute.runicages.items.magic.enchanted.opal;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.woodcutting.RunicAgesWoodcuttingCapability;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.items.ItemHandlerHelper;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.jetbrains.annotations.NotNull;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.List;
import java.util.Random;

@Mod.EventBusSubscriber(modid = RunicAgesMain.MODID)
public class AmuletOfBountifulHarvestItem extends Item implements ICurioItem {

    public AmuletOfBountifulHarvestItem(Item.Properties properties) {
        super(properties.stacksTo(1).durability(10));
    }

    @SubscribeEvent
    public void woodcutting(PlayerInteractEvent.RightClickBlock event)
    {
        if(RunicAgesConfig.modernrs.get()) {
            Player player = event.getEntity();
            BlockPos pos = event.getPos();
            Level level = player.level();
            Block block = level.getBlockState(pos).getBlock();
            ItemStack handstack = player.getItemBySlot(EquipmentSlot.MAINHAND);
            //Pre-checks based on level/tool
            ItemStack stack =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.AMULET_OF_BOUNTIFUL_HARVEST.get(), event.getEntity()).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            if (!stack.isEmpty() && block == Blocks.FARMLAND) {
                Random rand = new Random();
                int chance = rand.nextInt(100);
                if (handstack.getItem() == Items.WHEAT_SEEDS ||
                        handstack.getItem() == Items.MELON_SEEDS ||
                        handstack.getItem() == Items.PUMPKIN_SEEDS ||
                        handstack.getItem() == Items.BEETROOT_SEEDS ||
                        handstack.getItem() == Items.CARROT ||
                        handstack.getItem() == Items.POTATO) {
                    handstack.shrink(1);
                }
                if (chance <= 5) {
                    if (handstack.getItem() == Items.WHEAT_SEEDS ||
                            handstack.getItem() == Items.MELON_SEEDS ||
                            handstack.getItem() == Items.PUMPKIN_SEEDS ||
                            handstack.getItem() == Items.BEETROOT_SEEDS ||
                            handstack.getItem() == Items.CARROT ||
                            handstack.getItem() == Items.POTATO)
                        handstack.setCount(handstack.getCount() + 1);
                    stack.hurtAndBreak(1, event.getEntity(), (p_220045_0_) -> {
                        p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                    });
                }
            }
        }
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Wearing this amulet gives a chance to save seeds when farming."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @NotNull
    @Override
    public ICurio.DropRule getDropRule(SlotContext slotContext, DamageSource source, int lootingLevel, boolean recentlyHit, ItemStack stack) {
        return ICurio.DropRule.ALWAYS_DROP;
    }
}

