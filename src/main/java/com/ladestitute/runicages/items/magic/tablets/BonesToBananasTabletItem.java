package com.ladestitute.runicages.items.magic.tablets;

import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.registry.ItemInit;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;

public class BonesToBananasTabletItem extends Item {

    public BonesToBananasTabletItem(Item.Properties properties) {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("A tablet containing a magic spell. Converts all bones in the inventory into edible bananas."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level p_41432_, Player p_41433_, InteractionHand p_41434_) {
        if(!p_41433_.level().isClientSide())
        {
            for (ItemStack bones : p_41433_.getInventory().items) {
                if (bones.getItem() == Items.BONE && bones.getCount() >= 1) {
                    ItemStack banana = ItemInit.BANANA.get().getDefaultInstance();
                    ItemHandlerHelper.giveItemToPlayer(p_41433_, banana);
                    if(banana.getCount() > 0) {
                        banana.setCount(banana.getCount() + bones.getCount()-1);
                    }
                    else banana.setCount(bones.getCount()-1);
                    bones.setCount(0);
                    break;

                }
            }
            p_41433_.getMainHandItem().shrink(1);
        }
        return super.use(p_41432_, p_41433_, p_41434_);
    }
}
