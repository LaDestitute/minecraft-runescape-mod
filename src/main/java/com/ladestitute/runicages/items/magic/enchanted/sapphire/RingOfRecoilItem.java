package com.ladestitute.runicages.items.magic.enchanted.sapphire;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.registry.ItemInit;
import net.minecraft.network.chat.Component;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.jetbrains.annotations.NotNull;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotContext;
import top.theillusivec4.curios.api.type.capability.ICurio;
import top.theillusivec4.curios.api.type.capability.ICurioItem;

import java.util.List;

@Mod.EventBusSubscriber(modid = RunicAgesMain.MODID)
public class RingOfRecoilItem extends Item implements ICurioItem {

    public RingOfRecoilItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("An enchanted ring."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @SubscribeEvent
    public static void ringofrecoil(LivingHurtEvent event)
    {
        event.getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            Entity source = event.getSource().getDirectEntity();
            ItemStack stack =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_RECOIL.get(), event.getEntity()).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            if (!stack.isEmpty() && source instanceof Monster) {
                if(event.getAmount() < 10) {
                    source.hurt(event.getEntity().damageSources().generic(), 1);
                }
                if(event.getAmount() >= 10) {
                    source.hurt(event.getEntity().damageSources().generic(), Math.round(event.getAmount() / 10));
                }
                ed.decreaserecoilringcharges(1);
                RunicAgesExtraDataCapability.levelClientUpdate((Player) source);
            }
            if(ed.getRecoilRingCharges() == 0)
            {
                stack.shrink(1);
                ed.setrecoilringcharges(80);
                RunicAgesExtraDataCapability.levelClientUpdate((Player) source);
            }
        });
    }

    @NotNull
    @Override
    public ICurio.DropRule getDropRule(SlotContext slotContext, DamageSource source, int lootingLevel, boolean recentlyHit, ItemStack stack) {
        return ICurio.DropRule.ALWAYS_DROP;
    }
}
