package com.ladestitute.runicages.items.woodcutting;

import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class NormalTreeLogItem extends Item {

    public NormalTreeLogItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(RunicAgesConfig.modernrs.get()) {
            tooltip.add(Component.literal("A number of wooden logs. Used in Firemaking (1), Fletching (1), Construction (1)."));
        }
        else tooltip.add(Component.literal("A number of wooden logs."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}