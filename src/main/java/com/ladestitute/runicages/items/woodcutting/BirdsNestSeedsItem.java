package com.ladestitute.runicages.items.woodcutting;

import com.ladestitute.runicages.registry.ItemInit;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;
import java.util.Random;

public class BirdsNestSeedsItem extends Item {

    public BirdsNestSeedsItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("It's a bird's nest with some seeds in it."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level p_41432_, Player p_77659_2_, InteractionHand p_41434_) {
        Random rand = new Random();
        int itemtype = rand.nextInt(9);
        if(itemtype == 0)
        {
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, Items.BEETROOT_SEEDS.getDefaultInstance());
            p_77659_2_.getMainHandItem().shrink(1);
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, ItemInit.BIRDS_NEST.get().getDefaultInstance());
        }
        if(itemtype == 1)
        {
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, Items.PUMPKIN_SEEDS.getDefaultInstance());
            p_77659_2_.getMainHandItem().shrink(1);
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, ItemInit.BIRDS_NEST.get().getDefaultInstance());
        }
        if(itemtype == 2)
        {
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, Items.MELON_SEEDS.getDefaultInstance());
            p_77659_2_.getMainHandItem().shrink(1);
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, ItemInit.BIRDS_NEST.get().getDefaultInstance());
        }
        if(itemtype == 3)
        {
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, Items.WHEAT_SEEDS.getDefaultInstance());
            p_77659_2_.getMainHandItem().shrink(1);
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, ItemInit.BIRDS_NEST.get().getDefaultInstance());
        }
        if(itemtype == 4)
        {
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, ItemInit.ONION_SEED.get().getDefaultInstance());
            p_77659_2_.getMainHandItem().shrink(1);
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, ItemInit.BIRDS_NEST.get().getDefaultInstance());
        }
        if(itemtype == 5)
        {
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, ItemInit.CABBAGE_SEED.get().getDefaultInstance());
            p_77659_2_.getMainHandItem().shrink(1);
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, ItemInit.BIRDS_NEST.get().getDefaultInstance());
        }
        if(itemtype == 6)
        {
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, ItemInit.BARLEY_SEED.get().getDefaultInstance());
            p_77659_2_.getMainHandItem().shrink(1);
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, ItemInit.BIRDS_NEST.get().getDefaultInstance());
        }
        if(itemtype == 7)
        {
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, ItemInit.GUAM_SEED.get().getDefaultInstance());
            p_77659_2_.getMainHandItem().shrink(1);
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, ItemInit.BIRDS_NEST.get().getDefaultInstance());
        }
        if(itemtype == 8)
        {
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, ItemInit.REDBERRY_SEED.get().getDefaultInstance());
            p_77659_2_.getMainHandItem().shrink(1);
            ItemHandlerHelper.giveItemToPlayer(p_77659_2_, ItemInit.BIRDS_NEST.get().getDefaultInstance());
        }

        return super.use(p_41432_, p_77659_2_, p_41434_);
    }
}

