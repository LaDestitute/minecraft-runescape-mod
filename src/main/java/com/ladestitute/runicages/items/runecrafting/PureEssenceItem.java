package com.ladestitute.runicages.items.runecrafting;

import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class PureEssenceItem extends Item {

    public PureEssenceItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(RunicAgesConfig.modernrs.get()) {
            tooltip.add(Component.literal("An uncharged Rune Stone of extra capability. Used in Runecrafting for training and production."));
        }
        else tooltip.add(Component.literal("An unimbued rune of extra capability."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
