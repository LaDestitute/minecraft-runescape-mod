package com.ladestitute.runicages.items.runecrafting.runes;

import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class EarthRuneItem extends Item {

    public EarthRuneItem(Item.Properties properties) {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(RunicAgesConfig.modernrs.get()) {
            tooltip.add(Component.literal("One of the four basic elemental runes. Used in Magic (9)."));
        }
        else tooltip.add(Component.literal("One of the 4 basic elemental runes."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}

