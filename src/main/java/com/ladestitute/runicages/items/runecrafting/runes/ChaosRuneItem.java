package com.ladestitute.runicages.items.runecrafting.runes;

import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class ChaosRuneItem extends Item {

    public ChaosRuneItem(Item.Properties properties) {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(RunicAgesConfig.modernrs.get()) {
            tooltip.add(Component.literal("A medium level catalytic rune."));
        }
        else tooltip.add(Component.literal("Used for low level missile spells."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
