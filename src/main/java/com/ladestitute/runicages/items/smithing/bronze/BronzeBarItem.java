package com.ladestitute.runicages.items.smithing.bronze;

import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class BronzeBarItem extends Item {

    public BronzeBarItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        if(RunicAgesConfig.modernrs.get()) {
            tooltip.add(Component.literal("Used in Smithing (1)."));
        }
        else tooltip.add(Component.literal("It's a bar of bronze."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
