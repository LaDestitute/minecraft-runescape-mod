package com.ladestitute.runicages.items.smithing.unfinishedbars;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class UnfinishedBronzePickaxeItem extends Item {

    public UnfinishedBronzePickaxeItem(Item.Properties properties) {
        super(properties.stacksTo(50));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Item being created: Bronze pickaxe"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
