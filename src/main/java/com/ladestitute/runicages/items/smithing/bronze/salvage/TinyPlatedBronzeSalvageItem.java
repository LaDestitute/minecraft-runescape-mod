package com.ladestitute.runicages.items.smithing.bronze.salvage;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class TinyPlatedBronzeSalvageItem extends Item {

    public TinyPlatedBronzeSalvageItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Salvageable fragments of smithed armor."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
