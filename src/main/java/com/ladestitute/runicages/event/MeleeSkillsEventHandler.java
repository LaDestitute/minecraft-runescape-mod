package com.ladestitute.runicages.event;
import com.ladestitute.runicages.capability.attack.RunicAgesAttackCapability;
import com.ladestitute.runicages.capability.defense.RunicAgesDefenseCapability;
import com.ladestitute.runicages.capability.magic.RunicAgesMagicCapability;
import com.ladestitute.runicages.capability.ranged.RunicAgesRangedCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.capability.strength.RunicAgesStrengthCapability;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ambient.Bat;
import net.minecraft.world.entity.animal.*;
import net.minecraft.world.entity.animal.frog.Frog;
import net.minecraft.world.entity.monster.*;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.Objects;
import java.util.Random;

public class MeleeSkillsEventHandler {

    @SubscribeEvent
    public void restrictitems(TickEvent.PlayerTickEvent event) {
        ItemStack handstack = event.player.getItemBySlot(EquipmentSlot.MAINHAND);
        ItemStack headstack = event.player.getItemBySlot(EquipmentSlot.HEAD);
        ItemStack cheststack = event.player.getItemBySlot(EquipmentSlot.CHEST);
        ItemStack legstack = event.player.getItemBySlot(EquipmentSlot.LEGS);
        ItemStack feetstack = event.player.getItemBySlot(EquipmentSlot.FEET);
        event.player.getCapability(RunicAgesAttackCapability.Provider.ATTACK_LEVEL).ifPresent(attack ->
        {
            if (handstack.getItem() == Items.IRON_SWORD && !event.player.level().isClientSide)
            {
                if(RunicAgesConfig.modernrs.get() && attack.getAttackLevel()+attack.getAttackBoost() < 10)
                {
                    Random rand = new Random();
                    int slotpick = rand.nextInt(36);
                    event.player.getMainHandItem().setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(event.player, Items.IRON_SWORD.getDefaultInstance(), slotpick);
                    event.player.displayClientMessage(Component.literal("Level 10 Attack is required to wield this weapon."), false);
                }
                if(!RunicAgesConfig.modernrs.get() && attack.getAttackLevel() < 10)
                {
                    Random rand = new Random();
                    int slotpick = rand.nextInt(36);
                    event.player.getMainHandItem().setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(event.player, Items.IRON_SWORD.getDefaultInstance(), slotpick);
                    event.player.displayClientMessage(Component.literal("Level 10 Attack is required to wield this weapon."), false);
                }
            }
            if (handstack.getItem() == Items.IRON_AXE && !event.player.level().isClientSide)
            {
                if(RunicAgesConfig.modernrs.get() && attack.getAttackLevel()+attack.getAttackBoost() < 10)
                {
                    Random rand = new Random();
                    int slotpick = rand.nextInt(36);
                    event.player.getMainHandItem().setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(event.player, Items.IRON_AXE.getDefaultInstance(), slotpick);
                    event.player.displayClientMessage(Component.literal("Level 10 Attack is required to wield this weapon."), false);
                }
                if(!RunicAgesConfig.modernrs.get() && attack.getAttackLevel() < 10)
                {
                    Random rand = new Random();
                    int slotpick = rand.nextInt(36);
                    event.player.getMainHandItem().setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(event.player, Items.IRON_AXE.getDefaultInstance(), slotpick);
                    event.player.displayClientMessage(Component.literal("Level 10 Attack is required to wield this weapon."), false);
                }
            }
            if (handstack.getItem() == Items.IRON_PICKAXE && !event.player.level().isClientSide)
            {
                if(RunicAgesConfig.modernrs.get() && attack.getAttackLevel()+attack.getAttackBoost() < 10)
                {
                    Random rand = new Random();
                    int slotpick = rand.nextInt(36);
                    event.player.getMainHandItem().setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(event.player, Items.IRON_PICKAXE.getDefaultInstance(), slotpick);
                    event.player.displayClientMessage(Component.literal("Level 10 Attack is required to wield this weapon."), false);
                }
                if(!RunicAgesConfig.modernrs.get() && attack.getAttackLevel() < 10)
                {
                    Random rand = new Random();
                    int slotpick = rand.nextInt(36);
                    event.player.getMainHandItem().setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(event.player, Items.IRON_PICKAXE.getDefaultInstance(), slotpick);
                    event.player.displayClientMessage(Component.literal("Level 10 Attack is required to wield this weapon."), false);
                }
            }
            if (handstack.getItem() == Items.IRON_SHOVEL && !event.player.level().isClientSide)
            {
                if(RunicAgesConfig.modernrs.get() && attack.getAttackLevel()+attack.getAttackBoost() < 10)
                {
                    Random rand = new Random();
                    int slotpick = rand.nextInt(36);
                    event.player.getMainHandItem().setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(event.player, Items.IRON_SHOVEL.getDefaultInstance(), slotpick);
                    event.player.displayClientMessage(Component.literal("Level 10 Attack is required to wield this weapon."), false);
                }
                if(!RunicAgesConfig.modernrs.get() && attack.getAttackLevel() < 10)
                {
                    Random rand = new Random();
                    int slotpick = rand.nextInt(36);
                    event.player.getMainHandItem().setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(event.player, Items.IRON_SHOVEL.getDefaultInstance(), slotpick);
                    event.player.displayClientMessage(Component.literal("Level 10 Attack is required to wield this weapon."), false);
                }
            }
            if (handstack.getItem() == Items.IRON_HOE && !event.player.level().isClientSide)
            {
                if(RunicAgesConfig.modernrs.get() && attack.getAttackLevel()+attack.getAttackBoost() < 10)
                {
                    Random rand = new Random();
                    int slotpick = rand.nextInt(36);
                    event.player.getMainHandItem().setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(event.player, Items.IRON_HOE.getDefaultInstance(), slotpick);
                    event.player.displayClientMessage(Component.literal("Level 10 Attack is required to wield this weapon."), false);
                }
                if(!RunicAgesConfig.modernrs.get() && attack.getAttackLevel() < 10)
                {
                    Random rand = new Random();
                    int slotpick = rand.nextInt(36);
                    event.player.getMainHandItem().setCount(0);
                    ItemHandlerHelper.giveItemToPlayer(event.player, Items.IRON_HOE.getDefaultInstance(), slotpick);
                    event.player.displayClientMessage(Component.literal("Level 10 Attack is required to wield this weapon."), false);
                }
            }
        });
        event.player.getCapability(RunicAgesDefenseCapability.Provider.DEFENSE_LEVEL).ifPresent(attack ->
        {
            if (attack.getDefenseLevel() < 10 && headstack.getItem() == Items.IRON_HELMET && !event.player.level().isClientSide) {
                Random rand = new Random();
                int slotpick = rand.nextInt(36);
                headstack.shrink(1);
                ItemHandlerHelper.giveItemToPlayer(event.player, Items.IRON_HELMET.getDefaultInstance(), slotpick);
                event.player.displayClientMessage(Component.literal("Level 10 Defense is required to wear this."), false);
            }
        });
        event.player.getCapability(RunicAgesDefenseCapability.Provider.DEFENSE_LEVEL).ifPresent(attack ->
        {
            if (attack.getDefenseLevel() < 10 && cheststack.getItem() == Items.IRON_CHESTPLATE && !event.player.level().isClientSide) {
                Random rand = new Random();
                int slotpick = rand.nextInt(36);
                cheststack.shrink(1);
                ItemHandlerHelper.giveItemToPlayer(event.player, Items.IRON_CHESTPLATE.getDefaultInstance(), slotpick);
                event.player.displayClientMessage(Component.literal("Level 10 Defense is required to wear this."), false);
            }
        });
        event.player.getCapability(RunicAgesDefenseCapability.Provider.DEFENSE_LEVEL).ifPresent(attack ->
        {
            if (attack.getDefenseLevel() < 10 && legstack.getItem() == Items.IRON_LEGGINGS && !event.player.level().isClientSide) {
                Random rand = new Random();
                int slotpick = rand.nextInt(36);
               legstack.shrink(1);
                ItemHandlerHelper.giveItemToPlayer(event.player, Items.IRON_LEGGINGS.getDefaultInstance(), slotpick);
                event.player.displayClientMessage(Component.literal("Level 10 Defense is required to wear this."), false);
            }
        });
        event.player.getCapability(RunicAgesDefenseCapability.Provider.DEFENSE_LEVEL).ifPresent(attack ->
        {
            if (attack.getDefenseLevel() < 10 && feetstack.getItem() == Items.IRON_BOOTS && !event.player.level().isClientSide) {
                Random rand = new Random();
                int slotpick = rand.nextInt(36);
               feetstack.shrink(1);
                ItemHandlerHelper.giveItemToPlayer(event.player, Items.IRON_BOOTS.getDefaultInstance(), slotpick);
                event.player.displayClientMessage(Component.literal("Level 10 Defense is required to wear this."), false);
            }
        });
    }

    @SubscribeEvent
    public void livingDamageEvent(LivingDamageEvent event) {
        //Check if is player doing the damage.
        if (event.getSource().getDirectEntity() instanceof Player) {

            event.getSource().getEntity().getCapability(RunicAgesMagicCapability.Provider.MAGIC_LEVEL).ifPresent(h ->
            {
                for (ItemStack held : event.getSource().getEntity().getHandSlots()) {
                    if (held.getItem() == ItemInit.STAFF.get() ||
                            held.getItem() == ItemInit.MAGIC_WAND.get()||
                            held.getItem() == ItemInit.STAFF_OF_AIR.get()) {
                        event.setAmount(Math.round((float) (event.getAmount() + (0.2 * h.getMagicLevel()))));
                    }
                }
            });

            event.getSource().getEntity().getCapability(RunicAgesStrengthCapability.Provider.STRENGTH_LEVEL).ifPresent(h ->
            {
                event.getSource().getEntity().getCapability(RunicAgesAttackCapability.Provider.ATTACK_LEVEL).ifPresent(h2 ->
                {
                    event.setAmount(Math.round((float) (event.getAmount() + (0.1 * h.getStrengthLevel()) + (0.1*(h2.getAttackLevel()+h2.getAttackBoost())))));
                });
            });
        }
    }

    @SubscribeEvent
    public void onLivingHurt(LivingHurtEvent event) {
        //Check if it is the Player who takes damage.
        if(!(event.getEntity() instanceof Player))
        {
            return;
        }
        if(!event.getEntity().isAlive())
        {
            return;
        }
        if(event.getSource() == event.getEntity().damageSources().lava()||
                event.getSource() == event.getEntity().damageSources().stalagmite()||
                event.getSource() == event.getEntity().damageSources().sweetBerryBush()||
                event.getSource() == event.getEntity().damageSources().hotFloor()||
                event.getSource() == event.getEntity().damageSources().fallingStalactite(event.getEntity())||
                event.getSource() == event.getEntity().damageSources().fall()||
                event.getSource() == event.getEntity().damageSources().cactus()||
                event.getSource() == event.getEntity().damageSources().anvil(event.getEntity())||
                event.getSource() == event.getEntity().damageSources().inFire()||
                event.getSource() == event.getEntity().damageSources().cramming()||
                event.getSource() == event.getEntity().damageSources().drown()||
                event.getSource() == event.getEntity().damageSources().starve()||
                event.getSource() == event.getEntity().damageSources().fellOutOfWorld()||
                event.getSource() == event.getEntity().damageSources().outOfBorder()||
                event.getSource() == event.getEntity().damageSources().dryOut()||
                event.getSource() == event.getEntity().damageSources().fallingBlock(event.getEntity())||
                event.getSource() == event.getEntity().damageSources().flyIntoWall()||
                event.getSource() == event.getEntity().damageSources().freeze()||
                event.getSource() == event.getEntity().damageSources().inWall())
        {
            return;
        }
        event.getSource().getEntity().getCapability(RunicAgesDefenseCapability.Provider.DEFENSE_LEVEL).ifPresent(h ->
        {
            if (event.getEntity() instanceof Player && RunicAgesConfig.modernrs.get()) {
                event.setAmount(event.getAmount()-(float) ((0.1/100)*h.getDefenseLevel()));

            }
        });
    }

    @SubscribeEvent
    public void skilldraintimer(TickEvent.PlayerTickEvent event) {
        event.player.getCapability(RunicAgesAttackCapability.Provider.ATTACK_LEVEL).ifPresent(boost ->
        {
            if(boost.getAttackBoost() >= 1)
            {
                boost.incrementattackboostdraintimer(event.player, 1);
                if(boost.getAttackBoostTimer() >= 1200)
                {
                    boost.subAttackBoost(event.player, 1);
                    boost.setattackboostdraintimer(0);
                }
            }
            if(boost.getAttackBoost() < 0)
            {
                boost.setAttackBoost(0);
            }
        });
        event.player.getCapability(RunicAgesDefenseCapability.Provider.DEFENSE_LEVEL).ifPresent(boost ->
        {
            if(boost.getDefenseBoost() >= 1)
            {
                boost.incrementdefenseboostdraintimer(event.player, 1);
                if(boost.getDefenseBoostTimer() >= 1200)
                {
                    boost.subDefenseBoost(event.player, 1);
                    boost.setdefenseboostdraintimer(0);
                }
            }
            if(boost.getDefenseBoost() < 0)
            {
                boost.setDefenseBoost(0);
            }
        });
        event.player.getCapability(RunicAgesMagicCapability.Provider.MAGIC_LEVEL).ifPresent(boost ->
        {
            if(boost.getMagicBoost() >= 1)
            {
                boost.incrementmagicboostdraintimer(event.player, 1);
                if(boost.getMagicBoostTimer() >= 1200)
                {
                    boost.subMagicBoost(event.player, 1);
                    boost.setmagicboostdraintimer(0);
                }
            }
            if(boost.getMagicBoost() < 0)
            {
                boost.setMagicBoost(0);
            }
        });
    }
}
