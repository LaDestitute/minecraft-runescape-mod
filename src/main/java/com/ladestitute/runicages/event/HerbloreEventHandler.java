package com.ladestitute.runicages.event;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.herblore.RunicAgesHerbloreCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.Random;

public class HerbloreEventHandler {

    @SubscribeEvent
    public void herblorerecipeunlock(PlayerEvent.PlayerLoggedInEvent event)
    {
        ResourceLocation[] recipe = new ResourceLocation[3];
        recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "herblore/guam_potion_unf");
        recipe[1] = new ResourceLocation(RunicAgesMain.MODID, "herblore/attack_potion");
        recipe[2] = new ResourceLocation(RunicAgesMain.MODID, "herblore/clean_guam");
        event.getEntity().awardRecipesByKey(recipe);

        event.getEntity().getCapability(RunicAgesHerbloreCapability.Provider.HERBLORE_LEVEL).ifPresent(h ->
        {
            //Todo: figure out how to relock recipes
            if(h.getHerbloreLevel() >= 3)
            {
                ResourceLocation[] unlock = new ResourceLocation[1];
                unlock[0] = new ResourceLocation(RunicAgesMain.MODID, "herblore/ranging_potion");
                event.getEntity().awardRecipesByKey(unlock);
            }
            if(h.getHerbloreLevel() >= 5)
            {
                ResourceLocation[] unlock = new ResourceLocation[2];
                unlock[0] = new ResourceLocation(RunicAgesMain.MODID, "herblore/tarromin_potion_unf");
                unlock[1] = new ResourceLocation(RunicAgesMain.MODID, "herblore/magic_potion");
                event.getEntity().awardRecipesByKey(unlock);
            }
            if(h.getHerbloreLevel() >= 9 && RunicAgesConfig.modernrs.get())
            {
                ResourceLocation[] unlock = new ResourceLocation[2];
                unlock[0] = new ResourceLocation(RunicAgesMain.MODID, "herblore/marrentill_potion_unf");
                unlock[1] = new ResourceLocation(RunicAgesMain.MODID, "herblore/defense_potion");
                event.getEntity().awardRecipesByKey(unlock);
            }
            if(h.getHerbloreLevel() >= 30 && !RunicAgesConfig.modernrs.get())
            {
                ResourceLocation[] unlock = new ResourceLocation[2];
                unlock[0] = new ResourceLocation(RunicAgesMain.MODID, "herblore/marrentill_potion_unf");
                unlock[1] = new ResourceLocation(RunicAgesMain.MODID, "herblore/defense_potion");
                event.getEntity().awardRecipesByKey(unlock);
            }
            if(h.getHerbloreLevel() >= 13 && RunicAgesConfig.modernrs.get())
            {
                ResourceLocation[] unlock = new ResourceLocation[1];
                unlock[0] = new ResourceLocation(RunicAgesMain.MODID, "herblore/antiposion_potion");
                event.getEntity().awardRecipesByKey(unlock);
            }
            if(h.getHerbloreLevel() >= 5 && !RunicAgesConfig.modernrs.get())
            {
                ResourceLocation[] unlock = new ResourceLocation[1];
                unlock[0] = new ResourceLocation(RunicAgesMain.MODID, "herblore/antiposion_potion");
                event.getEntity().awardRecipesByKey(unlock);
            }
        });
    }

    @SubscribeEvent
    public void craftinglevelup (PlayerEvent.ItemCraftedEvent event) {
        event.getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            event.getEntity().getCapability(RunicAgesHerbloreCapability.Provider.HERBLORE_LEVEL).ifPresent(h ->
            {
                if (!event.getEntity().level().isClientSide()) {
                    if (event.getCrafting().getItem() == ItemInit.ATTACK_POTION.get()) {
                        h.addHerbloreXP(event.getEntity(), 25);
                        ed.addxptotalxp(25);
                    }
                    if (event.getCrafting().getItem() == ItemInit.GUAM_POTION_UNF.get()||
                            event.getCrafting().getItem() == ItemInit.MARRENTILL_POTION_UNF.get()||
                            event.getCrafting().getItem() == ItemInit.TARROMIN_POTION_UNF.get()) {
                        if(RunicAgesConfig.modernrs.get()) {
                            h.addHerbloreXP(event.getEntity(), 1);
                            ed.addxptotalxp(1);
                        }
                    }
                    if (event.getCrafting().getItem() == ItemInit.RANGING_POTION.get()) {
                        h.addHerbloreXP(event.getEntity(), 30);
                        ed.addxptotalxp(30);
                    }
                    if (event.getCrafting().getItem() == ItemInit.MAGIC_POTION.get()) {
                        h.addHerbloreXP(event.getEntity(), 35);
                        ed.addxptotalxp(35);
                    }
                    if (event.getCrafting().getItem() == ItemInit.DEFENSE_POTION.get()) {
                        h.addHerbloreXP(event.getEntity(), 45);
                        ed.addxptotalxp(45);
                    }
                    if (event.getCrafting().getItem() == ItemInit.ANTIPOISON_POTION.get()) {
                        if(RunicAgesConfig.modernrs.get()) {
                            h.addHerbloreXP(event.getEntity(), 50);
                            ed.addxptotalxp(50);
                        }
                        else h.addHerbloreXP(event.getEntity(), (int) 37.5);
                        ed.addxptotalxp((int) 37.5);
                    }
                }
            });
        });
    }

}
