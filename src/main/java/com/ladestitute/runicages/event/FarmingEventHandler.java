package com.ladestitute.runicages.event;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.farming.RunicAgesFarmingCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.registry.BlockInit;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.registry.SpecialBlockInit;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;

import java.util.Random;

public class FarmingEventHandler {

    @SubscribeEvent
    public void farmingrecipeunlock(PlayerEvent.PlayerLoggedInEvent event)
    {
        ResourceLocation[] allotment = new ResourceLocation[1];
        allotment[0] = new ResourceLocation(RunicAgesMain.MODID, "farming/allotment");
        event.getEntity().awardRecipesByKey(allotment);

        event.getEntity().getCapability(RunicAgesFarmingCapability.Provider.FARMING_LEVEL).ifPresent(h ->
        {
            if(h.getFarmingLevel() >= 3)
            {
                ResourceLocation[] patch = new ResourceLocation[1];
                patch[0] = new ResourceLocation(RunicAgesMain.MODID, "farming/hops_patch");
                event.getEntity().awardRecipesByKey(patch);
            }
        });

        event.getEntity().getCapability(RunicAgesFarmingCapability.Provider.FARMING_LEVEL).ifPresent(h ->
        {
            if(h.getFarmingLevel() >= 9)
            {
                ResourceLocation[] patch = new ResourceLocation[1];
                patch[0] = new ResourceLocation(RunicAgesMain.MODID, "farming/herb_patch");
                event.getEntity().awardRecipesByKey(patch);
            }
        });

        event.getEntity().getCapability(RunicAgesFarmingCapability.Provider.FARMING_LEVEL).ifPresent(h ->
        {
            if(h.getFarmingLevel() >= 10)
            {
                ResourceLocation[] patch = new ResourceLocation[1];
                patch[0] = new ResourceLocation(RunicAgesMain.MODID, "farming/bush_patch");
                event.getEntity().awardRecipesByKey(patch);
            }
        });
    }

    int xp;
    int seedsreq;

    @SubscribeEvent
    //Subscribe event for planting pine cones (they're used to plant saplings instead)
    public void plantingcrops(PlayerInteractEvent.RightClickBlock event) {
        ItemStack bounty =
                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.AMULET_OF_BOUNTY.get(), event.getEntity()).map(
                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
        event.getEntity().getCapability(RunicAgesFarmingCapability.Provider.FARMING_LEVEL).ifPresent(h ->
        {
            event.getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
            {
                if(event.getEntity().level().isClientSide())
                {
                    return;
                }
                BlockPos blockpos = event.getPos();
                BlockState state = event.getLevel().getBlockState(blockpos);
                if (event.getLevel().isClientSide) {
                    return;
                }
                //
                if (h.getFarmingLevel() >= 1 && event.getItemStack().getItem() == Items.POTATO)
                {
                    if (state.getBlock() == BlockInit.ALLOTMENT.get()) {
                        ItemStack item = Items.POTATO.getDefaultInstance();
                        if (event.getEntity().getInventory().contains(item))
                            for (ItemStack held : event.getEntity().getInventory().items)
                                if (held.getItem() == Items.POTATO)
                                {
                                    xp = (int) Math.round(8);
                                    seedsreq = 1;
                                    h.addFarmingXP(event.getEntity(), xp);
                                    ed.addxptotalxp(xp);
                                    event.getEntity().sendSystemMessage(Component.literal("You have gained Farming XP: " + xp));
                                    event.getEntity().level().setBlock(blockpos.above(), SpecialBlockInit.ALLOTMENT_POTATO_CROP.get().defaultBlockState(), 2);
                                    if (!event.getEntity().isCreative())
                                    {
                                        held.shrink(seedsreq);
                                        break;
                                    }
                                }
                    }
                }
                //
                if (h.getFarmingLevel() >= 3 && event.getItemStack().getItem() == ItemInit.BARLEY_SEED.get()
                && !event.getEntity().isCreative()||
                        h.getFarmingLevel() >= 1 && event.getItemStack().getItem() == ItemInit.BARLEY_SEED.get() && event.getEntity().isCreative())
                {
                    if (state.getBlock() == BlockInit.HOPS_PATCH.get()) {
                        ItemStack item = ItemInit.BARLEY_SEED.get().getDefaultInstance();
                        if (event.getEntity().getInventory().contains(item))
                            for (ItemStack held : event.getEntity().getInventory().items)
                                if (held.getItem() == ItemInit.BARLEY_SEED.get() && held.getCount() >= 4)
                                {
                                    xp = (int) Math.round(8.5);
                                    seedsreq = 4;
                                    h.addFarmingXP(event.getEntity(), xp);
                                    ed.addxptotalxp(xp);
                                    event.getEntity().sendSystemMessage(Component.literal("You have gained Farming XP: " + xp));
                                    event.getEntity().level().setBlock(blockpos.above(), SpecialBlockInit.BARLEY_CROP.get().defaultBlockState(), 2);
                                    if (!event.getEntity().isCreative())
                                    {
                                        held.shrink(seedsreq);
                                        break;
                                    }
                                }
                    }
                }
                //
                if (h.getFarmingLevel() >= 5 && event.getItemStack().getItem() == ItemInit.ONION_SEED.get()
                        && !event.getEntity().isCreative()||
                        h.getFarmingLevel() >= 1 && event.getItemStack().getItem() == ItemInit.ONION_SEED.get() && event.getEntity().isCreative())
                {
                    if (state.getBlock() == BlockInit.ALLOTMENT.get()) {
                        ItemStack item = ItemInit.ONION_SEED.get().getDefaultInstance();
                        if (event.getEntity().getInventory().contains(item))
                            for (ItemStack held : event.getEntity().getInventory().items)
                                if (held.getItem() == ItemInit.ONION_SEED.get() && held.getCount() >= 3)
                                {
                                    xp = (int) Math.round(9.5);
                                    Random rand = new Random();
                                    int bountydiscountchance = rand.nextInt(101);
                                    if(bountydiscountchance <= 25) {
                                        seedsreq = 1;
                                        ed.decreasebountycharges(1);
                                        if(ed.getBountyCharges() == 0)
                                        {
                                            bounty.shrink(1);
                                            ed.setbountycharges(10);
                                        }
                                    }
                                    else seedsreq = 3;
                                    h.addFarmingXP(event.getEntity(), xp);
                                    ed.addxptotalxp(xp);
                                    event.getEntity().sendSystemMessage(Component.literal("You have gained Farming XP: " + xp));
                                    event.getEntity().level().setBlock(blockpos.above(), SpecialBlockInit.ONION_CROP.get().defaultBlockState(), 2);
                                    if (!event.getEntity().isCreative())
                                    {
                                        held.shrink(seedsreq);
                                        break;
                                    }
                                }
                    }
                }
                //
                if (h.getFarmingLevel() >= 7 && event.getItemStack().getItem() == ItemInit.CABBAGE_SEED.get()
                        && !event.getEntity().isCreative()||
                        h.getFarmingLevel() >= 1 && event.getItemStack().getItem() == ItemInit.CABBAGE_SEED.get() && event.getEntity().isCreative())
                {
                    if (state.getBlock() == BlockInit.ALLOTMENT.get()) {
                        ItemStack item = ItemInit.CABBAGE_SEED.get().getDefaultInstance();
                        if (event.getEntity().getInventory().contains(item))
                            for (ItemStack held : event.getEntity().getInventory().items)
                                if (held.getItem() == ItemInit.CABBAGE_SEED.get() && held.getCount() >= 3)
                                {
                                    xp = (int) Math.round(10);
                                    Random rand = new Random();
                                    int bountydiscountchance = rand.nextInt(101);
                                    if(bountydiscountchance <= 25) {
                                        seedsreq = 1;
                                        ed.decreasebountycharges(1);
                                        if(ed.getBountyCharges() == 0)
                                        {
                                            bounty.shrink(1);
                                            ed.setbountycharges(10);
                                        }
                                    }
                                    else seedsreq = 3;
                                    h.addFarmingXP(event.getEntity(), xp);
                                    ed.addxptotalxp(xp);
                                    event.getEntity().sendSystemMessage(Component.literal("You have gained Farming XP: " + xp));
                                    event.getEntity().level().setBlock(blockpos.above(), SpecialBlockInit.CABBAGE_CROP.get().defaultBlockState(), 2);
                                    if (!event.getEntity().isCreative())
                                    {
                                        held.shrink(seedsreq);
                                        break;
                                    }
                                }
                    }
                }
                //
                if (h.getFarmingLevel() >= 9 && event.getItemStack().getItem() == ItemInit.GUAM_SEED.get()
                        && !event.getEntity().isCreative()||
                        h.getFarmingLevel() >= 1 && event.getItemStack().getItem() == ItemInit.GUAM_SEED.get() && event.getEntity().isCreative())
                {
                    if (state.getBlock() == BlockInit.HERB_PATCH.get()) {
                        ItemStack item = ItemInit.GUAM_SEED.get().getDefaultInstance();
                        if (event.getEntity().getInventory().contains(item))
                            for (ItemStack held : event.getEntity().getInventory().items)
                                if (held.getItem() == ItemInit.GUAM_SEED.get() && held.getCount() >= 1)
                                {
                                    xp = Math.round(11);
                                    seedsreq = 1;
                                    h.addFarmingXP(event.getEntity(), xp);
                                    ed.addxptotalxp(xp);
                                    event.getEntity().sendSystemMessage(Component.literal("You have gained Farming XP: " + xp));
                                    event.getEntity().level().setBlock(blockpos.above(), SpecialBlockInit.GUAM_CROP.get().defaultBlockState(), 2);
                                    if (!event.getEntity().isCreative())
                                    {
                                        held.shrink(seedsreq);
                                        break;
                                    }
                                }
                    }
                }
                //
                if (h.getFarmingLevel() >= 10 && event.getItemStack().getItem() == ItemInit.REDBERRY_SEED.get()
                        && !event.getEntity().isCreative()||
                        h.getFarmingLevel() >= 1 && event.getItemStack().getItem() == ItemInit.REDBERRY_SEED.get() && event.getEntity().isCreative())
                {
                    if (state.getBlock() == BlockInit.BUSH_PATCH.get()) {
                        ItemStack item = ItemInit.REDBERRY_SEED.get().getDefaultInstance();
                        if (event.getEntity().getInventory().contains(item))
                            for (ItemStack held : event.getEntity().getInventory().items)
                                if (held.getItem() == ItemInit.REDBERRY_SEED.get() && held.getCount() >= 1)
                                {
                                    xp = (int) Math.round(11.5);
                                    seedsreq = 1;
                                    h.addFarmingXP(event.getEntity(), xp);
                                    ed.addxptotalxp(xp);
                                    event.getEntity().sendSystemMessage(Component.literal("You have gained Farming XP: " + xp));
                                    event.getEntity().level().setBlock(blockpos.above(), SpecialBlockInit.REDBERRY_CROP.get().defaultBlockState(), 2);
                                    if (!event.getEntity().isCreative())
                                    {
                                        held.shrink(seedsreq);
                                        break;
                                    }
                                }
                    }
                }
                //
            });
        });
    }

    @SubscribeEvent
    //Subscribe event for planting pine cones (they're used to plant saplings instead)
    public void notenoughseeds(PlayerInteractEvent.RightClickBlock event) {
        event.getEntity().getCapability(RunicAgesFarmingCapability.Provider.FARMING_LEVEL).ifPresent(h ->
        {
                if(event.getEntity().level().isClientSide())
                {
                    return;
                }
                BlockPos blockpos = event.getPos();
                BlockState state = event.getLevel().getBlockState(blockpos);
                if (event.getLevel().isClientSide) {
                    return;
                }
                //
                if (h.getFarmingLevel() >= 3 && event.getItemStack().getItem() == ItemInit.BARLEY_SEED.get())
                {
                    if (state.getBlock() == BlockInit.HOPS_PATCH.get()) {
                        ItemStack item = ItemInit.BARLEY_SEED.get().getDefaultInstance();
                        if (event.getEntity().getInventory().contains(item))
                            for (ItemStack held : event.getEntity().getInventory().items)
                                if (held.getItem() == ItemInit.BARLEY_SEED.get() && held.getCount() <= 3 && !event.getEntity().isCreative())
                                {
                                    event.getEntity().sendSystemMessage(Component.literal("You don't have enough barley seeds."));
                                        break;
                                }
                    }
                }
                //
            if (h.getFarmingLevel() >= 5 && event.getItemStack().getItem() == ItemInit.ONION_SEED.get())
            {
                if (state.getBlock() == BlockInit.ALLOTMENT.get()) {
                    ItemStack item = ItemInit.ONION_SEED.get().getDefaultInstance();
                    if (event.getEntity().getInventory().contains(item))
                        for (ItemStack held : event.getEntity().getInventory().items)
                            if (held.getItem() == ItemInit.ONION_SEED.get() && held.getCount() <= 2 && !event.getEntity().isCreative())
                            {
                                event.getEntity().sendSystemMessage(Component.literal("You don't have enough onion seeds."));
                                break;
                            }
                }
            }
            //
            if (h.getFarmingLevel() >= 7 && event.getItemStack().getItem() == ItemInit.CABBAGE_SEED.get())
            {
                if (state.getBlock() == BlockInit.ALLOTMENT.get()) {
                    ItemStack item = ItemInit.CABBAGE_SEED.get().getDefaultInstance();
                    if (event.getEntity().getInventory().contains(item))
                        for (ItemStack held : event.getEntity().getInventory().items)
                            if (held.getItem() == ItemInit.CABBAGE_SEED.get() && held.getCount() <= 2 && !event.getEntity().isCreative())
                            {
                                event.getEntity().sendSystemMessage(Component.literal("You don't have enough cabbage seeds."));
                                break;
                            }
                }
            }
            //
        });
    }

    @SubscribeEvent
    //Subscribe event for planting pine cones (they're used to plant saplings instead)
    public void levelreqnotmet(PlayerInteractEvent.RightClickBlock event) {
        event.getEntity().getCapability(RunicAgesFarmingCapability.Provider.FARMING_LEVEL).ifPresent(h ->
        {
                if(event.getEntity().level().isClientSide())
                {
                    return;
                }
                BlockPos blockpos = event.getPos();
                BlockState state = event.getLevel().getBlockState(blockpos);
                if (event.getLevel().isClientSide) {
                    return;
                }
                //
                if (h.getFarmingLevel() <= 2 && event.getItemStack().getItem() == ItemInit.BARLEY_SEED.get() && !event.getEntity().isCreative())
                {
                    if (state.getBlock() == BlockInit.HOPS_PATCH.get()) {

                        event.getEntity().sendSystemMessage(Component.literal("You need level 3 Farming to plant barley."));

                    }
                }
                //
            if (h.getFarmingLevel() <= 4 && event.getItemStack().getItem() == ItemInit.ONION_SEED.get() && !event.getEntity().isCreative())
            {
                if (state.getBlock() == BlockInit.ALLOTMENT.get()) {

                    event.getEntity().sendSystemMessage(Component.literal("You need level 5 Farming to plant onions."));

                }
            }
            //
            if (h.getFarmingLevel() <= 6 && event.getItemStack().getItem() == ItemInit.CABBAGE_SEED.get() && !event.getEntity().isCreative())
            {
                if (state.getBlock() == BlockInit.ALLOTMENT.get()) {

                    event.getEntity().sendSystemMessage(Component.literal("You need level 7 Farming to plant cabbage."));

                }
            }
            //
            if (h.getFarmingLevel() <= 8 && event.getItemStack().getItem() == ItemInit.GUAM_SEED.get() && !event.getEntity().isCreative())
            {
                if (state.getBlock() == BlockInit.HERB_PATCH.get()) {

                    event.getEntity().sendSystemMessage(Component.literal("You need level 9 Farming to plant guam herbs."));

                }
            }
            //
            if (h.getFarmingLevel() <= 9 && event.getItemStack().getItem() == ItemInit.REDBERRY_SEED.get() && !event.getEntity().isCreative())
            {
                if (state.getBlock() == BlockInit.BUSH_PATCH.get()) {

                    event.getEntity().sendSystemMessage(Component.literal("You need level 10 Farming to plant redberries."));

                }
            }
            //
        });
    }
}
