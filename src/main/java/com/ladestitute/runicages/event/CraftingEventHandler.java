package com.ladestitute.runicages.event;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.crafting.RunicAgesCraftingCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.items.ItemHandlerHelper;

public class CraftingEventHandler {

    @SubscribeEvent
    public void craftrecipeunlock(PlayerEvent.PlayerLoggedInEvent event)
    {
        ResourceLocation[] recipe = new ResourceLocation[19];
        recipe[0] = new ResourceLocation("minecraft", "leather_helmet");
        recipe[1] = new ResourceLocation("minecraft", "leather_chestplate");
        recipe[2] = new ResourceLocation("minecraft", "leather_leggings");
        recipe[3] = new ResourceLocation("minecraft", "leather_boots");
        recipe[4] = new ResourceLocation(RunicAgesMain.MODID, "crafting/strip_of_cloth");
        recipe[5] = new ResourceLocation(RunicAgesMain.MODID, "crafting/unfired_pot");
        recipe[6] = new ResourceLocation(RunicAgesMain.MODID, "crafting/wizard_wand");
        recipe[7] = new ResourceLocation(RunicAgesMain.MODID, "crafting/gold_amulet");
        recipe[8] = new ResourceLocation(RunicAgesMain.MODID, "crafting/mold_press");
        recipe[9] = new ResourceLocation(RunicAgesMain.MODID, "crafting/spinning_wheel");
        recipe[10] = new ResourceLocation(RunicAgesMain.MODID, "crafting/sewing_hoop");
        recipe[11] = new ResourceLocation(RunicAgesMain.MODID, "crafting/chisel");
        recipe[12] = new ResourceLocation(RunicAgesMain.MODID, "crafting/needle");
        recipe[13] = new ResourceLocation(RunicAgesMain.MODID, "crafting/thread");
        recipe[14] = new ResourceLocation(RunicAgesMain.MODID, "pumpkin_pie_from_pie_dish");
        recipe[15] = new ResourceLocation(RunicAgesMain.MODID, "beetroot_soup");
        recipe[16] = new ResourceLocation(RunicAgesMain.MODID, "mushroom_stew");
        recipe[17] = new ResourceLocation(RunicAgesMain.MODID, "rabbit_stew_from_brown_mushroom");
        recipe[18] = new ResourceLocation(RunicAgesMain.MODID, "rabbit_stew_from_red_mushroom");
        event.getEntity().awardRecipesByKey(recipe);
    }

    @SubscribeEvent
    public void craftingrecipeunlocks(TickEvent.PlayerTickEvent event) {
        event.player.getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(h ->
        {
            if(h.getCraftingLevel() >= 7)
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "crafting/unfired_pie_dish");
                event.player.awardRecipesByKey(recipe);
            }
                if(h.getCraftingLevel() >= 8)
                {
                    ResourceLocation[] recipe = new ResourceLocation[1];
                    recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "crafting/unfired_clay_bowl");
                    event.player.awardRecipesByKey(recipe);
                }
            if(h.getCraftingLevel() >= 8 && RunicAgesConfig.modernrs.get())
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "crafting/gold_amulet");
                event.player.awardRecipesByKey(recipe);
            }
            if(h.getCraftingLevel() >= 27 && RunicAgesConfig.modernrs.get())
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "crafting/opal_amulet");
                event.player.awardRecipesByKey(recipe);
            }
            if(h.getCraftingLevel() >= 24 && RunicAgesConfig.modernrs.get())
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "crafting/sapphire_amulet");
                event.player.awardRecipesByKey(recipe);
            }
            if(h.getCraftingLevel() >= 1 && !RunicAgesConfig.modernrs.get())
            {
                ResourceLocation[] recipe = new ResourceLocation[3];
                recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "crafting/opal_amulet");
                recipe[1] = new ResourceLocation(RunicAgesMain.MODID, "crafting/sapphire_amulet");
                recipe[2] = new ResourceLocation(RunicAgesMain.MODID, "crafting/gold_amulet");
                event.player.awardRecipesByKey(recipe);
            }
            });

    }

    @SubscribeEvent
    public void craftinglevelup (PlayerEvent.ItemCraftedEvent event) {
        event.getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            event.getEntity().getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(h ->
            {
                if (!event.getEntity().level().isClientSide()) {
                    if (event.getCrafting().getItem() == ItemInit.STRIP_OF_CLOTH.get()) {
                        h.addCraftingXP(event.getEntity(), 1);
                        ed.addxptotalxp(1);
                        event.getEntity().sendSystemMessage(Component.literal("You have gained Crafting XP: "
                                + h.getCraftingXP() + " XP"));

                    }
                    if (event.getCrafting().getItem() == ItemInit.OPAL_AMULET.get()||
                            event.getCrafting().getItem() == ItemInit.SAPPHIRE_AMULET.get()||
                            event.getCrafting().getItem() == ItemInit.GOLD_AMULET.get()) {
                        h.addCraftingXP(event.getEntity(), 4);
                        ed.addxptotalxp(4);
                        event.getEntity().sendSystemMessage(Component.literal("You have gained Crafting XP: "
                                + h.getCraftingXP() + " XP"));

                    }
                    if (event.getCrafting().getItem() == ItemInit.UNFIRED_POT.get()) {
                        h.addCraftingXP(event.getEntity(), 6);
                        ed.addxptotalxp(6);
                        event.getEntity().sendSystemMessage(Component.literal("You have gained Crafting XP: "
                                + h.getCraftingXP() + " XP"));

                    }
                    if (event.getCrafting().getItem() == Items.LEATHER_HELMET) {
                        h.addCraftingXP(event.getEntity(), 18);
                        ed.addxptotalxp(18);
                        event.getEntity().sendSystemMessage(Component.literal("You have gained Crafting XP: "
                                + h.getCraftingXP() + " XP"));

                    }
                    if (event.getCrafting().getItem() == Items.LEATHER_CHESTPLATE) {
                        h.addCraftingXP(event.getEntity(), 27);
                        ed.addxptotalxp(27);
                        event.getEntity().sendSystemMessage(Component.literal("You have gained Crafting XP: "
                                + h.getCraftingXP() + " XP"));

                    }
                    if (event.getCrafting().getItem() == Items.LEATHER_LEGGINGS) {
                        h.addCraftingXP(event.getEntity(), 25);
                        ed.addxptotalxp(25);
                        event.getEntity().sendSystemMessage(Component.literal("You have gained Crafting XP: "
                                + h.getCraftingXP() + " XP"));

                    }
                    if (event.getCrafting().getItem() == Items.LEATHER_BOOTS) {
                        h.addCraftingXP(event.getEntity(), 16);
                        ed.addxptotalxp(16);
                        event.getEntity().sendSystemMessage(Component.literal("You have gained Crafting XP: "
                                + h.getCraftingXP() + " XP"));

                    }
                    if (event.getCrafting().getItem() == ItemInit.MAGIC_WAND.get()) {
                        h.addCraftingXP(event.getEntity(), 15);
                        ed.addxptotalxp(15);
                        event.getEntity().sendSystemMessage(Component.literal("You have gained Crafting XP: "
                                + h.getCraftingXP() + " XP"));

                    }
                }
            });
        });
    }

    @SubscribeEvent
    public void cuttinglapis(PlayerInteractEvent.RightClickItem event)
    {
        ItemStack chisel = ItemInit.CHISEL.get().getDefaultInstance();
        ItemStack handstack = event.getEntity().getItemBySlot(EquipmentSlot.MAINHAND);
        event.getEntity().getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(h ->
        {
            event.getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
            {
                if (!event.getEntity().isCrouching() && !event.getEntity().level().isClientSide()) {
                    if (h.getCraftingLevel() >= 1 && event.getEntity().getInventory().contains(chisel)
                            && handstack.getItem() == Items.LAPIS_LAZULI ||
                            h.getCraftingLevel() >= 1 && ed.getHasToolbeltChisel() == 1 && handstack.getItem() == Items.LAPIS_LAZULI &&
                                    RunicAgesConfig.qualityoflifechanges.get()) {

                        h.addCraftingXP(event.getEntity(), 20);
                        ed.addxptotalxp(20);
                        ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.CUT_LAPIS_LAZULI.get().getDefaultInstance());
                        handstack.shrink(1);

                    }


                }


        });
        });


    }
}
