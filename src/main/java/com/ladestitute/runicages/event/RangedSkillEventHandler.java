package com.ladestitute.runicages.event;

import com.ladestitute.runicages.capability.ranged.RunicAgesRangedCapability;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class RangedSkillEventHandler {


    @SubscribeEvent
    public void skilldraintimer(TickEvent.PlayerTickEvent event) {
        event.player.getCapability(RunicAgesRangedCapability.Provider.RANGED_LEVEL).ifPresent(boost ->
        {
            if(boost.getRangedBoost() >= 1)
            {
                boost.incrementrangedboostdraintimer(event.player, 1);
                if(boost.getRangedBoostTimer() >= 1200)
                {
                    boost.subRangedBoost(event.player, 1);
                    boost.setrangedboostdraintimer(0);
                }
            }
            if(boost.getRangedBoost() < 0)
            {
                boost.setRangedBoost(0);
            }
        });
    }

    @SubscribeEvent
    public void livingDamageEvent(LivingDamageEvent event) {
        //Check if is player doing the damage.
        if (event.getSource().getDirectEntity() instanceof Player) {
            event.getSource().getEntity().getCapability(RunicAgesRangedCapability.Provider.RANGED_LEVEL).ifPresent(h ->
            {
                for (ItemStack held : event.getSource().getEntity().getHandSlots()) {
                    if (held.getItem() == Items.BOW ||
                            held.getItem() == Items.CROSSBOW) {
                        event.setAmount(Math.round((float) (event.getAmount() + (0.2 * (h.getRangedLevel()+h.getRangedBoost()) ))));
                    }
                }
            });
        }
    }



}
        

