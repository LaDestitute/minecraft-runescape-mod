package com.ladestitute.runicages.event;

import com.ladestitute.runicages.registry.ItemInit;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class FoodEffectsHandler {

    @SubscribeEvent
    public void applyeffectsandcooldown(LivingEntityUseItemEvent.Finish event)

    {
        if (!(event.getEntity() instanceof Player))
        {
            return;
        }
        Player player = (Player) event.getEntity();
        if (event.getEntity() instanceof Player)
        {
            if (event.getItem().getItem() == ItemInit.BANANA.get()||
                    event.getItem().getItem() == ItemInit.ONION.get()||
                    event.getItem().getItem() == ItemInit.CABBAGE.get()) {
                if (!player.isCreative()||!player.isSpectator()) {
                    int health = (int) (player.getHealth() + 4);
                    if (player.level().isClientSide) {
                        return;
                    }
                    if (health > 20) {
                        player.setHealth(20);
                    }
                    {
                        player.setHealth(health);
                    }
                }
            }
            //
            if (event.getItem().getItem() == ItemInit.COOKED_MEAT.get()||
                    event.getItem().getItem() == ItemInit.SPIDER_ON_STICK.get()) {
                if (!player.isCreative()||!player.isSpectator()) {
                    int health = (int) (player.getHealth() + 8);
                    if (player.level().isClientSide) {
                        return;
                    }
                    if (health > 20) {
                        player.setHealth(20);
                    }
                    {
                        player.setHealth(health);
                    }
                }
            }
            //
        }
        // // //
        }
    }



