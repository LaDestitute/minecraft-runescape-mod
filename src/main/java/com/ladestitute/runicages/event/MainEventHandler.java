package com.ladestitute.runicages.event;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.crafting.RunicAgesCraftingCapability;
import com.ladestitute.runicages.capability.magic.RunicAgesMagicCapability;
import com.ladestitute.runicages.capability.mining.RunicAgesMiningCapability;
import com.ladestitute.runicages.capability.runecrafting.RunicAgesRunecraftingCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.capability.smithing.RunicAgesSmithingCapability;
import com.ladestitute.runicages.capability.woodcutting.RunicAgesWoodcuttingCapability;
import com.ladestitute.runicages.client.menu.BankMenu;
import com.ladestitute.runicages.registry.BlockInit;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.registry.VillagerInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import com.ladestitute.runicages.util.bank.BankData;
import com.ladestitute.runicages.util.bank.BankManager;
import com.ladestitute.runicages.util.bank.BankStorageEnum;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.npc.Villager;
import net.minecraft.world.entity.npc.VillagerProfession;
import net.minecraft.world.entity.npc.VillagerTrades;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.trading.MerchantOffer;
import net.minecraft.world.level.GameRules;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.common.BasicItemListing;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.village.VillagerTradesEvent;
import net.minecraftforge.event.village.WandererTradesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.network.NetworkHooks;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotResult;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

public class MainEventHandler {

    @SubscribeEvent
    public void addCustomTrades(VillagerTradesEvent event) {
        if(event.getType() == VillagerInit.WIZARD.get()) {
            Int2ObjectMap<List<VillagerTrades.ItemListing>> trades = event.getTrades();
            ItemStack air_rune = new ItemStack(ItemInit.AIR_RUNE.get(), 20);
            ItemStack mind_rune = new ItemStack(ItemInit.MIND_RUNE.get(), 20);
            ItemStack cosmic_rune = new ItemStack(ItemInit.COSMIC_RUNE.get(), 20);
            ItemStack nature_rune = new ItemStack(ItemInit.NATURE_RUNE.get(), 20);
            ItemStack staff = new ItemStack(ItemInit.STAFF.get(), 1);
            int villagerLevel = 1;
            int villagerLevel2 = 2;

            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 4),
                    air_rune,999999999,10,0.00F));
            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 4),
                    mind_rune,999999999,10,0.00F));
            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 2),
                    staff,999999999,10,0.00F));
            trades.get(villagerLevel2).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 6),
                    cosmic_rune,999999999,10,0.00F));
            trades.get(villagerLevel2).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 6),
                    nature_rune,999999999,10,0.00F));
        }

        if(event.getType() == VillagerInit.CRAFTSMAN.get()) {
            Int2ObjectMap<List<VillagerTrades.ItemListing>> trades = event.getTrades();
            ItemStack moldpack = new ItemStack(ItemInit.JEWELRY_MOLD_PACK.get(), 1);
            ItemStack leather = new ItemStack(ItemInit.SOFT_LEATHER.get(), 1);
            ItemStack hard_leather = new ItemStack(ItemInit.HARD_LEATHER.get(), 1);
            ItemStack gold_bar = new ItemStack(Items.GOLD_INGOT, 1);
            ItemStack lapis = new ItemStack(Items.LAPIS_LAZULI, 1);
            ItemStack uncut_opal = new ItemStack(ItemInit.UNCUT_OPAL.get(), 1);
            ItemStack sapphire = new ItemStack(ItemInit.SAPPHIRE.get(), 1);
            ItemStack chisel = new ItemStack(ItemInit.CHISEL.get(), 1);
            ItemStack clay = new ItemStack(Items.CLAY_BALL, 10);
            ItemStack wool = new ItemStack(Items.WHITE_WOOL, 10);
            int villagerLevel = 1;
            int villagerLevel2 = 2;

            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 1),
                    moldpack,999999999,10,0.00F));
            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(ItemInit.COWHIDE.get(), 1),
                    leather,999999999,10,0.00F));
            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(ItemInit.COWHIDE.get(), 1),
                   leather,999999999,10,0.00F));
            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(ItemInit.COWHIDE.get(), 1),
                    new ItemStack(Items.EMERALD, 1),
                    hard_leather,999999999,10,0.00F));
            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 1),
                    chisel,999999999,10,0.00F));
            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 2),
                    clay,999999999,10,0.00F));
            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 2),
                    wool,999999999,10,0.00F));
            trades.get(villagerLevel2).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 2),
                    gold_bar,999999999,10,0.00F));
            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 2),
                    lapis,999999999,10,0.00F));
            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 2),
                    uncut_opal,999999999,10,0.00F));
            trades.get(villagerLevel2).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 2),
                    sapphire,999999999,10,0.00F));
        }
        if(event.getType() == VillagerProfession.LEATHERWORKER) {
            Int2ObjectMap<List<VillagerTrades.ItemListing>> trades = event.getTrades();
            ItemStack leather = new ItemStack(ItemInit.SOFT_LEATHER.get(), 1);
            ItemStack hard_leather = new ItemStack(ItemInit.HARD_LEATHER.get(), 1);
            ItemStack gold_bar = new ItemStack(Items.GOLD_INGOT, 1);
            ItemStack lapis = new ItemStack(Items.LAPIS_LAZULI, 1);
            ItemStack uncut_opal = new ItemStack(ItemInit.UNCUT_OPAL.get(), 1);
            ItemStack sapphire = new ItemStack(ItemInit.SAPPHIRE.get(), 1);
            int villagerLevel = 1;

            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(ItemInit.COWHIDE.get(), 1),
                    leather,999999999,10,0.00F));
            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(ItemInit.COWHIDE.get(), 1),
                    new ItemStack(Items.EMERALD, 1),
                    hard_leather,999999999,10,0.00F));
        }
        if(event.getType() == VillagerInit.DRUID.get()) {
            Int2ObjectMap<List<VillagerTrades.ItemListing>> trades = event.getTrades();
            ItemStack water_vial = new ItemStack(ItemInit.VIAL_OF_WATER.get(), 1);
            ItemStack eye_of_newt = new ItemStack(ItemInit.EYE_OF_NEWT.get(), 1);
            ItemStack pestle = new ItemStack(ItemInit.PESTLE_AND_MORTAR.get(), 1);
            ItemStack waterpack = new ItemStack(ItemInit.VIAL_OF_WATER_PACK.get(), 1);
            int villagerLevel = 1;

            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 1),
                    water_vial,999999999,10,0.00F));
            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 1),
                    eye_of_newt,999999999,10,0.00F));
            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 1),
                    pestle,999999999,10,0.00F));
            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 4),
                    waterpack,999999999,10,0.00F));
        }
        if(event.getType() == VillagerInit.BANKER.get()) {
            Int2ObjectMap<List<VillagerTrades.ItemListing>> trades = event.getTrades();
            ItemStack bank_cert = new ItemStack(ItemInit.BANK_CERTIFICATE.get(), 1);
            ItemStack magic_notepaper = new ItemStack(ItemInit.MAGIC_NOTEPAPER.get(), 32);
            int villagerLevel = 1;

            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 6),
                    bank_cert,999999999,10,0.00F));
            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 2),
                    magic_notepaper,999999999,10,0.00F));
        }
        if(event.getType() == VillagerProfession.LIBRARIAN) {
            Int2ObjectMap<List<VillagerTrades.ItemListing>> trades = event.getTrades();
            ItemStack law_rune = new ItemStack(ItemInit.LAW_RUNE.get(), 20);
            int villagerLevel = 1;

            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 6),
                    law_rune,999999999,10,0.00F));
        }
        if(event.getType() == VillagerProfession.FARMER) {
            Int2ObjectMap<List<VillagerTrades.ItemListing>> trades = event.getTrades();
            ItemStack law_rune = new ItemStack(BlockInit.ALLOTMENT.get(), 3);
            int villagerLevel = 1;

            trades.get(villagerLevel).add((trader, rand) -> new MerchantOffer(
                    new ItemStack(Items.EMERALD, 2),
                    law_rune,999999999,10,0.00F));
        }
    }

    @SubscribeEvent
    public void addwanderingtrades(WandererTradesEvent event)
    {
        if(RunicAgesConfig.modernrs.get()) {
            event.getGenericTrades().add(new BasicItemListing(2, new ItemStack(ItemInit.TIN_STONE_SPIRIT.get(), 8)
                    , 999999999, 10));
            event.getGenericTrades().add(new BasicItemListing(3, new ItemStack(ItemInit.IRON_STONE_SPIRIT.get(), 8)
                    , 999999999, 10));
            event.getGenericTrades().add(new BasicItemListing(4, new ItemStack(ItemInit.SILVER_STONE_SPIRIT.get(), 8)
                    , 999999999, 10));
        }
    }

    @SubscribeEvent
    public void xptracking(TickEvent.PlayerTickEvent event) {

        RunicAgesMagicCapability.levelClientUpdate(event.player);
            event.player.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
            {
                RunicAgesExtraDataCapability.levelClientUpdate(event.player);
            });

    }

    @SubscribeEvent
    public void starterkit(PlayerEvent.PlayerLoggedInEvent event)
    {
        event.getEntity().getCapability(RunicAgesMiningCapability.Provider.MINING_LEVEL).ifPresent(cap ->
        {
            RunicAgesMiningCapability.levelClientUpdate(event.getEntity());
            event.getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
            {
                if (cap.getstarterkit() == 0) {

                    ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.BRONZE_PICKAXE.get().getDefaultInstance());
                    ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.BRONZE_HATCHET.get().getDefaultInstance());
                    ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.BRONZE_DAGGER.get().getDefaultInstance());
                    ed.resetclaybraceletcharges(35);
                    ed.setrecoilringcharges(80);
                    cap.setstarterkitobtained(1);
                    RunicAgesMiningCapability.levelClientUpdate(event.getEntity());
                }
            });
        });
        event.getEntity().getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(cap ->
        {
            RunicAgesCraftingCapability.levelClientUpdate(event.getEntity());
        });
        event.getEntity().getCapability(RunicAgesMagicCapability.Provider.MAGIC_LEVEL).ifPresent(cap ->
        {
            RunicAgesMagicCapability.levelClientUpdate(event.getEntity());
            System.out.println("MAINHAND IS: " + cap.getMainhandSpell());
        });
        event.getEntity().getCapability(RunicAgesRunecraftingCapability.Provider.RUNECRAFTING_LEVEL).ifPresent(cap ->
        {
            RunicAgesRunecraftingCapability.levelClientUpdate(event.getEntity());
        });
        event.getEntity().getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(cap ->
        {
            RunicAgesSmithingCapability.levelClientUpdate(event.getEntity());
        });
        event.getEntity().getCapability(RunicAgesWoodcuttingCapability.Provider.WOODCUTTING_LEVEL).ifPresent(cap ->
        {
            RunicAgesWoodcuttingCapability.levelClientUpdate(event.getEntity());
        });
    }

    @SubscribeEvent
    public void recipeunlock(PlayerEvent.PlayerLoggedInEvent event)
    {
        //Unlock a bunch of starter recipes and essential mc recipes to not break the game too much in difficulty
        if(RunicAgesConfig.restricthealing.get())
        {
            event.getEntity().level().getGameRules().getRule(GameRules.RULE_NATURAL_REGENERATION).set(
                    false, event.getEntity().level().getServer());
        }
        if(!RunicAgesConfig.restricthealing.get())
        {
            event.getEntity().level().getGameRules().getRule(GameRules.RULE_NATURAL_REGENERATION).set(
                    true, event.getEntity().level().getServer());
        }
               if(RunicAgesConfig.restrictrecipes.get())
               {
                   event.getEntity().level().getGameRules().getRule(GameRules.RULE_LIMITED_CRAFTING).set(
                           true, event.getEntity().level().getServer());
               }
        if(!RunicAgesConfig.restrictrecipes.get())
        {
            event.getEntity().level().getGameRules().getRule(GameRules.RULE_LIMITED_CRAFTING).set(
                    false, event.getEntity().level().getServer());
        }
                ResourceLocation[] recipe = new ResourceLocation[71];
                recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_pickaxe");
                recipe[1] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_axe");
                recipe[2] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_dagger");
                recipe[3] = new ResourceLocation(RunicAgesMain.MODID, "soft_clay");
                recipe[4] = new ResourceLocation(RunicAgesMain.MODID, "skewer_stick");
                recipe[5] = new ResourceLocation(RunicAgesMain.MODID, "strung_rabbit_foot");
                recipe[6] = new ResourceLocation(RunicAgesMain.MODID, "raw_spider_on_stick");
                recipe[7] = new ResourceLocation(RunicAgesMain.MODID, "alloy_furnace");
        recipe[8] = new ResourceLocation(RunicAgesMain.MODID, "make_normal_planks");
        recipe[9] = new ResourceLocation(RunicAgesMain.MODID, "make_birch_planks");
                recipe[10] = new ResourceLocation("minecraft", "scaffolding");
                recipe[11] = new ResourceLocation("minecraft", "item_frame");
                recipe[12] = new ResourceLocation("minecraft", "black_bed");
        recipe[13] = new ResourceLocation("minecraft", "blue_bed");
        recipe[14] = new ResourceLocation("minecraft", "brown_bed");
        recipe[15] = new ResourceLocation("minecraft", "cyan_bed");
        recipe[16] = new ResourceLocation("minecraft", "gray_bed");
        recipe[17] = new ResourceLocation("minecraft", "green_bed");
        recipe[18] = new ResourceLocation("minecraft", "light_blue_bed");
        recipe[19] = new ResourceLocation("minecraft", "light_gray_bed");
        recipe[20] = new ResourceLocation("minecraft", "lime_bed");
        recipe[21] = new ResourceLocation("minecraft", "magenta_bed");
        recipe[22] = new ResourceLocation("minecraft", "orange_bed");
        recipe[23] = new ResourceLocation("minecraft", "pink_bed");
        recipe[24] = new ResourceLocation("minecraft", "purple_bed");
        recipe[25] = new ResourceLocation("minecraft", "red_bed");
        recipe[26] = new ResourceLocation("minecraft", "white_bed");
        recipe[27] = new ResourceLocation("minecraft", "yellow_bed");
                recipe[28] = new ResourceLocation("minecraft", "campfire");
                recipe[29] = new ResourceLocation("minecraft", "barrel");
                recipe[30] = new ResourceLocation("minecraft", "chest");
                recipe[31] = new ResourceLocation("minecraft", "armor_stand");
                recipe[32] = new ResourceLocation("minecraft", "lantern");
        recipe[33] = new ResourceLocation("minecraft", "torch");
        recipe[34] = new ResourceLocation("minecraft", "ladder");
        recipe[35] = new ResourceLocation("minecraft", "oak_sign");
        recipe[36] = new ResourceLocation("minecraft", "birch_sign");
        recipe[37] = new ResourceLocation("minecraft", "crafting_table");
        recipe[38] = new ResourceLocation("minecraft", "enchanting_table");
        recipe[39] = new ResourceLocation("minecraft", "oak_door");
        recipe[40] = new ResourceLocation("minecraft", "birch_door");
        recipe[41] = new ResourceLocation("minecraft", "oak_boat");
        recipe[42] = new ResourceLocation("minecraft", "birch_boat");
        recipe[43] = new ResourceLocation("minecraft", "beetroot_soup");
        recipe[44] = new ResourceLocation("minecraft", "bread");
        recipe[45] = new ResourceLocation("minecraft", "cake");
        recipe[46] = new ResourceLocation("minecraft", "cookie");
        recipe[47] = new ResourceLocation("minecraft", "golden_carrot");
        recipe[48] = new ResourceLocation("minecraft", "golden_apple");
        recipe[49] = new ResourceLocation("minecraft", "honey_bottle");
        recipe[50] = new ResourceLocation("minecraft", "pumpkin_pie");
        recipe[51] = new ResourceLocation("minecraft", "mushroom_stew");
        recipe[52] = new ResourceLocation("minecraft", "rabbit_stew");
        recipe[53] = new ResourceLocation("minecraft", "suspicious_stew");
        recipe[54] = new ResourceLocation("minecraft", "sugar_from_sugar_cane");
        recipe[55] = new ResourceLocation("minecraft", "bucket");
        recipe[56] = new ResourceLocation("minecraft", "flint_and_steel");
        recipe[57] = new ResourceLocation("minecraft", "stone_hoe");
        recipe[58] = new ResourceLocation("minecraft", "stone_shovel");
        recipe[59] = new ResourceLocation("minecraft", "clock");
        recipe[60] = new ResourceLocation("minecraft", "compass");
        recipe[61] = new ResourceLocation("minecraft", "arrow");
        recipe[62] = new ResourceLocation("minecraft", "bow");
        recipe[63] = new ResourceLocation("minecraft", "crossbow");
        recipe[64] = new ResourceLocation("minecraft", "light_blue_dye_from_blue_orchid");
        recipe[65] = new ResourceLocation("minecraft", "light_blue_dye");
        recipe[66] = new ResourceLocation("minecraft", "bone_meal");
        recipe[67] = new ResourceLocation("minecraft", "stick");
        recipe[68] = new ResourceLocation("minecraft", "bowl");
        recipe[69] = new ResourceLocation("minecraft", "paper");
        recipe[70] = new ResourceLocation(RunicAgesMain.MODID, "magic_notepaper");
                event.getEntity().awardRecipesByKey(recipe);
    }

    @SubscribeEvent
    public void bankerregen(TickEvent.PlayerTickEvent event)
    {
        AABB axis = new AABB(event.player.getX(), event.player.getY(), event.player.getZ(), event.player.xOld, event.player.yOld, event.player.zOld).inflate(2);
        List<LivingEntity> entities = event.player.level().getEntitiesOfClass(LivingEntity.class, axis);
        for (LivingEntity living : entities) {
            {
                if(living instanceof Villager)
                {
                    if(((Villager) living).getVillagerData().getProfession().equals(VillagerInit.BANKER.get()))
                    {
                        event.player.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 600, 2));
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public void notedrecipeunlock(TickEvent.PlayerTickEvent event)
    {
        //Unlock a bunch of starter recipes and essential mc recipes to not break the game too much in difficulty
        if(event.player.getInventory().contains(ItemInit.MAGIC_NOTEPAPER.get().getDefaultInstance())) {
            ResourceLocation[] recipe = new ResourceLocation[294];
            recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_air_talisman");
            recipe[1] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_air_talisman_unnote");
            recipe[2] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_birch_log");
            recipe[3] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_birch_log_unnote");
            recipe[4] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_blurite_bar");
            recipe[5] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_blurite_bar_unnote");
            recipe[6] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_blurite_ore");
            recipe[7] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_blurite_ore_unnote");
            recipe[8] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_bar");
            recipe[9] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_bar_unnote");
            recipe[10] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_earth_talisman");
            recipe[11] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_earth_talisman_unnote");
            recipe[12] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_fire_talisman");
            recipe[13] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_fire_talisman_unnote");
            recipe[14] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_hard_clay");
            recipe[15] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_hard_clay_unnote");
            recipe[16] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_log");
            recipe[17] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_log_unnote");
            recipe[18] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_mind_talisman");
            recipe[19] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_mind_talisman_unnote");
            recipe[20] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_pure_essence");
            recipe[21] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_pure_essence_unnote");
            recipe[22] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_rune_essence");
            recipe[23] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_rune_essence_unnote");
            recipe[24] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_spider_carcass");
            recipe[25] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_spider_carcass_unnote");
            recipe[26] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_thatch_spar");
            recipe[27] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_thatch_spar_unnote");
            recipe[28] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_tin_ore");
            recipe[29] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_tin_ore_unnote");
            recipe[30] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_water_talisman");
            recipe[31] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_water_talisman_unnote");

            recipe[32] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_body_talisman");
            recipe[33] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_body_talisman_unnote");
            recipe[34] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_air_tiara");
            recipe[35] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_air_tiara_unnote");
            recipe[36] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_mind_tiara");
            recipe[37] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_mind_tiara_unnote");
            recipe[38] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_water_tiara");
            recipe[39] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_water_tiara_unnote");
            recipe[40] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_earth_tiara");
            recipe[41] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_earth_tiara_unnote");
            recipe[42] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_fire_tiara");
            recipe[43] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_fire_tiara_unnote");
            recipe[44] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_body_tiara");
            recipe[45] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_body_tiara_unnote");
            recipe[46] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_silver_ore");
            recipe[47] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_silver_ore_unnote");
            recipe[48] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_mithril_ore");
            recipe[49] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_mithril_ore_unnote");
            recipe[50] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_adamantite_ore");
            recipe[51] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_adamantite_ore_unnote");
            recipe[52] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_luminite");
            recipe[53] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_luminite_unnote");
            recipe[54] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_silver_bar");
            recipe[55] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_silver_bar_unnote");
            recipe[56] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_steel_bar");
            recipe[57] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_steel_bar_unnote");
            recipe[58] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_silvthril_bar");
            recipe[59] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_silvthril_bar_unnote");
            recipe[60] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_pickaxe");
            recipe[61] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_pickaxe_unnote");
            recipe[62] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_hatchet");
            recipe[63] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_hatchet_unnote");
            recipe[64] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_dagger");
            recipe[65] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_dagger_unnote");
            recipe[66] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_mace");
            recipe[67] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_mace_unnote");
            recipe[68] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_sword");
            recipe[69] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_sword_unnote");
            recipe[70] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_staff");
            recipe[71] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_staff_unnote");
            recipe[72] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_staff_of_air");
            recipe[73] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_staff_of_air_unnote");
            recipe[74] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_wizard_wand");
            recipe[75] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_wizard_wand_unnote");
            recipe[76] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bow");
            recipe[77] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bow_unnote");
            recipe[78] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_crossbow");
            recipe[79] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_crossbow_unnote");
            recipe[80] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_helm");
            recipe[81] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_helm_unnote");
            recipe[82] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_chainbody");
            recipe[83] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_chainbody_unnote");
            recipe[84] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_platebody");
            recipe[85] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_platebody_unnote");
            recipe[86] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_platelegs");
            recipe[87] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_platelegs_unnote");
            recipe[88] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_sq_shield");
            recipe[89] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_sq_shield_unnote");
            recipe[90] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_wizard_hat");
            recipe[91] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_wizard_hat_unnote");
            recipe[92] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_wizard_robe_top");
            recipe[93] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_wizard_robe_top_unnote");
            recipe[94] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_wizard_robe_skirt");
            recipe[95] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_wizard_robe_skirt_unnote");
            recipe[96] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_wizard_boots");
            recipe[97] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_wizard_boots_unnote");
            recipe[98] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_leather_cap");
            recipe[99] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_leather_cap_unnote");
            recipe[100] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_leather_tunic");
            recipe[101] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_leather_tunic_unnote");
            recipe[102] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_leather_pants");
            recipe[103] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_leather_pants_unnote");
            recipe[104] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_leather_boots");
            recipe[105] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_leather_boots_unnote");
            recipe[106] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_grimy_guam");
            recipe[107] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_grimy_guam_unnote");
            recipe[108] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_grimy_tarromin");
            recipe[109] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_grimy_tarromin_unnote");
            recipe[110] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_grimy_marrentill");
            recipe[111] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_grimy_marrentill_unnote");
            recipe[112] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_clean_guam");
            recipe[113] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_clean_guam_unnote");
            recipe[114] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_clean_tarromin");
            recipe[115] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_clean_tarromin_unnote");
            recipe[116] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_clean_marrentill");
            recipe[117] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_clean_marrentill_unnote");
            recipe[118] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_guam_potion_unf");
            recipe[119] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_guam_potion_unf_unnote");
            recipe[120] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_tarromin_potion_unf");
            recipe[121] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_tarromin_potion_unf_unnote");
            recipe[123] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_marrentill_potion_unf");
            recipe[124] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_marrentill_potion_unf_unnote");
            recipe[125] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_eye_of_newt");
            recipe[126] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_eye_of_newt_unnote");
            recipe[127] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bear_fur");
            recipe[128] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bear_fur_unnote");
            recipe[129] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_unicorn_horn");
            recipe[130] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_unicorn_horn_unnote");
            recipe[131] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_unicorn_horn_dust");
            recipe[132] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_unicorn_horn_dust_unnote");
            recipe[133] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_black_bead");
            recipe[134] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_black_bead_unnote");
            recipe[135] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_white_bead");
            recipe[136] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_white_bead_unnote");
            recipe[137] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_red_bead");
            recipe[138] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_red_bead_unnote");
            recipe[139] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_yellow_bead");
            recipe[140] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_yellow_bead_unnote");
            recipe[141] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_onion");
            recipe[142] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_onion_unnote");
            recipe[143] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_cabbage");
            recipe[144] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_cabbage_unnote");
            recipe[145] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_barley");
            recipe[146] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_barley_unnote");
            recipe[147] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_redberries");
            recipe[148] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_redberries_unnote");
            recipe[149] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_flax");
            recipe[150] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_flax_unnote");
            recipe[151] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_chisel");
            recipe[152] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_chisel_unnote");
            recipe[153] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_pestle_and_mortar");
            recipe[154] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_pestle_and_mortar_unnote");
            recipe[155] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_needle");
            recipe[156] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_needle_unnote");
            recipe[157] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_ring_mold");
            recipe[158] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_ring_mold_unnote");
            recipe[159] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_amulet_mold");
            recipe[160] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_amulet_mold_unnote");
            recipe[161] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_necklace_mold");
            recipe[162] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_necklace_mold_unnote");
            recipe[163] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bracelet_mold");
            recipe[164] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bracelet_mold_unnote");
            recipe[165] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_tiara_mold");
            recipe[166] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_tiara_mold_unnote");
            recipe[167] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_cowhide");
            recipe[168] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_cowhide_unnote");
            recipe[169] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_leather");
            recipe[170] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_leather_unnote");
            recipe[171] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_hard_leather");
            recipe[172] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_hard_leather_unnote");
            recipe[173] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_uncut_opal");
            recipe[174] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_uncut_opal_unnote");
            recipe[175] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_uncut_sapphire");
            recipe[176] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_uncut_sapphire_unnote");
            recipe[177] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_cut_lapis_lazuli");
            recipe[178] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_cut_lapis_lazuli_unnote");
            recipe[179] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_opal");
            recipe[180] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_opal_unnote");
            recipe[181] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_sapphire");
            recipe[182] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_sapphire_unnote");
            recipe[183] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_ball_of_wool");
            recipe[184] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_ball_of_wool_unnote");
            recipe[185] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_strip_of_cloth");
            recipe[186] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_strip_of_cloth_unnote");
            recipe[187] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_unfired_pot");
            recipe[188] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_unfired_pot_unnote");
            recipe[189] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_pot");
            recipe[190] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_pot_unnote");
            recipe[191] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_unfired_pie_dish");
            recipe[192] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_unfired_pie_dish_unnote");
            recipe[193] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_pie_dish");
            recipe[194] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_pie_dish_unnote");
            recipe[195] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_unfired_bowl");
            recipe[196] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_unfired_bowl_unnote");
            recipe[197] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bowl");
            recipe[198] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bowl_unnote");
            recipe[199] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_silver_ring_mold");
            recipe[200] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_silver_ring_mold_unnote");
            recipe[201] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_tiara_mold");
            recipe[202] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_tiara_mold_unnote");
            recipe[203] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_opal_ring_mold");
            recipe[204] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_opal_ring_mold_unnote");
            recipe[205] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_opal_amulet_mold");
            recipe[206] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_opal_amulet_mold_unnote");
            recipe[207] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_lapis_lazuli_ring_mold");
            recipe[208] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_lapis_lazuli_ring_mold_unnote");
            recipe[209] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_gold_ring_mold");
            recipe[210] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_gold_ring_mold_unnote");
            recipe[211] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_gold_amulet_mold");
            recipe[212] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_gold_amulet_mold_unnote");
            recipe[213] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_gold_necklace_mold");
            recipe[214] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_gold_necklace_mold_unnote");
            recipe[215] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_gold_bracelet_mold");
            recipe[216] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_gold_bracelet_mold_unnote");
            recipe[217] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_sapphire_ring_mold");
            recipe[218] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_sapphire_ring_mold_unnote");
            recipe[219] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_sapphire_amulet_mold");
            recipe[220] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_sapphire_amulet_mold_unnote");
            recipe[221] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_sapphire_bracelet_mold");
            recipe[222] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_filled_sapphire_bracelet_mold_unnote");
            recipe[223] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_silver_ring");
            recipe[224] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_silver_ring_unnote");
            recipe[225] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_tiara");
            recipe[226] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_tiara_unnote");
            recipe[227] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_opal_ring");
            recipe[228] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_opal_ring_unnote");
            recipe[229] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_unstrung_opal_amulet");
            recipe[230] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_unstrung_opal_amulet_unnote");
            recipe[231] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_opal_amulet");
            recipe[232] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_opal_amulet_unnote");
            recipe[233] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_lapis_lazuli_ring");
            recipe[234] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_lapis_lazuli_ring_unnote");
            recipe[235] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_gold_ring");
            recipe[236] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_gold_ring_unnote");
            recipe[237] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_unstrung_gold_amulet");
            recipe[238] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_unstrung_gold_amulet_unnote");
            recipe[239] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_gold_amulet");
            recipe[240] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_gold_amulet_unnote");
            recipe[241] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_gold_necklace");
            recipe[242] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_gold_necklace_unnote");
            recipe[243] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_gold_bracelet");
            recipe[244] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_gold_bracelet_unnote");
            recipe[245] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_sapphire_ring");
            recipe[246] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_sapphire_ring_unnote");
            recipe[247] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_unstrung_sapphire_amulet");
            recipe[248] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_unstrung_sapphire_amulet_unnote");
            recipe[249] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_sapphire_amulet");
            recipe[250] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_sapphire_amulet_unnote");
            recipe[251] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_sapphire_bracelet");
            recipe[252] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_sapphire_bracelet_unnote");
            recipe[253] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_amulet_of_bountiful_harvest");
            recipe[254] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_amulet_of_bountiful_harvest_unnote");
            recipe[255] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_amulet_of_bounty");
            recipe[256] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_amulet_of_bounty_unnote");
            recipe[257] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_ring_of_luck");
            recipe[258] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_ring_of_luck_unnote");
            recipe[259] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_ring_of_recoil");
            recipe[260] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_ring_of_recoil_unnote");
            recipe[261] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_amulet_of_magic");
            recipe[262] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_amulet_of_magic_unnote");
            recipe[263] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bracelet_of_clay");
            recipe[264] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bracelet_of_clay_unnote");
            recipe[265] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_iron_pickaxe");
            recipe[266] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_iron_pickaxe_unnote");
            recipe[267] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_iron_axe");
            recipe[268] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_iron_axe_unnote");
            recipe[269] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_iron_dagger");
            recipe[270] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_iron_dagger_unnote");
            recipe[271] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bowstring");
            recipe[272] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bowstring_unnote");
            recipe[273] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_raw_bear_meat");
            recipe[274] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_raw_bear_meat_unnote");
            recipe[275] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_cooked_meat");
            recipe[276] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_cooked_meat_unnote");
            recipe[277] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bowl_of_water");
            recipe[278] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bowl_of_water_unnote");
            recipe[279] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_boots");
            recipe[280] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bronze_boots_unnote");
            recipe[281] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_banana");
            recipe[282] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_banana_unnote");
            recipe[283] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_blurite_sword");
            recipe[284] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_blurite_sword_unnote");
            recipe[285] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_strung_rabbit_foot");
            recipe[286] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_strung_rabbit_foot_unnote");
            recipe[287] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bird_nest");
            recipe[288] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_bird_nest_unnote");
            recipe[289] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_egg_bird_nest");
            recipe[290] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_egg_bird_nest_unnote");
            recipe[291] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_seeds_bird_nest");
            recipe[293] = new ResourceLocation(RunicAgesMain.MODID, "noted/noted_seeds_bird_nest_unnote");
            //don't forget misc items (mob drops? bowstring? missing ingots? bowl of water?)
            event.player.awardRecipesByKey(recipe);
        }
    }

    public static BankData getData(ItemStack stack) {
        UUID uuid;
        CompoundTag tag = stack.getOrCreateTag();
        if (!tag.contains("UUID")) {
            uuid = UUID.randomUUID();
            tag.putUUID("UUID", uuid);
        } else
            uuid = tag.getUUID("UUID");
        return BankManager.get().getOrCreateBackpack(uuid, BankStorageEnum.BANK);
    }



    @SubscribeEvent
    public void openbank(PlayerInteractEvent.EntityInteract event)
    {
        if(!event.getEntity().level().isClientSide() && event.getEntity().isCrouching() && event.getTarget() instanceof Villager && ((Villager) event.getTarget()).getVillagerData().getProfession().equals(VillagerInit.BANKER.get()))
        {
            ItemStack certstack = event.getEntity().getItemBySlot(EquipmentSlot.MAINHAND);
            UUID uuid = event.getEntity().getUUID();
            ItemStack backpack = event.getEntity().getItemInHand(event.getEntity().getUsedItemHand());
            BankData data = getData(backpack);
            if(certstack.getItem() == ItemInit.BANK_CERTIFICATE.get()) {
                NetworkHooks.openScreen(((ServerPlayer) event.getEntity()), new
                        SimpleMenuProvider((windowId, playerInventory, playerEntity) ->
                        new BankMenu(windowId, playerInventory, uuid, BankStorageEnum.BANK, data.getHandler()),
                        backpack.getHoverName()), (buffer -> buffer.writeUUID(uuid).writeInt(BankStorageEnum.BANK.ordinal())));
            }
        }
    }


    @SubscribeEvent
    public static void ringofrecoil(LivingHurtEvent event)
    {
        event.getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            Entity source = event.getSource().getDirectEntity();
            ItemStack stack =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_RECOIL.get(), event.getEntity()).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            if (!stack.isEmpty() && source instanceof Monster) {
                if(event.getAmount() < 10) {
                    source.hurt(source.damageSources().generic(), 1);
                }
                if(event.getAmount() >= 10) {
                    source.hurt(source.damageSources().generic(), Math.round(event.getAmount() / 10));
                }
                ed.decreaserecoilringcharges(1);
                RunicAgesExtraDataCapability.levelClientUpdate((Player) source);
            }
            if(ed.getRecoilRingCharges() == 0)
            {
                stack.shrink(1);
                ed.setrecoilringcharges(80);
                RunicAgesExtraDataCapability.levelClientUpdate((Player) source);
            }
        });
    }


    @SubscribeEvent
    public void getwaterfromsource(PlayerInteractEvent.RightClickBlock event)
    {
                Player player = event.getEntity();
                BlockPos pos = event.getPos();
                Level level = player.level();
                Block block = level.getBlockState(pos).getBlock();
                ItemStack handstack = player.getItemBySlot(EquipmentSlot.MAINHAND);
                if (handstack.getItem() == ItemInit.CLAY_BOWL.get()) {
                    if (block == Blocks.WATER_CAULDRON) {
                        event.getLevel().setBlockAndUpdate(event.getPos(), Blocks.CAULDRON.defaultBlockState());
                        handstack.shrink(1);
                        ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.BOWL_OF_WATER.get().getDefaultInstance());
                    }
                }
        if (handstack.getItem() == ItemInit.BOWL_OF_WATER.get()) {
            if (block == Blocks.CAULDRON) {
                event.getLevel().setBlockAndUpdate(event.getPos(), Blocks.WATER_CAULDRON.defaultBlockState());
                handstack.shrink(1);
                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.CLAY_BOWL.get().getDefaultInstance());
            }
            if (block != Blocks.AIR||
                    block != Blocks.WATER) {
                event.getLevel().setBlockAndUpdate(event.getPos().above(), Blocks.WATER.defaultBlockState());
                handstack.shrink(1);
                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.CLAY_BOWL.get().getDefaultInstance());
            }
        }
    }

}
