package com.ladestitute.runicages.event.combatxp;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.attack.RunicAgesAttackCapability;
import com.ladestitute.runicages.capability.defense.RunicAgesDefenseCapability;
import com.ladestitute.runicages.capability.magic.RunicAgesMagicCapability;
import com.ladestitute.runicages.capability.ranged.RunicAgesRangedCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.capability.strength.RunicAgesStrengthCapability;
import com.ladestitute.runicages.entities.AirStrikeEntity;
import com.ladestitute.runicages.entities.EarthStrikeEntity;
import com.ladestitute.runicages.entities.FireStrikeEntity;
import com.ladestitute.runicages.entities.WaterStrikeEntity;
import com.ladestitute.runicages.entities.mobs.*;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.damagesource.DamageTypes;
import net.minecraft.world.entity.ambient.Bat;
import net.minecraft.world.entity.animal.*;
import net.minecraft.world.entity.animal.frog.Frog;
import net.minecraft.world.entity.monster.*;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.biome.Biome;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;

public class MeleeXPEventHandler {

    @SubscribeEvent
    public void splitmeleexp(LivingDeathEvent event) {
        if (event.getSource().getEntity() instanceof Player)
            event.getSource().getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
                    event.getSource().getEntity().getCapability(RunicAgesAttackCapability.Provider.ATTACK_LEVEL).ifPresent(a ->
                            event.getSource().getEntity().getCapability(RunicAgesStrengthCapability.Provider.STRENGTH_LEVEL).ifPresent(s ->
                                    event.getSource().getEntity().getCapability(RunicAgesDefenseCapability.Provider.DEFENSE_LEVEL).ifPresent(d ->
                                    {
                                        for (ItemStack held : event.getSource().getEntity().getHandSlots())
                                        {
                                            if (held.getItem() == Items.WOODEN_AXE ||
                                                    held.getItem() == Items.WOODEN_SWORD ||
                                                    held.getItem() == Items.WOODEN_PICKAXE ||
                                                    held.getItem() == Items.WOODEN_SHOVEL ||
                                                    held.getItem() == Items.WOODEN_HOE ||
                                                    held.getItem() == Items.STONE_SWORD ||
                                                    held.getItem() == Items.STONE_AXE ||
                                                    held.getItem() == Items.STONE_PICKAXE ||
                                                    held.getItem() == Items.STONE_HOE ||
                                                    held.getItem() == Items.STONE_SHOVEL ||
                                                    held.getItem() == ItemInit.BRONZE_HATCHET.get() ||
                                                    held.getItem() == ItemInit.BRONZE_DAGGER.get() ||
                                                    held.getItem() == ItemInit.BRONZE_MACE.get() ||
                                                    held.getItem() == ItemInit.BRONZE_SWORD.get() ||
                                                    held.getItem() == ItemInit.BRONZE_PICKAXE.get() ||
                                                    held.getItem() == Items.GOLDEN_AXE ||
                                                    held.getItem() == Items.GOLDEN_SWORD ||
                                                    held.getItem() == Items.GOLDEN_PICKAXE ||
                                                    held.getItem() == Items.GOLDEN_SHOVEL ||
                                                    held.getItem() == Items.GOLDEN_HOE ||
                                                    held.getItem() == Items.IRON_SWORD ||
                                                    held.getItem() == Items.IRON_AXE ||
                                                    held.getItem() == Items.IRON_PICKAXE ||
                                                    held.getItem() == Items.IRON_HOE ||
                                                    held.getItem() == Items.IRON_SHOVEL ||
                                                    held.getItem() == ItemInit.IRON_DAGGER.get()||
                                                    held.getItem() == Items.TRIDENT ||
                                                    held.getItem() == Items.SHIELD ||
                                                    held.getItem() == ItemInit.BRONZE_SQ_SHIELD.get()) {
                                                Player player = (Player) event.getSource().getEntity();
                                                int xp;
                                                if (event.getEntity() instanceof ImpEntity) {
                                                    xp = 10;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                }
                                                if (event.getEntity() instanceof Bat) {
                                                    xp = 32;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                    RunicAgesConfig.receivestrengthxp.get() &&
                                                    RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                }
                                                if (event.getEntity() instanceof Fox) {
                                                    xp = 72;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                }
                                                if (event.getEntity() instanceof Frog) {
                                                    xp = 35;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                }
                                                if (event.getEntity() instanceof Wolf) {
                                                    xp = 27;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                }
                                                if (event.getEntity() instanceof Husk ||
                                                        event.getEntity() instanceof Stray) {
                                                    xp = 77;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                }
                                                if (event.getEntity() instanceof Zombie ||
                                                        event.getEntity() instanceof Drowned ||
                                                        event.getEntity() instanceof ZombieVillager) {
                                                    xp = 47;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                }
                                                if (event.getEntity() instanceof Skeleton) {
                                                    xp = 53;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                }
                                                if (event.getEntity() instanceof Slime) {
                                                    xp = 42;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                }
                                                if (event.getEntity() instanceof IronGolem) {
                                                    xp = 48;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                }
                                                if (event.getEntity() instanceof PolarBear) {
                                                    xp = 50;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                }
                                                if (event.getEntity() instanceof MushroomCow) {
                                                    xp = 63;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                }
                                                //
                                                if (event.getEntity() instanceof BlackBearEntity) {
                                                    xp = (int) 72.5;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                }
                                                //
                                                if (event.getEntity() instanceof DeadlyRedSpiderEntity) {
                                                    xp = (int) 307.5;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                }
                                                //
                                                if (event.getEntity() instanceof UnicornEntity) {
                                                    xp = (int) 52.5;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                }
                                                //
                                                if (event.getEntity() instanceof BlackBearEntity) {
                                                    xp = (int) 72.5;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                }
                                                //
                                                if (event.getEntity() instanceof GrizzlyBearEntity) {
                                                    xp = (int) 93.75;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                }
                                                //
                                                if (event.getEntity() instanceof Spider || event.getEntity() instanceof CaveSpider) {
                                                    xp = 37;
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                    }
                                                    if(RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                    }
                                                    if(RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            RunicAgesConfig.receivestrengthxp.get() &&
                                                            !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                    }
                                                    if(!RunicAgesConfig.receiveattackxp.get() &&
                                                            !RunicAgesConfig.receivestrengthxp.get() &&
                                                            RunicAgesConfig.receivemeleedefensexp.get()) {
                                                        d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                        ed.addxptotalxp(xp);
                                                        event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                    }
                                                } else
                                                    xp = 25;
                                                if(RunicAgesConfig.receiveattackxp.get() &&
                                                        RunicAgesConfig.receivestrengthxp.get() &&
                                                        RunicAgesConfig.receivemeleedefensexp.get()) {
                                                    a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                    s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/3));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/3)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/3)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/3)));
                                                }
                                                else if(RunicAgesConfig.receiveattackxp.get() &&
                                                        RunicAgesConfig.receivestrengthxp.get()) {
                                                    a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                }
                                                else if(RunicAgesConfig.receiveattackxp.get() &&
                                                        RunicAgesConfig.receivemeleedefensexp.get()) {
                                                    a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/23)));
                                                }
                                                else if(RunicAgesConfig.receivestrengthxp.get() &&
                                                        RunicAgesConfig.receivemeleedefensexp.get()) {
                                                    s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                else if(RunicAgesConfig.receiveattackxp.get() &&
                                                        !RunicAgesConfig.receivestrengthxp.get() &&
                                                        !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                    a.addAttackXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Attack XP: " + Math.round(xp)));
                                                }
                                                else if(!RunicAgesConfig.receiveattackxp.get() &&
                                                        RunicAgesConfig.receivestrengthxp.get() &&
                                                        !RunicAgesConfig.receivemeleedefensexp.get()) {
                                                    s.addStrengthXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Strength XP: " + Math.round(xp)));
                                                }
                                                else if(!RunicAgesConfig.receiveattackxp.get() &&
                                                        !RunicAgesConfig.receivestrengthxp.get() &&
                                                        RunicAgesConfig.receivemeleedefensexp.get()) {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp)));
                                                }
                                            }
                                            break;
                                        }
                                    }))));
    }
    //

}
