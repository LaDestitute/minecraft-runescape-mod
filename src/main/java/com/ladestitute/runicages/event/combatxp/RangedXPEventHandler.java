package com.ladestitute.runicages.event.combatxp;

import com.ladestitute.runicages.capability.defense.RunicAgesDefenseCapability;
import com.ladestitute.runicages.capability.ranged.RunicAgesRangedCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.entities.mobs.BlackBearEntity;
import com.ladestitute.runicages.entities.mobs.GrizzlyBearEntity;
import com.ladestitute.runicages.entities.mobs.ImpEntity;
import com.ladestitute.runicages.entities.mobs.UnicornEntity;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.ambient.Bat;
import net.minecraft.world.entity.animal.*;
import net.minecraft.world.entity.animal.frog.Frog;
import net.minecraft.world.entity.monster.*;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class RangedXPEventHandler {

    @SubscribeEvent
    public void splitrangedxp(LivingDeathEvent event) {
        if (event.getSource().getEntity() instanceof Player)
            event.getSource().getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
                    event.getSource().getEntity().getCapability(RunicAgesRangedCapability.Provider.RANGED_LEVEL).ifPresent(r ->
                            event.getSource().getEntity().getCapability(RunicAgesDefenseCapability.Provider.DEFENSE_LEVEL).ifPresent(d ->
                            {
                                for (ItemStack held : event.getSource().getEntity().getHandSlots())
                                {
                                    if (held.getItem() == Items.BOW ||
                                            held.getItem() == Items.CROSSBOW)
                                    {
                                        int xp;
                                        if (event.getEntity() instanceof ImpEntity) {
                                            xp = 10;
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + Math.round(xp/2)));
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                            }
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && !RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + xp));
                                            }
                                            if(!RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                            }
                                        }
                                        if (event.getEntity() instanceof Bat) {
                                            xp = 32;
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + Math.round(xp/2)));
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                            }
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && !RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + xp));
                                            }
                                            if(!RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                            }
                                        }
                                        if (event.getEntity() instanceof Fox) {
                                            xp = 72;
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + Math.round(xp/2)));
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                            }
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && !RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + xp));
                                            }
                                            if(!RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                            }
                                        }
                                        if (event.getEntity() instanceof Frog) {
                                            xp = 35;
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + Math.round(xp/2)));
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                            }
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && !RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + xp));
                                            }
                                            if(!RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                            }
                                        }
                                        if (event.getEntity() instanceof Wolf) {
                                            xp = 27;
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + Math.round(xp/2)));
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                            }
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && !RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + xp));
                                            }
                                            if(!RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                            }
                                        }
                                        if (event.getEntity() instanceof Husk ||
                                                event.getEntity() instanceof Stray) {
                                            xp = 77;
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + Math.round(xp/2)));
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                            }
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && !RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + xp));
                                            }
                                            if(!RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                            }
                                        }
                                        if (event.getEntity() instanceof Zombie ||
                                                event.getEntity() instanceof Drowned ||
                                                event.getEntity() instanceof ZombieVillager)
                                        {
                                            xp = 47;
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + Math.round(xp/2)));
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                            }
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && !RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + xp));
                                            }
                                            if(!RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                            }
                                        }
                                        if (event.getEntity() instanceof Skeleton) {
                                            xp = 53;
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + Math.round(xp/2)));
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                            }
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && !RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + xp));
                                            }
                                            if(!RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                            }
                                        }
                                        if (event.getEntity() instanceof Slime) {
                                            xp = 42;
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + Math.round(xp/2)));
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                            }
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && !RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + xp));
                                            }
                                            if(!RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                            }
                                        }
                                        if (event.getEntity() instanceof IronGolem) {
                                            xp = 48;
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + Math.round(xp/2)));
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                            }
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && !RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + xp));
                                            }
                                            if(!RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                            }
                                        }
                                        if (event.getEntity() instanceof PolarBear) {
                                            xp = 50;
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + Math.round(xp/2)));
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                            }
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && !RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + xp));
                                            }
                                            if(!RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                            }
                                        }
                                        //
                                        if (event.getEntity() instanceof UnicornEntity) {
                                            xp = (int) 52.5;
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + Math.round(xp/2)));
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                            }
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && !RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + xp));
                                            }
                                            if(!RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                            }
                                        }
                                        //
                                        if (event.getEntity() instanceof BlackBearEntity) {
                                            xp = (int) 72.5;
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + Math.round(xp/2)));
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                            }
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && !RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + xp));
                                            }
                                            if(!RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                            }
                                        }
                                        //
                                        if (event.getEntity() instanceof GrizzlyBearEntity) {
                                            xp = (int) 93.75;
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + Math.round(xp/2)));
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                            }
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && !RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + xp));
                                            }
                                            if(!RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                            }
                                        }
                                        //
                                        if (event.getEntity() instanceof MushroomCow) {
                                            xp = 63;
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + Math.round(xp/2)));
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                            }
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && !RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + xp));
                                            }
                                            if(!RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                            }
                                        }
                                        if (event.getEntity() instanceof Spider || event.getEntity() instanceof CaveSpider) {
                                            xp = 37;
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + Math.round(xp/2)));
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                            }
                                            if(RunicAgesConfig.receiverangedxp.get()
                                                    && !RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                r.addRangedXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + xp));
                                            }
                                            if(!RunicAgesConfig.receiverangedxp.get()
                                                    && RunicAgesConfig.receiverangeddefensexp.get())
                                            {
                                                d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                            }
                                        }
                                        else xp = 25;
                                        if(RunicAgesConfig.receiverangedxp.get()
                                                && RunicAgesConfig.receiverangeddefensexp.get())
                                        {
                                            r.addRangedXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                            d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                            ed.addxptotalxp(xp);
                                            event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + Math.round(xp/2)));
                                            event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                        }
                                        if(RunicAgesConfig.receiverangedxp.get()
                                                && !RunicAgesConfig.receiverangeddefensexp.get())
                                        {
                                            r.addRangedXP((Player) event.getSource().getEntity(), xp);
                                            ed.addxptotalxp(xp);
                                            event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Ranged XP: " + xp));
                                        }
                                        if(!RunicAgesConfig.receiverangedxp.get()
                                                && RunicAgesConfig.receiverangeddefensexp.get())
                                        {
                                            d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                            ed.addxptotalxp(xp);
                                            event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                        }
                                    }
                                    break;
                                }

                            })));
    }
    //
}
