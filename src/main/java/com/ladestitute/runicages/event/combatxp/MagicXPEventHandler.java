package com.ladestitute.runicages.event.combatxp;

import com.ladestitute.runicages.capability.defense.RunicAgesDefenseCapability;
import com.ladestitute.runicages.capability.magic.RunicAgesMagicCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.entities.AirStrikeEntity;
import com.ladestitute.runicages.entities.EarthStrikeEntity;
import com.ladestitute.runicages.entities.FireStrikeEntity;
import com.ladestitute.runicages.entities.WaterStrikeEntity;
import com.ladestitute.runicages.entities.mobs.*;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.ambient.Bat;
import net.minecraft.world.entity.animal.*;
import net.minecraft.world.entity.animal.frog.Frog;
import net.minecraft.world.entity.monster.*;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;

public class MagicXPEventHandler {

    @SubscribeEvent
    public void splitmagicxp(LivingDeathEvent event) {
        if (event.getSource().getEntity() instanceof Player)
            event.getSource().getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
                    event.getSource().getEntity().getCapability(RunicAgesMagicCapability.Provider.MAGIC_LEVEL).ifPresent(m ->
                            event.getSource().getEntity().getCapability(RunicAgesDefenseCapability.Provider.DEFENSE_LEVEL).ifPresent(d ->
                            {
                                DamageSource source = event.getSource();
                                if (source.getDirectEntity() instanceof AirStrikeEntity ||
                                        source.getDirectEntity() instanceof WaterStrikeEntity ||
                                        source.getDirectEntity() instanceof EarthStrikeEntity ||
                                        source.getDirectEntity() instanceof FireStrikeEntity) {
                                    for (ItemStack held : event.getSource().getEntity().getHandSlots()) {

                                        if (held.getItem() == ItemInit.STAFF.get() ||
                                                held.getItem() == ItemInit.MAGIC_WAND.get()||
                                                held.getItem() == ItemInit.STAFF_OF_AIR.get()) {
                                            int xp;
                                            if (event.getEntity() instanceof ImpEntity) {
                                                xp = 10;
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && !RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                                }
                                                if(!RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                                }
                                            }
                                            if (event.getEntity() instanceof Bat) {
                                                xp = 32;
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && !RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                                }
                                                if(!RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                                }
                                            }
                                            if (event.getEntity() instanceof Fox) {
                                                xp = 72;
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && !RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                                }
                                                if(!RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                                }
                                            }
                                            if (event.getEntity() instanceof Frog) {
                                                xp = 35;
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && !RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                                }
                                                if(!RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                                }
                                            }
                                            if (event.getEntity() instanceof Wolf) {
                                                xp = 27;
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && !RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                                }
                                                if(!RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                                }
                                            }
                                            if (event.getEntity() instanceof Husk ||
                                                    event.getEntity() instanceof Stray) {
                                                xp = 77;
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && !RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                                }
                                                if(!RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                                }
                                            }
                                            if (event.getEntity() instanceof Zombie ||
                                                    event.getEntity() instanceof Drowned ||
                                                    event.getEntity() instanceof ZombieVillager) {
                                                xp = 47;
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && !RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                                }
                                                if(!RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                                }
                                            }
                                            if (event.getEntity() instanceof Skeleton) {
                                                xp = 53;
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && !RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                                }
                                                if(!RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                                }
                                            }
                                            if (event.getEntity() instanceof Slime) {
                                                xp = 42;
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && !RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                                }
                                                if(!RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                                }
                                            }
                                            if (event.getEntity() instanceof IronGolem) {
                                                xp = 48;
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && !RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                                }
                                                if(!RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                                }
                                            }
                                            if (event.getEntity() instanceof PolarBear) {
                                                xp = 50;
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && !RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                                }
                                                if(!RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                                }
                                            }
                                            //
                                            if (event.getEntity() instanceof GrizzlyBearEntity) {
                                                xp = (int) 93.75;
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && !RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                                }
                                                if(!RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                                }
                                            }
                                            //
                                            if (event.getEntity() instanceof BlackBearEntity) {
                                                xp = (int) 72.5;
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && !RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                                }
                                                if(!RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                                }
                                            }
                                            //
                                            if (event.getEntity() instanceof DeadlyRedSpiderEntity) {
                                                xp = (int) 153.75;
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && !RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                                }
                                                if(!RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                                }
                                            }
                                            //
                                            if (event.getEntity() instanceof UnicornEntity) {
                                                xp = (int) 52.5;
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && !RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                                }
                                                if(!RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                                }
                                            }
                                            //
                                            if (event.getEntity() instanceof MushroomCow) {
                                                xp = 63;
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && !RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                                }
                                                if(!RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                                }
                                            }
                                            //
                                            if (event.getEntity() instanceof Spider || event.getEntity() instanceof CaveSpider) {
                                                xp = 37;
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                                }
                                                if(RunicAgesConfig.receivemagicxp.get()
                                                        && !RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                                }
                                                if(!RunicAgesConfig.receivemagicxp.get()
                                                        && RunicAgesConfig.receivemagicdefensexp.get())
                                                {
                                                    d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                    ed.addxptotalxp(xp);
                                                    event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                                }
                                            }
                                            else
                                                xp = 25;
                                            if(RunicAgesConfig.receivemagicxp.get()
                                                    && RunicAgesConfig.receivemagicdefensexp.get())
                                            {
                                                m.addMagicXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                d.addDefenseXP((Player) event.getSource().getEntity(), Math.round(xp/2));
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + Math.round(xp/2)));
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + Math.round(xp/2)));
                                            }
                                            if(RunicAgesConfig.receivemagicxp.get()
                                                    && !RunicAgesConfig.receivemagicdefensexp.get())
                                            {
                                                m.addMagicXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                            }
                                            if(!RunicAgesConfig.receivemagicxp.get()
                                                    && RunicAgesConfig.receivemagicdefensexp.get())
                                            {
                                                d.addDefenseXP((Player) event.getSource().getEntity(), xp);
                                                ed.addxptotalxp(xp);
                                                event.getSource().getEntity().sendSystemMessage(Component.literal("You have gained Defense XP: " + xp));
                                            }

                                        }
                                        break;
                                    }
                                }
                            })));
    }

    @SubscribeEvent
    public void magicdamage(LivingDamageEvent event) {
        //Check if is player doing the damage.
        if (event.getSource().getDirectEntity() instanceof Player) {
            ItemStack magic_amulet =
                    CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.AMULET_OF_MAGIC.get(), event.getEntity()).map(
                            ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
            event.getSource().getEntity().getCapability(RunicAgesMagicCapability.Provider.MAGIC_LEVEL).ifPresent(h ->
            {
                DamageSource source = event.getSource();
                if (source.getDirectEntity() instanceof AirStrikeEntity||
                        source.getDirectEntity() instanceof WaterStrikeEntity ||
                        source.getDirectEntity() instanceof EarthStrikeEntity ||
                        source.getDirectEntity() instanceof FireStrikeEntity)
                {
                    if(!magic_amulet.isEmpty()) {
                        event.setAmount(Math.round((float) (event.getAmount() + 7.5 + (0.2 * (h.getMagicBoost()+h.getMagicLevel())))));
                    }
                    else event.setAmount(Math.round((float) (event.getAmount() + (0.2 * (h.getMagicBoost()+h.getMagicLevel())))));

                }
            });

        }
    }
}
