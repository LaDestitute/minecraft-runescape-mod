package com.ladestitute.runicages.event;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.farming.RunicAgesFarmingCapability;
import com.ladestitute.runicages.capability.herblore.RunicAgesHerbloreCapability;
import com.ladestitute.runicages.capability.magic.RunicAgesMagicCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.client.menu.MagicBookMenu;
import com.ladestitute.runicages.entities.*;
import com.ladestitute.runicages.network.ClientMagicSkillPacket;
import com.ladestitute.runicages.network.SimpleNetworkHandler;
import com.ladestitute.runicages.registry.BlockInit;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.registry.SpecialBlockInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import com.ladestitute.runicages.util.RunicAgesKeyboardUtil;
import io.netty.buffer.Unpooled;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.network.NetworkHooks;
import net.minecraftforge.network.PacketDistributor;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;

public class MagicEventHandler {

    int costcheck = 0;

    @SubscribeEvent
    public void strikespells(PlayerInteractEvent.RightClickItem event)
    {
        ItemStack air_strike_spell =
                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.AIR_STRIKE_SPELL.get(), event.getEntity()).map(
                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
        ItemStack water_strike_spell =
                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.WATER_STRIKE_SPELL.get(), event.getEntity()).map(
                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
        ItemStack earth_strike_spell =
                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.EARTH_STRIKE_SPELL.get(), event.getEntity()).map(
                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
        ItemStack fire_strike_spell =
                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.FIRE_STRIKE_SPELL.get(), event.getEntity()).map(
                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
        ItemStack handstack = event.getEntity().getItemBySlot(EquipmentSlot.MAINHAND);
        event.getEntity().getCapability(RunicAgesMagicCapability.Provider.MAGIC_LEVEL).ifPresent(h ->
        {
            if (!event.getEntity().isCrouching())
            {
                if (handstack.getItem() == ItemInit.STAFF.get() ||
                        handstack.getItem() == ItemInit.MAGIC_WAND.get())
                {
                    if (!air_strike_spell.isEmpty())
                    {
                        if (RunicAgesConfig.modernrs.get()) {
                            if (event.getEntity().getInventory().contains(ItemInit.AIR_RUNE.get().getDefaultInstance()))
                            {
                                for (ItemStack runecost2 : event.getEntity().getInventory().items) {
                                    if (runecost2.getItem() == ItemInit.AIR_RUNE.get() && runecost2.getCount() >= 1) {
                                        runecost2.shrink(1);
                                        break;

                                    }
                                }
                                AirStrikeEntity airstrike = new AirStrikeEntity(event.getEntity(), event.getEntity().level());
                                airstrike.setItem(ItemInit.AIR_STRIKE_PROJECTILE.get().getDefaultInstance());
                                airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                event.getEntity().level().addFreshEntity(airstrike);

                            }
                        }
                        if (!RunicAgesConfig.modernrs.get() && h.getMagicLevel() >= 1) {
                            if (event.getEntity().getInventory().contains(ItemInit.AIR_RUNE.get().getDefaultInstance()) &&
                                    event.getEntity().getInventory().contains(ItemInit.MIND_RUNE.get().getDefaultInstance()))
                            {
                                for (ItemStack runecost1 : event.getEntity().getInventory().items) {
                                    if (runecost1.getItem() == ItemInit.MIND_RUNE.get() && runecost1.getCount() >= 1) {
                                        runecost1.shrink(1);
                                        break;
                                    }
                                }
                                for (ItemStack runecost2 : event.getEntity().getInventory().items) {
                                    if (runecost2.getItem() == ItemInit.AIR_RUNE.get() && runecost2.getCount() >= 1) {
                                        runecost2.shrink(1);
                                        break;

                                    }
                                }
                                AirStrikeEntity airstrike = new AirStrikeEntity(event.getEntity(), event.getEntity().level());
                                airstrike.setItem(ItemInit.AIR_STRIKE_PROJECTILE.get().getDefaultInstance());
                                airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                event.getEntity().level().addFreshEntity(airstrike);

                            }
                        }
                    }
                    if (!water_strike_spell.isEmpty())
                    {
                        if (RunicAgesConfig.modernrs.get()) {
                            if (event.getEntity().getInventory().contains(ItemInit.AIR_RUNE.get().getDefaultInstance()) &&
                                    event.getEntity().getInventory().contains(ItemInit.WATER_RUNE.get().getDefaultInstance()))
                            {
                                for (ItemStack runecost2 : event.getEntity().getInventory().items) {
                                    if (runecost2.getItem() == ItemInit.AIR_RUNE.get() && runecost2.getCount() >= 1) {
                                        runecost2.shrink(1);
                                        break;

                                    }
                                }
                                for (ItemStack runecost3 : event.getEntity().getInventory().items) {
                                    if (runecost3.getItem() == ItemInit.WATER_RUNE.get() && runecost3.getCount() >= 1) {
                                        runecost3.shrink(1);
                                        break;

                                    }
                                }
                                WaterStrikeEntity airstrike = new WaterStrikeEntity(event.getEntity(), event.getEntity().level());
                                airstrike.setItem(ItemInit.WATER_STRIKE_PROJECTILE.get().getDefaultInstance());
                                airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                event.getEntity().level().addFreshEntity(airstrike);
                            }
                        }
                        if (!RunicAgesConfig.modernrs.get() && h.getMagicLevel() >= 5) {
                            if (event.getEntity().getInventory().contains(ItemInit.AIR_RUNE.get().getDefaultInstance()) &&
                                    event.getEntity().getInventory().contains(ItemInit.MIND_RUNE.get().getDefaultInstance()) &&
                                    event.getEntity().getInventory().contains(ItemInit.WATER_RUNE.get().getDefaultInstance()))
                            {
                                for (ItemStack runecost1 : event.getEntity().getInventory().items) {
                                    if (runecost1.getItem() == ItemInit.MIND_RUNE.get() && runecost1.getCount() >= 1) {
                                        runecost1.shrink(1);
                                        break;
                                    }
                                }
                                for (ItemStack runecost2 : event.getEntity().getInventory().items) {
                                    if (runecost2.getItem() == ItemInit.AIR_RUNE.get() && runecost2.getCount() >= 1) {
                                        runecost2.shrink(1);
                                        break;

                                    }
                                }
                                for (ItemStack runecost3 : event.getEntity().getInventory().items) {
                                    if (runecost3.getItem() == ItemInit.WATER_RUNE.get() && runecost3.getCount() >= 1) {
                                        runecost3.shrink(1);
                                        break;

                                    }
                                }
                                WaterStrikeEntity airstrike = new WaterStrikeEntity(event.getEntity(), event.getEntity().level());
                                airstrike.setItem(ItemInit.WATER_STRIKE_PROJECTILE.get().getDefaultInstance());
                                airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                event.getEntity().level().addFreshEntity(airstrike);
                            }
                        }
                    }
                    if (!earth_strike_spell.isEmpty())
                    {
                        if (RunicAgesConfig.modernrs.get())
                        {
                            if (event.getEntity().getInventory().contains(ItemInit.AIR_RUNE.get().getDefaultInstance()) &&
                                    event.getEntity().getInventory().contains(ItemInit.EARTH_RUNE.get().getDefaultInstance()))
                            {
                                for (ItemStack runecost2 : event.getEntity().getInventory().items) {
                                    if (runecost2.getItem() == ItemInit.AIR_RUNE.get() && runecost2.getCount() >= 1) {
                                        runecost2.shrink(1);
                                        break;

                                    }
                                }
                                for (ItemStack runecost3 : event.getEntity().getInventory().items) {
                                    if (runecost3.getItem() == ItemInit.EARTH_RUNE.get() && runecost3.getCount() >= 1) {
                                        runecost3.shrink(1);
                                        break;

                                    }
                                }
                                EarthStrikeEntity airstrike = new EarthStrikeEntity(event.getEntity(), event.getEntity().level());
                                airstrike.setItem(ItemInit.EARTH_STRIKE_PROJECTILE.get().getDefaultInstance());
                                airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                event.getEntity().level().addFreshEntity(airstrike);
                            }
                        }
                        if(!RunicAgesConfig.modernrs.get() && h.getMagicLevel() >= 9)
                        {
                            if (event.getEntity().getInventory().contains(ItemInit.AIR_RUNE.get().getDefaultInstance()) &&
                                    event.getEntity().getInventory().contains(ItemInit.MIND_RUNE.get().getDefaultInstance()) &&
                                    event.getEntity().getInventory().contains(ItemInit.EARTH_RUNE.get().getDefaultInstance()))
                            {
                                for (ItemStack runecost1 : event.getEntity().getInventory().items) {
                                    if (runecost1.getItem() == ItemInit.MIND_RUNE.get() && runecost1.getCount() >= 1) {
                                        runecost1.shrink(1);
                                        break;
                                    }
                                }
                                for (ItemStack runecost2 : event.getEntity().getInventory().items) {
                                    if (runecost2.getItem() == ItemInit.AIR_RUNE.get() && runecost2.getCount() >= 1) {
                                        runecost2.shrink(1);
                                        break;

                                    }
                                }
                                for (ItemStack runecost3 : event.getEntity().getInventory().items) {
                                    if (runecost3.getItem() == ItemInit.EARTH_RUNE.get() && runecost3.getCount() >= 1) {
                                        runecost3.shrink(1);
                                        break;

                                    }
                                }
                                EarthStrikeEntity airstrike = new EarthStrikeEntity(event.getEntity(), event.getEntity().level());
                                airstrike.setItem(ItemInit.EARTH_STRIKE_PROJECTILE.get().getDefaultInstance());
                                airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                event.getEntity().level().addFreshEntity(airstrike);
                            }
                    }
                    }
                    if (!fire_strike_spell.isEmpty())
                    {
                        if (RunicAgesConfig.modernrs.get())
                        {
                            if (event.getEntity().getInventory().contains(ItemInit.AIR_RUNE.get().getDefaultInstance()) &&
                                    event.getEntity().getInventory().contains(ItemInit.FIRE_RUNE.get().getDefaultInstance()))
                            {
                                for (ItemStack runecost2 : event.getEntity().getInventory().items) {
                                    if (runecost2.getItem() == ItemInit.AIR_RUNE.get() && runecost2.getCount() >= 1) {
                                        runecost2.shrink(1);
                                        break;

                                    }
                                }
                                for (ItemStack runecost3 : event.getEntity().getInventory().items) {
                                    if (runecost3.getItem() == ItemInit.FIRE_RUNE.get() && runecost3.getCount() >= 1) {
                                        runecost3.shrink(1);
                                        break;

                                    }
                                }
                                FireStrikeEntity airstrike = new FireStrikeEntity(event.getEntity(), event.getEntity().level());
                                airstrike.setItem(ItemInit.FIRE_STRIKE_PROJECTILE.get().getDefaultInstance());
                                airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                event.getEntity().level().addFreshEntity(airstrike);
                            }
                        }
                        if(!RunicAgesConfig.modernrs.get() && h.getMagicLevel() >= 13)
                        {
                            if (event.getEntity().getInventory().contains(ItemInit.AIR_RUNE.get().getDefaultInstance()) &&
                                    event.getEntity().getInventory().contains(ItemInit.MIND_RUNE.get().getDefaultInstance()) &&
                                    event.getEntity().getInventory().contains(ItemInit.FIRE_RUNE.get().getDefaultInstance()))
                            {
                                for (ItemStack runecost1 : event.getEntity().getInventory().items) {
                                    if (runecost1.getItem() == ItemInit.MIND_RUNE.get() && runecost1.getCount() >= 1) {
                                        runecost1.shrink(1);
                                        break;
                                    }
                                }
                                for (ItemStack runecost2 : event.getEntity().getInventory().items) {
                                    if (runecost2.getItem() == ItemInit.AIR_RUNE.get() && runecost2.getCount() >= 1) {
                                        runecost2.shrink(1);
                                        break;

                                    }
                                }
                                for (ItemStack runecost3 : event.getEntity().getInventory().items) {
                                    if (runecost3.getItem() == ItemInit.FIRE_RUNE.get() && runecost3.getCount() >= 1) {
                                        runecost3.shrink(1);
                                        break;

                                    }
                                }
                                FireStrikeEntity airstrike = new FireStrikeEntity(event.getEntity(), event.getEntity().level());
                                airstrike.setItem(ItemInit.FIRE_STRIKE_PROJECTILE.get().getDefaultInstance());
                                airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                event.getEntity().level().addFreshEntity(airstrike);
                            }
                        }
                    }
            }
            //Air staff
            if (!event.getEntity().isCrouching() && !event.getEntity().level().isClientSide())
            {
                if (handstack.getItem() == ItemInit.STAFF_OF_AIR.get())
                {
                    if (!air_strike_spell.isEmpty())
                    {
                        if (RunicAgesConfig.modernrs.get()) {
                                AirStrikeEntity airstrike = new AirStrikeEntity(event.getEntity(), event.getEntity().level());
                                airstrike.setItem(ItemInit.AIR_STRIKE_PROJECTILE.get().getDefaultInstance());
                                airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                event.getEntity().level().addFreshEntity(airstrike);
                        }
                        if (!RunicAgesConfig.modernrs.get() && h.getMagicLevel() >= 1) {
                            if (event.getEntity().getInventory().contains(ItemInit.MIND_RUNE.get().getDefaultInstance()))
                            {
                                for (ItemStack runecost1 : event.getEntity().getInventory().items) {
                                    if (runecost1.getItem() == ItemInit.MIND_RUNE.get() && runecost1.getCount() >= 1) {
                                        runecost1.shrink(1);
                                        break;
                                    }
                                }

                                AirStrikeEntity airstrike = new AirStrikeEntity(event.getEntity(), event.getEntity().level());
                                airstrike.setItem(ItemInit.AIR_STRIKE_PROJECTILE.get().getDefaultInstance());
                                airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                event.getEntity().level().addFreshEntity(airstrike);

                            }
                        }
                    }
                    if (!water_strike_spell.isEmpty())
                    {
                        if (RunicAgesConfig.modernrs.get()) {
                            if (event.getEntity().getInventory().contains(ItemInit.WATER_RUNE.get().getDefaultInstance()))
                            {

                                for (ItemStack runecost3 : event.getEntity().getInventory().items) {
                                    if (runecost3.getItem() == ItemInit.WATER_RUNE.get() && runecost3.getCount() >= 1) {
                                        runecost3.shrink(1);
                                        break;

                                    }
                                }
                                WaterStrikeEntity airstrike = new WaterStrikeEntity(event.getEntity(), event.getEntity().level());
                                airstrike.setItem(ItemInit.WATER_STRIKE_PROJECTILE.get().getDefaultInstance());
                                airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                event.getEntity().level().addFreshEntity(airstrike);
                            }
                        }
                        if (!RunicAgesConfig.modernrs.get() && h.getMagicLevel() >= 5) {
                            if (event.getEntity().getInventory().contains(ItemInit.MIND_RUNE.get().getDefaultInstance()) &&
                                    event.getEntity().getInventory().contains(ItemInit.WATER_RUNE.get().getDefaultInstance()))
                            {
                                for (ItemStack runecost1 : event.getEntity().getInventory().items) {
                                    if (runecost1.getItem() == ItemInit.MIND_RUNE.get() && runecost1.getCount() >= 1) {
                                        runecost1.shrink(1);
                                        break;
                                    }
                                }

                                for (ItemStack runecost3 : event.getEntity().getInventory().items) {
                                    if (runecost3.getItem() == ItemInit.WATER_RUNE.get() && runecost3.getCount() >= 1) {
                                        runecost3.shrink(1);
                                        break;

                                    }
                                }
                                WaterStrikeEntity airstrike = new WaterStrikeEntity(event.getEntity(), event.getEntity().level());
                                airstrike.setItem(ItemInit.WATER_STRIKE_PROJECTILE.get().getDefaultInstance());
                                airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                event.getEntity().level().addFreshEntity(airstrike);
                            }
                        }
                    }
                    if (!earth_strike_spell.isEmpty())
                    {
                        if (RunicAgesConfig.modernrs.get())
                        {
                            if (event.getEntity().getInventory().contains(ItemInit.EARTH_RUNE.get().getDefaultInstance()))
                            {

                                for (ItemStack runecost3 : event.getEntity().getInventory().items) {
                                    if (runecost3.getItem() == ItemInit.EARTH_RUNE.get() && runecost3.getCount() >= 1) {
                                        runecost3.shrink(1);
                                        break;

                                    }
                                }
                                EarthStrikeEntity airstrike = new EarthStrikeEntity(event.getEntity(), event.getEntity().level());
                                airstrike.setItem(ItemInit.EARTH_STRIKE_PROJECTILE.get().getDefaultInstance());
                                airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                event.getEntity().level().addFreshEntity(airstrike);
                            }
                        }
                        if(!RunicAgesConfig.modernrs.get() && h.getMagicLevel() >= 9)
                        {
                            if (event.getEntity().getInventory().contains(ItemInit.MIND_RUNE.get().getDefaultInstance()) &&
                                    event.getEntity().getInventory().contains(ItemInit.EARTH_RUNE.get().getDefaultInstance()))
                            {
                                for (ItemStack runecost1 : event.getEntity().getInventory().items) {
                                    if (runecost1.getItem() == ItemInit.MIND_RUNE.get() && runecost1.getCount() >= 1) {
                                        runecost1.shrink(1);
                                        break;
                                    }
                                }

                                for (ItemStack runecost3 : event.getEntity().getInventory().items) {
                                    if (runecost3.getItem() == ItemInit.EARTH_RUNE.get() && runecost3.getCount() >= 1) {
                                        runecost3.shrink(1);
                                        break;

                                    }
                                }
                                EarthStrikeEntity airstrike = new EarthStrikeEntity(event.getEntity(), event.getEntity().level());
                                airstrike.setItem(ItemInit.EARTH_STRIKE_PROJECTILE.get().getDefaultInstance());
                                airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                event.getEntity().level().addFreshEntity(airstrike);
                            }
                        }
                    }
                    if (!fire_strike_spell.isEmpty())
                    {
                        if (RunicAgesConfig.modernrs.get())
                        {
                            if (event.getEntity().getInventory().contains(ItemInit.FIRE_RUNE.get().getDefaultInstance()))
                            {

                                for (ItemStack runecost3 : event.getEntity().getInventory().items) {
                                    if (runecost3.getItem() == ItemInit.FIRE_RUNE.get() && runecost3.getCount() >= 1) {
                                        runecost3.shrink(1);
                                        break;

                                    }
                                }
                                FireStrikeEntity airstrike = new FireStrikeEntity(event.getEntity(), event.getEntity().level());
                                airstrike.setItem(ItemInit.FIRE_STRIKE_PROJECTILE.get().getDefaultInstance());
                                airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                event.getEntity().level().addFreshEntity(airstrike);
                            }
                        }
                        if(!RunicAgesConfig.modernrs.get() && h.getMagicLevel() >= 13)
                        {
                            if (event.getEntity().getInventory().contains(ItemInit.MIND_RUNE.get().getDefaultInstance()) &&
                                    event.getEntity().getInventory().contains(ItemInit.FIRE_RUNE.get().getDefaultInstance()))
                            {
                                for (ItemStack runecost1 : event.getEntity().getInventory().items) {
                                    if (runecost1.getItem() == ItemInit.MIND_RUNE.get() && runecost1.getCount() >= 1) {
                                        runecost1.shrink(1);
                                        break;
                                    }
                                }
                                for (ItemStack runecost3 : event.getEntity().getInventory().items) {
                                    if (runecost3.getItem() == ItemInit.FIRE_RUNE.get() && runecost3.getCount() >= 1) {
                                        runecost3.shrink(1);
                                        break;

                                    }
                                }
                                FireStrikeEntity airstrike = new FireStrikeEntity(event.getEntity(), event.getEntity().level());
                                airstrike.setItem(ItemInit.FIRE_STRIKE_PROJECTILE.get().getDefaultInstance());
                                airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                event.getEntity().level().addFreshEntity(airstrike);
                            }
                        }
                    }
                }
                }
        }
        });
    }

    @SubscribeEvent
    public void offhandspells(PlayerInteractEvent.RightClickItem event)
    {
        ItemStack confuse_spell =
                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.CONFUSE_SPELL.get(), event.getEntity()).map(
                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
        ItemStack weaken_spell =
                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.WEAKEN_SPELL.get(), event.getEntity()).map(
                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
        ItemStack handstack = event.getEntity().getItemBySlot(EquipmentSlot.MAINHAND);
        event.getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            event.getEntity().getCapability(RunicAgesMagicCapability.Provider.MAGIC_LEVEL).ifPresent(h ->
            {
                if (event.getEntity().isCrouching() && !event.getEntity().level().isClientSide()) {
                    if (!confuse_spell.isEmpty()) {
                        if (handstack.getItem() == ItemInit.STAFF.get() ||
                                handstack.getItem() == ItemInit.MAGIC_WAND.get()) {
                            for (ItemStack runecost : event.getEntity().getInventory().items)
                                if(RunicAgesConfig.modernrs.get() && h.getMagicLevel() >= 1)
                                {
                                    if (event.getEntity().getInventory().contains(ItemInit.MIND_RUNE.get().getDefaultInstance()))
                                    {
                                        for (ItemStack runecost1 : event.getEntity().getInventory().items) {
                                            if (runecost1.getItem() == ItemInit.MIND_RUNE.get() && runecost1.getCount() >= 1) {
                                                runecost1.shrink(1);
                                                break;
                                            }
                                        }
                                        ConfuseEntity airstrike = new ConfuseEntity(event.getEntity(), event.getEntity().level());
                                        airstrike.setItem(ItemInit.CONFUSE_SPELL.get().getDefaultInstance());
                                        airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                        event.getEntity().level().addFreshEntity(airstrike);
                                    }
                                }
                               if(!RunicAgesConfig.modernrs.get() && h.getMagicLevel() >= 3)
                               {
                                   if (event.getEntity().getInventory().contains(ItemInit.EARTH_RUNE.get().getDefaultInstance()) &&
                                           event.getEntity().getInventory().contains(ItemInit.BODY_RUNE.get().getDefaultInstance()) &&
                                           event.getEntity().getInventory().contains(ItemInit.WATER_RUNE.get().getDefaultInstance()))
                                   {
                                       for (ItemStack runecost1 : event.getEntity().getInventory().items) {
                                           if (runecost1.getItem() == ItemInit.BODY_RUNE.get() && runecost1.getCount() >= 1) {
                                               runecost1.shrink(1);
                                               break;
                                           }
                                       }
                                       for (ItemStack runecost2 : event.getEntity().getInventory().items) {
                                           if (runecost2.getItem() == ItemInit.EARTH_RUNE.get() && runecost2.getCount() >= 2) {
                                               runecost2.shrink(2);
                                               break;

                                           }
                                       }
                                       for (ItemStack runecost3 : event.getEntity().getInventory().items) {
                                           if (runecost3.getItem() == ItemInit.WATER_RUNE.get() && runecost3.getCount() >= 3) {
                                               runecost3.shrink(3);
                                               break;

                                           }
                                       }
                                       ConfuseEntity airstrike = new ConfuseEntity(event.getEntity(), event.getEntity().level());
                                       airstrike.setItem(ItemInit.CONFUSE_SPELL.get().getDefaultInstance());
                                       airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                       event.getEntity().level().addFreshEntity(airstrike);
                                   }
                               }


                        }
                    }
                    if (!weaken_spell.isEmpty()) {
                        if (handstack.getItem() == ItemInit.STAFF.get() ||
                                handstack.getItem() == ItemInit.MAGIC_WAND.get()) {
                            for (ItemStack runecost : event.getEntity().getInventory().items)
                                if(RunicAgesConfig.modernrs.get() && h.getMagicLevel() >= 11)
                                {
                                    if (event.getEntity().getInventory().contains(ItemInit.BODY_RUNE.get().getDefaultInstance()))
                                    {
                                        for (ItemStack runecost1 : event.getEntity().getInventory().items) {
                                            if (runecost1.getItem() == ItemInit.BODY_RUNE.get() && runecost1.getCount() >= 1) {
                                                runecost1.shrink(1);
                                                break;
                                            }
                                        }
                                        WeakenEntity airstrike = new WeakenEntity(event.getEntity(), event.getEntity().level());
                                        airstrike.setItem(ItemInit.WEAKEN_SPELL.get().getDefaultInstance());
                                        airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                        event.getEntity().level().addFreshEntity(airstrike);
                                    }
                                }
                            if(!RunicAgesConfig.modernrs.get() && h.getMagicLevel() >= 11)
                            {
                                if (event.getEntity().getInventory().contains(ItemInit.EARTH_RUNE.get().getDefaultInstance()) &&
                                        event.getEntity().getInventory().contains(ItemInit.BODY_RUNE.get().getDefaultInstance()) &&
                                        event.getEntity().getInventory().contains(ItemInit.WATER_RUNE.get().getDefaultInstance()))
                                {
                                    for (ItemStack runecost1 : event.getEntity().getInventory().items) {
                                        if (runecost1.getItem() == ItemInit.BODY_RUNE.get() && runecost1.getCount() >= 1) {
                                            runecost1.shrink(1);
                                            break;
                                        }
                                    }
                                    for (ItemStack runecost2 : event.getEntity().getInventory().items) {
                                        if (runecost2.getItem() == ItemInit.EARTH_RUNE.get() && runecost2.getCount() >= 2) {
                                            runecost2.shrink(2);
                                            break;

                                        }
                                    }
                                    for (ItemStack runecost3 : event.getEntity().getInventory().items) {
                                        if (runecost3.getItem() == ItemInit.WATER_RUNE.get() && runecost3.getCount() >= 3) {
                                            runecost3.shrink(3);
                                            break;

                                        }
                                    }
                                    WeakenEntity airstrike = new WeakenEntity(event.getEntity(), event.getEntity().level());
                                    airstrike.setItem(ItemInit.WEAKEN_SPELL.get().getDefaultInstance());
                                    airstrike.shootFromRotation(event.getEntity(), event.getEntity().getXRot(), event.getEntity().getYRot(), 0.0F, 1.5F, 1F);
                                    event.getEntity().level().addFreshEntity(airstrike);
                                }
                            }

                        }
                    }


                }

            });
        });
    }

    @SubscribeEvent
    public void spellsunlock(PlayerEvent.PlayerLoggedInEvent event)
    {
        ResourceLocation[] recipe = new ResourceLocation[4];
        recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "magic/unenchanted_home_teleport_tablet");
        recipe[1] = new ResourceLocation(RunicAgesMain.MODID, "magic/unenchanted_bones_to_bananas_tablet");
        recipe[2] = new ResourceLocation(RunicAgesMain.MODID, "magic/unenchanted_enchant_sapphire_tablet");
        recipe[3] = new ResourceLocation(RunicAgesMain.MODID, "magic/air_strike_spell");
        event.getEntity().awardRecipesByKey(recipe);

        event.getEntity().getCapability(RunicAgesMagicCapability.Provider.MAGIC_LEVEL).ifPresent(h ->
        {
            if(!RunicAgesConfig.modernrs.get() && h.getMagicLevel() >= 3)
            {
                ResourceLocation[] spell = new ResourceLocation[1];
                spell[0] = new ResourceLocation(RunicAgesMain.MODID, "magic/confuse_spell");
                event.getEntity().awardRecipesByKey(spell);
            }
            if(RunicAgesConfig.modernrs.get() && h.getMagicLevel() >= 1)
            {
                ResourceLocation[] spell = new ResourceLocation[1];
                spell[0] = new ResourceLocation(RunicAgesMain.MODID, "magic/confuse_spell");
                event.getEntity().awardRecipesByKey(spell);
            }
            if(h.getMagicLevel() >= 5)
            {
                ResourceLocation[] spell = new ResourceLocation[1];
                spell[0] = new ResourceLocation(RunicAgesMain.MODID, "magic/water_strike_spell");
                event.getEntity().awardRecipesByKey(spell);
            }
            if(h.getMagicLevel() >= 9)
            {
                ResourceLocation[] spell = new ResourceLocation[1];
                spell[0] = new ResourceLocation(RunicAgesMain.MODID, "magic/earth_strike_spell");
                event.getEntity().awardRecipesByKey(spell);
            }
            if(h.getMagicLevel() >= 11)
            {
                ResourceLocation[] spell = new ResourceLocation[1];
                spell[0] = new ResourceLocation(RunicAgesMain.MODID, "magic/weaken_spell");
                event.getEntity().awardRecipesByKey(spell);
            }
            if(h.getMagicLevel() >= 13)
            {
                ResourceLocation[] spell = new ResourceLocation[1];
                spell[0] = new ResourceLocation(RunicAgesMain.MODID, "magic/fire_strike_spell");
                event.getEntity().awardRecipesByKey(spell);
            }
        });

    }

    @SubscribeEvent
    public void unenchantedtablets(PlayerEvent.ItemCraftedEvent event) {
        if (!event.getEntity().level().isClientSide()) {
                    if (event.getCrafting().getItem() == ItemInit.UNENCHANTED_HOME_TELEPORT_TABLET.get()) {
                     ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.AIR_RUNE.get().getDefaultInstance());
                        ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.EARTH_RUNE.get().getDefaultInstance());
                        ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.LAW_RUNE.get().getDefaultInstance());
                    }
            if (event.getCrafting().getItem() == ItemInit.UNENCHANTED_BONES_TO_BANANAS_TABLET.get()) {
                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.WATER_RUNE.get().getDefaultInstance());
                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.WATER_RUNE.get().getDefaultInstance());
                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.EARTH_RUNE.get().getDefaultInstance());
                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.EARTH_RUNE.get().getDefaultInstance());
                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.NATURE_RUNE.get().getDefaultInstance());
            }
            if (event.getCrafting().getItem() == ItemInit.UNENCHANTED_ENCHANT_SAPPHIRE_TABLET.get()) {
                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.WATER_RUNE.get().getDefaultInstance());
                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.COSMIC_RUNE.get().getDefaultInstance());
                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.WATER_RUNE.get().getDefaultInstance());
                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.COSMIC_RUNE.get().getDefaultInstance());
                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.WATER_RUNE.get().getDefaultInstance());
                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.COSMIC_RUNE.get().getDefaultInstance());
                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.WATER_RUNE.get().getDefaultInstance());
                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), ItemInit.COSMIC_RUNE.get().getDefaultInstance());
            }
                }

    }

    int xp;

    @SubscribeEvent
    public void maketabletatlectern(PlayerInteractEvent.RightClickBlock event) {
        event.getEntity().getCapability(RunicAgesMagicCapability.Provider.MAGIC_LEVEL).ifPresent(h ->
        {
            event.getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
            {
                BlockPos blockpos = event.getPos();
                BlockState state = event.getLevel().getBlockState(blockpos);
                if (event.getLevel().isClientSide) {
                    return;
                }
                //
                if (h.getMagicLevel() >= 1 && event.getItemStack().getItem() == Items.CLAY_BALL
                    && !event.getEntity().isCreative()||
                    h.getMagicLevel() >= 1 && event.getItemStack().getItem() == Items.CLAY_BALL && event.getEntity().isCreative()||
                        h.getMagicLevel() >= 1 && event.getItemStack().getItem() == ItemInit.UNENCHANTED_HOME_TELEPORT_TABLET.get()
                                && !event.getEntity().isCreative()||
                        h.getMagicLevel() >= 1 && event.getItemStack().getItem() == ItemInit.UNENCHANTED_HOME_TELEPORT_TABLET.get()
                                && event.getEntity().isCreative())
                {
                    if (state.getBlock() == Blocks.LECTERN
                            && event.getEntity().getInventory().contains(ItemInit.AIR_RUNE.get().getDefaultInstance()) &&
                            event.getEntity().getInventory().contains(ItemInit.EARTH_RUNE.get().getDefaultInstance()) &&
                            event.getEntity().getInventory().contains(ItemInit.LAW_RUNE.get().getDefaultInstance()))
                    {
                        for (ItemStack runecost1 : event.getEntity().getInventory().items) {
                            if (runecost1.getItem() == ItemInit.AIR_RUNE.get() && runecost1.getCount() >= 1) {
                                runecost1.shrink(1);
                                break;
                            }
                        }
                        for (ItemStack runecost2 : event.getEntity().getInventory().items) {
                            if (runecost2.getItem() == ItemInit.LAW_RUNE.get() && runecost2.getCount() >= 1) {
                                runecost2.shrink(1);
                                break;

                            }
                        }
                        for (ItemStack runecost3 : event.getEntity().getInventory().items) {
                            if (runecost3.getItem() == ItemInit.EARTH_RUNE.get() && runecost3.getCount() >= 1) {
                                runecost3.shrink(1);
                                break;

                            }
                        }
                        for (ItemStack pretablet : event.getEntity().getInventory().items)
                        {
                            if (pretablet.getItem() == Items.CLAY_BALL && pretablet.getCount() >= 1||
                                    pretablet.getItem() == ItemInit.UNENCHANTED_HOME_TELEPORT_TABLET.get() && pretablet.getCount() >= 1) {
                                xp = (int) Math.round(13.125);
                                h.addMagicXP(event.getEntity(), xp);
                                ed.addxptotalxp(xp);
                                event.getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                ItemStack tablet = ItemInit.HOME_TELEPORT_TABLET.get().getDefaultInstance();
                                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), tablet);
                                if (!event.getEntity().isCreative())
                                {
                                    pretablet.shrink(1);
                                    break;
                                }
                                break;
                            }
                        }
                    }
                }
                //
                if (h.getMagicLevel() >= 7 && event.getItemStack().getItem() == Items.CLAY_BALL
                        && !event.getEntity().isCreative()||
                        h.getMagicLevel() >= 7 && event.getItemStack().getItem() == Items.CLAY_BALL && event.getEntity().isCreative()||
                        h.getMagicLevel() >= 7 && event.getItemStack().getItem() == ItemInit.UNENCHANTED_ENCHANT_SAPPHIRE_TABLET.get()
                                && !event.getEntity().isCreative()||
                        h.getMagicLevel() >= 7 && event.getItemStack().getItem() == ItemInit.UNENCHANTED_ENCHANT_SAPPHIRE_TABLET.get()
                                && event.getEntity().isCreative())
                {
                    if (state.getBlock() == Blocks.LECTERN
                            && event.getEntity().getInventory().contains(ItemInit.WATER_RUNE.get().getDefaultInstance()) &&
                            event.getEntity().getInventory().contains(ItemInit.COSMIC_RUNE.get().getDefaultInstance()))
                    {
                        for (ItemStack runecost1 : event.getEntity().getInventory().items) {
                            if (runecost1.getItem() == ItemInit.WATER_RUNE.get() && runecost1.getCount() >= 4) {
                                runecost1.shrink(4);
                                break;
                            }
                        }
                        for (ItemStack runecost2 : event.getEntity().getInventory().items) {
                            if (runecost2.getItem() == ItemInit.COSMIC_RUNE.get() && runecost2.getCount() >= 4) {
                                runecost2.shrink(4);
                                break;

                            }
                        }
                        for (ItemStack pretablet : event.getEntity().getInventory().items)
                        {
                            if (pretablet.getItem() == Items.CLAY_BALL && pretablet.getCount() >= 1||
                                    pretablet.getItem() == ItemInit.UNENCHANTED_ENCHANT_SAPPHIRE_TABLET.get() && pretablet.getCount() >= 1) {
                                xp = (int) Math.round(17.5);
                                h.addMagicXP(event.getEntity(), xp);
                                ed.addxptotalxp(xp);
                                event.getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                ItemStack tablet = ItemInit.ENCHANT_SAPPHIRE_TABLET.get().getDefaultInstance();
                                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), tablet);
                                if (!event.getEntity().isCreative())
                                {
                                    pretablet.shrink(1);
                                    break;
                                }
                                break;
                            }
                        }
                    }
                }
                //
                if (h.getMagicLevel() >= 15 && event.getItemStack().getItem() == Items.CLAY_BALL
                        && !event.getEntity().isCreative()||
                        h.getMagicLevel() >= 15 && event.getItemStack().getItem() == Items.CLAY_BALL && event.getEntity().isCreative()||
                        h.getMagicLevel() >= 15 && event.getItemStack().getItem() == ItemInit.UNENCHANTED_BONES_TO_BANANAS_TABLET.get()
                                && !event.getEntity().isCreative()||
                        h.getMagicLevel() >= 15 && event.getItemStack().getItem() == ItemInit.UNENCHANTED_BONES_TO_BANANAS_TABLET.get()
                                && event.getEntity().isCreative())
                {
                    if (state.getBlock() == Blocks.LECTERN
                            && event.getEntity().getInventory().contains(ItemInit.NATURE_RUNE.get().getDefaultInstance()) &&
                            event.getEntity().getInventory().contains(ItemInit.EARTH_RUNE.get().getDefaultInstance()) &&
                            event.getEntity().getInventory().contains(ItemInit.WATER_RUNE.get().getDefaultInstance()))
                    {
                        for (ItemStack runecost1 : event.getEntity().getInventory().items) {
                            if (runecost1.getItem() == ItemInit.WATER_RUNE.get() && runecost1.getCount() >= 2) {
                                runecost1.shrink(2);
                                break;
                            }
                        }
                        for (ItemStack runecost2 : event.getEntity().getInventory().items) {
                            if (runecost2.getItem() == ItemInit.NATURE_RUNE.get() && runecost2.getCount() >= 1) {
                                runecost2.shrink(1);
                                break;

                            }
                        }
                        for (ItemStack runecost3 : event.getEntity().getInventory().items) {
                            if (runecost3.getItem() == ItemInit.EARTH_RUNE.get() && runecost3.getCount() >= 2) {
                                runecost3.shrink(2);
                                break;

                            }
                        }
                        for (ItemStack pretablet : event.getEntity().getInventory().items)
                        {
                            if (pretablet.getItem() == Items.CLAY_BALL && pretablet.getCount() >= 1||
                                    pretablet.getItem() == ItemInit.UNENCHANTED_BONES_TO_BANANAS_TABLET.get() && pretablet.getCount() >= 1) {
                                xp = (int) Math.round(25);
                                h.addMagicXP(event.getEntity(), xp);
                                ed.addxptotalxp(xp);
                                event.getEntity().sendSystemMessage(Component.literal("You have gained Magic XP: " + xp));
                                ItemStack tablet = ItemInit.BONES_TO_BANANAS_TABLET.get().getDefaultInstance();
                                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), tablet);
                                if (!event.getEntity().isCreative())
                                {
                                    pretablet.shrink(1);
                                    break;
                                }
                                break;
                            }
                        }
                    }
                }
                //
            });
        });
    }
}
