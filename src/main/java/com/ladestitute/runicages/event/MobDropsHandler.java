package com.ladestitute.runicages.event;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.entities.mobs.BlackBearEntity;
import com.ladestitute.runicages.entities.mobs.GrizzlyBearEntity;
import com.ladestitute.runicages.entities.mobs.ImpEntity;
import com.ladestitute.runicages.entities.mobs.UnicornEntity;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.GlowSquid;
import net.minecraft.world.entity.ambient.Bat;
import net.minecraft.world.entity.animal.AbstractFish;
import net.minecraft.world.entity.animal.Squid;
import net.minecraft.world.entity.animal.Wolf;
import net.minecraft.world.entity.animal.allay.Allay;
import net.minecraft.world.entity.animal.axolotl.Axolotl;
import net.minecraft.world.entity.animal.camel.Camel;
import net.minecraft.world.entity.animal.frog.Frog;
import net.minecraft.world.entity.animal.goat.Goat;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.monster.*;
import net.minecraft.world.entity.monster.piglin.Piglin;
import net.minecraft.world.entity.monster.piglin.PiglinBrute;
import net.minecraft.world.entity.npc.Villager;
import net.minecraft.world.entity.npc.VillagerProfession;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.registries.ForgeRegistries;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;

import java.util.Objects;
import java.util.Random;

public class MobDropsHandler {
    //A dirty drops handler, may redo with GLM later on
    //Todo: simplify and/or improve

    public static final TagKey<Item> imp_drops = TagKey.create(Registries.ITEM,
            new ResourceLocation(RunicAgesMain.MODID, "imp_drops"));
    public static final TagKey<Item> rs3_villager_drops = TagKey.create(Registries.ITEM,
            new ResourceLocation(RunicAgesMain.MODID, "rs3_villager_drops"));
    public static final TagKey<Item> osrs_villager_drops = TagKey.create(Registries.ITEM,
            new ResourceLocation(RunicAgesMain.MODID, "osrs_villager_drops"));

    int osrsherbdropchance;
    int talismanchance;
    int itemchance;
    int droptableroll;
    int doubledropchance;
    int rs3herbdropchance;
    int rs3secondarydropchance;
    int osrssecondarydropchance;

    @SubscribeEvent
    public void mobdrops(LivingDeathEvent event)
    {
        ItemStack ring_of_luck =
                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_LUCK.get(), event.getEntity()).map(
                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);

        if(event.getEntity() instanceof Spider)
        {
            Random rand = new Random();
            int talismanchance = rand.nextInt(48);
            ItemStack talismanstack = new ItemStack(ItemInit.AIR_TALISMAN.get());
            ItemStack corpsestack = new ItemStack(ItemInit.SPIDER_CARCASS.get());
            ItemEntity corpse = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    corpsestack);
            ItemEntity talisman = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    talismanstack);
            if(talismanchance == 0)
            {
                event.getEntity().level().addFreshEntity(talisman);
            }
            event.getEntity().level().addFreshEntity(corpse);

            if(!ring_of_luck.isEmpty())
            {
                osrsherbdropchance = rand.nextInt(14);
            }
            else osrsherbdropchance = rand.nextInt(18);

            ItemStack itemstack2 = new ItemStack(ItemInit.GRIMY_GUAM.get());
            ItemEntity item2 = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    itemstack2);
            if(!RunicAgesConfig.modernrs.get() && osrsherbdropchance == 0)
            {
                event.getEntity().level().addFreshEntity(item2);
            }
        }
        if(event.getEntity() instanceof Bat ||
                event.getEntity() instanceof Skeleton)
        {
            Random rand = new Random();
            int talismanchance = rand.nextInt(48);
            ItemStack talismanstack = new ItemStack(ItemInit.AIR_TALISMAN.get());
            ItemEntity talisman = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    talismanstack);
            if(talismanchance == 0)
            {
                event.getEntity().level().addFreshEntity(talisman);
            }
        }
        //
        if(event.getEntity() instanceof Bat ||
                event.getEntity() instanceof Skeleton||
                event.getEntity() instanceof Zombie)
        {
            Random rand = new Random();
            if(!ring_of_luck.isEmpty())
            {
                talismanchance = rand.nextInt(97);
            }
            else talismanchance = rand.nextInt(129);
            ItemStack talismanstack = new ItemStack(ItemInit.MIND_TALISMAN.get());
            ItemEntity talisman = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    talismanstack);
            if(talismanchance == 0)
            {
                event.getEntity().level().addFreshEntity(talisman);
            }
        }
        //
        if(event.getEntity() instanceof Villager||
                event.getEntity() instanceof Allay||
                event.getEntity() instanceof Evoker||
                event.getEntity() instanceof Pillager||
                event.getEntity() instanceof Piglin||
                event.getEntity() instanceof PiglinBrute||
                event.getEntity() instanceof Witch||
                event.getEntity() instanceof EnderMan||
                event.getEntity() instanceof Vindicator)
        {
            Random rand = new Random();
            int talismanchance = rand.nextInt(28);
            ItemStack talismanstack = new ItemStack(ItemInit.MIND_TALISMAN.get());
            ItemEntity talisman = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    talismanstack);
            if(talismanchance == 0)
            {
                event.getEntity().level().addFreshEntity(talisman);
            }
        }
        //
        if(event.getEntity() instanceof Spider ||
                event.getEntity() instanceof Skeleton||
                event.getEntity() instanceof Zombie)
        {
            Random rand = new Random();
            int talismanchance = rand.nextInt(33);
            ItemStack talismanstack = new ItemStack(ItemInit.WATER_TALISMAN.get());
            ItemEntity talisman = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    talismanstack);
            if(talismanchance == 0)
            {
                event.getEntity().level().addFreshEntity(talisman);
            }
        }
        //
        if(event.getEntity() instanceof Drowned ||
                event.getEntity() instanceof Squid ||
                event.getEntity() instanceof AbstractFish||
                event.getEntity() instanceof Axolotl||
                event.getEntity() instanceof Frog||
                event.getEntity() instanceof GlowSquid)
        {
            Random rand = new Random();
            int talismanchance = rand.nextInt(21);
            ItemStack talismanstack = new ItemStack(ItemInit.WATER_TALISMAN.get());
            ItemEntity talisman = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    talismanstack);
            if(talismanchance == 0)
            {
                event.getEntity().level().addFreshEntity(talisman);
            }
        }
        //
        // if (event.getEntity() instanceof EntityVillager && ((EntityVillager) event.getEntity()).getProfessionForge().equals(MatterOverdriveEntities.MAD_SCIENTIST_PROFESSION)
        if(event.getEntity() instanceof Spider ||
                event.getEntity() instanceof Skeleton||
                event.getEntity() instanceof Zombie||
                event.getEntity() instanceof Bat||
                event.getEntity() instanceof Frog)
        {
            Random rand = new Random();
            int talismanchance = rand.nextInt(64);
            ItemStack talismanstack = new ItemStack(ItemInit.EARTH_TALISMAN.get());
            ItemEntity talisman = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    talismanstack);
            if(talismanchance <= 9)
            {
                event.getEntity().level().addFreshEntity(talisman);
            }
        }
        //
        if(event.getEntity() instanceof Frog||event.getEntity() instanceof Husk)
        {
            Random rand = new Random();
            if(!ring_of_luck.isEmpty())
            {
                itemchance = rand.nextInt(14);
            }
            else itemchance = rand.nextInt(19);
            ItemStack itemstack = new ItemStack(ItemInit.GRIMY_TARROMIN.get());
            ItemEntity item = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    itemstack);
            if(itemchance <= 1)
            {
                event.getEntity().level().addFreshEntity(item);
            }
        }
        //
        if(event.getEntity() instanceof Frog||event.getEntity() instanceof Husk)
        {
            Random rand = new Random();
            if(!ring_of_luck.isEmpty())
            {
                itemchance = rand.nextInt(14);
            }
            else itemchance = rand.nextInt(19);
            ItemStack itemstack = new ItemStack(ItemInit.GRIMY_MARRENTILL.get());
            ItemEntity item = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    itemstack);
            if(itemchance <= 1)
            {
                event.getEntity().level().addFreshEntity(item);
            }
        }
        //
        if(event.getEntity() instanceof Husk)
        {
            Random rand = new Random();
            if(!ring_of_luck.isEmpty())
            {
                itemchance = rand.nextInt(14);
            }
            else itemchance = rand.nextInt(19);
            ItemStack itemstack = new ItemStack(ItemInit.SILVER_STONE_SPIRIT.get());
            ItemEntity item = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    itemstack);
            if(itemchance <= 1)
            {
                event.getEntity().level().addFreshEntity(item);
            }
        }
        //
        if(event.getEntity() instanceof Skeleton)
        {
            Random rand = new Random();
            if(!ring_of_luck.isEmpty())
            {
                itemchance = rand.nextInt(34);
            }
            else
                itemchance = rand.nextInt(45);
            ItemStack itemstack = new ItemStack(ItemInit.GRIMY_TARROMIN.get());
            ItemEntity item = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    itemstack);
            if(itemchance <= 1)
            {
                event.getEntity().level().addFreshEntity(item);
            }
        }
        if(event.getEntity() instanceof Skeleton)
        {
            Random rand = new Random();
            int itemchance = rand.nextInt(45);
            ItemStack itemstack = new ItemStack(ItemInit.GRIMY_MARRENTILL.get());
            ItemEntity item = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    itemstack);
            if(itemchance <= 1)
            {
                event.getEntity().level().addFreshEntity(item);
            }
        }
        //
        if(event.getEntity() instanceof Zombie)
        {
            Random rand = new Random();
            if(!ring_of_luck.isEmpty())
            {
                itemchance = rand.nextInt(52);
            }
            else itemchance = rand.nextInt(71);
            ItemStack itemstack = new ItemStack(ItemInit.GRIMY_TARROMIN.get());
            ItemEntity item = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    itemstack);
            if(itemchance <= 1)
            {
                event.getEntity().level().addFreshEntity(item);
            }
        }
        //
        if(event.getEntity() instanceof Villager && ((Villager) event.getEntity()).getVillagerData().getProfession().equals(VillagerProfession.FARMER))
        {
            Random rand = new Random();
            int talismanchance = rand.nextInt(64);
            ItemStack talismanstack = new ItemStack(ItemInit.EARTH_TALISMAN.get());
            ItemEntity talisman = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    talismanstack);
            if(talismanchance <= 18)
            {
                event.getEntity().level().addFreshEntity(talisman);
            }
        }
        //
        if(event.getEntity() instanceof Wolf)
        {
            Random rand = new Random();
            if(!ring_of_luck.isEmpty())
            {
                itemchance = rand.nextInt(49);
            }
            else itemchance = rand.nextInt(65);
            ItemStack item1 = new ItemStack(ItemInit.GRIMY_GUAM.get());
            ItemEntity itemdrop = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    item1);
            if(itemchance == 0)
            {
                event.getEntity().level().addFreshEntity(itemdrop);
            }
        }
        //
        if(event.getEntity() instanceof Witch)
        {
            Random rand = new Random();
            if(!ring_of_luck.isEmpty())
            {
                rs3herbdropchance = rand.nextInt(24);
                osrsherbdropchance = rand.nextInt(21);
                rs3secondarydropchance = rand.nextInt(24);
                osrssecondarydropchance = rand.nextInt(21);
            } else
            rs3herbdropchance = rand.nextInt(32);
            osrsherbdropchance = rand.nextInt(28);
            rs3secondarydropchance = rand.nextInt(32);
            osrssecondarydropchance = rand.nextInt(28);
            ItemStack itemstack1 = new ItemStack(ItemInit.GRIMY_GUAM.get());
            ItemEntity item1 = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    itemstack1);
            ItemStack itemstack2 = new ItemStack(ItemInit.EYE_OF_NEWT.get());
            ItemEntity item2 = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    itemstack2);
            if(RunicAgesConfig.modernrs.get() && rs3herbdropchance == 0)
            {
                event.getEntity().level().addFreshEntity(item1);
            }
            if(!RunicAgesConfig.modernrs.get() && osrsherbdropchance == 0)
            {
                event.getEntity().level().addFreshEntity(item1);
            }
            if(RunicAgesConfig.modernrs.get() && rs3secondarydropchance == 0)
            {
                event.getEntity().level().addFreshEntity(item2);
            }
            if(!RunicAgesConfig.modernrs.get() && osrssecondarydropchance == 0)
            {
                event.getEntity().level().addFreshEntity(item2);
            }
        }
        //
        if(event.getEntity() instanceof Skeleton)
        {
            Random rand = new Random();
            if(!ring_of_luck.isEmpty())
            {
                rs3herbdropchance = rand.nextInt(48);
                osrsherbdropchance = rand.nextInt(59);
            } else
            rs3herbdropchance = rand.nextInt(64);
            osrsherbdropchance = rand.nextInt(78);
            ItemStack itemstack1 = new ItemStack(ItemInit.GRIMY_GUAM.get());
            ItemEntity item1 = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    itemstack1);
            if(RunicAgesConfig.modernrs.get() && rs3herbdropchance == 0)
            {
                event.getEntity().level().addFreshEntity(item1);
            }
            if(!RunicAgesConfig.modernrs.get() && osrsherbdropchance == 0)
            {
                event.getEntity().level().addFreshEntity(item1);
            }
        }
        //
        if(event.getEntity() instanceof Goat)
        {
            Random rand = new Random();
            int talismanchance = rand.nextInt(21);
            ItemStack talismanstack = new ItemStack(ItemInit.EARTH_TALISMAN.get());
            ItemEntity talisman = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    talismanstack);
            if(talismanchance == 0)
            {
                event.getEntity().level().addFreshEntity(talisman);
            }
        }
        //
        if(event.getEntity() instanceof Allay||
                event.getEntity() instanceof Vex)
        {
            Random rand = new Random();
            int talismanchance = rand.nextInt(21);
            ItemStack talismanstack = new ItemStack(ItemInit.AIR_TALISMAN.get());
            ItemEntity talisman = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    talismanstack);
            if(talismanchance == 0)
            {
                event.getEntity().level().addFreshEntity(talisman);
            }
        }
        //
        if(event.getEntity() instanceof Blaze ||
                event.getEntity() instanceof MagmaCube||
                event.getEntity() instanceof Strider||
                event.getEntity() instanceof Camel)
        {
            Random rand = new Random();
            int talismanchance = rand.nextInt(21);
            ItemStack talismanstack = new ItemStack(ItemInit.FIRE_TALISMAN.get());
            ItemEntity talisman = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    talismanstack);
            if(talismanchance == 0)
            {
                event.getEntity().level().addFreshEntity(talisman);
            }
        }
        //
        if(event.getEntity() instanceof WitherSkeleton)
        {
            Random rand = new Random();
            int talismanchance = rand.nextInt(26);
            ItemStack talismanstack = new ItemStack(ItemInit.FIRE_TALISMAN.get());
            ItemEntity talisman = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    talismanstack);
            if(talismanchance == 0)
            {
                event.getEntity().level().addFreshEntity(talisman);
            }
        }
        //
        if(event.getEntity() instanceof Skeleton)
        {
            Random rand = new Random();
            int talismanchance = rand.nextInt(33);
            ItemStack talismanstack = new ItemStack(ItemInit.FIRE_TALISMAN.get());
            ItemEntity talisman = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    talismanstack);
            if(talismanchance == 0)
            {
                event.getEntity().level().addFreshEntity(talisman);
            }
        }
        if(!(event.getEntity() instanceof Vex)||
                !(event.getEntity() instanceof Allay))
        {
            Random rand = new Random();
            int talismanchance = rand.nextInt(49);
            ItemStack talismanstack = new ItemStack(ItemInit.BODY_TALISMAN.get());
            ItemEntity talisman = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    talismanstack);
            if(talismanchance == 0)
            {
                event.getEntity().level().addFreshEntity(talisman);
            }
        }
        //
        if(event.getEntity() instanceof Zombie)
        {
            Random rand = new Random();
            if(!ring_of_luck.isEmpty())
            {
                rs3herbdropchance = rand.nextInt(48);
                osrsherbdropchance = rand.nextInt(42);
                rs3secondarydropchance = rand.nextInt(48);
                osrssecondarydropchance = rand.nextInt(43);
            } else
            rs3herbdropchance = rand.nextInt(64);
            osrsherbdropchance = rand.nextInt(55);
            rs3secondarydropchance = rand.nextInt(64);
            osrssecondarydropchance = rand.nextInt(57);
            ItemStack itemstack1 = new ItemStack(ItemInit.GRIMY_GUAM.get());
            ItemEntity item1 = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    itemstack1);
            ItemStack itemstack2 = new ItemStack(ItemInit.EYE_OF_NEWT.get());
            ItemEntity item2 = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    itemstack2);
            if(RunicAgesConfig.modernrs.get() && rs3herbdropchance == 0)
            {
                event.getEntity().level().addFreshEntity(item1);
            }
            if(!RunicAgesConfig.modernrs.get() && osrsherbdropchance == 0)
            {
                event.getEntity().level().addFreshEntity(item1);
            }
            if(RunicAgesConfig.modernrs.get() && rs3secondarydropchance == 0)
            {
                event.getEntity().level().addFreshEntity(item2);
            }
            if(!RunicAgesConfig.modernrs.get() && osrssecondarydropchance == 0)
            {
                event.getEntity().level().addFreshEntity(item2);
            }
        }
        if(event.getEntity() instanceof GrizzlyBearEntity||
                event.getEntity() instanceof BlackBearEntity)
        {
            ItemStack itemstack1 = new ItemStack(Items.BONE);
            ItemEntity item1 = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    itemstack1);
            ItemStack itemstack2 = new ItemStack(ItemInit.BEAR_FUR.get());
            ItemEntity item2 = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    itemstack2);
            ItemStack itemstack3 = new ItemStack(ItemInit.RAW_BEAR_MEAT.get());
            ItemEntity item3 = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    itemstack3);

            event.getEntity().level().addFreshEntity(item1);
            event.getEntity().level().addFreshEntity(item2);
            event.getEntity().level().addFreshEntity(item3);

        }
        //
        if(event.getEntity() instanceof UnicornEntity)
        {
            ItemStack itemstack1 = new ItemStack(Items.BONE);
            ItemEntity item1 = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    itemstack1);
            ItemStack itemstack2 = new ItemStack(ItemInit.UNICORN_HORN.get());
            ItemEntity item2 = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    itemstack2);
            event.getEntity().level().addFreshEntity(item1);
            event.getEntity().level().addFreshEntity(item2);

        }
        //
        if(event.getEntity() instanceof ImpEntity)
        {
            Random rand = new Random();
            if(!ring_of_luck.isEmpty())
            {
                droptableroll = rand.nextInt(76);
                doubledropchance = rand.nextInt(76);
            } else
            droptableroll = rand.nextInt(101);
            doubledropchance = rand.nextInt(101);

            int bonusrunemodifier = 5;

            ItemStack firestack = new ItemStack(ItemInit.FIRE_RUNE.get());
            ItemEntity fire_rune = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    firestack);
            ItemStack bodystack = new ItemStack(ItemInit.BODY_RUNE.get());
            ItemEntity body_rune = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    bodystack);
            ItemStack earthstack = new ItemStack(ItemInit.EARTH_RUNE.get());
            ItemEntity earth_rune = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    earthstack);
            ItemStack mindstack = new ItemStack(ItemInit.MIND_RUNE.get());
            ItemEntity mind_rune = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                   mindstack);
            ItemStack waterstack = new ItemStack(ItemInit.WATER_RUNE.get());
            ItemEntity water_rune = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    waterstack);


            ItemStack whitebeadstack = new ItemStack(ItemInit.WHITE_BEAD.get());
            ItemEntity whitebead = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    whitebeadstack);
            ItemStack redbeadstack = new ItemStack(ItemInit.RED_BEAD.get());
            ItemEntity redbead = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    redbeadstack);
            ItemStack yellowbeadstack = new ItemStack(ItemInit.YELLOW_BEAD.get());
            ItemEntity yellowbead = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    yellowbeadstack);
            ItemStack blackbeadstack = new ItemStack(ItemInit.BLACK_BEAD.get());
            ItemEntity blackbead = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    blackbeadstack);

            ItemStack droptable1 = new ItemStack(ForgeRegistries.ITEMS.tags().getTag(imp_drops).getRandomElement(event.getEntity().getRandom()).get());
            ItemEntity droptableitem1 = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    droptable1);
            ItemStack droptable2 = new ItemStack(ForgeRegistries.ITEMS.tags().getTag(imp_drops).getRandomElement(event.getEntity().getRandom()).get());
            ItemEntity droptableitem2 = new ItemEntity(event.getEntity().level(), event.getEntity().getX(), event.getEntity().getY(), event.getEntity().getZ(),
                    droptable2);

            if(droptableroll == 0) {
                if(doubledropchance == 0)
                {
                    firestack.setCount(5+bonusrunemodifier);
                }
                else firestack.setCount(5);
                event.getEntity().level().addFreshEntity(fire_rune);
            }
            if(droptableroll >= 1 && droptableroll < 3) {
                if(doubledropchance == 0)
                {
                    bodystack.setCount(5+bonusrunemodifier);
                }
                else bodystack.setCount(5);
                event.getEntity().level().addFreshEntity(body_rune);
            }
            if(droptableroll >= 3 && droptableroll < 6) {
                if(doubledropchance == 0)
                {
                    earthstack.setCount(5+bonusrunemodifier);
                }
                else earthstack.setCount(5);
                event.getEntity().level().addFreshEntity(earth_rune);
            }
            if(droptableroll >= 6 && droptableroll < 11) {
                if(doubledropchance == 0)
                {
                   mindstack.setCount(5+bonusrunemodifier);
                }
                else mindstack.setCount(5);
                event.getEntity().level().addFreshEntity(mind_rune);
            }
            if(droptableroll >= 11 && droptableroll < 16) {
                if(doubledropchance == 0)
                {
                    waterstack.setCount(5+bonusrunemodifier);
                }
                else waterstack.setCount(5);
                event.getEntity().level().addFreshEntity(water_rune);
            }
            if(droptableroll >= 16 && droptableroll < 20)
            {
                if(doubledropchance == 0)
                {
                    whitebeadstack.setCount(2);
                }
                    event.getEntity().level().addFreshEntity(whitebead);
            }
            if(droptableroll >= 20 && droptableroll < 26)
            {
                if(doubledropchance == 0)
                {
                    redbeadstack.setCount(2);
                }
                event.getEntity().level().addFreshEntity(redbead);
            }
            if(droptableroll >= 26 && droptableroll < 31)
            {
                if(doubledropchance == 0)
                {
                    yellowbeadstack.setCount(2);
                }
                event.getEntity().level().addFreshEntity(yellowbead);
            }
            if(droptableroll >= 31 && droptableroll < 39)
            {
                if(doubledropchance == 0)
                {
                    blackbeadstack.setCount(2);
                }
                event.getEntity().level().addFreshEntity(blackbead);
            }
            if(droptableroll >= 39)
            {
                event.getEntity().level().addFreshEntity(droptableitem1);
                if(doubledropchance == 0)
                {
                    event.getEntity().level().addFreshEntity(droptableitem2);
                }
            }
//

        }
    }

}
