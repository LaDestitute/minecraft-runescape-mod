package com.ladestitute.runicages.event;

import com.ladestitute.runicages.capability.agility.RunicAgesAgilityCapability;
import com.ladestitute.runicages.capability.attack.RunicAgesAttackCapability;
import com.ladestitute.runicages.capability.crafting.RunicAgesCraftingCapability;
import com.ladestitute.runicages.capability.defense.RunicAgesDefenseCapability;
import com.ladestitute.runicages.capability.farming.RunicAgesFarmingCapability;
import com.ladestitute.runicages.capability.herblore.RunicAgesHerbloreCapability;
import com.ladestitute.runicages.capability.magic.RunicAgesMagicCapability;
import com.ladestitute.runicages.capability.mining.RunicAgesMiningCapability;
import com.ladestitute.runicages.capability.ranged.RunicAgesRangedCapability;
import com.ladestitute.runicages.capability.runecrafting.RunicAgesRunecraftingCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.capability.smithing.RunicAgesSmithingCapability;
import com.ladestitute.runicages.capability.strength.RunicAgesStrengthCapability;
import com.ladestitute.runicages.capability.thieving.RunicAgesThievingCapability;
import com.ladestitute.runicages.capability.woodcutting.RunicAgesWoodcuttingCapability;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class RunicAgesSkillsHandler {

//    @SubscribeEvent
//    public void magicbookuiopen(TickEvent.PlayerTickEvent event) {
//        event.player.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
//        {
//            if(ed.getreceiveattackxp() >= 2)
//            {
//                ed.setreceiveattackxp(0);
//                RunicAgesExtraDataCapability.levelClientUpdate(event.player);
//            }
//        });
//
//    }

    @SubscribeEvent
    public void onPlayerClone(PlayerEvent.Clone e) {
        if (e.isWasDeath()) e.getOriginal().reviveCaps();

        RunicAgesMiningCapability.Provider.getFrom(e.getOriginal())
                .ifPresent(oldData -> RunicAgesMiningCapability.Provider.getFrom(e.getEntity())
                        .ifPresent(data -> data.deserializeNBT(oldData.serializeNBT())));
        RunicAgesCraftingCapability.Provider.getFrom(e.getOriginal())
                .ifPresent(oldData -> RunicAgesCraftingCapability.Provider.getFrom(e.getEntity())
                        .ifPresent(data -> data.deserializeNBT(oldData.serializeNBT())));
        RunicAgesMagicCapability.Provider.getFrom(e.getOriginal())
                .ifPresent(oldData -> RunicAgesMagicCapability.Provider.getFrom(e.getEntity())
                        .ifPresent(data -> data.deserializeNBT(oldData.serializeNBT())));
        RunicAgesRunecraftingCapability.Provider.getFrom(e.getOriginal())
                .ifPresent(oldData -> RunicAgesRunecraftingCapability.Provider.getFrom(e.getEntity())
                        .ifPresent(data -> data.deserializeNBT(oldData.serializeNBT())));
        RunicAgesSmithingCapability.Provider.getFrom(e.getOriginal())
                .ifPresent(oldData -> RunicAgesSmithingCapability.Provider.getFrom(e.getEntity())
                        .ifPresent(data -> data.deserializeNBT(oldData.serializeNBT())));
        RunicAgesWoodcuttingCapability.Provider.getFrom(e.getOriginal())
                .ifPresent(oldData -> RunicAgesWoodcuttingCapability.Provider.getFrom(e.getEntity())
                        .ifPresent(data -> data.deserializeNBT(oldData.serializeNBT())));
        RunicAgesExtraDataCapability.Provider.getFrom(e.getOriginal())
                .ifPresent(oldData -> RunicAgesExtraDataCapability.Provider.getFrom(e.getEntity())
                        .ifPresent(data -> data.deserializeNBT(oldData.serializeNBT())));
        RunicAgesAttackCapability.Provider.getFrom(e.getOriginal())
                .ifPresent(oldData -> RunicAgesAttackCapability.Provider.getFrom(e.getEntity())
                        .ifPresent(data -> data.deserializeNBT(oldData.serializeNBT())));
        RunicAgesStrengthCapability.Provider.getFrom(e.getOriginal())
                .ifPresent(oldData -> RunicAgesStrengthCapability.Provider.getFrom(e.getEntity())
                        .ifPresent(data -> data.deserializeNBT(oldData.serializeNBT())));
        RunicAgesDefenseCapability.Provider.getFrom(e.getOriginal())
                .ifPresent(oldData -> RunicAgesDefenseCapability.Provider.getFrom(e.getEntity())
                        .ifPresent(data -> data.deserializeNBT(oldData.serializeNBT())));
        RunicAgesRangedCapability.Provider.getFrom(e.getOriginal())
                .ifPresent(oldData -> RunicAgesRangedCapability.Provider.getFrom(e.getEntity())
                        .ifPresent(data -> data.deserializeNBT(oldData.serializeNBT())));
        RunicAgesHerbloreCapability.Provider.getFrom(e.getOriginal())
                .ifPresent(oldData -> RunicAgesHerbloreCapability.Provider.getFrom(e.getEntity())
                        .ifPresent(data -> data.deserializeNBT(oldData.serializeNBT())));
        RunicAgesFarmingCapability.Provider.getFrom(e.getOriginal())
                .ifPresent(oldData -> RunicAgesFarmingCapability.Provider.getFrom(e.getEntity())
                        .ifPresent(data -> data.deserializeNBT(oldData.serializeNBT())));
        RunicAgesThievingCapability.Provider.getFrom(e.getOriginal())
                .ifPresent(oldData -> RunicAgesThievingCapability.Provider.getFrom(e.getEntity())
                        .ifPresent(data -> data.deserializeNBT(oldData.serializeNBT())));
        RunicAgesAgilityCapability.Provider.getFrom(e.getOriginal())
                .ifPresent(oldData -> RunicAgesAgilityCapability.Provider.getFrom(e.getEntity())
                        .ifPresent(data -> data.deserializeNBT(oldData.serializeNBT())));

        if(e.isWasDeath()) e.getOriginal().invalidateCaps();
    }

    //Todo: temp-measure to stop farming level not updating upon login, checked all farming code
    //Todo: and couldn't find the issue currently
    @SubscribeEvent
    public void onlogin(PlayerEvent.PlayerLoggedInEvent e)
    {
        RunicAgesExtraDataCapability.levelClientUpdate(e.getEntity());
        RunicAgesCraftingCapability.levelClientUpdate(e.getEntity());
        RunicAgesMagicCapability.levelClientUpdate(e.getEntity());
        RunicAgesRunecraftingCapability.levelClientUpdate(e.getEntity());
        RunicAgesSmithingCapability.levelClientUpdate(e.getEntity());
        RunicAgesWoodcuttingCapability.levelClientUpdate(e.getEntity());
        RunicAgesMiningCapability.levelClientUpdate(e.getEntity());
        RunicAgesAttackCapability.levelClientUpdate(e.getEntity());
        RunicAgesStrengthCapability.levelClientUpdate(e.getEntity());
        RunicAgesDefenseCapability.levelClientUpdate(e.getEntity());
        RunicAgesRangedCapability.levelClientUpdate(e.getEntity());
        RunicAgesHerbloreCapability.levelClientUpdate(e.getEntity());
        RunicAgesFarmingCapability.levelClientUpdate(e.getEntity());
        RunicAgesThievingCapability.levelClientUpdate(e.getEntity());
        RunicAgesAgilityCapability.levelClientUpdate(e.getEntity());
    }

    @SubscribeEvent
    public void onPlayerRespawn(PlayerEvent.PlayerRespawnEvent e) {
        RunicAgesExtraDataCapability.levelClientUpdate(e.getEntity());
        RunicAgesCraftingCapability.levelClientUpdate(e.getEntity());
        RunicAgesMagicCapability.levelClientUpdate(e.getEntity());
        RunicAgesRunecraftingCapability.levelClientUpdate(e.getEntity());
        RunicAgesSmithingCapability.levelClientUpdate(e.getEntity());
        RunicAgesWoodcuttingCapability.levelClientUpdate(e.getEntity());
        RunicAgesMiningCapability.levelClientUpdate(e.getEntity());
        RunicAgesAttackCapability.levelClientUpdate(e.getEntity());
        RunicAgesStrengthCapability.levelClientUpdate(e.getEntity());
        RunicAgesDefenseCapability.levelClientUpdate(e.getEntity());
        RunicAgesRangedCapability.levelClientUpdate(e.getEntity());
        RunicAgesHerbloreCapability.levelClientUpdate(e.getEntity());
        RunicAgesFarmingCapability.levelClientUpdate(e.getEntity());
        RunicAgesThievingCapability.levelClientUpdate(e.getEntity());
        RunicAgesAgilityCapability.levelClientUpdate(e.getEntity());
    }

    @SubscribeEvent
    public void onPlayerChangeDimension(PlayerEvent.PlayerChangedDimensionEvent e) {
        RunicAgesExtraDataCapability.levelClientUpdate(e.getEntity());
        RunicAgesCraftingCapability.levelClientUpdate(e.getEntity());
        RunicAgesMagicCapability.levelClientUpdate(e.getEntity());
        RunicAgesRunecraftingCapability.levelClientUpdate(e.getEntity());
        RunicAgesSmithingCapability.levelClientUpdate(e.getEntity());
        RunicAgesWoodcuttingCapability.levelClientUpdate(e.getEntity());
        RunicAgesMiningCapability.levelClientUpdate(e.getEntity());
        RunicAgesAttackCapability.levelClientUpdate(e.getEntity());
        RunicAgesStrengthCapability.levelClientUpdate(e.getEntity());
        RunicAgesDefenseCapability.levelClientUpdate(e.getEntity());
        RunicAgesRangedCapability.levelClientUpdate(e.getEntity());
        RunicAgesHerbloreCapability.levelClientUpdate(e.getEntity());
        RunicAgesFarmingCapability.levelClientUpdate(e.getEntity());
        RunicAgesThievingCapability.levelClientUpdate(e.getEntity());
        RunicAgesAgilityCapability.levelClientUpdate(e.getEntity());
    }
}
