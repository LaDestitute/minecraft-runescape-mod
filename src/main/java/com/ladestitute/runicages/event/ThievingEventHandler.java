package com.ladestitute.runicages.event;

import com.ladestitute.runicages.capability.attack.RunicAgesAttackCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.capability.smithing.RunicAgesSmithingCapability;
import com.ladestitute.runicages.capability.thieving.RunicAgesThievingCapability;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.registry.VillagerInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.damagesource.DamageSources;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.damagesource.DamageTypes;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.npc.Villager;
import net.minecraft.world.entity.npc.VillagerProfession;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.Random;

public class ThievingEventHandler {

    @SubscribeEvent
    public void mobdrops(PlayerInteractEvent.EntityInteract event)
    {
        event.getEntity().getCapability(RunicAgesThievingCapability.Provider.THIEVING_LEVEL).ifPresent(t ->
        {
            Random rand = new Random();
            if (!event.getEntity().level().isClientSide() && event.getTarget() instanceof Villager && event.getEntity().isCrouching())
            {
                if(((Villager) event.getTarget()).getVillagerData().getProfession().equals(VillagerInit.BANKER.get()))
                {
                    return;
                }
                if(!((Villager) event.getTarget()).getVillagerData().getProfession().equals(VillagerProfession.FARMER))
                {
                int thievingchance = rand.nextInt(101);
                int quantity = rand.nextInt(1)+1;
                ItemStack goldnuggetsstack = new ItemStack(Items.GOLD_NUGGET);
                if(t.getThievingLevel() >= 4) {
                    if (thievingchance < 71 + Math.round(0.25 * t.getThievingLevel())) {
                        goldnuggetsstack.setCount(quantity);
                        ItemHandlerHelper.giveItemToPlayer(event.getEntity(), goldnuggetsstack);
                        t.addThievingXP(event.getEntity(), 8);
                        event.getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
                                ed.addxptotalxp(8));
                        RunicAgesThievingCapability.levelClientUpdate(event.getEntity());
                        RunicAgesExtraDataCapability.levelClientUpdate(event.getEntity());
                    }
                    else if (thievingchance > 71 + Math.round(0.25 * t.getThievingLevel())) {
                        event.getEntity().sendSystemMessage(Component.literal("You have been stunned from getting caught pickpocketing!"));
                        event.getEntity().addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SLOWDOWN, 80, 10));
                            event.getEntity().hurt(event.getEntity().damageSources().generic(), 1F);

                    }
                }
                if(t.getThievingLevel() <= 3) {
                    if (thievingchance < 71) {
                        goldnuggetsstack.setCount(quantity);
                        ItemHandlerHelper.giveItemToPlayer(event.getEntity(), goldnuggetsstack);
                        t.addThievingXP(event.getEntity(), 8);
                        event.getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
                                ed.addxptotalxp(8));
                        RunicAgesThievingCapability.levelClientUpdate(event.getEntity());
                        RunicAgesExtraDataCapability.levelClientUpdate(event.getEntity());
                    }
                    else if (thievingchance > 71) {
                        event.getEntity().sendSystemMessage(Component.literal("You have been stunned from getting caught pickpocketing!"));
                        event.getEntity().addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SLOWDOWN, 80, 10));
                        event.getEntity().hurt(event.getEntity().damageSources().generic(), 1F);
                    }
                }
            }
                if(((Villager) event.getTarget()).getVillagerData().getProfession().equals(VillagerProfession.FARMER) && t.getThievingLevel() >= 10)
                {
                    int thievingchance = rand.nextInt(101);
                    ItemStack goldnuggetsstack = new ItemStack(Items.GOLD_NUGGET);
                    ItemStack potatostack = new ItemStack(Items.POTATO);
                    if(t.getThievingLevel() >= 4) {
                        if (thievingchance < 53 + Math.round(0.36 * t.getThievingLevel())) {
                            int potatochance = rand.nextInt(101);
                            if(potatochance <= 4)
                            {
                                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), potatostack);
                            }
                            else ItemHandlerHelper.giveItemToPlayer(event.getEntity(), goldnuggetsstack);
                            t.addThievingXP(event.getEntity(), (int) Math.round(14.5));
                            event.getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
                                    ed.addxptotalxp((int) Math.round(14.5)));
                            RunicAgesThievingCapability.levelClientUpdate(event.getEntity());
                            RunicAgesExtraDataCapability.levelClientUpdate(event.getEntity());
                        }
                        else if (thievingchance > 53 + Math.round(0.36 * t.getThievingLevel())) {
                            event.getEntity().sendSystemMessage(Component.literal("You have been stunned from getting caught pickpocketing!"));
                            event.getEntity().addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SLOWDOWN, 80, 10));
                            event.getEntity().hurt(event.getEntity().damageSources().generic(), 1F);
                        }
                    }
                    if(t.getThievingLevel() <= 3) {
                        if (thievingchance < 53) {
                            int potatochance = rand.nextInt(101);
                            if(potatochance <= 4)
                            {
                                ItemHandlerHelper.giveItemToPlayer(event.getEntity(), potatostack);
                            }
                            else ItemHandlerHelper.giveItemToPlayer(event.getEntity(), goldnuggetsstack);
                            t.addThievingXP(event.getEntity(), (int) Math.round(14.5));
                            event.getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
                                    ed.addxptotalxp((int) Math.round(14.5)));
                            RunicAgesThievingCapability.levelClientUpdate(event.getEntity());
                            RunicAgesExtraDataCapability.levelClientUpdate(event.getEntity());
                        }
                        else if (thievingchance > 53) {
                            event.getEntity().sendSystemMessage(Component.literal("You have been stunned from getting caught pickpocketing!"));
                            event.getEntity().addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SLOWDOWN, 80, 10));
                            event.getEntity().hurt(event.getEntity().damageSources().generic(), 1.6F);
                        }
                    }
                }
            }
        });
    }
}
