package com.ladestitute.runicages.event;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.capability.smithing.RunicAgesSmithingCapability;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.items.ItemHandlerHelper;

public class SmithingEventHandler {

    //Keeping, saving for when I can figure out how to send updates to the client with the inventory state
//    @SubscribeEvent
//    public void smelting(TickEvent.PlayerTickEvent event) {
//        event.player.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(s ->
//        {
//            if(s.getIsSmelting() == 1 && s.getUsingFurnace() == 1)
//            {
//                s.addSmeltTicks(1);
//                RunicAgesSmithingCapability.levelClientUpdate(event.player);
//            }
//            if(s.getMakeCount() == 1)
//            {
//                if(s.getSmeltTicks()==1) {
//                    for (ItemStack ores1 : event.player.getInventory().items) {
//                        if (ores1.getItem() == ItemInit.TIN_ORE.get() && ores1.getCount() >= 1) {
//                            if(!event.player.level.isClientSide) {
//                                ores1.setCount(ores1.getCount() - 1);
//                            }
//                            s.setSmeltStage(1);
//                            RunicAgesSmithingCapability.levelClientUpdate(event.player);
//                            break;
//                        }
//                    }
//                    if (s.getSmeltStage() == 1) {
//                        for (ItemStack ores2 : event.player.getInventory().items) {
//                            if (ores2.getItem() == Items.RAW_COPPER && ores2.getCount() >= 1 ||
//                                    ores2.getItem() == Items.COPPER_INGOT && ores2.getCount() >= 1) {
//                                    ores2.setCount(ores2.getCount() - 1);
//                                s.setSmeltStage(2);
//                                s.setSmelting(1);
//                                s.setBarType(1);
//                                RunicAgesSmithingCapability.levelClientUpdate(event.player);
//                                break;
//                            }
//                        }
//                    }
//                }
//                if (s.getSmeltTicks() == 120 && s.getBarType() == 1) {
//                        ItemHandlerHelper.giveItemToPlayer(event.player, ItemInit.BRONZE_BAR.get().getDefaultInstance());
//                    if (RunicAgesConfig.modernrs.get())
//                        s.addSmithingXP(event.player, 1);
//                    else s.addSmithingXP(event.player, (int) Math.round(6.2));
//                    event.player.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
//                    {
//                        if (RunicAgesConfig.modernrs.get())
//                            ed.addxptotalxp(1);
//                        else ed.addxptotalxp((int) Math.round(6.2));
//                    });
//                    //   s.setSmeltCount(1);
//                    s.setSmeltTicks(0);
//                    s.setBarType(0);
//                    s.setSmeltStage(0);
//                    s.setSmelting(0);
//                    RunicAgesSmithingCapability.levelClientUpdate(event.player);
//                }
//            }
//            //
//            if(s.getMakeCount() == 5||s.getMakeCount() == 10) {
//                if(s.getSmeltCount() < s.getMakeCount()) {
//                  if(s.getSmeltTicks()==1) {
//                      for (ItemStack ores1 : event.player.getInventory().items) {
//                          if (ores1.getItem() == ItemInit.TIN_ORE.get() && ores1.getCount() >= 1) {
//                              ores1.setCount(ores1.getCount() - 1);
//                              s.setSmeltStage(1);
//                              RunicAgesSmithingCapability.levelClientUpdate(event.player);
//                              break;
//                          }
//                      }
//                      if (s.getSmeltStage() == 1) {
//                          for (ItemStack ores2 : event.player.getInventory().items) {
//                              if (ores2.getItem() == Items.RAW_COPPER && ores2.getCount() >= 1 ||
//                                      ores2.getItem() == Items.COPPER_INGOT && ores2.getCount() >= 1) {
//                                  ores2.setCount(ores2.getCount() - 1);
//                                  s.setSmeltStage(2);
//                                  s.setSmelting(1);
//                                  s.setBarType(1);
//                                  RunicAgesSmithingCapability.levelClientUpdate(event.player);
//                                  break;
//                              }
//                          }
//                      }
//                  }
//                    if (s.getSmeltTicks() == 120 && s.getBarType() == 1) {
//                        ItemHandlerHelper.giveItemToPlayer(event.player, ItemInit.BRONZE_BAR.get().getDefaultInstance());
//                        if (RunicAgesConfig.modernrs.get())
//                            s.addSmithingXP(event.player, 1);
//                        else s.addSmithingXP(event.player, (int) Math.round(6.2));
//                        event.player.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
//                        {
//                            if (RunicAgesConfig.modernrs.get())
//                                ed.addxptotalxp(1);
//                            else ed.addxptotalxp((int) Math.round(6.2));
//                        });
//                        s.setSmeltCount(s.getSmeltCount() + 1);
//                        s.setSmeltTicks(0);
//                        s.setSmeltStage(0);
//                        RunicAgesSmithingCapability.levelClientUpdate(event.player);
//                    }
//                }
//                if(s.getSmeltCount() == s.getMakeCount())
//                {
//                    s.setSmeltTicks(0);
//                    s.setSmeltStage(0);
//                    s.setSmeltTicks(0);
//                    s.setBarType(0);
//                    s.setSmeltCount(0);
//                    s.setSmelting(0);
//                    RunicAgesSmithingCapability.levelClientUpdate(event.player);
//                }
//            }
//        });
//
//    }

    @SubscribeEvent
    public void smithinglevelup(TickEvent.PlayerTickEvent event) {
        event.player.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(h ->
        {
            if(!event.player.level().isClientSide && h.getSmithingLevel() >= 1)
            {
                ResourceLocation[] recipe = new ResourceLocation[5];
                recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "alloy_furnace");
                recipe[1] = new ResourceLocation(RunicAgesMain.MODID, "smithing_furnace");
                recipe[2] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_nugget");
                recipe[3] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_chain");
                recipe[4] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_bar_from_nuggets");
                event.player.awardRecipesByKey(recipe);
            }
            if(!event.player.level().isClientSide && h.getSmithingLevel() >= 1 && RunicAgesConfig.modernrs.get())
            {
                ResourceLocation[] recipe = new ResourceLocation[7];
                recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_helm");
                recipe[1] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_mace");
                recipe[2] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_sword");
                recipe[3] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_platebody");
                recipe[4] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_platelegs");
                recipe[5] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_boots");
                recipe[6] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_chainbody");
                event.player.awardRecipesByKey(recipe);
            }
            if(!event.player.level().isClientSide && h.getSmithingLevel() >=2 && !RunicAgesConfig.modernrs.get())
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_mace");
                event.player.awardRecipesByKey(recipe);
            }
            if(!event.player.level().isClientSide && h.getSmithingLevel() >=3 && !RunicAgesConfig.modernrs.get())
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_helm");
                event.player.awardRecipesByKey(recipe);
            }
            if(!event.player.level().isClientSide && h.getSmithingLevel() >=4 && !RunicAgesConfig.modernrs.get())
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_sword");
                event.player.awardRecipesByKey(recipe);
            }
            if(!event.player.level().isClientSide && h.getSmithingLevel() >=4 && !RunicAgesConfig.modernrs.get())
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_chainbody");
                event.player.awardRecipesByKey(recipe);
            }

            if(!event.player.level().isClientSide && h.getSmithingLevel() >=18 && !RunicAgesConfig.modernrs.get())
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_platebody");
                event.player.awardRecipesByKey(recipe);
            }
            if(!event.player.level().isClientSide && h.getSmithingLevel() >=16 && !RunicAgesConfig.modernrs.get())
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_platelegs");
                event.player.awardRecipesByKey(recipe);
            }
            if(!event.player.level().isClientSide && h.getSmithingLevel() >=9 && !RunicAgesConfig.modernrs.get())
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation(RunicAgesMain.MODID, "bronze/bronze_boots");
                event.player.awardRecipesByKey(recipe);
            }

            if(!event.player.level().isClientSide && h.getSmithingLevel() >= 10)
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation("runicages", "iron_alloy_furnace");
                event.player.awardRecipesByKey(recipe);
                //   event.player.sendMessage(new StringTextComponent("You have hit Smithing level 4 with new Smithing unlocks, check your guide book!"), event.player.getUUID());
            }
            if(!event.player.level().isClientSide && RunicAgesConfig.modernrs.get() && h.getSmithingLevel() >= 10)
            {
                ResourceLocation[] recipe = new ResourceLocation[9];
                recipe[0] = new ResourceLocation("minecraft", "iron_helmet");
                recipe[1] = new ResourceLocation("minecraft", "iron_sword");
                recipe[2] = new ResourceLocation("minecraft", "shield");
                recipe[3] = new ResourceLocation("minecraft", "iron_boots");
                recipe[4] = new ResourceLocation("minecraft", "iron_leggings");
                recipe[5] = new ResourceLocation("minecraft", "iron_chestplate");
                recipe[6] = new ResourceLocation("minecraft", "iron_pickaxe");
                recipe[7] = new ResourceLocation("minecraft", "iron_axe");
                recipe[8] = new ResourceLocation(RunicAgesMain.MODID, "iron/iron_dagger");
                event.player.awardRecipesByKey(recipe);
                //   event.player.sendMessage(new StringTextComponent("You have hit Smithing level 4 with new Smithing unlocks, check your guide book!"), event.player.getUUID());
            }
            if(!event.player.level().isClientSide && !RunicAgesConfig.modernrs.get() && h.getSmithingLevel() >= 15)
            {
                ResourceLocation[] recipe = new ResourceLocation[3];
                recipe[0] = new ResourceLocation("minecraft", "iron_pickaxe");
                recipe[1] = new ResourceLocation("minecraft", "iron_axe");
                recipe[2] = new ResourceLocation(RunicAgesMain.MODID, "iron/iron_dagger");
                event.player.awardRecipesByKey(recipe);
                //   event.player.sendMessage(new StringTextComponent("You have hit Smithing level 4 with new Smithing unlocks, check your guide book!"), event.player.getUUID());
            }
            if(!event.player.level().isClientSide && !RunicAgesConfig.modernrs.get() && h.getSmithingLevel() >= 18)
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation("minecraft", "iron_helmet");
                event.player.awardRecipesByKey(recipe);
                //   event.player.sendMessage(new StringTextComponent("You have hit Smithing level 4 with new Smithing unlocks, check your guide book!"), event.player.getUUID());
            }
            if(!event.player.level().isClientSide && !RunicAgesConfig.modernrs.get() && h.getSmithingLevel() >= 19)
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation("minecraft", "iron_sword");
                event.player.awardRecipesByKey(recipe);
                //   event.player.sendMessage(new StringTextComponent("You have hit Smithing level 4 with new Smithing unlocks, check your guide book!"), event.player.getUUID());
            }
            if(!event.player.level().isClientSide && h.getSmithingLevel() >= 20)
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation("runicages", "silver_smithing_furnace");
                event.player.awardRecipesByKey(recipe);
                //   event.player.sendMessage(new StringTextComponent("You have hit Smithing level 4 with new Smithing unlocks, check your guide book!"), event.player.getUUID());
            }
            if(!event.player.level().isClientSide && !RunicAgesConfig.modernrs.get() && h.getSmithingLevel() >= 23)
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation("minecraft", "shield");
                event.player.awardRecipesByKey(recipe);
                //   event.player.sendMessage(new StringTextComponent("You have hit Smithing level 4 with new Smithing unlocks, check your guide book!"), event.player.getUUID());
            }
            if(!event.player.level().isClientSide && !RunicAgesConfig.modernrs.get() && h.getSmithingLevel() >= 24)
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation("minecraft", "iron_boots");
                event.player.awardRecipesByKey(recipe);
                //   event.player.sendMessage(new StringTextComponent("You have hit Smithing level 4 with new Smithing unlocks, check your guide book!"), event.player.getUUID());
            }
            if(!event.player.level().isClientSide && !RunicAgesConfig.modernrs.get() && h.getSmithingLevel() >= 31)
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation("minecraft", "iron_leggings");
                event.player.awardRecipesByKey(recipe);
                //   event.player.sendMessage(new StringTextComponent("You have hit Smithing level 4 with new Smithing unlocks, check your guide book!"), event.player.getUUID());
            }
            if(!event.player.level().isClientSide && !RunicAgesConfig.modernrs.get() && h.getSmithingLevel() >= 33)
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation("minecraft", "iron_chestplate");
                event.player.awardRecipesByKey(recipe);
                //   event.player.sendMessage(new StringTextComponent("You have hit Smithing level 4 with new Smithing unlocks, check your guide book!"), event.player.getUUID());
            }
            if(!event.player.level().isClientSide && h.getSmithingLevel() >= 30)
            {
                ResourceLocation[] recipe = new ResourceLocation[1];
                recipe[0] = new ResourceLocation("runicages", "mithril_alloy_furnace");
                event.player.awardRecipesByKey(recipe);
                //   event.player.sendMessage(new StringTextComponent("You have hit Smithing level 4 with new Smithing unlocks, check your guide book!"), event.player.getUUID());
            }
            if(!event.player.level().isClientSide && h.getSmithingLevel() >= 40)
            {
                ResourceLocation[] recipe = new ResourceLocation[2];
                recipe[0] = new ResourceLocation("runicages", "gold_smithing_furnace");
                recipe[1] = new ResourceLocation("runicages", "adamantite_alloy_furnace");
                event.player.awardRecipesByKey(recipe);
                //   event.player.sendMessage(new StringTextComponent("You have hit Smithing level 4 with new Smithing unlocks, check your guide book!"), event.player.getUUID());
            }

        });
    }

    @SubscribeEvent
    public void smithinglevelup (PlayerEvent.ItemCraftedEvent event){
        event.getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            event.getEntity().getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(h ->
            {
                if (!event.getEntity().level().isClientSide()) {
                    if (event.getCrafting().getItem() == ItemInit.BRONZE_HATCHET.get()) {
                        if (RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 15);
                            ed.addxptotalxp(15);
                        }
                        if (!RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 12);
                            ed.addxptotalxp(12);
                        }
                        event.getEntity().sendSystemMessage(Component.literal("You have gained smithing xp: "
                                + h.getSmithingXP() + " XP"));

                    }
                    if (event.getCrafting().getItem() == ItemInit.BRONZE_PICKAXE.get()) {
                        if (RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 30);
                            ed.addxptotalxp(30);
                        }
                        if (!RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 24);
                            ed.addxptotalxp(24);
                        }
                        event.getEntity().sendSystemMessage(Component.literal("You have gained smithing xp: "
                                + h.getSmithingXP() + " XP"));
                    }
                    if (event.getCrafting().getItem() == ItemInit.IRON_DAGGER.get()) {
                        if (RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 80);
                            ed.addxptotalxp(80);
                        }
                        if (!RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 25);
                            ed.addxptotalxp(25);
                        }
                        event.getEntity().sendSystemMessage(Component.literal("You have gained smithing xp: "
                                + h.getSmithingXP() + " XP"));
                    }
                    if (event.getCrafting().getItem() == ItemInit.BRONZE_DAGGER.get() ||
                            event.getCrafting().getItem() == ItemInit.BRONZE_HELM.get() ||
                            event.getCrafting().getItem() == ItemInit.BRONZE_SWORD.get() ||
                            event.getCrafting().getItem() == ItemInit.BRONZE_MACE.get()) {
                        if (RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 30);
                            ed.addxptotalxp(30);
                        }
                        if (!RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 12);
                            ed.addxptotalxp(12);
                        }
                        event.getEntity().sendSystemMessage(Component.literal("You have gained smithing xp: "
                                + h.getSmithingXP() + " XP"));
                    }
                    if (event.getCrafting().getItem() == ItemInit.BRONZE_BOOTS.get()) {
                        if (RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 15);
                            ed.addxptotalxp(15);
                        }
                        if (!RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 18);
                            ed.addxptotalxp(18);
                        }
                        event.getEntity().sendSystemMessage(Component.literal("You have gained smithing xp: "
                                + h.getSmithingXP() + " XP"));
                    }
                    if (event.getCrafting().getItem() == ItemInit.BRONZE_PLATEBODY.get()) {
                        if (RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 75);
                            ed.addxptotalxp(75);
                        }
                        if (!RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 62);
                            ed.addxptotalxp(62);
                        }
                        event.getEntity().sendSystemMessage(Component.literal("You have gained smithing xp: "
                                + h.getSmithingXP() + " XP"));
                    }
                    if (event.getCrafting().getItem() == ItemInit.BRONZE_PLATELEGS.get()) {
                        if (RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 45);
                            ed.addxptotalxp(45);
                        }
                        if (!RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 37);
                            ed.addxptotalxp(37);
                        }
                        event.getEntity().sendSystemMessage(Component.literal("You have gained smithing xp: "
                                + h.getSmithingXP() + " XP"));
                    }
                    if (event.getCrafting().getItem() == Items.IRON_PICKAXE) {
                        if (RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 80);
                            ed.addxptotalxp(80);
                        }
                        if (!RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 50);
                            ed.addxptotalxp(50);
                        }
                        event.getEntity().sendSystemMessage(Component.literal("You have gained smithing xp: "
                                + h.getSmithingXP() + " XP"));

                    }
                    if (event.getCrafting().getItem() == Items.IRON_AXE) {
                        if (RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 40);
                            ed.addxptotalxp(40);
                        }
                        if (!RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 25);
                            ed.addxptotalxp(25);
                        }
                        event.getEntity().sendSystemMessage(Component.literal("You have gained smithing xp: "
                                + h.getSmithingXP() + " XP"));
                    }
                    if (event.getCrafting().getItem() == Items.IRON_HELMET ||
                            event.getCrafting().getItem() == Items.IRON_SWORD) {
                        if (RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 80);
                            ed.addxptotalxp(80);
                        }
                        if (!RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 25);
                            ed.addxptotalxp(25);
                        }
                        event.getEntity().sendSystemMessage(Component.literal("You have gained smithing xp: "
                                + h.getSmithingXP() + " XP"));
                    }
                    if (event.getCrafting().getItem() == Items.SHIELD) {
                        if (RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 80);
                            ed.addxptotalxp(80);
                        }
                        if (!RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 50);
                            ed.addxptotalxp(50);
                        }
                        event.getEntity().sendSystemMessage(Component.literal("You have gained smithing xp: "
                                + h.getSmithingXP() + " XP"));
                    }
                    if (event.getCrafting().getItem() == Items.IRON_BOOTS) {
                        if (RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 40);
                            ed.addxptotalxp(40);
                        }
                        if (!RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 50);
                            ed.addxptotalxp(50);
                        }
                        event.getEntity().sendSystemMessage(Component.literal("You have gained smithing xp: "
                                + h.getSmithingXP() + " XP"));
                    }
                    if (event.getCrafting().getItem() == Items.IRON_LEGGINGS) {
                        if (RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 120);
                            ed.addxptotalxp(120);
                        }
                        if (!RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 75);
                            ed.addxptotalxp(75);
                        }
                        event.getEntity().sendSystemMessage(Component.literal("You have gained smithing xp: "
                                + h.getSmithingXP() + " XP"));
                    }
                    if (event.getCrafting().getItem() == Items.IRON_CHESTPLATE) {
                        if (RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 200);
                            ed.addxptotalxp(200);
                        }
                        if (!RunicAgesConfig.modernrs.get()) {
                            h.addSmithingXP(event.getEntity(), 125);
                            ed.addxptotalxp(125);
                        }
                        event.getEntity().sendSystemMessage(Component.literal("You have gained smithing xp: "
                                + h.getSmithingXP() + " XP"));

                    }
                    RunicAgesSmithingCapability.levelClientUpdate(event.getEntity());
                }
            });
        });
    }
}
