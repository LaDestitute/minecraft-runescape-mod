package com.ladestitute.runicages.event;

import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.capability.woodcutting.RunicAgesWoodcuttingCapability;
import com.ladestitute.runicages.registry.BlockInit;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.level.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.items.ItemHandlerHelper;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;

import java.util.Random;

public class WoodcuttingEventHandler {
    @SubscribeEvent
    public void breakdeny(BlockEvent.BreakEvent event) {
        event.getPlayer().getCapability(RunicAgesWoodcuttingCapability.Provider.WOODCUTTING_LEVEL).ifPresent(h ->
        {
            if(!event.getPlayer().isCreative() && RunicAgesConfig.restrictchopping.get()) {
                if (event.getState().getBlock() == BlockInit.NORMAL_TREE_LOG.get()) {
                    event.getPlayer().displayClientMessage(Component.literal("To mine this block, right-click it with an axe in mainhand."),
                            true);
                    event.setCanceled(true);
                }
                if (h.getWoodcuttingLevel() < 8 & event.getState().getBlock() == Blocks.BIRCH_LOG||
                        h.getWoodcuttingLevel() < 8 && event.getState().getBlock() == Blocks.STRIPPED_BIRCH_LOG) {
                    event.getPlayer().displayClientMessage(Component.literal("Level 8 Woodcutting is required to cut this tree."),
                            true);
                    event.setCanceled(true);
                }
                if (h.getWoodcuttingLevel() < 15 && event.getState().getBlock() == Blocks.OAK_LOG||
                        h.getWoodcuttingLevel() < 15 && event.getState().getBlock() == Blocks.OAK_WOOD||
                        h.getWoodcuttingLevel() < 15 && event.getState().getBlock() == Blocks.DARK_OAK_LOG||
                        h.getWoodcuttingLevel() < 15 && event.getState().getBlock() == Blocks.DARK_OAK_WOOD) {
                    event.getPlayer().displayClientMessage(Component.literal("Level 15 Woodcutting is required to cut this tree."),
                            true);
                    event.setCanceled(true);
                }
                if (h.getWoodcuttingLevel() < 26 && event.getState().getBlock() == Blocks.MANGROVE_LOG||
                        h.getWoodcuttingLevel() < 26 && event.getState().getBlock() == Blocks.MANGROVE_WOOD) {
                    event.getPlayer().displayClientMessage(Component.literal("Level 26 Woodcutting is required to cut this tree."),
                            true);
                    event.setCanceled(true);
                }
                if (h.getWoodcuttingLevel() < 38 && event.getState().getBlock() == Blocks.JUNGLE_LOG||
                        h.getWoodcuttingLevel() < 38 && event.getState().getBlock() == Blocks.JUNGLE_WOOD) {
                    event.getPlayer().displayClientMessage(Component.literal("Level 38 Woodcutting is required to cut this tree."),
                            true);
                    event.setCanceled(true);
                }
                if (h.getWoodcuttingLevel() < 47 && event.getState().getBlock() == Blocks.ACACIA_LOG||
                        h.getWoodcuttingLevel() < 47 && event.getState().getBlock() == Blocks.ACACIA_WOOD) {
                    event.getPlayer().displayClientMessage(Component.literal("Level 47 Woodcutting is required to cut this tree."),
                            true);
                    event.setCanceled(true);
                }
                if (h.getWoodcuttingLevel() < 54 && event.getState().getBlock() == Blocks.SPRUCE_LOG||
                        h.getWoodcuttingLevel() < 54 && event.getState().getBlock() == Blocks.SPRUCE_WOOD) {
                    event.getPlayer().displayClientMessage(Component.literal("Level 54 Woodcutting is required to cut this tree."),
                            true);
                    event.setCanceled(true);
                }
            }
        });
    }

    @SubscribeEvent
    public void woodcutting(PlayerInteractEvent.RightClickBlock event)
    {
        event.getEntity().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            event.getEntity().getCapability(RunicAgesWoodcuttingCapability.Provider.WOODCUTTING_LEVEL).ifPresent(h ->
            {
                Player player = event.getEntity();
                BlockPos pos = event.getPos();
                Level level = player.level();
                Block block = level.getBlockState(pos).getBlock();
                ItemStack handstack = player.getItemBySlot(EquipmentSlot.MAINHAND);
                //Pre-checks based on level/tool
                if (handstack.getItem() == ItemInit.BRONZE_HATCHET.get() ||
                        handstack.getItem() == Items.IRON_AXE ||
                        handstack.getItem() == Items.WOODEN_AXE ||
                        handstack.getItem() == Items.STONE_AXE ||
                        handstack.getItem() == Items.DIAMOND_AXE ||
                        handstack.getItem() == Items.GOLDEN_AXE ||
                        handstack.getItem() == Items.NETHERITE_AXE) {
                    if (block == Blocks.OAK_LOG ||
                            block == Blocks.SPRUCE_LOG ||
                            block == Blocks.JUNGLE_LOG ||
                            block == Blocks.ACACIA_LOG ||
                            block == Blocks.DARK_OAK_LOG ||
                            block == Blocks.MANGROVE_LOG) {
                        event.setCanceled(true);
                    }
                }
                if (!player.level().isClientSide() && h.getWoodcuttingLevel() >= 1
                        && handstack.getItem() == ItemInit.BRONZE_HATCHET.get() ||
                        !player.level().isClientSide() && h.getWoodcuttingLevel() >= 1
                                && handstack.getItem() == Items.IRON_AXE) {
                    if (block == BlockInit.NORMAL_TREE_LOG.get()) {

                        Random rand = new Random();
                        int nestchance;
                        int nesttype;
                        ItemStack ring_of_luck =
                                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_LUCK.get(), player).map(
                                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                        ItemStack strung_rabbit_foot =
                                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.STRUNG_RABBIT_FOOT.get(), event.getEntity()).map(
                                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                        if (!strung_rabbit_foot.isEmpty() && !ring_of_luck.isEmpty()) {
                            nestchance = rand.nextInt(43);
                        }
                        else if (!strung_rabbit_foot.isEmpty()|!ring_of_luck.isEmpty()) {
                            nestchance = rand.nextInt(87);
                        } else nestchance = rand.nextInt(129);
                        if (nestchance == 0) {
                            nesttype = rand.nextInt(2);
                            if (nesttype == 0) {
                                ItemHandlerHelper.giveItemToPlayer(player, ItemInit.BIRDS_NEST_EGG.get().getDefaultInstance());
                            }
                            if (nesttype == 1) {
                                ItemHandlerHelper.giveItemToPlayer(player, ItemInit.BIRDS_NEST_SEEDS.get().getDefaultInstance());
                            }
                        }
                    }
                }
                //
                if (!player.level().isClientSide() && h.getWoodcuttingLevel() >= 8
                        && handstack.getItem() == ItemInit.BRONZE_HATCHET.get()) {
                    if (block == Blocks.BIRCH_LOG ||
                            block == Blocks.STRIPPED_BIRCH_LOG)
                    {
                        if (player.getInventory().getFreeSlot() == -1)
                        {
                            player.displayClientMessage(Component.literal("Your inventory is too full to cut any more logs."), false);
                        }
                        //
                        Random rand = new Random();
                        int nestchance;
                        int nesttype;
                        ItemStack ring_of_luck =
                                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_LUCK.get(), player).map(
                                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                        ItemStack strung_rabbit_foot =
                                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.STRUNG_RABBIT_FOOT.get(), event.getEntity()).map(
                                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                        if (!strung_rabbit_foot.isEmpty() && !ring_of_luck.isEmpty()) {
                            nestchance = rand.nextInt(43);
                        }
                        else if (!strung_rabbit_foot.isEmpty()|!ring_of_luck.isEmpty()) {
                            nestchance = rand.nextInt(87);
                        } else nestchance = rand.nextInt(129);
                        if (nestchance == 0) {
                            nesttype = rand.nextInt(2);
                            if (nesttype == 0) {
                                ItemHandlerHelper.giveItemToPlayer(player, ItemInit.BIRDS_NEST_EGG.get().getDefaultInstance());
                            }
                            if (nesttype == 1) {
                                ItemHandlerHelper.giveItemToPlayer(player, ItemInit.BIRDS_NEST_SEEDS.get().getDefaultInstance());
                            }
                        }
                        //
                        int logchance = Math.round(rand.nextInt(101));
                        int bonus = (int) Math.round(h.getWoodcuttingLevel() * 0.7852);

                        int roll = rand.nextInt(101);
                        if (h.getWoodcuttingLevel() <= 3 && logchance > roll) {
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.BIRCH_TREE_LOG.get().getDefaultInstance());
                            h.addWoodcuttingXP(player, 32);
                            ed.addxptotalxp(32);
                            player.displayClientMessage(Component.literal("You have gained XP: " + h.getWoodcuttingXP()), false);
                            int deplete = rand.nextInt(3);
                            if (deplete == 0) {
                                level.removeBlock(pos, false);
                                player.displayClientMessage(Component.literal("The log you were cutting has depleted."), false);
                            }
                        }
                        if (h.getWoodcuttingLevel() >= 4 && logchance > roll + bonus) {
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.BIRCH_TREE_LOG.get().getDefaultInstance());
                            h.addWoodcuttingXP(player, 32);
                            ed.addxptotalxp(32);
                            player.displayClientMessage(Component.literal("You have gained XP: " + h.getWoodcuttingXP()), false);
                            int deplete = rand.nextInt(3);
                            if (deplete == 0) {
                                level.removeBlock(pos, false);
                                player.displayClientMessage(Component.literal("The log you were cutting has depleted."), false);
                            }
                        }
                    }

                }
                //
                if (!player.level().isClientSide() && h.getWoodcuttingLevel() >= 10
                        && handstack.getItem() == Items.IRON_AXE) {
                    if (block == Blocks.BIRCH_LOG ||
                            block == Blocks.STRIPPED_BIRCH_LOG)
                    {
                        if (player.getInventory().getFreeSlot() == -1)
                        {
                            player.displayClientMessage(Component.literal("Your inventory is too full to cut any more logs."), false);
                        }
                        //
                        Random rand = new Random();
                        int nestchance;
                        int nesttype;
                        ItemStack ring_of_luck =
                                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_LUCK.get(), player).map(
                                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                        ItemStack strung_rabbit_foot =
                                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.STRUNG_RABBIT_FOOT.get(), event.getEntity()).map(
                                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                        if (!strung_rabbit_foot.isEmpty() && !ring_of_luck.isEmpty()) {
                            nestchance = rand.nextInt(43);
                        }
                        else if (!strung_rabbit_foot.isEmpty()|!ring_of_luck.isEmpty()) {
                            nestchance = rand.nextInt(87);
                        } else nestchance = rand.nextInt(129);
                        if (nestchance == 0) {
                            nesttype = rand.nextInt(2);
                            if (nesttype == 0) {
                                ItemHandlerHelper.giveItemToPlayer(player, ItemInit.BIRDS_NEST_EGG.get().getDefaultInstance());
                            }
                            if (nesttype == 1) {
                                ItemHandlerHelper.giveItemToPlayer(player, ItemInit.BIRDS_NEST_SEEDS.get().getDefaultInstance());
                            }
                        }
                        //
                        int logchance = Math.round(rand.nextInt(101));
                        int bonus = (int) Math.round(h.getWoodcuttingLevel() * 0.7852);

                        int roll = rand.nextInt(101);
                        if (h.getWoodcuttingLevel() <= 3 && logchance > roll) {
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.BIRCH_TREE_LOG.get().getDefaultInstance());
                            h.addWoodcuttingXP(player, 32);
                            ed.addxptotalxp(32);
                            player.displayClientMessage(Component.literal("You have gained XP: " + h.getWoodcuttingXP()), false);
                            int deplete = rand.nextInt(3);
                            if (deplete == 0) {
                                level.removeBlock(pos, false);
                                player.displayClientMessage(Component.literal("The log you were cutting has depleted."), false);
                            }
                        }
                        if (h.getWoodcuttingLevel() >= 4 && logchance > roll + bonus) {
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.BIRCH_TREE_LOG.get().getDefaultInstance());
                            h.addWoodcuttingXP(player, 32);
                            ed.addxptotalxp(32);
                            player.displayClientMessage(Component.literal("You have gained XP: " + h.getWoodcuttingXP()), false);
                            int deplete = rand.nextInt(3);
                            if (deplete == 0) {
                                level.removeBlock(pos, false);
                                player.displayClientMessage(Component.literal("The log you were cutting has depleted."), false);
                            }
                        }
                    }

                }
            });
        });
    }

}
