package com.ladestitute.runicages.event;

import com.ladestitute.runicages.registry.EffectInit;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.Random;

public class MobEffectsHandler {

    @SubscribeEvent
    public void confuseffect (LivingDamageEvent event)
    {
        if(event.getEntity().hasEffect(EffectInit.CONFUSE.get()))
        {
            Random rand = new Random();
            int chance = rand.nextInt(100);
            if(chance <= 10) {
                event.setAmount(0);
            }
        }
    }

}
