package com.ladestitute.runicages.event;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.mining.RunicAgesMiningCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.registry.BlockInit;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.level.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.registries.ForgeRegistries;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;

import java.util.Random;

@Mod.EventBusSubscriber(modid = RunicAgesMain.MODID)
public class MiningEventHandler {


    @SubscribeEvent
    public void breakdeny(BlockEvent.BreakEvent event) {
        event.getPlayer().getCapability(RunicAgesMiningCapability.Provider.MINING_LEVEL).ifPresent(h ->
        {
            if(!event.getPlayer().isCreative() && RunicAgesConfig.restrictmining.get()) {
                if (h.getMiningLevel() < 10 && event.getState().getBlock() == Blocks.IRON_ORE||
                        h.getMiningLevel() < 10 && event.getState().getBlock() == Blocks.DEEPSLATE_IRON_ORE) {
                    event.getPlayer().sendSystemMessage(Component.literal("Level 10 Mining is required to mine this ore."));
                    event.setCanceled(true);
                }
                if (h.getMiningLevel() < 20 && event.getState().getBlock() == Blocks.COAL_ORE||
                        h.getMiningLevel() < 20 && event.getState().getBlock() == Blocks.DEEPSLATE_COAL_ORE||
                        h.getMiningLevel() < 20 && event.getState().getBlock() == BlockInit.SILVER_ORE.get()||
                        h.getMiningLevel() < 20 && event.getState().getBlock() == BlockInit.DEEPSLATE_SILVER_ORE.get()) {
                    event.getPlayer().sendSystemMessage(Component.literal("Level 20 Mining is required to mine this ore."));
                    event.setCanceled(true);
                }
                if (h.getMiningLevel() < 23 && event.getState().getBlock() == Blocks.EMERALD_ORE||
                        h.getMiningLevel() < 23 && event.getState().getBlock() == Blocks.DEEPSLATE_EMERALD_ORE) {
                    event.getPlayer().sendSystemMessage(Component.literal("Level 23 Mining is required to mine this ore."));
                    event.setCanceled(true);
                }
                if (h.getMiningLevel() < 30 && event.getState().getBlock() == BlockInit.MITHRIL_ORE.get()||
                        h.getMiningLevel() < 30 && event.getState().getBlock() == BlockInit.DEEPSLATE_MITHRIL_ORE.get()) {
                    event.getPlayer().sendSystemMessage(Component.literal("Level 30 Mining is required to mine this ore."));
                    event.setCanceled(true);
                }
                if (h.getMiningLevel() < 40 && event.getState().getBlock() == BlockInit.ADAMANTITE_ORE.get()||
                        h.getMiningLevel() < 40 && event.getState().getBlock() == BlockInit.DEEPSLATE_ADAMANTITE_ORE.get()||
                        h.getMiningLevel() < 40 && event.getState().getBlock() == BlockInit.LUMINITE_ORE.get()||
                        h.getMiningLevel() < 40 && event.getState().getBlock() == BlockInit.DEEPSLATE_LUMINITE_ORE.get()) {
                    event.getPlayer().sendSystemMessage(Component.literal("Level 40 Mining is required to mine this ore."));
                    event.setCanceled(true);
                }
                if (h.getMiningLevel() < 38 && event.getState().getBlock() == Blocks.DIAMOND_ORE||
                        h.getMiningLevel() < 38 && event.getState().getBlock() == Blocks.DEEPSLATE_DIAMOND_ORE) {
                    event.getPlayer().sendSystemMessage(Component.literal("Level 38 Mining is required to mine this ore."));
                    event.setCanceled(true);
                }
                if (h.getMiningLevel() < 40 && event.getState().getBlock() == Blocks.GOLD_ORE||
                        h.getMiningLevel() < 40 && event.getState().getBlock() == Blocks.DEEPSLATE_GOLD_ORE) {
                    event.getPlayer().sendSystemMessage(Component.literal("Level 40 Mining is required to mine this ore."));
                    event.setCanceled(true);
                }
            }
        });
    }

    public static final TagKey<Item> GEM_DROPS = ItemTags.create(new ResourceLocation(RunicAgesMain.MODID, "gem_drops"));
    public int gemchance;

    @SubscribeEvent
    public void addxp(BlockEvent.BreakEvent event) {
        event.getPlayer().getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
        event.getPlayer().getCapability(RunicAgesMiningCapability.Provider.MINING_LEVEL).ifPresent(h ->
        {
            if (event.getState().getBlock() == Blocks.CLAY) {
                h.addMiningXP(event.getPlayer(), 5);
                ed.addxptotalxp(5);
                event.getPlayer().sendSystemMessage(Component.literal("+5 mining xp"));
                RunicAgesMiningCapability.levelClientUpdate(event.getPlayer());
            }
            if (event.getState().getBlock() == Blocks.IRON_ORE) {
                if(RunicAgesConfig.modernrs.get())
                {
                    for (ItemStack cost : event.getPlayer().getInventory().items)
                        if (cost.getItem() == ItemInit.IRON_STONE_SPIRIT.get() && cost.getCount() >= 1)
                        {
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), Items.RAW_IRON.getDefaultInstance());
                            cost.shrink(1);
                            break;
                        }
                }
                //Gem drop stuff
                Random rand = new Random();
                ItemStack ring_of_luck =
                        CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_LUCK.get(), event.getPlayer()).map(
                                ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                if(!ring_of_luck.isEmpty()) {
                    gemchance = rand.nextInt(86);
                }
                else gemchance = rand.nextInt(128);
                ItemStack GEM =
                        new ItemStack(ForgeRegistries.ITEMS.tags().getTag(GEM_DROPS).getRandomElement(event.getPlayer().level().getRandom()).get());
                ItemEntity gemdrop = new ItemEntity(event.getPlayer().level(), event.getPlayer().blockPosition().getX(), event.getPlayer().blockPosition().getY(), event.getPlayer().blockPosition().getZ(),
                        GEM);
                if (gemchance == 0) {
                    event.getPlayer().level().addFreshEntity(gemdrop);
                }
                //Gem drop stuff

                h.addMiningXP(event.getPlayer(), 17);
                ed.addxptotalxp(17);
                RunicAgesMiningCapability.levelClientUpdate(event.getPlayer());
            }
            if (event.getState().getBlock() == Blocks.COPPER_ORE) {
                if(RunicAgesConfig.modernrs.get())
                {
                    for (ItemStack cost : event.getPlayer().getInventory().items)
                        if (cost.getItem() == ItemInit.COPPER_STONE_SPIRIT.get() && cost.getCount() >= 1)
                        {
                            ItemHandlerHelper.giveItemToPlayer(event.getPlayer(), Items.RAW_COPPER.getDefaultInstance());
                            cost.shrink(1);
                            break;
                        }
                }
                //Gem drop stuff
                Random rand = new Random();
                ItemStack ring_of_luck =
                        CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_LUCK.get(), event.getPlayer()).map(
                                ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                if(!ring_of_luck.isEmpty()) {
                    gemchance = rand.nextInt(86);
                }
                else gemchance = rand.nextInt(128);
                ItemStack GEM =
                        new ItemStack(ForgeRegistries.ITEMS.tags().getTag(GEM_DROPS).getRandomElement(event.getPlayer().level().getRandom()).get());
                ItemEntity gemdrop = new ItemEntity(event.getPlayer().level(), event.getPlayer().blockPosition().getX(), event.getPlayer().blockPosition().getY(), event.getPlayer().blockPosition().getZ(),
                        GEM);
                if (gemchance == 0) {
                    event.getPlayer().level().addFreshEntity(gemdrop);
                }
                //Gem drop stuff

                h.addMiningXP(event.getPlayer(), 17);
                ed.addxptotalxp(17);
                RunicAgesMiningCapability.levelClientUpdate(event.getPlayer());
            }
        });
        });
    }

    @SubscribeEvent
    public void breakdeny(PlayerEvent.PlayerLoggedInEvent event)
    {
      //  event.getEntity().level.getGameRules().getRule(GameRules.RULE_LIMITED_CRAFTING).set(true, event.getEntity().level.getServer());
    }

}

