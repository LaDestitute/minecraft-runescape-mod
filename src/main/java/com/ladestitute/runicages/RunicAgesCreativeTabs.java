package com.ladestitute.runicages;

import com.ladestitute.runicages.registry.BlockInit;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.registry.SpecialBlockInit;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.event.BuildCreativeModeTabContentsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

@Mod.EventBusSubscriber(modid = RunicAgesMain.MODID, bus = Mod.EventBusSubscriber.Bus.MOD , value = Dist.CLIENT)
public class RunicAgesCreativeTabs {
    public static final DeferredRegister<CreativeModeTab> TABS = DeferredRegister.create(Registries.CREATIVE_MODE_TAB, RunicAgesMain.MODID);

    public static final List<Supplier<? extends ItemLike>> MINING_TAB_ITEMS = new ArrayList<>();
    public static final RegistryObject<CreativeModeTab> MINING_TAB = TABS.register("rsmining_tab",
            () -> CreativeModeTab.builder()
                    .title(Component.literal("Runic Ages Mining"))
                    .icon(() -> new ItemStack(ItemInit.TIN_ORE.get()))
                    .displayItems((displayParams, output) ->
                            MINING_TAB_ITEMS.forEach(itemLike -> output.accept(itemLike.get())))
                    .build()
    );

    public static final List<Supplier<? extends ItemLike>> COOKING_TAB_ITEMS = new ArrayList<>();
    public static final RegistryObject<CreativeModeTab> COOKING_TAB = TABS.register("rscooking_tab",
            () -> CreativeModeTab.builder()
                    .title(Component.literal("Runic Ages Cooking"))
                    .withTabsBefore(MINING_TAB.getId())
                    .icon(() -> new ItemStack(ItemInit.BANANA.get()))
                    .displayItems((displayParams, output) ->
                            COOKING_TAB_ITEMS.forEach(itemLike -> output.accept(itemLike.get())))
                    .build()
    );

    public static final List<Supplier<? extends ItemLike>> CRAFTING_TAB_ITEMS = new ArrayList<>();
    public static final RegistryObject<CreativeModeTab> CRAFTING_TAB = TABS.register("rscrafting_tab",
            () -> CreativeModeTab.builder()
                    .title(Component.literal("Runic Ages Crafting"))
                    .withTabsBefore(COOKING_TAB.getId())
                    .icon(() -> new ItemStack(ItemInit.CHISEL.get()))
                    .displayItems((displayParams, output) ->
                            CRAFTING_TAB_ITEMS.forEach(itemLike -> output.accept(itemLike.get())))
                    .build()
    );

    public static final List<Supplier<? extends ItemLike>> MAGIC_TAB_ITEMS = new ArrayList<>();
    public static final RegistryObject<CreativeModeTab> MAGIC_TAB = TABS.register("rsmagic_tab",
            () -> CreativeModeTab.builder()
                    .title(Component.literal("Runic Ages Magic"))
                    .withTabsBefore(CRAFTING_TAB.getId())
                    .icon(() -> new ItemStack(ItemInit.AIR_RUNE.get()))
                    .displayItems((displayParams, output) ->
                            MAGIC_TAB_ITEMS.forEach(itemLike -> output.accept(itemLike.get())))
                    .build()
    );

    public static final List<Supplier<? extends ItemLike>> FLETCHING_TAB_ITEMS = new ArrayList<>();
    public static final RegistryObject<CreativeModeTab> FLETCHING_TAB = TABS.register("rsfletching_tab",
            () -> CreativeModeTab.builder()
                    .title(Component.literal("Runic Ages Fletching"))
                    .withTabsBefore(MAGIC_TAB.getId())
                    .icon(() -> new ItemStack(ItemInit.BRONZE_ARROW.get()))
                    .displayItems((displayParams, output) ->
                            FLETCHING_TAB_ITEMS.forEach(itemLike -> output.accept(itemLike.get())))
                    .build()
    );

    public static final List<Supplier<? extends ItemLike>> SMITHING_TAB_ITEMS = new ArrayList<>();
    public static final RegistryObject<CreativeModeTab> SMITHING_TAB = TABS.register("rssmithing_tab",
            () -> CreativeModeTab.builder()
                    .title(Component.literal("Runic Ages Smithing"))
                    .withTabsBefore(FLETCHING_TAB.getId())
                    .icon(() -> new ItemStack(ItemInit.BRONZE_BAR.get()))
                    .displayItems((displayParams, output) ->
                            SMITHING_TAB_ITEMS.forEach(itemLike -> output.accept(itemLike.get())))
                    .build()
    );

    public static final List<Supplier<? extends ItemLike>> HERBLORE_TAB_ITEMS = new ArrayList<>();
    public static final RegistryObject<CreativeModeTab> HERBLORE_TAB = TABS.register("rsherblore_tab",
            () -> CreativeModeTab.builder()
                    .title(Component.literal("Runic Ages Herblore"))
                    .withTabsBefore(SMITHING_TAB.getId())
                    .icon(() -> new ItemStack(ItemInit.GRIMY_GUAM.get()))
                    .displayItems((displayParams, output) ->
                            HERBLORE_TAB_ITEMS.forEach(itemLike -> output.accept(itemLike.get())))
                    .build()
    );


    public static final List<Supplier<? extends ItemLike>> WOODCUTTING_TAB_ITEMS = new ArrayList<>();
    public static final RegistryObject<CreativeModeTab> WOODCUTTING_TAB = TABS.register("rswoodcutting_tab",
            () -> CreativeModeTab.builder()
                    .title(Component.literal("Runic Ages Woodcutting"))
                    .withTabsBefore(SMITHING_TAB.getId())
                    .icon(() -> new ItemStack(ItemInit.NORMAL_TREE_LOG.get()))
                    .displayItems((displayParams, output) ->
                            WOODCUTTING_TAB_ITEMS.forEach(itemLike -> output.accept(itemLike.get())))
                    .build()
    );

    public static final List<Supplier<? extends ItemLike>> RUNECRAFTING_TAB_ITEMS = new ArrayList<>();
    public static final RegistryObject<CreativeModeTab> RUNECRAFTING_TAB = TABS.register("rsrunecraft_tab",
            () -> CreativeModeTab.builder()
                    .title(Component.literal("Runic Ages Runecrafting"))
                    .withTabsBefore(WOODCUTTING_TAB.getId())
                    .icon(() -> new ItemStack(ItemInit.AIR_TALISMAN.get()))
                    .displayItems((displayParams, output) ->
                            RUNECRAFTING_TAB_ITEMS.forEach(itemLike -> output.accept(itemLike.get())))
                    .build()
    );

    public static final List<Supplier<? extends ItemLike>> FARMING_TAB_ITEMS = new ArrayList<>();
    public static final RegistryObject<CreativeModeTab> FARMING_TAB = TABS.register("rsfarming_tab",
            () -> CreativeModeTab.builder()
                    .title(Component.literal("Runic Ages Farming"))
                    .withTabsBefore(RUNECRAFTING_TAB.getId())
                    .icon(() -> new ItemStack(ItemInit.ONION_SEED.get()))
                    .displayItems((displayParams, output) ->
                            FARMING_TAB_ITEMS.forEach(itemLike -> output.accept(itemLike.get())))
                    .build()
    );

    public static final List<Supplier<? extends ItemLike>> MISC_TAB_ITEMS = new ArrayList<>();
    public static final RegistryObject<CreativeModeTab> MISC_TAB = TABS.register("rsmisc_tab",
            () -> CreativeModeTab.builder()
                    .title(Component.literal("Runic Ages Misc"))
                    .withTabsBefore(FARMING_TAB.getId())
                    .icon(() -> new ItemStack(ItemInit.FLIER.get()))
                    .displayItems((displayParams, output) ->
                            MISC_TAB_ITEMS.forEach(itemLike -> output.accept(itemLike.get())))
                    .build()
    );

    @SubscribeEvent
    public static void buildContents(BuildCreativeModeTabContentsEvent event) {
        if(event.getTab() == MINING_TAB.get()) {
            event.accept(BlockInit.TIN_ROCK);
            event.accept(BlockInit.CLAY_ROCK);
            event.accept(BlockInit.IRON_ROCK);
            event.accept(BlockInit.BLURITE_ROCK);
            event.accept(BlockInit.SILVER_ROCK);
            event.accept(BlockInit.MITHRIL_ROCK);
            event.accept(BlockInit.ADAMANTITE_ROCK);
            event.accept(BlockInit.LUMINITE_ROCK);
            event.accept(BlockInit.TIN_ORE);
            event.accept(BlockInit.DEEPSLATE_TIN_ORE);
            event.accept(BlockInit.RUNE_ESSENCE_ORE);
            event.accept(BlockInit.BLURITE_ORE);
            event.accept(BlockInit.DEEPSLATE_BLURITE_ORE);
            event.accept(BlockInit.SILVER_ORE);
            event.accept(BlockInit.DEEPSLATE_SILVER_ORE);
            event.accept(BlockInit.MITHRIL_ORE);
            event.accept(BlockInit.DEEPSLATE_MITHRIL_ORE);
            event.accept(BlockInit.ADAMANTITE_ORE);
            event.accept(BlockInit.DEEPSLATE_ADAMANTITE_ORE);
            event.accept(BlockInit.LUMINITE_ORE);
            event.accept(BlockInit.DEEPSLATE_LUMINITE_ORE);
            event.accept(ItemInit.TIN_ORE);
            event.accept(ItemInit.UNCUT_OPAL);
            event.accept(ItemInit.RUNE_ESSENCE);
            event.accept(ItemInit.PURE_ESSENCE);
            event.accept(ItemInit.HARD_CLAY);
            event.accept(ItemInit.BLURITE_ORE);
            event.accept(ItemInit.SILVER_ORE);
            event.accept(ItemInit.UNCUT_SAPPHIRE);
            event.accept(ItemInit.MITHRIL_ORE);
            event.accept(ItemInit.ADAMANTITE_ORE);
            event.accept(ItemInit.LUMINITE);
            event.accept(ItemInit.COPPER_STONE_SPIRIT);
            event.accept(ItemInit.TIN_STONE_SPIRIT);
            event.accept(ItemInit.IRON_STONE_SPIRIT);
            event.accept(ItemInit.SILVER_STONE_SPIRIT);
            event.accept(ItemInit.BRONZE_PICKAXE);
            event.accept(ItemInit.BRACELET_OF_CLAY);
        }
        if(event.getTab() == COOKING_TAB.get()) {
            event.accept(ItemInit.SPIDER_CARCASS);
            event.accept(ItemInit.RAW_SPIDER_ON_STICK);
            event.accept(ItemInit.SPIDER_ON_STICK);
            event.accept(ItemInit.BANANA);
            event.accept(ItemInit.RAW_BEAR_MEAT);
            event.accept(ItemInit.COOKED_MEAT);
        }
        if(event.getTab() == CRAFTING_TAB.get()) {
            event.accept(BlockInit.SPINNING_WHEEL);
            event.accept(BlockInit.SEWING_HOOP);
            event.accept(BlockInit.MOLD_PRESS);
            event.accept(ItemInit.RING_MOLD);
            event.accept(ItemInit.AMULET_MOLD);
            event.accept(ItemInit.NECKLACE_MOLD);
            event.accept(ItemInit.BRACELET_MOLD);
            event.accept(ItemInit.TIARA_MOLD);
            event.accept(ItemInit.CHISEL);
            event.accept(ItemInit.PESTLE_AND_MORTAR);
            event.accept(ItemInit.NEEDLE);
            event.accept(ItemInit.THREAD);
            event.accept(ItemInit.COWHIDE);
            event.accept(ItemInit.SOFT_LEATHER);
            event.accept(ItemInit.HARD_LEATHER);
            event.accept(ItemInit.OPAL);
            event.accept(ItemInit.CUT_LAPIS_LAZULI);
            event.accept(ItemInit.SAPPHIRE);
            event.accept(ItemInit.BALL_OF_WOOL);
            event.accept(ItemInit.STRIP_OF_CLOTH);
            event.accept(ItemInit.UNFIRED_POT);
            event.accept(ItemInit.UNFIRED_PIE_DISH);
            event.accept(ItemInit.PIE_DISH);
            event.accept(ItemInit.UNFIRED_BOWL);
            event.accept(ItemInit.CLAY_BOWL);
            event.accept(ItemInit.FILLED_SILVER_RING_MOLD);
            event.accept(ItemInit.FILLED_OPAL_RING_MOLD);
            event.accept(ItemInit.FILLED_OPAL_AMULET_MOLD);
            event.accept(ItemInit.FILLED_LAPIS_LAZULI_RING_MOLD);
            event.accept(ItemInit.FILLED_GOLD_RING_MOLD);
            event.accept(ItemInit.FILLED_GOLD_AMULET_MOLD);
            event.accept(ItemInit.FILLED_GOLD_NECKLACE_MOLD);
            event.accept(ItemInit.FILLED_GOLD_BRACELET_MOLD);
            event.accept(ItemInit.FILLED_SAPPHIRE_RING_MOLD);
            event.accept(ItemInit.FILLED_SAPPHIRE_AMULET_MOLD);
            event.accept(ItemInit.FILLED_SAPPHIRE_BRACELET_MOLD);
            event.accept(ItemInit.SILVER_RING);
            event.accept(ItemInit.TIARA);
            event.accept(ItemInit.OPAL_RING);
            event.accept(ItemInit.UNSTRUNG_OPAL_AMULET);
            event.accept(ItemInit.OPAL_AMULET);
            event.accept(ItemInit.LAPIS_LAZULI_RING);
            event.accept(ItemInit.GOLD_RING);
            event.accept(ItemInit.UNSTRUNG_GOLD_AMULET);
            event.accept(ItemInit.GOLD_AMULET);
            event.accept(ItemInit.GOLD_NECKLACE);
            event.accept(ItemInit.GOLD_BRACELET);
            event.accept(ItemInit.SAPPHIRE_RING);
            event.accept(ItemInit.UNSTRUNG_SAPPHIRE_AMULET);
            event.accept(ItemInit.SAPPHIRE_AMULET);
            event.accept(ItemInit.SAPPHIRE_BRACELET);
        }
        if(event.getTab() == FLETCHING_TAB.get()) {
            event.accept(ItemInit.BOWSTRING);
            event.accept(ItemInit.BRONZE_ARROW);
        }
        if(event.getTab() == MAGIC_TAB.get()) {
            event.accept(ItemInit.AIR_RUNE);
            event.accept(ItemInit.MIND_RUNE);
            event.accept(ItemInit.WATER_RUNE);
            event.accept(ItemInit.EARTH_RUNE);
            event.accept(ItemInit.FIRE_RUNE);
            event.accept(ItemInit.BODY_RUNE);
            event.accept(ItemInit.COSMIC_RUNE);
            event.accept(ItemInit.CHAOS_RUNE);
            event.accept(ItemInit.NATURE_RUNE);
            event.accept(ItemInit.LAW_RUNE);
            event.accept(ItemInit.STAFF);
            event.accept(ItemInit.MAGIC_WAND);
            event.accept(ItemInit.STAFF_OF_AIR);
            event.accept(ItemInit.WIZARD_HAT);
            event.accept(ItemInit.WIZARD_ROBE_TOP);
            event.accept(ItemInit.WIZARD_ROBE_SKIRT);
            event.accept(ItemInit.WIZARD_BOOTS);
            event.accept(ItemInit.RING_OF_LUCK);
            event.accept(ItemInit.AMULET_OF_MAGIC);
            event.accept(ItemInit.RING_OF_RECOIL);
            event.accept(ItemInit.BRACELET_OF_CLAY);
            event.accept(ItemInit.AMULET_OF_BOUNTIFUL_HARVEST);
            event.accept(ItemInit.AIR_STRIKE_SPELL);
            event.accept(ItemInit.WATER_STRIKE_SPELL);
            event.accept(ItemInit.EARTH_STRIKE_SPELL);
            event.accept(ItemInit.FIRE_STRIKE_SPELL);
            event.accept(ItemInit.CONFUSE_SPELL);
            event.accept(ItemInit.WEAKEN_SPELL);
            event.accept(ItemInit.HOME_TELEPORT_TABLET);
            event.accept(ItemInit.ENCHANT_SAPPHIRE_TABLET);
            event.accept(ItemInit.BONES_TO_BANANAS_TABLET);
        }
        if(event.getTab() == SMITHING_TAB.get()) {
            event.accept(BlockInit.SMITHING_FURNACE);
            event.accept(BlockInit.SILVER_SMITHING_FURNACE);
            event.accept(BlockInit.GOLD_SMITHING_FURNACE);
            event.accept(BlockInit.ALLOY_FURNACE);
            event.accept(BlockInit.IRON_ALLOY_FURNACE);
            event.accept(BlockInit.STEEL_ALLOY_FURNACE);
            event.accept(BlockInit.MITHRIL_ALLOY_FURNACE);
            event.accept(BlockInit.ADAMANTITE_ALLOY_FURNACE);
            event.accept(ItemInit.BRONZE_BAR);
            event.accept(ItemInit.BLURITE_BAR);
            event.accept(ItemInit.STEEL_BAR);
            event.accept(ItemInit.SILVER_BAR);
            event.accept(ItemInit.MITHRIL_BAR);
            event.accept(ItemInit.ADAMANTITE_BAR);
            event.accept(ItemInit.SILVTHRIL_BAR);
            event.accept(ItemInit.BRONZE_NUGGET);
            event.accept(ItemInit.BRONZE_CHAIN);
            event.accept(ItemInit.TINY_PLATED_BRONZE_SALVAGE);
            event.accept(ItemInit.TINY_SPIKY_IRON_SALVAGE);
            event.accept(ItemInit.BRONZE_DAGGER);
            event.accept(ItemInit.BRONZE_MACE);
            event.accept(ItemInit.BRONZE_SWORD);
            event.accept(ItemInit.BRONZE_PICKAXE);
            event.accept(ItemInit.BRONZE_HATCHET);
            event.accept(ItemInit.BRONZE_ARROW);
            event.accept(ItemInit.BRONZE_HELM);
            event.accept(ItemInit.BRONZE_BOOTS);
            event.accept(ItemInit.BRONZE_SQ_SHIELD);
            event.accept(ItemInit.BRONZE_PLATELEGS);
            event.accept(ItemInit.BRONZE_CHAINBODY);
            event.accept(ItemInit.BRONZE_PLATEBODY);
            event.accept(ItemInit.IRON_DAGGER);
            event.accept(ItemInit.BLURITE_SWORD);
        }
        if(event.getTab() == HERBLORE_TAB.get()) {
            event.accept(ItemInit.PESTLE_AND_MORTAR);
            event.accept(ItemInit.VIAL_OF_WATER);
            event.accept(ItemInit.EYE_OF_NEWT);
            event.accept(ItemInit.YELLOW_BEAD);
            event.accept(ItemInit.RED_BEAD);
            event.accept(ItemInit.WHITE_BEAD);
            event.accept(ItemInit.BLACK_BEAD);
            event.accept(ItemInit.REDBERRY);
            event.accept(ItemInit.BEAR_FUR);
            event.accept(ItemInit.UNICORN_HORN);
            event.accept(ItemInit.UNICORN_HORN_DUST);
            event.accept(ItemInit.GRIMY_GUAM);
            event.accept(ItemInit.GRIMY_TARROMIN);
            event.accept(ItemInit.GRIMY_MARRENTILL);
            event.accept(ItemInit.CLEAN_GUAM);
            event.accept(ItemInit.CLEAN_TARROMIN);
            event.accept(ItemInit.CLEAN_MARRENTILL);
            event.accept(ItemInit.GUAM_POTION_UNF);
            event.accept(ItemInit.TARROMIN_POTION_UNF);
            event.accept(ItemInit.MARRENTILL_POTION_UNF);
            event.accept(ItemInit.ATTACK_POTION);
            event.accept(ItemInit.RANGING_POTION);
            event.accept(ItemInit.MAGIC_POTION);
            event.accept(ItemInit.DEFENSE_POTION);
            event.accept(ItemInit.ANTIPOISON_POTION);
        }
        if(event.getTab() == WOODCUTTING_TAB.get()) {
            event.accept(BlockInit.NORMAL_TREE_LOG);
            event.accept(BlockInit.LIGHT_JUNGLE);
            event.accept(ItemInit.NORMAL_TREE_LOG);
            event.accept(ItemInit.BIRCH_TREE_LOG);
            event.accept(ItemInit.BIRDS_NEST_SEEDS);
            event.accept(ItemInit.BIRDS_NEST_EGG);
            event.accept(ItemInit.BIRDS_NEST);
            event.accept(ItemInit.LIGHT_THATCH_SPAR);
            event.accept(ItemInit.SKEWER_STICK);
            event.accept(ItemInit.STRUNG_RABBIT_FOOT);
            event.accept(ItemInit.BRONZE_HATCHET);
        }
        if(event.getTab() == RUNECRAFTING_TAB.get()) {
            event.accept(BlockInit.AIR_ALTAR);
            event.accept(BlockInit.AIR_ALTAR_CYCLIC_STONE);
            event.accept(BlockInit.MIND_ALTAR);
            event.accept(BlockInit.MIND_ALTAR_CYCLIC_STONE);
            event.accept(BlockInit.WATER_ALTAR);
            event.accept(BlockInit.WATER_ALTAR_CYCLIC_STONE);
            event.accept(BlockInit.EARTH_ALTAR);
            event.accept(BlockInit.EARTH_ALTAR_CYCLIC_STONE);
            event.accept(BlockInit.FIRE_ALTAR);
            event.accept(BlockInit.FIRE_ALTAR_CYCLIC_STONE);
            event.accept(BlockInit.BODY_ALTAR);
            event.accept(BlockInit.BODY_ALTAR_CYCLIC_STONE);
            event.accept(BlockInit.ALTAR_SLAB);
            event.accept(BlockInit.ALTAR_BRICK_SIDE);
            event.accept(BlockInit.ALTAR_BRICK_CENTER);
            event.accept(ItemInit.AIR_TALISMAN);
            event.accept(ItemInit.MIND_TALISMAN);
            event.accept(ItemInit.WATER_TALISMAN);
            event.accept(ItemInit.EARTH_TALISMAN);
            event.accept(ItemInit.FIRE_TALISMAN);
            event.accept(ItemInit.BODY_TALISMAN);
            event.accept(ItemInit.RUNE_ESSENCE);
            event.accept(ItemInit.PURE_ESSENCE);
            event.accept(ItemInit.AIR_RUNE);
            event.accept(ItemInit.MIND_RUNE);
            event.accept(ItemInit.WATER_RUNE);
            event.accept(ItemInit.EARTH_RUNE);
            event.accept(ItemInit.FIRE_RUNE);
            event.accept(ItemInit.BODY_RUNE);
            event.accept(ItemInit.COSMIC_RUNE);
            event.accept(ItemInit.CHAOS_RUNE);
            event.accept(ItemInit.NATURE_RUNE);
            event.accept(ItemInit.LAW_RUNE);
            event.accept(ItemInit.AIR_TIARA);
            event.accept(ItemInit.MIND_TIARA);
            event.accept(ItemInit.WATER_TIARA);
            event.accept(ItemInit.EARTH_TIARA);
            event.accept(ItemInit.FIRE_TIARA);
            event.accept(ItemInit.BODY_TIARA);
        }
        if(event.getTab() == FARMING_TAB.get()) {
            event.accept(BlockInit.ALLOTMENT);
            event.accept(BlockInit.HOPS_PATCH);
            event.accept(BlockInit.HERB_PATCH);
            event.accept(BlockInit.BUSH_PATCH);
            event.accept(BlockInit.FLAX);
            event.accept(ItemInit.BARLEY_SEED);
            event.accept(ItemInit.ONION_SEED);
            event.accept(ItemInit.CABBAGE_SEED);
            event.accept(ItemInit.GUAM_SEED);
            event.accept(ItemInit.REDBERRY_SEED);
            event.accept(ItemInit.FLAX);
            event.accept(ItemInit.BARLEY);
            event.accept(ItemInit.ONION);
            event.accept(ItemInit.CABBAGE);
            event.accept(ItemInit.REDBERRY);
            event.accept(ItemInit.AMULET_OF_BOUNTIFUL_HARVEST);
            event.accept(ItemInit.AMULET_OF_BOUNTY);
        }
        if(event.getTab() == MISC_TAB.get()) {
         //   event.accept(ItemInit.MAGIC_NOTEPAPER);
            event.accept(ItemInit.CRUSHED_GEM);
            event.accept(ItemInit.ASHES);
            event.accept(ItemInit.FLIER);
            event.accept(ItemInit.BURNT_MEAT);
            event.accept(ItemInit.CHEFS_HAT);
            event.accept(ItemInit.IMP_SPAWN_EGG);
            event.accept(ItemInit.GRIZZLY_BEAR_SPAWN_EGG);
            event.accept(ItemInit.BLACK_BEAR_SPAWN_EGG);
            event.accept(ItemInit.UNICORN_SPAWN_EGG);
            event.accept(ItemInit.NOTED_BODY_TALISMAN);
            event.accept(ItemInit.NOTED_AIR_TIARA);
            event.accept(ItemInit.NOTED_MIND_TIARA);
            event.accept(ItemInit.NOTED_WATER_TIARA);
            event.accept(ItemInit.NOTED_EARTH_TIARA);
            event.accept(ItemInit.NOTED_FIRE_TIARA);
            event.accept(ItemInit.NOTED_BODY_TIARA);
            event.accept(ItemInit.NOTED_SILVER_ORE);
            event.accept(ItemInit.NOTED_MITHRIL_ORE);
            event.accept(ItemInit.NOTED_ADAMANTITE_ORE);
            event.accept(ItemInit.NOTED_LUMINITE);
            event.accept(ItemInit.NOTED_SILVER_BAR);
            event.accept(ItemInit.NOTED_STEEL_BAR);
            event.accept(ItemInit.NOTED_SILVTHRIL_BAR);
            event.accept(ItemInit.NOTED_BRONZE_PICKAXE);
            event.accept(ItemInit.NOTED_BRONZE_HATCHET);
            event.accept(ItemInit.NOTED_BRONZE_DAGGER);
            event.accept(ItemInit.NOTED_BRONZE_MACE);
            event.accept(ItemInit.NOTED_BRONZE_SWORD);
            event.accept(ItemInit.NOTED_STAFF);
            event.accept(ItemInit.NOTED_STAFF_OF_AIR);
            event.accept(ItemInit.NOTED_WIZARD_WAND);
            event.accept(ItemInit.NOTED_BOW);
            event.accept(ItemInit.NOTED_CROSSBOW);
            event.accept(ItemInit.NOTED_BRONZE_HELM);
            event.accept(ItemInit.NOTED_BRONZE_CHAINBODY);
            event.accept(ItemInit.NOTED_BRONZE_PLATEBODY);
            event.accept(ItemInit.NOTED_BRONZE_PLATELEGS);
            event.accept(ItemInit.NOTED_BRONZE_SQ_SHIELD);
            event.accept(ItemInit.NOTED_WIZARD_HAT);
            event.accept(ItemInit.NOTED_WIZARD_ROBE_TOP);
            event.accept(ItemInit.NOTED_WIZARD_ROBE_SKIRT);
            event.accept(ItemInit.NOTED_WIZARD_BOOTS);
            event.accept(ItemInit.NOTED_LEATHER_CAP);
            event.accept(ItemInit.NOTED_LEATHER_TUNIC);
            event.accept(ItemInit.NOTED_LEATHER_PANTS);
            event.accept(ItemInit.NOTED_LEATHER_BOOTS);
            event.accept(ItemInit.NOTED_GRIMY_GUAM);
            event.accept(ItemInit.NOTED_GRIMY_TARROMIN);
            event.accept(ItemInit.NOTED_GRIMY_MARRENTILL);
            event.accept(ItemInit.NOTED_CLEAN_GUAM);
            event.accept(ItemInit.NOTED_CLEAN_TARROMIN);
            event.accept(ItemInit.NOTED_CLEAN_MARRENTILL);
            event.accept(ItemInit.NOTED_GUAM_POTION_UNF);
            event.accept(ItemInit.NOTED_TARROMIN_POTION_UNF);
            event.accept(ItemInit.NOTED_MARRENTILL_POTION_UNF);
            event.accept(ItemInit.NOTED_EYE_OF_NEWT);
            event.accept(ItemInit.NOTED_BEAR_FUR);
            event.accept(ItemInit.NOTED_UNICORN_HORN);
            event.accept(ItemInit.NOTED_UNICORN_HORN_DUST);
            event.accept(ItemInit.NOTED_BLACK_BEAD);
            event.accept(ItemInit.NOTED_RED_BEAD);
            event.accept(ItemInit.NOTED_YELLOW_BEAD);
            event.accept(ItemInit.NOTED_WHITE_BEAD);
            event.accept(ItemInit.NOTED_ONION);
            event.accept(ItemInit.NOTED_CABBAGE);
            event.accept(ItemInit.NOTED_BARLEY);
            event.accept(ItemInit.NOTED_REDBERRIES);
            event.accept(ItemInit.NOTED_FLAX);

            event.accept(ItemInit.NOTED_CHISEL);
            event.accept(ItemInit.NOTED_PESTLE_AND_MORTAR);
            event.accept(ItemInit.NOTED_NEEDLE);
            event.accept(ItemInit.NOTED_RING_MOLD);
            event.accept(ItemInit.NOTED_TIARA_MOLD);
            event.accept(ItemInit.NOTED_BRACELET_MOLD);
            event.accept(ItemInit.NOTED_AMULET_MOLD);
            event.accept(ItemInit.NOTED_NECKLACE_MOLD);

            event.accept(ItemInit.NOTED_COWHIDE);
            event.accept(ItemInit.NOTED_LEATHER);
            event.accept(ItemInit.NOTED_HARD_LEATHER);
            event.accept(ItemInit.NOTED_UNCUT_OPAL);
            event.accept(ItemInit.NOTED_UNCUT_SAPPHIRE);
            event.accept(ItemInit.NOTED_CUT_LAPIS_LAZULI);
            event.accept(ItemInit.NOTED_SAPPHIRE);
            event.accept(ItemInit.NOTED_OPAL);
            event.accept(ItemInit.NOTED_BALL_OF_WOOL);
            event.accept(ItemInit.NOTED_STRIP_OF_CLOTH);
            event.accept(ItemInit.NOTED_UNFIRED_BOWL);
            event.accept(ItemInit.NOTED_UNFIRED_PIE_DISH);
            event.accept(ItemInit.NOTED_UNFIRED_POT);
            event.accept(ItemInit.NOTED_BOWL);
            event.accept(ItemInit.NOTED_PIE_DISH);
            event.accept(ItemInit.NOTED_POT);
            event.accept(ItemInit.NOTED_BOWL_OF_WATER);
            event.accept(ItemInit.NOTED_BOWSTRING);
            event.accept(ItemInit.NOTED_COOKED_MEAT);
            event.accept(ItemInit.NOTED_RAW_BEAR_MEAT);
            event.accept(ItemInit.NOTED_IRON_DAGGER);
            event.accept(ItemInit.NOTED_IRON_AXE);
            event.accept(ItemInit.NOTED_IRON_PICKAXE);
            event.accept(ItemInit.NOTED_BRACELET_OF_CLAY);
            event.accept(ItemInit.NOTED_AMULET_OF_MAGIC);
            event.accept(ItemInit.NOTED_RING_OF_RECOIL);
            event.accept(ItemInit.NOTED_RING_OF_LUCK);
            event.accept(ItemInit.NOTED_AMULET_OF_BOUNTY);
            event.accept(ItemInit.NOTED_AMULET_OF_BOUNTIFUL_HARVEST);
            event.accept(ItemInit.NOTED_SAPPHIRE_BRACELET);
            event.accept(ItemInit.NOTED_SAPPHIRE_AMULET);
            event.accept(ItemInit.NOTED_UNSTRUNG_SAPPHIRE_AMULET);
            event.accept(ItemInit.NOTED_SAPPHIRE_RING);
            event.accept(ItemInit.NOTED_GOLD_BRACELET);
            event.accept(ItemInit.NOTED_GOLD_NECKLACE);
            event.accept(ItemInit.NOTED_GOLD_AMULET);
            event.accept(ItemInit.NOTED_UNSTRUNG_GOLD_AMULET);
            event.accept(ItemInit.NOTED_GOLD_RING);
            event.accept(ItemInit.NOTED_LAPIS_LAZULI_RING);
            event.accept(ItemInit.NOTED_OPAL_AMULET);
            event.accept(ItemInit.NOTED_UNSTRUNG_OPAL_AMULET);
            event.accept(ItemInit.NOTED_OPAL_RING);
            event.accept(ItemInit.NOTED_TIARA);
            event.accept(ItemInit.NOTED_SILVER_RING);
            event.accept(ItemInit.NOTED_SPIDER_CARCASS);
            event.accept(ItemInit.NOTED_RAW_SPIDER_ON_STICK);
            event.accept(ItemInit.NOTED_SPIDER_ON_STICK);

            event.accept(ItemInit.NOTED_FILLED_SILVER_RING_MOLD);
            event.accept(ItemInit.NOTED_FILLED_TIARA_MOLD);
            event.accept(ItemInit.NOTED_FILLED_OPAL_RING_MOLD);
            event.accept(ItemInit.NOTED_FILLED_OPAL_AMULET_MOLD);
            event.accept(ItemInit.NOTED_FILLED_LAPIS_LAZULI_RING_MOLD);
            event.accept(ItemInit.NOTED_FILLED_GOLD_RING_MOLD);
            event.accept(ItemInit.NOTED_FILLED_GOLD_AMULET_MOLD);
            event.accept(ItemInit.NOTED_FILLED_GOLD_NECKLACE_MOLD);
            event.accept(ItemInit.NOTED_FILLED_GOLD_BRACELET_MOLD);
            event.accept(ItemInit.NOTED_FILLED_SAPPHIRE_RING_MOLD);
            event.accept(ItemInit.NOTED_FILLED_SAPPHIRE_AMULET_MOLD);
            event.accept(ItemInit.NOTED_FILLED_SAPPHIRE_BRACELET_MOLD);
            event.accept(ItemInit.NOTED_BRONZE_BOOTS);
            event.accept(ItemInit.NOTED_BANANA);

            event.accept(ItemInit.NOTED_BLURITE_SWORD);
            event.accept(ItemInit.NOTED_STRUNG_RABBIT_FOOT);
            event.accept(ItemInit.NOTED_BIRD_NEST);
            event.accept(ItemInit.NOTED_EGG_BIRD_NEST);
            event.accept(ItemInit.NOTED_SEEDS_BIRD_NEST);
        }
    }

}
