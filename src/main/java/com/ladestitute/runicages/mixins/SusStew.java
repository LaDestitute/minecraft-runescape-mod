//package com.ladestitute.runicages.mixins;
//
//import com.ladestitute.runicages.registry.ItemInit;
//import net.minecraft.core.RegistryAccess;
//import net.minecraft.resources.ResourceLocation;
//import net.minecraft.tags.ItemTags;
//import net.minecraft.world.inventory.CraftingContainer;
//import net.minecraft.world.item.ItemStack;
//import net.minecraft.world.item.Items;
//import net.minecraft.world.item.SuspiciousStewItem;
//import net.minecraft.world.item.crafting.CraftingBookCategory;
//import net.minecraft.world.item.crafting.CustomRecipe;
//import net.minecraft.world.item.crafting.RecipeSerializer;
//import net.minecraft.world.item.crafting.SuspiciousStewRecipe;
//import net.minecraft.world.level.Level;
//import net.minecraft.world.level.block.Blocks;
//import net.minecraft.world.level.block.SuspiciousEffectHolder;
//import org.spongepowered.asm.mixin.Mixin;
//import org.spongepowered.asm.mixin.injection.At;
//import org.spongepowered.asm.mixin.injection.Inject;
//import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
//
//@Mixin(SuspiciousStewRecipe.class)
//public class SusStew extends CustomRecipe {
//    public SusStew(ResourceLocation p_248870_, CraftingBookCategory p_250392_) {
//        super(p_248870_, p_250392_);
//    }
//
//    @Inject(method = "matches(Lnet/minecraft/world/inventory/CraftingContainer;Lnet/minecraft/world/level/Level;)Z", at = @At("HEAD"), cancellable = true)
//    public void matches(CraftingContainer p_44499_, Level p_44500_, CallbackInfoReturnable<Boolean> cir) {
//        boolean flag = false;
//        boolean flag1 = false;
//        boolean flag2 = false;
//        boolean flag3 = false;
//
//        for(int i = 0; i < p_44499_.getContainerSize(); ++i) {
//            ItemStack itemstack = p_44499_.getItem(i);
//            if (!itemstack.isEmpty()) {
//                if (itemstack.is(Blocks.BROWN_MUSHROOM.asItem()) && !flag2) {
//                    flag2 = true;
//                } else if (itemstack.is(Blocks.RED_MUSHROOM.asItem()) && !flag1) {
//                    flag1 = true;
//                } else if (itemstack.is(ItemTags.SMALL_FLOWERS) && !flag) {
//                    flag = true;
//                } else {
//                    if(!itemstack.is(ItemInit.CLAY_BOWL.get()) || flag3) {
//                        cir.setReturnValue(false);
//                    }
//
//                    flag3 = true;
//                }
//            }
//        }
//    }
//
//    public ItemStack assemble(CraftingContainer p_44497_, RegistryAccess p_266871_) {
//        ItemStack itemstack = new ItemStack(Items.SUSPICIOUS_STEW, 1);
//
//        for(int i = 0; i < p_44497_.getContainerSize(); ++i) {
//            ItemStack itemstack1 = p_44497_.getItem(i);
//            if (!itemstack1.isEmpty()) {
//                SuspiciousEffectHolder suspiciouseffectholder = SuspiciousEffectHolder.tryGet(itemstack1.getItem());
//                if (suspiciouseffectholder != null) {
//                    SuspiciousStewItem.saveMobEffect(itemstack, suspiciouseffectholder.getSuspiciousEffect(), suspiciouseffectholder.getEffectDuration());
//                    break;
//                }
//            }
//        }
//
//        return itemstack;
//    }
//
//    public boolean canCraftInDimensions(int p_44489_, int p_44490_) {
//        return p_44489_ >= 2 && p_44490_ >= 2;
//    }
//
//    public RecipeSerializer<?> getSerializer() {
//        return RecipeSerializer.SUSPICIOUS_STEW;
//    }
//}
