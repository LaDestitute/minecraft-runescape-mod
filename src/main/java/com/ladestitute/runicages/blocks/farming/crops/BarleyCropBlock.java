package com.ladestitute.runicages.blocks.farming.crops;

import com.ladestitute.runicages.capability.farming.RunicAgesFarmingCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.registry.BlockInit;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.BlockHitResult;

import java.util.Random;

public class BarleyCropBlock extends CropBlock {
    public static final IntegerProperty AGE = IntegerProperty.create("age", 0, 5);

    //Custom harvest lives blockstate value
    public static final IntegerProperty LIVES = IntegerProperty.create("lives", 0, 3);

    public BarleyCropBlock(Properties p_52247_) {
        super(p_52247_);
        this.registerDefaultState(this.stateDefinition.any().setValue(LIVES, 3));
    }

    //Patch type
    @Override
    protected boolean mayPlaceOn(BlockState p_52302_, BlockGetter p_52303_, BlockPos p_52304_) {
        return p_52302_.is(BlockInit.HOPS_PATCH.get());
    }

    @Override
    public boolean canSurvive(BlockState p_51028_, LevelReader p_51029_, BlockPos p_51030_) {
        BlockPos blockpos = p_51030_.below();
        return this.mayPlaceOn(p_51029_.getBlockState(blockpos), p_51029_, blockpos);
    }

    @Override
    public IntegerProperty getAgeProperty() {
        return AGE;
    }

    @Override
    public int getMaxAge() {
        return 5;
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(AGE, LIVES);
    }

    //Harvest lives stuff
    public IntegerProperty getLivesProperty() {
        return LIVES;
    }

    protected int getLives(BlockState p_52306_) {
        return p_52306_.getValue(this.getLivesProperty());
    }

    public void reducelives(Level p_55856_, BlockPos p_55857_, BlockState p_55858_) {
        if(this.getLives(p_55858_) >= 1) {
            p_55856_.setBlock(p_55857_, p_55858_.setValue(LIVES, p_55858_.getValue(LIVES) - 1), 3);
        }
    }
    //Harvest lives stuff

    //Helper ints
    int xp;
    int farmlevelreq;
    int minproduce;
    int roll;
    int savechance;

    @Override
    public InteractionResult use(BlockState state, Level level, BlockPos pos, Player player, InteractionHand p_60507_, BlockHitResult p_60508_) {
        player.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            player.getCapability(RunicAgesFarmingCapability.Provider.FARMING_LEVEL).ifPresent(h ->
            {
                ItemStack handstack = player.getItemBySlot(EquipmentSlot.MAINHAND);
                if(handstack.getItem() == Items.WOODEN_SHOVEL||
                        handstack.getItem() == Items.STONE_SHOVEL||
                        handstack.getItem() == Items.GOLDEN_SHOVEL||
                        handstack.getItem() == Items.IRON_SHOVEL||
                        handstack.getItem() == Items.DIAMOND_SHOVEL||
                        handstack.getItem() == Items.NETHERITE_SHOVEL)
                {
                    if(!level.isClientSide())
                    {
                        return;
                    }
                    level.setBlockAndUpdate(pos, Blocks.AIR.defaultBlockState());
                }
                if (this.getAge(state) >= 5 && !player.isCreative() && !level.isClientSide()) {
                    Random rand = new Random();
                    //Setting helper ints
                    farmlevelreq = 3;
                    if(RunicAgesConfig.modernrs.get())
                    {
                        minproduce = 7;
                    }
                    else minproduce = 1;
                    xp = (int) 9.5;
                    savechance = 6;
                    roll = rand.nextInt(256);
                    if(handstack.getItem() == Items.WOODEN_SHOVEL||
                            handstack.getItem() == Items.STONE_SHOVEL||
                            handstack.getItem() == Items.GOLDEN_SHOVEL||
                            handstack.getItem() == Items.IRON_SHOVEL||
                            handstack.getItem() == Items.DIAMOND_SHOVEL||
                            handstack.getItem() == Items.NETHERITE_SHOVEL)
                    {
                        level.setBlockAndUpdate(pos, Blocks.AIR.defaultBlockState());
                    }
                    else if (h.getFarmingLevel() >= farmlevelreq) {
                        //Produce for the crop
                        popResource(level, pos, new ItemStack(ItemInit.BARLEY.get(), minproduce));
                        h.addFarmingXP(player, Math.round(xp));
                        ed.addxptotalxp(Math.round(xp));
                        player.sendSystemMessage(Component.literal("You have gained Farming XP: " + Math.round(xp)));
                        if (roll > savechance + h.getFarmingLevel())
                        {
                            reducelives(level, pos, state);
                            if(this.getLives(state) == 0)
                            {
                                level.setBlockAndUpdate(pos, Blocks.AIR.defaultBlockState());
                            }
                        }
                        else if(roll <= savechance + h.getFarmingLevel())
                        {
                            //Produce for the crop
                            popResource(level, pos, new ItemStack(ItemInit.BARLEY.get(), 1));
                            h.addFarmingXP(player, Math.round(xp));
                            ed.addxptotalxp(Math.round(xp));
                            player.sendSystemMessage(Component.literal("You have gained additional Farming XP: " + Math.round(xp)));
                        }
                    }
                }
            });
        });
        return super.use(state, level, pos, player, p_60507_, p_60508_);
    }

}
