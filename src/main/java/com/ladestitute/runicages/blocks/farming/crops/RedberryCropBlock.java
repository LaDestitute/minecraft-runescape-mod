package com.ladestitute.runicages.blocks.farming.crops;

import com.ladestitute.runicages.capability.farming.RunicAgesFarmingCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.registry.BlockInit;
import com.ladestitute.runicages.registry.ItemInit;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.BlockHitResult;

import java.util.Random;

public class RedberryCropBlock extends CropBlock {
    public static final IntegerProperty AGE = IntegerProperty.create("age", 0, 7);

    //Custom harvest lives blockstate value
    public static final IntegerProperty LIVES = IntegerProperty.create("lives", 0, 4);

    public RedberryCropBlock(Properties p_52247_) {
        super(p_52247_);
        this.registerDefaultState(this.stateDefinition.any().setValue(LIVES, 4));
    }

    //Patch type
    @Override
    protected boolean mayPlaceOn(BlockState p_52302_, BlockGetter p_52303_, BlockPos p_52304_) {
        return p_52302_.is(BlockInit.BUSH_PATCH.get());
    }

    @Override
    public boolean canSurvive(BlockState p_51028_, LevelReader p_51029_, BlockPos p_51030_) {
        BlockPos blockpos = p_51030_.below();
        return this.mayPlaceOn(p_51029_.getBlockState(blockpos), p_51029_, blockpos);
    }

    @Override
    public IntegerProperty getAgeProperty() {
        return AGE;
    }

    @Override
    public int getMaxAge() {
        return 7;
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(AGE, LIVES);
    }

    //Harvest lives stuff
    public IntegerProperty getLivesProperty() {
        return LIVES;
    }

    protected int getLives(BlockState p_52306_) {
        return p_52306_.getValue(this.getLivesProperty());
    }

    public void reducelives(Level p_55856_, BlockPos p_55857_, BlockState p_55858_) {
        if(this.getLives(p_55858_) >= 1) {
            p_55856_.setBlock(p_55857_, p_55858_.setValue(LIVES, p_55858_.getValue(LIVES) - 1), 3);
        }
    }

    public void addlives(Level p_55856_, BlockPos p_55857_, BlockState p_55858_) {
        if(this.getLives(p_55858_) >= 0 && this.getLives(p_55858_) <= 3) {
            p_55856_.setBlock(p_55857_, p_55858_.setValue(LIVES, p_55858_.getValue(LIVES) + 1), 3);
        }
    }

    @Override
    public void randomTick(BlockState p_221050_, ServerLevel p_221051_, BlockPos p_221052_, RandomSource p_221053_) {
        if(this.getLives(p_221050_) >= 0 && this.getLives(p_221050_) <= 3 && this.getAge(p_221050_) <= 5)
        {
            addlives(p_221051_, p_221052_, p_221050_);
        }
        super.randomTick(p_221050_, p_221051_, p_221052_, p_221053_);
    }
    //Harvest lives stuff

    //Helper ints
    int xp;
    int farmlevelreq;
    int minproduce;

    @Override
    public InteractionResult use(BlockState state, Level level, BlockPos pos, Player player, InteractionHand p_60507_, BlockHitResult p_60508_) {
        player.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            player.getCapability(RunicAgesFarmingCapability.Provider.FARMING_LEVEL).ifPresent(h ->
            {
                ItemStack handstack = player.getItemBySlot(EquipmentSlot.MAINHAND);
                if(handstack.getItem() == Items.WOODEN_SHOVEL||
                        handstack.getItem() == Items.STONE_SHOVEL||
                        handstack.getItem() == Items.GOLDEN_SHOVEL||
                        handstack.getItem() == Items.IRON_SHOVEL||
                        handstack.getItem() == Items.DIAMOND_SHOVEL||
                        handstack.getItem() == Items.NETHERITE_SHOVEL)
                {
                    if(!level.isClientSide())
                    {
                        return;
                    }
                    level.setBlockAndUpdate(pos, Blocks.AIR.defaultBlockState());
                }
                if (this.getAge(state) >= 5 && !player.isCreative() && !level.isClientSide()) {
                    Random rand = new Random();
                    //Setting helper ints
                    farmlevelreq = 10;
                    xp = (int) 4.5;
                    minproduce = 1;
                    if(handstack.getItem() == Items.WOODEN_SHOVEL||
                            handstack.getItem() == Items.STONE_SHOVEL||
                            handstack.getItem() == Items.GOLDEN_SHOVEL||
                            handstack.getItem() == Items.IRON_SHOVEL||
                            handstack.getItem() == Items.DIAMOND_SHOVEL||
                            handstack.getItem() == Items.NETHERITE_SHOVEL)
                    {
                        level.setBlockAndUpdate(pos, Blocks.AIR.defaultBlockState());
                    }
                    else if (h.getFarmingLevel() >= farmlevelreq) {
                        //Produce for the crop
                        popResource(level, pos, new ItemStack(ItemInit.REDBERRY.get(), minproduce));
                        h.addFarmingXP(player, Math.round(xp));
                        ed.addxptotalxp(Math.round(xp));
                        player.sendSystemMessage(Component.literal("You have gained Farming XP: " + Math.round(xp)));
                            reducelives(level, pos, state);
                            System.out.println("LIVES IS: " + this.getLives(state));
                            if(this.getLives(state) == 0)
                            {
                                int i = this.getAge(state);
                                level.setBlockAndUpdate(pos, this.getStateForAge(i-2));
                            }
                    }
                }
            });
        });
        return super.use(state, level, pos, player, p_60507_, p_60508_);
    }

}



