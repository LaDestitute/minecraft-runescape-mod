package com.ladestitute.runicages.blocks.woodcutting;

import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.capability.woodcutting.RunicAgesWoodcuttingCapability;
import com.ladestitute.runicages.registry.ItemInit;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.Random;

public class NormalTreeLogBlock extends RotatedPillarBlock {

    public NormalTreeLogBlock(Properties properties) {
        super(properties);
    }

    @Override
    public InteractionResult use(BlockState p_60503_, Level level, BlockPos pos, Player player, InteractionHand p_60507_, BlockHitResult p_60508_) {
        ItemStack pickaxestack = player.getItemBySlot(EquipmentSlot.MAINHAND);
        Random rand = new Random();

        if(!level.isClientSide && player.getInventory().getFreeSlot() == -1) {
            player.displayClientMessage(Component.literal("Your inventory is too full to cut any more logs."), false);
        }
        else if(!level.isClientSide)
        {
            player.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
            {
                player.getCapability(RunicAgesWoodcuttingCapability.Provider.WOODCUTTING_LEVEL).ifPresent(h ->
                {
                    if(h.getWoodcuttingLevel() >= 1 && pickaxestack.getItem() == ItemInit.BRONZE_HATCHET.get()||
                            h.getWoodcuttingLevel() >= 10 && pickaxestack.getItem() == Items.IRON_AXE)
                    {
                        int logchance = Math.round(rand.nextInt(101));
                        int bonus = (int) Math.round(h.getWoodcuttingLevel() * 0.7852);

                        int roll = rand.nextInt(101);
                        if (h.getWoodcuttingLevel() <= 3 && logchance > roll) {
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.NORMAL_TREE_LOG.get().getDefaultInstance());
                            h.addWoodcuttingXP(player, 25);
                            ed.addxptotalxp(25);
                            player.displayClientMessage(Component.literal("You have gained XP: " + h.getWoodcuttingXP()), false);
                            int deplete = rand.nextInt(3);
                            if (deplete == 0) {
                                level.setBlockAndUpdate(pos, Blocks.AIR.defaultBlockState());
                                player.sendSystemMessage(Component.literal("The log you were cutting has depleted."));
                            }
                        }
                        if (h.getWoodcuttingLevel() >= 4 && logchance > roll + bonus) {
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.NORMAL_TREE_LOG.get().getDefaultInstance());
                            h.addWoodcuttingXP(player, 25);
                            ed.addxptotalxp(25);
                            player.displayClientMessage(Component.literal("You have gained XP: " + h.getWoodcuttingXP()), false);
                            int deplete = rand.nextInt(3);
                            if (deplete == 0) {
                                level.setBlockAndUpdate(pos, Blocks.AIR.defaultBlockState());
                                player.sendSystemMessage(Component.literal("The log you were cutting has depleted."));
                            }
                        }
                    }
                });
            });
        }
        return super.use(p_60503_, level, pos, player, p_60507_, p_60508_);
    }

    @Override
    public boolean isFlammable(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return true;
    }

    @Override
    public int getFlammability(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return 5;
    }

    @Override
    public int getFireSpreadSpeed(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return 5;
    }

}


