package com.ladestitute.runicages.blocks.woodcutting;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.registry.BlockInit;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

import javax.annotation.Nullable;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public class CutLightJungleBlock extends HorizontalDirectionalBlock {

    public CutLightJungleBlock(Properties properties) {
        super(properties.randomTicks());
        runCalculation(SHAPE.orElse(Shapes.block()));
    }

    protected void runCalculation(VoxelShape shape) {
        for (Direction direction : Direction.values())
            SHAPES.put(direction, RunicAgesMain.calculateShapes(direction, shape));
    }

    private static final Map<Direction, VoxelShape> SHAPES = new EnumMap<>(Direction.class);

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        super.createBlockStateDefinition(builder);
        builder.add(FACING);
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter level, BlockPos pos, CollisionContext context) {
        return SHAPES.get(state.getValue(FACING));
    }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
    }

    private static final Optional<VoxelShape> SHAPE = Stream
            .of(Block.box(11, 0, 4, 13, 2, 10),
                    Block.box(12, 2, 4, 13, 3, 10),
                    Block.box(11, 2, 4, 12, 3, 9),
                    Block.box(3, 0, 4, 11, 3, 12),
                    Block.box(3, 0, 3, 10, 3, 4),
                    Block.box(5, 4, 4, 8, 5, 5),
                    Block.box(5, 3, 4, 6, 4, 5),
                    Block.box(7, 5, 4, 8, 6, 5),
                    Block.box(4, 3, 4, 5, 6, 11),
                    Block.box(6, 3, 4, 7, 4, 11),
                    Block.box(7, 3, 4, 8, 4, 11),
                    Block.box(5, 5, 4, 6, 6, 11),
                    Block.box(6, 5, 4, 7, 6, 11),
                    Block.box(8, 3, 4, 9, 6, 11),
                    Block.box(9, 3, 4, 10, 6, 10),
                    Block.box(5, 6, 5, 8, 7, 10),
                    Block.box(8, 6, 5, 9, 7, 9),
                    Block.box(10, 3, 6, 12, 5, 8),
                    Block.box(4, 0, 12, 10, 2, 13),
                    Block.box(5, 2, 12, 10, 3, 13),
                    Block.box(11, 0, 10, 12, 1, 11),
                    Block.box(11, 1, 10, 12, 2, 11),
                    Block.box(9, 3, 10, 10, 4, 11),
                    Block.box(9, 4, 10, 10, 5, 11),
                    Block.box(9, 5, 10, 10, 6, 11),
                    Block.box(4, 2, 12, 5, 3, 13),
                    Block.box(5, 3, 10, 6, 4, 12),
                    Block.box(5, 4, 10, 6, 5, 11),
                    Block.box(6, 4, 10, 7, 5, 11),
                    Block.box(7, 4, 10, 8, 5, 11),
                    Block.box(8, 6, 9, 9, 7, 10),
                    Block.box(7, 5, 10, 8, 6, 11),
                    Block.box(10, 3, 5, 11, 4, 6),
                    Block.box(10, 5, 6, 11, 6, 7),
                    Block.box(10, 0, 3, 11, 1, 4),
                    Block.box(10, 1, 3, 11, 2, 4),
                    Block.box(11, 2, 9, 12, 3, 10))
            .reduce((v1, v2) -> Shapes.join(v1, v2, BooleanOp.OR));

    @Override
    public void randomTick(BlockState state, ServerLevel world, BlockPos pos, RandomSource p_60554_) {
        world.setBlock(pos, BlockInit.LIGHT_JUNGLE.get().defaultBlockState().setValue(FACING, state.getValue(FACING)), 2);
        super.randomTick(state, world, pos, p_60554_);
    }
}


