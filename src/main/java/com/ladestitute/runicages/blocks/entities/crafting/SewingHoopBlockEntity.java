package com.ladestitute.runicages.blocks.entities.crafting;

import com.ladestitute.runicages.capability.crafting.RunicAgesCraftingCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.client.menu.SewingKitMenu;
import com.ladestitute.runicages.registry.BlockEntityInit;
import com.ladestitute.runicages.registry.ItemInit;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.Containers;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.Nonnull;

public class SewingHoopBlockEntity extends BlockEntity implements MenuProvider {
    private final ItemStackHandler itemHandler = new ItemStackHandler(6) {
        @Override
        protected void onContentsChanged(int slot) {
            setChanged();
        }
    };

    private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();

    protected final ContainerData data;
    private int progress = 0;
    private int maxProgress = 54;
    private int materialused = 0;
    private int materialtype = 0;
    private int slotonequantityset = 0;
    private int slottwoquantityset = 0;
    private int slotthreequantityset = 0;
    private int slotfourquantityset = 0;

    public SewingHoopBlockEntity(BlockPos pWorldPosition, BlockState pBlockState) {
        super(BlockEntityInit.SEWING_KIT.get(), pWorldPosition, pBlockState);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    case 0: return SewingHoopBlockEntity.this.progress;
                    case 1: return SewingHoopBlockEntity.this.maxProgress;
                    case 2: return SewingHoopBlockEntity.this.materialused;
                    case 3: return SewingHoopBlockEntity.this.materialtype;
                    case 4: return SewingHoopBlockEntity.this.slotonequantityset;
                    case 5: return SewingHoopBlockEntity.this.slottwoquantityset;
                    case 6: return SewingHoopBlockEntity.this.slotthreequantityset;
                    case 7: return SewingHoopBlockEntity.this.slotfourquantityset;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    case 0: SewingHoopBlockEntity.this.progress = value; break;
                    case 1: SewingHoopBlockEntity.this.maxProgress = value; break;
                    case 2: SewingHoopBlockEntity.this.materialused = value; break;
                    case 3: SewingHoopBlockEntity.this.materialtype = value; break;
                    case 4: SewingHoopBlockEntity.this.slotonequantityset = value; break;
                    case 5: SewingHoopBlockEntity.this.slottwoquantityset = value; break;
                    case 6: SewingHoopBlockEntity.this.slotthreequantityset = value; break;
                    case 7: SewingHoopBlockEntity.this.slotfourquantityset = value; break;
                }
            }

            public int getCount() {
                return 8;
            }
        };
    }

    private Player usingPlayer;

    public void setPlayerEntity(Player player) {
        this.usingPlayer = player;
    }

    @Override
    public Component getDisplayName() {
        return Component.literal("Sewing Hoop");
    }

    @Nullable
    @Override
    public AbstractContainerMenu createMenu(int pContainerId, Inventory pInventory, Player pPlayer) {
        return new SewingKitMenu(pContainerId, pInventory, this, this.data);
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @javax.annotation.Nullable Direction side) {
        if (cap == ForgeCapabilities.ITEM_HANDLER) {
            return lazyItemHandler.cast();
        }

        return super.getCapability(cap, side);
    }

    @Override
    public void onLoad() {
        super.onLoad();
        lazyItemHandler = LazyOptional.of(() -> itemHandler);
    }

    @Override
    public void invalidateCaps()  {
        super.invalidateCaps();
        lazyItemHandler.invalidate();
    }

    @Override
    protected void saveAdditional(@NotNull CompoundTag tag) {
        tag.put("inventory", itemHandler.serializeNBT());
        tag.putInt("progress", progress);
        tag.putInt("materialused", materialused);
        tag.putInt("materialtype", materialtype);
        tag.putInt("slotonequantityset", slotonequantityset);

        tag.putInt("slottwoquantityset", slottwoquantityset);
        tag.putInt("slotthreequantityset", slotthreequantityset);
        tag.putInt("slotfourquantityset", slotfourquantityset);
        super.saveAdditional(tag);
    }

    @Override
    public void load(CompoundTag nbt) {
        super.load(nbt);
        itemHandler.deserializeNBT(nbt.getCompound("inventory"));
        progress = nbt.getInt("progress");
        materialused = nbt.getInt("materialused");
        materialtype = nbt.getInt("materialtype");
        slotonequantityset = nbt.getInt("slotonequantityset");

        slottwoquantityset = nbt.getInt("slottwoquantityset");
        slotthreequantityset = nbt.getInt("slotthreequantityset");
        slotfourquantityset = nbt.getInt("slotfourquantityset");
    }

    public void drops() {
        SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
        for (int i = 0; i < itemHandler.getSlots(); i++) {
            inventory.setItem(i, itemHandler.getStackInSlot(i));
        }

        Containers.dropContents(this.level, this.worldPosition, inventory);
    }


    public static void tick(Level level, BlockPos pos, BlockState state, SewingHoopBlockEntity pEntity) {
        if(level.isClientSide()) {
            return;
        }
        pEntity.progress++;
        if(pEntity.progress >= pEntity.maxProgress) {
            craftItem(pEntity);
        }

        setChanged(level, pos, state);
        if(pEntity.itemHandler.getStackInSlot(0).isEmpty()) {
            pEntity.resetProgress();
            setChanged(level, pos, state);
        }

    }

    private static void craftItem(SewingHoopBlockEntity entity) {
        if(entity.usingPlayer == null)
        {
            return;
        }
        entity.usingPlayer.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            entity.usingPlayer.getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(h ->
            {
                if (entity.materialused < 0) {
                    entity.materialused = 0;
                }
                //
                if (entity.itemHandler.getStackInSlot(0).is(ItemInit.SOFT_LEATHER.get()) && entity.slotonequantityset == 0) {
                    entity.materialused = entity.materialused + 1;
                    entity.slotonequantityset = 1;
                    entity.materialtype = 1;
                    System.out.println("MATERIAL USED IS: " + entity.materialused);
                }
                if (entity.itemHandler.getStackInSlot(1).is(ItemInit.SOFT_LEATHER.get()) && entity.slottwoquantityset == 0) {
                    entity.materialused = entity.materialused + 1;
                    entity.slottwoquantityset = 1;
                    entity.materialtype = 1;
                    System.out.println("MATERIAL USED IS: " + entity.materialused);
                }
                if (entity.itemHandler.getStackInSlot(2).is(ItemInit.SOFT_LEATHER.get()) && entity.slotthreequantityset == 0) {
                    entity.materialused = entity.materialused + 1;
                    entity.slotthreequantityset = 1;
                    entity.materialtype = 1;
                    System.out.println("MATERIAL USED IS: " + entity.materialused);
                }
                if (entity.itemHandler.getStackInSlot(3).is(ItemInit.SOFT_LEATHER.get()) && entity.slotfourquantityset == 0) {
                    entity.materialused = entity.materialused + 1;
                    entity.slotfourquantityset = 1;
                    entity.materialtype = 1;
                    System.out.println("MATERIAL USED IS: " + entity.materialused);
                }
                //
                if (entity.itemHandler.getStackInSlot(0).is(ItemInit.STRIP_OF_CLOTH.get()) && entity.slotonequantityset == 0) {
                    entity.materialused = entity.materialused + 1;
                    entity.slotonequantityset = 1;
                    entity.materialtype = 2;
                    System.out.println("MATERIAL USED IS: " + entity.materialused);
                }
                if (entity.itemHandler.getStackInSlot(1).is(ItemInit.STRIP_OF_CLOTH.get()) && entity.slottwoquantityset == 0) {
                    entity.materialused = entity.materialused + 1;
                    entity.slottwoquantityset = 1;
                    entity.materialtype = 2;
                    System.out.println("MATERIAL USED IS: " + entity.materialused);
                }
                if (entity.itemHandler.getStackInSlot(2).is(ItemInit.STRIP_OF_CLOTH.get()) && entity.slotthreequantityset == 0) {
                    entity.materialused = entity.materialused + 1;
                    entity.slotthreequantityset = 1;
                    entity.materialtype = 2;
                    System.out.println("MATERIAL USED IS: " + entity.materialused);
                }
                if (entity.itemHandler.getStackInSlot(3).is(ItemInit.STRIP_OF_CLOTH.get()) && entity.slotfourquantityset == 0) {
                    entity.materialused = entity.materialused + 1;
                    entity.slotfourquantityset = 1;
                    entity.materialtype = 2;
                    System.out.println("MATERIAL USED IS: " + entity.materialused);
                }
                //
                if (entity.materialused == 1 && entity.materialtype == 1 && entity.itemHandler.getStackInSlot(4).is(ItemInit.NEEDLE.get())
                        && entity.itemHandler.getStackInSlot(5).getCount() >= 1 && entity.itemHandler.getStackInSlot(5).is(ItemInit.THREAD.get())||
                        entity.materialused == 1 && entity.materialtype == 1 && ed.getHasToolbeltNeedle() == 1
                                && entity.itemHandler.getStackInSlot(5).getCount() >= 1 && entity.itemHandler.getStackInSlot(5).is(ItemInit.THREAD.get())) {

                    entity.itemHandler.extractItem(0, 1, false);
                    entity.itemHandler.extractItem(1, 1, false);
                    entity.itemHandler.extractItem(2, 1, false);
                    entity.itemHandler.extractItem(3, 1, false);
                    entity.itemHandler.extractItem(5, 1, false);
                    entity.itemHandler.setStackInSlot(0, new ItemStack(Items.LEATHER_HELMET,
                            entity.itemHandler.getStackInSlot(0).getCount() + 1));
                    entity.progress = 0;
                    h.addCraftingXP(entity.usingPlayer, 16);
                    ed.addxptotalxp(16);
                    entity.usingPlayer.sendSystemMessage(Component.literal("+16 Crafting XP"));
                    entity.materialused = 0;
                    entity.materialtype = 0;
                    entity.slotonequantityset = 0;
                    entity.slottwoquantityset = 0;
                    entity.slotthreequantityset = 0;
                    entity.slotfourquantityset = 0;
                }
                if (entity.materialused == 2 && entity.materialtype == 1 && entity.itemHandler.getStackInSlot(4).is(ItemInit.NEEDLE.get())
                        && entity.itemHandler.getStackInSlot(5).getCount() >= 2 && entity.itemHandler.getStackInSlot(5).is(ItemInit.THREAD.get())||
                        entity.materialused == 2 && entity.materialtype == 1 && ed.getHasToolbeltNeedle() == 1
                                && entity.itemHandler.getStackInSlot(5).getCount() >= 2 && entity.itemHandler.getStackInSlot(5).is(ItemInit.THREAD.get())) {

                    entity.itemHandler.extractItem(0, 1, false);
                    entity.itemHandler.extractItem(1, 1, false);
                    entity.itemHandler.extractItem(2, 1, false);
                    entity.itemHandler.extractItem(3, 1, false);
                    entity.itemHandler.extractItem(5, 2, false);
                    entity.itemHandler.setStackInSlot(0, new ItemStack(Items.LEATHER_BOOTS,
                            entity.itemHandler.getStackInSlot(0).getCount() + 1));
                    entity.progress = 0;
                    h.addCraftingXP(entity.usingPlayer, 18);
                    ed.addxptotalxp(18);
                    entity.usingPlayer.sendSystemMessage(Component.literal("+18 Crafting XP"));
                    entity.materialused = 0;
                    entity.materialtype = 0;
                    entity.slotonequantityset = 0;
                    entity.slottwoquantityset = 0;
                    entity.slotthreequantityset = 0;
                    entity.slotfourquantityset = 0;
                }
                if (entity.materialused == 3 && entity.materialtype == 1 && entity.itemHandler.getStackInSlot(4).is(ItemInit.NEEDLE.get())
                        && entity.itemHandler.getStackInSlot(5).getCount() >= 3 && entity.itemHandler.getStackInSlot(5).is(ItemInit.THREAD.get())||
                        entity.materialused == 3 && entity.materialtype == 1 && ed.getHasToolbeltNeedle() == 1
                                && entity.itemHandler.getStackInSlot(5).getCount() >= 3 && entity.itemHandler.getStackInSlot(5).is(ItemInit.THREAD.get())) {

                    entity.itemHandler.extractItem(0, 1, false);
                    entity.itemHandler.extractItem(1, 1, false);
                    entity.itemHandler.extractItem(2, 1, false);
                    entity.itemHandler.extractItem(3, 1, false);
                    entity.itemHandler.extractItem(5, 3, false);
                    entity.itemHandler.setStackInSlot(0, new ItemStack(Items.LEATHER_LEGGINGS,
                            entity.itemHandler.getStackInSlot(0).getCount() + 1));
                    entity.progress = 0;
                    h.addCraftingXP(entity.usingPlayer, 25);
                    ed.addxptotalxp(25);
                    entity.usingPlayer.sendSystemMessage(Component.literal("+25 Crafting XP"));
                    entity.materialused = 0;
                    entity.materialtype = 0;
                    entity.slotonequantityset = 0;
                    entity.slottwoquantityset = 0;
                    entity.slotthreequantityset = 0;
                    entity.slotfourquantityset = 0;
                }
                if (entity.materialused == 4 && entity.materialtype == 1 && entity.itemHandler.getStackInSlot(4).is(ItemInit.NEEDLE.get())
                        && entity.itemHandler.getStackInSlot(5).getCount() >= 4 && entity.itemHandler.getStackInSlot(5).is(ItemInit.THREAD.get())||
                        entity.materialused == 4 && entity.materialtype == 1 && ed.getHasToolbeltNeedle() == 1
                                && entity.itemHandler.getStackInSlot(5).getCount() >= 4 && entity.itemHandler.getStackInSlot(5).is(ItemInit.THREAD.get())) {

                    entity.itemHandler.extractItem(0, 1, false);
                    entity.itemHandler.extractItem(1, 1, false);
                    entity.itemHandler.extractItem(2, 1, false);
                    entity.itemHandler.extractItem(3, 1, false);
                    entity.itemHandler.extractItem(5, 4, false);
                    entity.itemHandler.setStackInSlot(0, new ItemStack(Items.LEATHER_CHESTPLATE,
                            entity.itemHandler.getStackInSlot(0).getCount() + 1));
                    entity.progress = 0;
                    h.addCraftingXP(entity.usingPlayer, 27);
                    ed.addxptotalxp(27);
                    entity.usingPlayer.sendSystemMessage(Component.literal("+27 Crafting XP"));
                    entity.materialused = 0;
                    entity.materialtype = 0;
                    entity.slotonequantityset = 0;
                    entity.slottwoquantityset = 0;
                    entity.slotthreequantityset = 0;
                    entity.slotfourquantityset = 0;
                }
                //
                if (entity.materialused == 1 && entity.materialtype == 2 && entity.itemHandler.getStackInSlot(4).is(ItemInit.NEEDLE.get())
                        && entity.itemHandler.getStackInSlot(5).getCount() >= 1 && entity.itemHandler.getStackInSlot(5).is(ItemInit.THREAD.get())||
                        entity.materialused == 1 && entity.materialtype == 2 && ed.getHasToolbeltNeedle() == 1
                                && entity.itemHandler.getStackInSlot(5).getCount() >= 1 && entity.itemHandler.getStackInSlot(5).is(ItemInit.THREAD.get())) {

                    entity.itemHandler.extractItem(0, 1, false);
                    entity.itemHandler.extractItem(1, 1, false);
                    entity.itemHandler.extractItem(2, 1, false);
                    entity.itemHandler.extractItem(3, 1, false);
                    entity.itemHandler.extractItem(5, 1, false);
                    entity.itemHandler.setStackInSlot(0, new ItemStack(ItemInit.WIZARD_HAT.get(),
                            entity.itemHandler.getStackInSlot(0).getCount() + 1));
                    entity.progress = 0;
                    h.addCraftingXP(entity.usingPlayer, 5);
                    ed.addxptotalxp(5);
                    entity.usingPlayer.sendSystemMessage(Component.literal("+5 Crafting XP"));
                    entity.materialused = 0;
                    entity.materialtype = 0;
                    entity.slotonequantityset = 0;
                    entity.slottwoquantityset = 0;
                    entity.slotthreequantityset = 0;
                    entity.slotfourquantityset = 0;
                }
                if (entity.materialused == 2 && entity.materialtype == 2 && entity.itemHandler.getStackInSlot(4).is(ItemInit.NEEDLE.get())
                        && entity.itemHandler.getStackInSlot(5).getCount() >= 2 && entity.itemHandler.getStackInSlot(5).is(ItemInit.THREAD.get())||
                        entity.materialused == 2 && entity.materialtype == 2 && ed.getHasToolbeltNeedle() == 1
                                && entity.itemHandler.getStackInSlot(5).getCount() >= 2 && entity.itemHandler.getStackInSlot(5).is(ItemInit.THREAD.get())) {

                    entity.itemHandler.extractItem(0, 1, false);
                    entity.itemHandler.extractItem(1, 1, false);
                    entity.itemHandler.extractItem(2, 1, false);
                    entity.itemHandler.extractItem(3, 1, false);
                    entity.itemHandler.extractItem(5, 2, false);
                    entity.itemHandler.setStackInSlot(0, new ItemStack(ItemInit.WIZARD_BOOTS.get(),
                            entity.itemHandler.getStackInSlot(0).getCount() + 1));
                    entity.progress = 0;
                    h.addCraftingXP(entity.usingPlayer, 5);
                    ed.addxptotalxp(5);
                    entity.usingPlayer.sendSystemMessage(Component.literal("+5 Crafting XP"));
                    entity.materialused = 0;
                    entity.materialtype = 0;
                    entity.slotonequantityset = 0;
                    entity.slottwoquantityset = 0;
                    entity.slotthreequantityset = 0;
                    entity.slotfourquantityset = 0;
                }
                if (entity.materialused == 3 && entity.materialtype == 2 && entity.itemHandler.getStackInSlot(4).is(ItemInit.NEEDLE.get())
                        && entity.itemHandler.getStackInSlot(5).getCount() >= 3 && entity.itemHandler.getStackInSlot(5).is(ItemInit.THREAD.get())||
                        entity.materialused == 3 && entity.materialtype == 2 && ed.getHasToolbeltNeedle() == 1
                                && entity.itemHandler.getStackInSlot(5).getCount() >= 3 && entity.itemHandler.getStackInSlot(5).is(ItemInit.THREAD.get())) {

                    entity.itemHandler.extractItem(0, 1, false);
                    entity.itemHandler.extractItem(1, 1, false);
                    entity.itemHandler.extractItem(2, 1, false);
                    entity.itemHandler.extractItem(3, 1, false);
                    entity.itemHandler.extractItem(5, 3, false);
                    entity.itemHandler.setStackInSlot(0, new ItemStack(ItemInit.WIZARD_ROBE_SKIRT.get(),
                            entity.itemHandler.getStackInSlot(0).getCount() + 1));
                    entity.progress = 0;
                    h.addCraftingXP(entity.usingPlayer, 10);
                    ed.addxptotalxp(10);
                    entity.usingPlayer.sendSystemMessage(Component.literal("+10 Crafting XP"));
                    entity.materialused = 0;
                    entity.materialtype = 0;
                    entity.slotonequantityset = 0;
                    entity.slottwoquantityset = 0;
                    entity.slotthreequantityset = 0;
                    entity.slotfourquantityset = 0;
                }
                if (entity.materialused == 4 && entity.materialtype == 2 && entity.itemHandler.getStackInSlot(4).is(ItemInit.NEEDLE.get())
                        && entity.itemHandler.getStackInSlot(5).getCount() >= 4 && entity.itemHandler.getStackInSlot(5).is(ItemInit.THREAD.get())||
                        entity.materialused == 4 && entity.materialtype == 2 && ed.getHasToolbeltNeedle() == 1
                                && entity.itemHandler.getStackInSlot(5).getCount() >= 4 && entity.itemHandler.getStackInSlot(5).is(ItemInit.THREAD.get())) {

                    entity.itemHandler.extractItem(0, 1, false);
                    entity.itemHandler.extractItem(1, 1, false);
                    entity.itemHandler.extractItem(2, 1, false);
                    entity.itemHandler.extractItem(3, 1, false);
                    entity.itemHandler.extractItem(5, 4, false);
                    entity.itemHandler.setStackInSlot(0, new ItemStack(ItemInit.WIZARD_ROBE_TOP.get(),
                            entity.itemHandler.getStackInSlot(0).getCount() + 1));
                    entity.progress = 0;
                    h.addCraftingXP(entity.usingPlayer, 10);
                    ed.addxptotalxp(10);
                    entity.usingPlayer.sendSystemMessage(Component.literal("+10 Crafting XP"));
                    entity.materialused = 0;
                    entity.materialtype = 0;
                    entity.slotonequantityset = 0;
                    entity.slottwoquantityset = 0;
                    entity.slotthreequantityset = 0;
                    entity.slotfourquantityset = 0;
                }
                //

            });
        });
    }

    private void resetProgress() {
        this.progress = 0;
    }
}

