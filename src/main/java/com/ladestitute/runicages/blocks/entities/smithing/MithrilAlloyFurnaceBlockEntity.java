package com.ladestitute.runicages.blocks.entities.smithing;

import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.capability.smithing.RunicAgesSmithingCapability;
import com.ladestitute.runicages.client.menu.MithrilAlloyFurnaceMenu;
import com.ladestitute.runicages.recipes.MithrilAlloyFurnaceRecipe;
import com.ladestitute.runicages.registry.BlockEntityInit;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.Containers;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.util.Random;

// !!!
public class MithrilAlloyFurnaceBlockEntity extends BlockEntity implements MenuProvider {
    private final ItemStackHandler itemHandler = new ItemStackHandler(4) {
        @Override
        protected void onContentsChanged(int slot) {
            setChanged();
        }
    };
    private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();
    protected final ContainerData data;
    private int progress = 0;
    private int maxProgress = 72;
    private int fuelTime = 0;
    private int maxFuelTime = 0;

    // !!!
    public MithrilAlloyFurnaceBlockEntity(BlockPos pWorldPosition, BlockState pBlockState) {
        // !!!
        super(BlockEntityInit.MITHRIL_ALLOY_FURNACE.get(), pWorldPosition, pBlockState);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    // !!!
                    case 0: return MithrilAlloyFurnaceBlockEntity.this.progress;
                    case 1: return MithrilAlloyFurnaceBlockEntity.this.maxProgress;
                    case 2: return MithrilAlloyFurnaceBlockEntity.this.fuelTime;
                    case 3: return MithrilAlloyFurnaceBlockEntity.this.maxFuelTime;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    // !!!
                    case 0: MithrilAlloyFurnaceBlockEntity.this.progress = value; break;
                    case 1: MithrilAlloyFurnaceBlockEntity.this.maxProgress = value; break;
                    case 2: MithrilAlloyFurnaceBlockEntity.this.fuelTime = value; break;
                    case 3: MithrilAlloyFurnaceBlockEntity.this.maxFuelTime = value; break;
                }
            }

            public int getCount() {
                return 4;
            }
        };
    }

    private Player usingPlayer;

    public void setPlayerEntity(Player player) {
        this.usingPlayer = player;
    }

    @Override
    // !!!
    public Component getDisplayName() {
        return Component.literal("Mithril Alloy Furnace");
    }


    @Nullable
    @Override
    // !!!
    public AbstractContainerMenu createMenu(int pContainerId, Inventory pInventory, Player pPlayer) {
        return new MithrilAlloyFurnaceMenu(pContainerId, pInventory, this, this.data);
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @javax.annotation.Nullable Direction side) {
        if (cap ==  ForgeCapabilities.ITEM_HANDLER) {
            return lazyItemHandler.cast();
        }

        return super.getCapability(cap, side);
    }

    @Override
    public void onLoad() {
        super.onLoad();
        lazyItemHandler = LazyOptional.of(() -> itemHandler);
    }

    @Override
    public void invalidateCaps()  {
        super.invalidateCaps();
        lazyItemHandler.invalidate();
    }

    @Override
    // !!!
    protected void saveAdditional(@NotNull CompoundTag tag) {
        tag.put("inventory", itemHandler.serializeNBT());
        tag.putInt("alloy.progress", progress);
        tag.putInt("alloy.fuelTime", fuelTime);
        tag.putInt("alloy.maxFuelTime", maxFuelTime);
        super.saveAdditional(tag);
    }

    @Override
    // !!!
    public void load(CompoundTag nbt) {
        super.load(nbt);
        itemHandler.deserializeNBT(nbt.getCompound("inventory"));
        progress = nbt.getInt("alloy.progress");
        fuelTime = nbt.getInt("alloy.fuelTime");
        maxFuelTime = nbt.getInt("alloy.maxFuelTime");
    }

    public void drops() {
        SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
        for (int i = 0; i < itemHandler.getSlots(); i++) {
            inventory.setItem(i, itemHandler.getStackInSlot(i));
        }

        Containers.dropContents(this.level, this.worldPosition, inventory);
    }

    private void consumeFuel() {
        if(!itemHandler.getStackInSlot(0).isEmpty()) {
            this.fuelTime = ForgeHooks.getBurnTime(this.itemHandler.extractItem(0, 1, false),
                    RecipeType.SMELTING);
            this.maxFuelTime = this.fuelTime;
        }
    }

    // !!!
    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, MithrilAlloyFurnaceBlockEntity pBlockEntity) {
        if(isConsumingFuel(pBlockEntity)) {
            pBlockEntity.fuelTime--;
        }

        if(hasRecipe(pBlockEntity)) {
            if(hasFuelInFuelSlot(pBlockEntity) && !isConsumingFuel(pBlockEntity)) {
                pBlockEntity.consumeFuel();
                setChanged(pLevel, pPos, pState);
            }
            if(isConsumingFuel(pBlockEntity)) {
                pBlockEntity.progress++;
                setChanged(pLevel, pPos, pState);
                if(pBlockEntity.progress > pBlockEntity.maxProgress) {

                    craftItem(pBlockEntity);

                }
            }
        } else {
            pBlockEntity.resetProgress();
            setChanged(pLevel, pPos, pState);
        }
    }

    // !!!
    private static boolean hasFuelInFuelSlot(MithrilAlloyFurnaceBlockEntity entity) {
        return !entity.itemHandler.getStackInSlot(0).isEmpty();
    }

    // !!!
    private static boolean isConsumingFuel(MithrilAlloyFurnaceBlockEntity entity) {
        return entity.fuelTime > 0;
    }

    // !!!
    private static boolean hasRecipe(MithrilAlloyFurnaceBlockEntity entity) {
        Level level = entity.level;
        SimpleContainer inventory = new SimpleContainer(entity.itemHandler.getSlots());
        for (int i = 0; i < entity.itemHandler.getSlots(); i++) {
            inventory.setItem(i, entity.itemHandler.getStackInSlot(i));
        }

        // !!!
        Optional<MithrilAlloyFurnaceRecipe> match = level.getRecipeManager()
                .getRecipeFor(MithrilAlloyFurnaceRecipe.Type.INSTANCE, inventory, level);

        return match.isPresent() && canInsertAmountIntoOutputSlot(inventory)
                && hasFueledSlot(entity) &&canInsertItemIntoOutputSlot(inventory, match.get().getOutput());
    }

    // !!!
    private static boolean hasFueledSlot(MithrilAlloyFurnaceBlockEntity entity) {
        return entity.itemHandler.getStackInSlot(0).getItem() == Items.COAL||
                entity.itemHandler.getStackInSlot(0).getItem() == Items.CHARCOAL||
                entity.itemHandler.getStackInSlot(0).getItem() == Blocks.COAL_BLOCK.asItem()||
                entity.itemHandler.getStackInSlot(0).getItem() == Items.LAVA_BUCKET||
                entity.itemHandler.getStackInSlot(0).getItem() == Items.BLAZE_ROD||
                entity.itemHandler.getStackInSlot(0).getItem() == ItemInit.NORMAL_TREE_LOG.get()||
                entity.itemHandler.getStackInSlot(0).getItem() == ItemInit.BIRCH_TREE_LOG.get();
    }

    // !!!
    private static void craftItem(MithrilAlloyFurnaceBlockEntity entity) {
        Level level = entity.level;
        SimpleContainer inventory = new SimpleContainer(entity.itemHandler.getSlots());
        for (int i = 0; i < entity.itemHandler.getSlots(); i++) {
            inventory.setItem(i, entity.itemHandler.getStackInSlot(i));
        }

        // !!!
        Optional<MithrilAlloyFurnaceRecipe> match = level.getRecipeManager()
                .getRecipeFor(MithrilAlloyFurnaceRecipe.Type.INSTANCE, inventory, level);

        if(match.isPresent()) {
            entity.itemHandler.extractItem(0,1, false);
            entity.itemHandler.extractItem(1,1, false);
            entity.itemHandler.extractItem(2,1, false);

            Random rand = new Random();
            int ironsuccesschance = rand.nextInt(2);

            if(entity.itemHandler.getStackInSlot(1).getItem() == Items.RAW_IRON &&
                    entity.itemHandler.getStackInSlot(2).getItem() == Items.RAW_IRON||
                    entity.itemHandler.getStackInSlot(1).getItem() == ItemInit.IRON_ORE.get() &&
                            entity.itemHandler.getStackInSlot(2).getItem() == Items.RAW_IRON||
                    entity.itemHandler.getStackInSlot(1).getItem() == Items.RAW_IRON &&
                            entity.itemHandler.getStackInSlot(2).getItem() == ItemInit.IRON_ORE.get()) {
                if (ironsuccesschance == 0) {
                    entity.itemHandler.setStackInSlot(3, new ItemStack(match.get().getOutput().getItem(),
                            entity.itemHandler.getStackInSlot(3).getCount() + 1));
                }
            }
            //
            if(entity.itemHandler.getStackInSlot(1).getItem() != Items.RAW_IRON &&
                    entity.itemHandler.getStackInSlot(2).getItem() != Items.RAW_IRON||
                    entity.itemHandler.getStackInSlot(1).getItem() != ItemInit.IRON_ORE.get() &&
                            entity.itemHandler.getStackInSlot(2).getItem() != Items.RAW_IRON||
                    entity.itemHandler.getStackInSlot(1).getItem() != Items.RAW_IRON &&
                            entity.itemHandler.getStackInSlot(2).getItem() != ItemInit.IRON_ORE.get()) {

                entity.itemHandler.setStackInSlot(3, new ItemStack(match.get().getOutput().getItem(),
                        entity.itemHandler.getStackInSlot(3).getCount() + 1));
            }

            if(entity.usingPlayer == null)
            {
                return;
            }

            //
            entity.usingPlayer.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
                    {
                        if (ironsuccesschance == 0 && !entity.level.isClientSide() && entity.itemHandler.getStackInSlot(3).getItem() == Items.IRON_INGOT) {
                            entity.usingPlayer.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(h ->
                            {
                                if (RunicAgesConfig.modernrs.get()) {
                                    h.addSmithingXP(entity.usingPlayer, 2);
                                    ed.addxptotalxp(2);
                                    entity.usingPlayer.sendSystemMessage(Component.literal("+2 Smithing XP"));
                                }
                                if (!RunicAgesConfig.modernrs.get()) {
                                    h.addSmithingXP(entity.usingPlayer, 12);
                                    ed.addxptotalxp(12);
                                    entity.usingPlayer.sendSystemMessage(Component.literal("+12 Smithing XP"));
                                }
                            });
                        }
                        //
                        if (!entity.level.isClientSide() && entity.itemHandler.getStackInSlot(3).getItem() == ItemInit.BRONZE_BAR.get()) {
                            entity.usingPlayer.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(h ->
                            {
                                if (RunicAgesConfig.modernrs.get()) {
                                    h.addSmithingXP(entity.usingPlayer, 1);
                                    ed.addxptotalxp(1);
                                    entity.usingPlayer.sendSystemMessage(Component.literal("+1 Smithing XP"));
                                }
                                if (!RunicAgesConfig.modernrs.get()) {
                                    h.addSmithingXP(entity.usingPlayer, 6);
                                    ed.addxptotalxp(6);
                                    entity.usingPlayer.sendSystemMessage(Component.literal("+6 Smithing XP"));
                                }
                            });
                        }
                        //
                        if (!entity.level.isClientSide() && entity.itemHandler.getStackInSlot(3).getItem() == ItemInit.STEEL_BAR.get()) {
                            entity.usingPlayer.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(h ->
                            {
                                if (RunicAgesConfig.modernrs.get()) {
                                    h.addSmithingXP(entity.usingPlayer, 3);
                                    ed.addxptotalxp(3);
                                    entity.usingPlayer.sendSystemMessage(Component.literal("+3 Smithing XP"));
                                }
                                if (!RunicAgesConfig.modernrs.get()) {
                                    h.addSmithingXP(entity.usingPlayer, 17);
                                    ed.addxptotalxp(17);
                                    entity.usingPlayer.sendSystemMessage(Component.literal("+17 Smithing XP"));
                                }
                            });
                        }
                        //
                        if (!entity.level.isClientSide() && entity.itemHandler.getStackInSlot(3).getItem() == ItemInit.MITHRIL_BAR.get()) {
                            entity.usingPlayer.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(h ->
                            {
                                if (RunicAgesConfig.modernrs.get()) {
                                    h.addSmithingXP(entity.usingPlayer, 5);
                                    ed.addxptotalxp(5);
                                    entity.usingPlayer.sendSystemMessage(Component.literal("+5 Smithing XP"));
                                }
                                if (!RunicAgesConfig.modernrs.get()) {
                                    h.addSmithingXP(entity.usingPlayer, 30);
                                    ed.addxptotalxp(30);
                                    entity.usingPlayer.sendSystemMessage(Component.literal("+30 Smithing XP"));
                                }
                            });
                        }
                        //
                        if (!entity.level.isClientSide() && entity.itemHandler.getStackInSlot(3).getItem() == ItemInit.SILVTHRIL_BAR.get()) {
                            entity.usingPlayer.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(h ->
                            {
                                if (RunicAgesConfig.modernrs.get()) {
                                    h.addSmithingXP(entity.usingPlayer, 4);
                                    ed.addxptotalxp(4);
                                    entity.usingPlayer.sendSystemMessage(Component.literal("+4 Smithing XP"));
                                }
                                if (!RunicAgesConfig.modernrs.get()) {
                                    h.addSmithingXP(entity.usingPlayer, 20);
                                    ed.addxptotalxp(20);
                                    entity.usingPlayer.sendSystemMessage(Component.literal("+20 Smithing XP"));
                                }
                            });
                        }
                    });
            entity.resetProgress();
        }

    }

    private void resetProgress() {
        this.progress = 0;
    }

    private static boolean canInsertItemIntoOutputSlot(SimpleContainer inventory, ItemStack output) {
        return inventory.getItem(3).getItem() == output.getItem() || inventory.getItem(3).isEmpty();
    }

    private static boolean canInsertAmountIntoOutputSlot(SimpleContainer inventory) {
        return inventory.getItem(3).getMaxStackSize() > inventory.getItem(3).getCount();
    }
}



