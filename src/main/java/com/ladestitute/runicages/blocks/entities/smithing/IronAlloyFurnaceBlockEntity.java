package com.ladestitute.runicages.blocks.entities.smithing;

import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.capability.smithing.RunicAgesSmithingCapability;
import com.ladestitute.runicages.client.menu.IronAlloyFurnaceMenu;
import com.ladestitute.runicages.recipes.IronAlloyFurnaceRecipe;
import com.ladestitute.runicages.registry.BlockEntityInit;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.Containers;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.Nonnull;
import java.util.Optional;
import java.util.Random;

// !!!
public class IronAlloyFurnaceBlockEntity extends BlockEntity implements MenuProvider {
    private final ItemStackHandler itemHandler = new ItemStackHandler(4) {
        @Override
        protected void onContentsChanged(int slot) {
            setChanged();
        }
    };
    private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();
    protected final ContainerData data;
    private int progress = 0;
    private int maxProgress = 72;
    private int fuelTime = 0;
    private int maxFuelTime = 0;

    // !!!
    public IronAlloyFurnaceBlockEntity(BlockPos pWorldPosition, BlockState pBlockState) {
        // !!!
        super(BlockEntityInit.IRON_ALLOY_FURNACE.get(), pWorldPosition, pBlockState);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    // !!!
                    case 0: return IronAlloyFurnaceBlockEntity.this.progress;
                    case 1: return IronAlloyFurnaceBlockEntity.this.maxProgress;
                    case 2: return IronAlloyFurnaceBlockEntity.this.fuelTime;
                    case 3: return IronAlloyFurnaceBlockEntity.this.maxFuelTime;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    // !!!
                    case 0: IronAlloyFurnaceBlockEntity.this.progress = value; break;
                    case 1: IronAlloyFurnaceBlockEntity.this.maxProgress = value; break;
                    case 2: IronAlloyFurnaceBlockEntity.this.fuelTime = value; break;
                    case 3: IronAlloyFurnaceBlockEntity.this.maxFuelTime = value; break;
                }
            }

            public int getCount() {
                return 4;
            }
        };
    }

    private Player usingPlayer;

    public void setPlayerEntity(Player player) {
        this.usingPlayer = player;
    }

    @Override
    // !!!
    public Component getDisplayName() {
        return Component.literal("Iron Alloy Furnace");
    }


    @Nullable
    @Override
    // !!!
    public AbstractContainerMenu createMenu(int pContainerId, Inventory pInventory, Player pPlayer) {
        return new IronAlloyFurnaceMenu(pContainerId, pInventory, this, this.data);
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @javax.annotation.Nullable Direction side) {
        if (cap ==  ForgeCapabilities.ITEM_HANDLER) {
            return lazyItemHandler.cast();
        }

        return super.getCapability(cap, side);
    }

    @Override
    public void onLoad() {
        super.onLoad();
        lazyItemHandler = LazyOptional.of(() -> itemHandler);
    }

    @Override
    public void invalidateCaps()  {
        super.invalidateCaps();
        lazyItemHandler.invalidate();
    }

    @Override
    // !!!
    protected void saveAdditional(@NotNull CompoundTag tag) {
        tag.put("inventory", itemHandler.serializeNBT());
        tag.putInt("iron_alloy.progress", progress);
        tag.putInt("iron_alloy.fuelTime", fuelTime);
        tag.putInt("iron_alloy.maxFuelTime", maxFuelTime);
        super.saveAdditional(tag);
    }

    @Override
    // !!!
    public void load(CompoundTag nbt) {
        super.load(nbt);
        itemHandler.deserializeNBT(nbt.getCompound("inventory"));
        progress = nbt.getInt("iron_alloy.progress");
        fuelTime = nbt.getInt("iron_alloy.fuelTime");
        maxFuelTime = nbt.getInt("iron_alloy.maxFuelTime");
    }

    public void drops() {
        SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
        for (int i = 0; i < itemHandler.getSlots(); i++) {
            inventory.setItem(i, itemHandler.getStackInSlot(i));
        }

        Containers.dropContents(this.level, this.worldPosition, inventory);
    }

    private void consumeFuel() {
        if(!itemHandler.getStackInSlot(0).isEmpty()) {
            this.fuelTime = ForgeHooks.getBurnTime(this.itemHandler.extractItem(0, 1, false),
                    RecipeType.SMELTING);
            this.maxFuelTime = this.fuelTime;
        }
    }

    // !!!
    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, IronAlloyFurnaceBlockEntity pBlockEntity) {
        if(isConsumingFuel(pBlockEntity)) {
            pBlockEntity.fuelTime--;
        }

        if(hasRecipe(pBlockEntity)) {
            if(hasFuelInFuelSlot(pBlockEntity) && !isConsumingFuel(pBlockEntity)) {
                pBlockEntity.consumeFuel();
                setChanged(pLevel, pPos, pState);
            }
            if(isConsumingFuel(pBlockEntity)) {
                pBlockEntity.progress++;
                setChanged(pLevel, pPos, pState);
                if(pBlockEntity.progress > pBlockEntity.maxProgress) {

                    craftItem(pBlockEntity);

                }
            }
        } else {
            pBlockEntity.resetProgress();
            setChanged(pLevel, pPos, pState);
        }
    }

    // !!!
    private static boolean hasFuelInFuelSlot(IronAlloyFurnaceBlockEntity entity) {
        return !entity.itemHandler.getStackInSlot(0).isEmpty();
    }

    // !!!
    private static boolean isConsumingFuel(IronAlloyFurnaceBlockEntity entity) {
        return entity.fuelTime > 0;
    }

    // !!!
    private static boolean hasRecipe(IronAlloyFurnaceBlockEntity entity) {
        Level level = entity.level;
        SimpleContainer inventory = new SimpleContainer(entity.itemHandler.getSlots());
        for (int i = 0; i < entity.itemHandler.getSlots(); i++) {
            inventory.setItem(i, entity.itemHandler.getStackInSlot(i));
        }

        // !!!
        Optional<IronAlloyFurnaceRecipe> match = level.getRecipeManager()
                .getRecipeFor(IronAlloyFurnaceRecipe.Type.INSTANCE, inventory, level);

        return match.isPresent() && canInsertAmountIntoOutputSlot(inventory)
                && hasFueledSlot(entity) &&canInsertItemIntoOutputSlot(inventory, match.get().getOutput());
    }

    // !!!
    private static boolean hasFueledSlot(IronAlloyFurnaceBlockEntity entity) {
        return entity.itemHandler.getStackInSlot(0).getItem() == Items.COAL||
                entity.itemHandler.getStackInSlot(0).getItem() == Items.CHARCOAL||
                entity.itemHandler.getStackInSlot(0).getItem() == Blocks.COAL_BLOCK.asItem()||
                entity.itemHandler.getStackInSlot(0).getItem() == Items.LAVA_BUCKET||
                entity.itemHandler.getStackInSlot(0).getItem() == Items.BLAZE_ROD||
                entity.itemHandler.getStackInSlot(0).getItem() == ItemInit.NORMAL_TREE_LOG.get()||
                entity.itemHandler.getStackInSlot(0).getItem() == ItemInit.BIRCH_TREE_LOG.get();
    }

    // !!!
    private static void craftItem(IronAlloyFurnaceBlockEntity entity) {
        Level level = entity.level;
        SimpleContainer inventory = new SimpleContainer(entity.itemHandler.getSlots());
        for (int i = 0; i < entity.itemHandler.getSlots(); i++) {
            inventory.setItem(i, entity.itemHandler.getStackInSlot(i));
        }

        // !!!
        Optional<IronAlloyFurnaceRecipe> match = level.getRecipeManager()
                .getRecipeFor(IronAlloyFurnaceRecipe.Type.INSTANCE, inventory, level);

        if(match.isPresent()) {
            entity.itemHandler.extractItem(0,1, false);
            entity.itemHandler.extractItem(1,1, false);
            entity.itemHandler.extractItem(2,1, false);

            Random rand = new Random();
            int ironsuccesschance = rand.nextInt(2);

            if(entity.itemHandler.getStackInSlot(1).getItem() == Items.RAW_IRON &&
                    entity.itemHandler.getStackInSlot(2).getItem() == Items.RAW_IRON||
                    entity.itemHandler.getStackInSlot(1).getItem() == ItemInit.IRON_ORE.get() &&
                            entity.itemHandler.getStackInSlot(2).getItem() == Items.RAW_IRON||
                    entity.itemHandler.getStackInSlot(1).getItem() == Items.RAW_IRON &&
                            entity.itemHandler.getStackInSlot(2).getItem() == ItemInit.IRON_ORE.get()) {
                if (ironsuccesschance == 0) {
                    entity.itemHandler.setStackInSlot(3, new ItemStack(match.get().getOutput().getItem(),
                            entity.itemHandler.getStackInSlot(3).getCount() + 1));
                }
            }
            if(entity.itemHandler.getStackInSlot(1).getItem() != Items.RAW_IRON &&
                    entity.itemHandler.getStackInSlot(2).getItem() != Items.RAW_IRON||
                    entity.itemHandler.getStackInSlot(1).getItem() != ItemInit.IRON_ORE.get() &&
                            entity.itemHandler.getStackInSlot(2).getItem() != Items.RAW_IRON||
                    entity.itemHandler.getStackInSlot(1).getItem() != Items.RAW_IRON &&
                            entity.itemHandler.getStackInSlot(2).getItem() != ItemInit.IRON_ORE.get()) {

                    entity.itemHandler.setStackInSlot(3, new ItemStack(match.get().getOutput().getItem(),
                            entity.itemHandler.getStackInSlot(3).getCount() + 1));

            }

            if(entity.usingPlayer == null)
            {
                return;
            }

            entity.usingPlayer.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
                    {
                        if (ironsuccesschance == 0 && !entity.level.isClientSide() && entity.itemHandler.getStackInSlot(3).getItem() == Items.IRON_INGOT) {
                            entity.usingPlayer.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(h ->
                            {
                                if (RunicAgesConfig.modernrs.get()) {
                                    h.addSmithingXP(entity.usingPlayer, 2);
                                    ed.addxptotalxp(2);
                                    entity.usingPlayer.sendSystemMessage(Component.literal("+2 Smithing XP"));
                                }
                                if (!RunicAgesConfig.modernrs.get()) {
                                    h.addSmithingXP(entity.usingPlayer, 12);
                                    ed.addxptotalxp(12);
                                    entity.usingPlayer.sendSystemMessage(Component.literal("+12 Smithing XP"));
                                }
                            });

                        }
                        if (!entity.level.isClientSide() && entity.itemHandler.getStackInSlot(3).getItem() == ItemInit.BRONZE_BAR.get()) {
                            entity.usingPlayer.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(h ->
                            {
                                if (RunicAgesConfig.modernrs.get()) {
                                    h.addSmithingXP(entity.usingPlayer, 1);
                                    ed.addxptotalxp(1);
                                    entity.usingPlayer.sendSystemMessage(Component.literal("+1 Smithing XP"));
                                }
                                if (!RunicAgesConfig.modernrs.get()) {
                                    h.addSmithingXP(entity.usingPlayer, 6);
                                    ed.addxptotalxp(6);
                                    entity.usingPlayer.sendSystemMessage(Component.literal("+6 Smithing XP"));
                                }
                            });

                        }
                    });
            entity.resetProgress();
        }

    }

    private void resetProgress() {
        this.progress = 0;
    }

    private static boolean canInsertItemIntoOutputSlot(SimpleContainer inventory, ItemStack output) {
        return inventory.getItem(3).getItem() == output.getItem() || inventory.getItem(3).isEmpty();
    }

    private static boolean canInsertAmountIntoOutputSlot(SimpleContainer inventory) {
        return inventory.getItem(3).getMaxStackSize() > inventory.getItem(3).getCount();
    }
}

