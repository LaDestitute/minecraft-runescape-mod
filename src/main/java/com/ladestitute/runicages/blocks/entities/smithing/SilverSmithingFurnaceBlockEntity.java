package com.ladestitute.runicages.blocks.entities.smithing;

import com.ladestitute.runicages.capability.crafting.RunicAgesCraftingCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.capability.smithing.RunicAgesSmithingCapability;
import com.ladestitute.runicages.client.menu.SilverSmithingFurnaceMenu;
import com.ladestitute.runicages.recipes.SilverSmithingFurnaceRecipe;
import com.ladestitute.runicages.registry.BlockEntityInit;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.Containers;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.Nonnull;
import java.util.Optional;

public class SilverSmithingFurnaceBlockEntity extends BlockEntity implements MenuProvider {
    private final ItemStackHandler itemHandler = new ItemStackHandler(3) {
        @Override
        protected void onContentsChanged(int slot) {
            setChanged();
        }
    };

    private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();

    protected final ContainerData data;
    private int progress = 0;
    private int maxProgress = 72;

    public SilverSmithingFurnaceBlockEntity(BlockPos pWorldPosition, BlockState pBlockState) {
        super(BlockEntityInit.SILVER_SMITHING_FURNACE.get(), pWorldPosition, pBlockState);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    case 0: return SilverSmithingFurnaceBlockEntity.this.progress;
                    case 1: return SilverSmithingFurnaceBlockEntity.this.maxProgress;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    case 0: SilverSmithingFurnaceBlockEntity.this.progress = value; break;
                    case 1: SilverSmithingFurnaceBlockEntity.this.maxProgress = value; break;
                }
            }

            public int getCount() {
                return 2;
            }
        };
    }

    private Player usingPlayer;

    public void setPlayerEntity(Player player) {
        this.usingPlayer = player;
    }

    @Override
    public Component getDisplayName() {
        return Component.literal("Silver Smithing Furnace");
    }

    @Nullable
    @Override
    public AbstractContainerMenu createMenu(int pContainerId, Inventory pInventory, Player pPlayer) {
        return new SilverSmithingFurnaceMenu(pContainerId, pInventory, this, this.data);
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @javax.annotation.Nullable Direction side) {
        if (cap ==  ForgeCapabilities.ITEM_HANDLER) {
            return lazyItemHandler.cast();
        }

        return super.getCapability(cap, side);
    }

    @Override
    public void onLoad() {
        super.onLoad();
        lazyItemHandler = LazyOptional.of(() -> itemHandler);
    }

    @Override
    public void invalidateCaps()  {
        super.invalidateCaps();
        lazyItemHandler.invalidate();
    }

    @Override
    protected void saveAdditional(@NotNull CompoundTag tag) {
        tag.put("inventory", itemHandler.serializeNBT());
        tag.putInt("smithing_furnace.progress", progress);
        super.saveAdditional(tag);
    }

    @Override
    public void load(CompoundTag nbt) {
        super.load(nbt);
        itemHandler.deserializeNBT(nbt.getCompound("inventory"));
        progress = nbt.getInt("smithing_furnace.progress");
    }

    public void drops() {
        SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
        for (int i = 0; i < itemHandler.getSlots(); i++) {
            inventory.setItem(i, itemHandler.getStackInSlot(i));
        }

        Containers.dropContents(this.level, this.worldPosition, inventory);
    }


    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, SilverSmithingFurnaceBlockEntity pBlockEntity) {
        if(hasRecipe(pBlockEntity)) {
            pBlockEntity.progress++;
            setChanged(pLevel, pPos, pState);
            if(pBlockEntity.progress > pBlockEntity.maxProgress) {
                craftItem(pBlockEntity);
            }
        } else {
            pBlockEntity.resetProgress();
            setChanged(pLevel, pPos, pState);
        }
    }

    private static boolean hasRecipe(SilverSmithingFurnaceBlockEntity entity) {
        Level level = entity.level;
        SimpleContainer inventory = new SimpleContainer(entity.itemHandler.getSlots());
        for (int i = 0; i < entity.itemHandler.getSlots(); i++) {
            inventory.setItem(i, entity.itemHandler.getStackInSlot(i));
        }

        Optional<SilverSmithingFurnaceRecipe> match = level.getRecipeManager()
                .getRecipeFor(SilverSmithingFurnaceRecipe.Type.INSTANCE, inventory, level);

        return match.isPresent() && canInsertAmountIntoOutputSlot(inventory)
                && canInsertItemIntoOutputSlot(inventory, match.get().getOutput())
                && hasFuel(entity);
    }

    private static boolean hasFuel(SilverSmithingFurnaceBlockEntity entity) {
        return entity.itemHandler.getStackInSlot(0).getItem() == Items.COAL||
                entity.itemHandler.getStackInSlot(0).getItem() == Items.CHARCOAL||
                entity.itemHandler.getStackInSlot(0).getItem() == Blocks.COAL_BLOCK.asItem()||
                entity.itemHandler.getStackInSlot(0).getItem() == Items.LAVA_BUCKET||
                entity.itemHandler.getStackInSlot(0).getItem() == Items.BLAZE_ROD||
                entity.itemHandler.getStackInSlot(0).getItem() == ItemInit.NORMAL_TREE_LOG.get()||
                entity.itemHandler.getStackInSlot(0).getItem() == ItemInit.BIRCH_TREE_LOG.get();
    }

    private static void craftItem(SilverSmithingFurnaceBlockEntity entity) {
        Level level = entity.level;
        SimpleContainer inventory = new SimpleContainer(entity.itemHandler.getSlots());
        for (int i = 0; i < entity.itemHandler.getSlots(); i++) {
            inventory.setItem(i, entity.itemHandler.getStackInSlot(i));
        }

        Optional<SilverSmithingFurnaceRecipe> match = level.getRecipeManager()
                .getRecipeFor(SilverSmithingFurnaceRecipe.Type.INSTANCE, inventory, level);

        if(match.isPresent()) {
            entity.itemHandler.extractItem(0,1, false);
            entity.itemHandler.extractItem(1,1, false);
            entity.itemHandler.setStackInSlot(2, new ItemStack(match.get().getOutput().getItem(),
                    entity.itemHandler.getStackInSlot(2).getCount() + 1));

            entity.resetProgress();
        }

        if(entity.usingPlayer == null)
        {
            return;
        }

        entity.usingPlayer.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            if (!entity.level.isClientSide() && entity.itemHandler.getStackInSlot(2).getItem() == ItemInit.BLURITE_BAR.get()) {
                entity.usingPlayer.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(h ->
                {
                    if (RunicAgesConfig.modernrs.get()) {
                        h.addSmithingXP(entity.usingPlayer, 2);
                        ed.addxptotalxp(2);
                        entity.usingPlayer.sendSystemMessage(Component.literal("+2 Smithing XP"));
                    }
                    if (!RunicAgesConfig.modernrs.get()) {
                        h.addSmithingXP(entity.usingPlayer, 8);
                        ed.addxptotalxp(8);
                        entity.usingPlayer.sendSystemMessage(Component.literal("+8 Smithing XP"));
                    }
                });
            }
            //
            if (!entity.level.isClientSide() && entity.itemHandler.getStackInSlot(2).getItem() == ItemInit.SILVER_BAR.get()) {
                entity.usingPlayer.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(h ->
                {
                    if (RunicAgesConfig.modernrs.get()) {
                        h.addSmithingXP(entity.usingPlayer, 2);
                        ed.addxptotalxp(2);
                        entity.usingPlayer.sendSystemMessage(Component.literal("+2 Smithing XP"));
                    }
                    if (!RunicAgesConfig.modernrs.get()) {
                        h.addSmithingXP(entity.usingPlayer, 8);
                        ed.addxptotalxp(8);
                        entity.usingPlayer.sendSystemMessage(Component.literal("+8 Smithing XP"));
                    }
                });
            }
            //
            if (!entity.level.isClientSide() && entity.itemHandler.getStackInSlot(2).getItem() == ItemInit.CLAY_POT.get()) {
                entity.usingPlayer.getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(h ->
                {
                    h.addCraftingXP(entity.usingPlayer, 6);
                    ed.addxptotalxp(6);
                    entity.usingPlayer.sendSystemMessage(Component.literal("+6 Crafting XP"));

                });
            }
            //
            if (!entity.level.isClientSide() && entity.itemHandler.getStackInSlot(2).getItem() == ItemInit.SILVER_RING.get()) {
                entity.usingPlayer.getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(h ->
                {
                    h.addCraftingXP(entity.usingPlayer, (int) 7.5);
                    ed.addxptotalxp((int) 7.5);
                    entity.usingPlayer.sendSystemMessage(Component.literal("+7.5 Crafting XP"));

                });
            }
            //
            if (!entity.level.isClientSide() && entity.itemHandler.getStackInSlot(2).getItem() == ItemInit.PIE_DISH.get()) {
                entity.usingPlayer.getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(h ->
                {
                    h.addCraftingXP(entity.usingPlayer, 10);
                    ed.addxptotalxp(10);
                    entity.usingPlayer.sendSystemMessage(Component.literal("+10 Crafting XP"));

                });
            }
            //
            if (!entity.level.isClientSide() && entity.itemHandler.getStackInSlot(2).getItem() == ItemInit.TIARA.get()) {
                entity.usingPlayer.getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(h ->
                {
                    h.addCraftingXP(entity.usingPlayer, 52);
                    ed.addxptotalxp(52);
                    entity.usingPlayer.sendSystemMessage(Component.literal("+52 Crafting XP"));

                });
            }
            //
            if (!entity.level.isClientSide() && entity.itemHandler.getStackInSlot(2).getItem() == ItemInit.LAPIS_LAZULI_RING.get()) {
                entity.usingPlayer.getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(h ->
                {
                    h.addCraftingXP(entity.usingPlayer, 38);
                    ed.addxptotalxp(38);
                    entity.usingPlayer.sendSystemMessage(Component.literal("+38 Crafting XP"));

                });
            }
            //
            if (!entity.level.isClientSide() && entity.itemHandler.getStackInSlot(2).getItem() == ItemInit.UNSTRUNG_OPAL_AMULET.get()) {
                entity.usingPlayer.getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(h ->
                {
                    h.addCraftingXP(entity.usingPlayer, 15);
                    ed.addxptotalxp(15);
                    entity.usingPlayer.sendSystemMessage(Component.literal("+15 Crafting XP"));

                });
            }
            //
            if (!entity.level.isClientSide() && entity.itemHandler.getStackInSlot(2).getItem() == ItemInit.OPAL_RING.get()) {
                entity.usingPlayer.getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(h ->
                {
                    h.addCraftingXP(entity.usingPlayer, 10);
                    ed.addxptotalxp(10);
                    entity.usingPlayer.sendSystemMessage(Component.literal("+10 Crafting XP"));

                });
            }
        });
    }

    private void resetProgress() {
        this.progress = 0;
    }

    private static boolean canInsertItemIntoOutputSlot(SimpleContainer inventory, ItemStack output) {
        return inventory.getItem(2).getItem() == output.getItem() || inventory.getItem(2).isEmpty();
    }

    private static boolean canInsertAmountIntoOutputSlot(SimpleContainer inventory) {
        return inventory.getItem(2).getMaxStackSize() > inventory.getItem(2).getCount();
    }
}
