package com.ladestitute.runicages.blocks.entities.crafting;

import com.ladestitute.runicages.capability.crafting.RunicAgesCraftingCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.client.menu.SpinningWheelMenu;
import com.ladestitute.runicages.recipes.SpinningWheelRecipe;
import com.ladestitute.runicages.registry.BlockEntityInit;
import com.ladestitute.runicages.registry.ItemInit;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.Containers;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.Nonnull;
import java.util.Optional;

public class SpinningWheelBlockEntity extends BlockEntity implements MenuProvider {
    private final ItemStackHandler itemHandler = new ItemStackHandler(2) {
        @Override
        protected void onContentsChanged(int slot) {
            setChanged();
        }
    };

    private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();

    protected final ContainerData data;
    private int progress = 0;
    private int maxProgress = 54;

    public SpinningWheelBlockEntity(BlockPos pWorldPosition, BlockState pBlockState) {
        super(BlockEntityInit.SPINNING_WHEEL.get(), pWorldPosition, pBlockState);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    case 0: return SpinningWheelBlockEntity.this.progress;
                    case 1: return SpinningWheelBlockEntity.this.maxProgress;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    case 0: SpinningWheelBlockEntity.this.progress = value; break;
                    case 1: SpinningWheelBlockEntity.this.maxProgress = value; break;
                }
            }

            public int getCount() {
                return 2;
            }
        };
    }

    private Player usingPlayer;

    public void setPlayerEntity(Player player) {
        this.usingPlayer = player;
    }

    @Override
    public Component getDisplayName() {
        return Component.literal("Spinning Wheel");
    }

    @Nullable
    @Override
    public AbstractContainerMenu createMenu(int pContainerId, Inventory pInventory, Player pPlayer) {
        return new SpinningWheelMenu(pContainerId, pInventory, this, this.data);
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @javax.annotation.Nullable Direction side) {
        if (cap == ForgeCapabilities.ITEM_HANDLER) {
            return lazyItemHandler.cast();
        }

        return super.getCapability(cap, side);
    }

    @Override
    public void onLoad() {
        super.onLoad();
        lazyItemHandler = LazyOptional.of(() -> itemHandler);
    }

    @Override
    public void invalidateCaps()  {
        super.invalidateCaps();
        lazyItemHandler.invalidate();
    }

    @Override
    protected void saveAdditional(@NotNull CompoundTag tag) {
        tag.put("inventory", itemHandler.serializeNBT());
        tag.putInt("progress", progress);
        super.saveAdditional(tag);
    }

    @Override
    public void load(CompoundTag nbt) {
        super.load(nbt);
        itemHandler.deserializeNBT(nbt.getCompound("inventory"));
        progress = nbt.getInt("progress");
    }

    public void drops() {
        SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
        for (int i = 0; i < itemHandler.getSlots(); i++) {
            inventory.setItem(i, itemHandler.getStackInSlot(i));
        }

        Containers.dropContents(this.level, this.worldPosition, inventory);
    }


    public static void tick(Level level, BlockPos pos, BlockState state, SpinningWheelBlockEntity pEntity) {
        if(level.isClientSide()) {
            return;
        }
        pEntity.progress++;
        if(pEntity.progress >= pEntity.maxProgress) {
            craftItem(pEntity);
        }

        setChanged(level, pos, state);
        if(pEntity.itemHandler.getStackInSlot(0).isEmpty()) {
            pEntity.resetProgress();
            setChanged(level, pos, state);
        }

    }

    private static boolean hasRecipe(SpinningWheelBlockEntity entity) {
        Level level = entity.level;
        SimpleContainer inventory = new SimpleContainer(entity.itemHandler.getSlots());
        for (int i = 0; i < entity.itemHandler.getSlots(); i++) {
            inventory.setItem(i, entity.itemHandler.getStackInSlot(i));
        }

        Optional<SpinningWheelRecipe> match = level.getRecipeManager()
                .getRecipeFor(SpinningWheelRecipe.Type.INSTANCE, inventory, level);

        return match.isPresent() && canInsertAmountIntoOutputSlot(inventory)
                && canInsertItemIntoOutputSlot(inventory, match.get().getOutput());
    }

    private static void craftItem(SpinningWheelBlockEntity entity) {
        if(entity.usingPlayer == null)
        {
            return;
        }
        if(entity.itemHandler.getStackInSlot(0).is(ItemTags.WOOL))
        {
            entity.itemHandler.extractItem(0, 1, false);
            entity.itemHandler.setStackInSlot(1, new ItemStack(ItemInit.BALL_OF_WOOL.get(),
                    entity.itemHandler.getStackInSlot(1).getCount() + 1));
            entity.progress = 0;
        }
        if(entity.itemHandler.getStackInSlot(0).getItem() == ItemInit.FLAX.get())
        {
            entity.itemHandler.extractItem(0, 1, false);
            entity.itemHandler.setStackInSlot(1, new ItemStack(ItemInit.BOWSTRING.get(),
                    entity.itemHandler.getStackInSlot(1).getCount() + 1));
            entity.progress = 0;
        }

        //XP
        entity.usingPlayer.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            if (!entity.level.isClientSide() && entity.itemHandler.getStackInSlot(1).getItem() == ItemInit.BALL_OF_WOOL.get()) {
                entity.usingPlayer.getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(h ->
                {
                    h.addCraftingXP(entity.usingPlayer, 3);
                    ed.addxptotalxp(3);
                    entity.usingPlayer.sendSystemMessage(Component.literal("+3 Crafting XP"));

                });
            }
            if (!entity.level.isClientSide() && entity.itemHandler.getStackInSlot(1).getItem() == ItemInit.BOWSTRING.get()) {
                entity.usingPlayer.getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(h ->
                {
                    h.addCraftingXP(entity.usingPlayer, 15);
                    ed.addxptotalxp(15);
                    entity.usingPlayer.sendSystemMessage(Component.literal("+15 Crafting XP"));

                });
            }
        });
    }

    private static boolean isWool(ItemStack state) {
        return state.is(ItemTags.WOOL);
    }

    private void resetProgress() {
        this.progress = 0;
    }

    private static boolean canInsertItemIntoOutputSlot(SimpleContainer inventory, ItemStack output) {
        return inventory.getItem(1).getItem() == output.getItem() || inventory.getItem(1).isEmpty();
    }

    private static boolean canInsertAmountIntoOutputSlot(SimpleContainer inventory) {
        return inventory.getItem(1).getMaxStackSize() > inventory.getItem(1).getCount();
    }
}
