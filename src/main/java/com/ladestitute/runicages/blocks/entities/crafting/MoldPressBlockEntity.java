package com.ladestitute.runicages.blocks.entities.crafting;

import com.ladestitute.runicages.client.menu.MoldPressMenu;
import com.ladestitute.runicages.registry.BlockEntityInit;
import com.ladestitute.runicages.registry.ItemInit;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.Containers;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.Nonnull;

public class MoldPressBlockEntity extends BlockEntity implements MenuProvider {
    private int numslots = 14;
    private final ItemStackHandler itemHandler = new ItemStackHandler(numslots) {
        @Override
        protected void onContentsChanged(int slot) {
            setChanged();
        }
    };

    private LazyOptional<IItemHandler> lazyItemHandler = LazyOptional.empty();

    private Player usingPlayer;

    public void setPlayerEntity(Player player) {
        this.usingPlayer = player;
    }

    protected final ContainerData data;
    private int progress = 0;
    private int maxProgress = 36;

    public MoldPressBlockEntity(BlockPos pWorldPosition, BlockState pBlockState) {
        super(BlockEntityInit.MOLD_PRESS.get(), pWorldPosition, pBlockState);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    case 0: return MoldPressBlockEntity.this.progress;
                    case 1: return MoldPressBlockEntity.this.maxProgress;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    case 0: MoldPressBlockEntity.this.progress = value; break;
                    case 1: MoldPressBlockEntity.this.maxProgress = value; break;
                }
            }

            public int getCount() {
                return 2;
            }
        };
    }

    @Override
    public Component getDisplayName() {
        return Component.literal("Mold Press");
    }

    @Nullable
    @Override
    public AbstractContainerMenu createMenu(int pContainerId, Inventory pInventory, Player pPlayer) {
        return new MoldPressMenu(pContainerId, pInventory, this, this.data);
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @javax.annotation.Nullable Direction side) {
        if (cap == ForgeCapabilities.ITEM_HANDLER) {
            return lazyItemHandler.cast();
        }

        return super.getCapability(cap, side);
    }

    @Override
    public void onLoad() {
        super.onLoad();
        lazyItemHandler = LazyOptional.of(() -> itemHandler);
    }

    @Override
    public void invalidateCaps()  {
        super.invalidateCaps();
        lazyItemHandler.invalidate();
    }

    @Override
    protected void saveAdditional(@NotNull CompoundTag tag) {
        tag.put("inventory", itemHandler.serializeNBT());
        tag.putInt("progress", progress);
        super.saveAdditional(tag);
    }

    @Override
    public void load(CompoundTag nbt) {
        super.load(nbt);
        itemHandler.deserializeNBT(nbt.getCompound("inventory"));
        progress = nbt.getInt("progress");
    }

    public void drops() {
        SimpleContainer inventory = new SimpleContainer(itemHandler.getSlots());
        for (int i = 0; i < itemHandler.getSlots(); i++) {
            inventory.setItem(i, itemHandler.getStackInSlot(i));
        }

        Containers.dropContents(this.level, this.worldPosition, inventory);
    }


    public static void tick(Level level, BlockPos pos, BlockState state, MoldPressBlockEntity pEntity) {
        if(level.isClientSide()) {
            return;
        }
        pEntity.progress++;
        if(pEntity.progress >= pEntity.maxProgress) {
            craftItem(pEntity);
        }

            setChanged(level, pos, state);

    }

    private static void craftItem(MoldPressBlockEntity entity) {
        if(entity.usingPlayer == null)
        {
            return;
        }
            if ( entity.itemHandler.getStackInSlot(0).getItem() == Items.CLAY_BALL) {
                entity.itemHandler.extractItem(0, 1, false);
                entity.itemHandler.setStackInSlot(0, new ItemStack(ItemInit.TIARA_MOLD.get(),
                        entity.itemHandler.getStackInSlot(0).getCount() + 1));
                entity.progress = 0;
            }
            if (entity.itemHandler.getStackInSlot(8).getItem() == Items.CLAY_BALL) {
                entity.itemHandler.extractItem(8, 1, false);
                entity.itemHandler.setStackInSlot(8, new ItemStack(ItemInit.RING_MOLD.get(),
                        entity.itemHandler.getStackInSlot(8).getCount() + 1));
                entity.progress = 0;
            }
           if ( entity.itemHandler.getStackInSlot(2).getItem() == Items.CLAY_BALL) {
                entity.itemHandler.extractItem(2, 1, false);
                entity.itemHandler.setStackInSlot(2, new ItemStack(ItemInit.NECKLACE_MOLD.get(),
                        entity.itemHandler.getStackInSlot(2).getCount() + 1));
                entity.progress = 0;
            }
             if (entity.itemHandler.getStackInSlot(11).getItem() == Items.CLAY_BALL) {
                entity.itemHandler.extractItem(11, 1, false);
                entity.itemHandler.setStackInSlot(11, new ItemStack(ItemInit.BRACELET_MOLD.get(),
                        entity.itemHandler.getStackInSlot(11).getCount() + 1));
                entity.progress = 0;
            }
             if ( entity.itemHandler.getStackInSlot(5).getItem() == Items.CLAY_BALL) {
                entity.itemHandler.extractItem(5, 1, false);
                entity.itemHandler.setStackInSlot(5, new ItemStack(ItemInit.AMULET_MOLD.get(),
                        entity.itemHandler.getStackInSlot(5).getCount() + 1));
                entity.progress = 0;
            }
             if ( entity.itemHandler.getStackInSlot(8).getItem() == ItemInit.RING_MOLD.get() &&
                    entity.itemHandler.getStackInSlot(9).getItem() == Items.GOLD_INGOT) {
                entity.itemHandler.extractItem(8, 1, false);
                entity.itemHandler.extractItem(9, 1, false);
                entity.itemHandler.setStackInSlot(8, new ItemStack(ItemInit.FILLED_GOLD_RING_MOLD.get(),
                        entity.itemHandler.getStackInSlot(8).getCount() + 1));
                entity.progress = 0;
            }
            //
             if ( entity.itemHandler.getStackInSlot(8).getItem() == ItemInit.RING_MOLD.get() &&
                    entity.itemHandler.getStackInSlot(9).getItem() == ItemInit.SILVER_BAR.get() &&
                    entity.itemHandler.getStackInSlot(10).getItem() == ItemInit.CUT_LAPIS_LAZULI.get()) {
                entity.itemHandler.extractItem(8, 1, false);
                entity.itemHandler.extractItem(9, 1, false);
                entity.itemHandler.extractItem(10, 1, false);
                entity.itemHandler.setStackInSlot(8, new ItemStack(ItemInit.FILLED_LAPIS_LAZULI_RING_MOLD.get(),
                        entity.itemHandler.getStackInSlot(8).getCount() + 1));
                entity.progress = 0;
            }
            //
             if ( entity.itemHandler.getStackInSlot(8).getItem() == ItemInit.RING_MOLD.get() &&
                    entity.itemHandler.getStackInSlot(9).getItem() == ItemInit.SILVER_BAR.get() &&
                    entity.itemHandler.getStackInSlot(10).getItem() == ItemInit.SAPPHIRE_RING.get()) {
                entity.itemHandler.extractItem(8, 1, false);
                entity.itemHandler.extractItem(9, 1, false);
                entity.itemHandler.extractItem(10, 1, false);
                entity.itemHandler.setStackInSlot(8, new ItemStack(ItemInit.FILLED_SAPPHIRE_RING_MOLD.get(),
                        entity.itemHandler.getStackInSlot(8).getCount() + 1));
                entity.progress = 0;
            }
            //
             if ( entity.itemHandler.getStackInSlot(2).getItem() == ItemInit.NECKLACE_MOLD.get() &&
                    entity.itemHandler.getStackInSlot(3).getItem() == Items.GOLD_INGOT) {
                entity.itemHandler.extractItem(2, 1, false);
                entity.itemHandler.extractItem(3, 1, false);
                entity.itemHandler.setStackInSlot(2, new ItemStack(ItemInit.FILLED_GOLD_NECKLACE_MOLD.get(),
                        entity.itemHandler.getStackInSlot(2).getCount() + 1));
                entity.progress = 0;
            }
            //
             if ( entity.itemHandler.getStackInSlot(11).getItem() == ItemInit.BRACELET_MOLD.get() &&
                    entity.itemHandler.getStackInSlot(12).getItem() == Items.GOLD_INGOT) {
                entity.itemHandler.extractItem(11, 1, false);
                entity.itemHandler.extractItem(12, 1, false);
                entity.itemHandler.setStackInSlot(11, new ItemStack(ItemInit.FILLED_GOLD_BRACELET_MOLD.get(),
                        entity.itemHandler.getStackInSlot(11).getCount() + 1));
                entity.progress = 0;
            }
            //
            if (entity.itemHandler.getStackInSlot(11).getItem() == ItemInit.BRACELET_MOLD.get() &&
                    entity.itemHandler.getStackInSlot(12).getItem() == Items.GOLD_INGOT
                    && entity.itemHandler.getStackInSlot(13).getItem() == ItemInit.SAPPHIRE.get()) {
                entity.itemHandler.extractItem(11, 1, false);
                entity.itemHandler.extractItem(12, 1, false);
                entity.itemHandler.extractItem(13, 1, false);
                entity.itemHandler.setStackInSlot(11, new ItemStack(ItemInit.FILLED_SAPPHIRE_BRACELET_MOLD.get(),
                        entity.itemHandler.getStackInSlot(11).getCount() + 1));
                entity.progress = 0;
            }
            //
             if (entity.itemHandler.getStackInSlot(5).getItem() == ItemInit.AMULET_MOLD.get() &&
                    entity.itemHandler.getStackInSlot(6).getItem() == Items.GOLD_INGOT) {
                entity.itemHandler.extractItem(5, 1, false);
                entity.itemHandler.extractItem(6, 1, false);
                entity.itemHandler.setStackInSlot(5, new ItemStack(ItemInit.FILLED_GOLD_AMULET_MOLD.get(),
                        entity.itemHandler.getStackInSlot(5).getCount() + 1));
                entity.progress = 0;
            }
            //
             if (entity.itemHandler.getStackInSlot(0).getItem() == ItemInit.TIARA_MOLD.get() &&
                    entity.itemHandler.getStackInSlot(1).getItem() == ItemInit.SILVER_BAR.get()) {
                entity.itemHandler.extractItem(0, 1, false);
                entity.itemHandler.extractItem(1, 1, false);
                entity.itemHandler.setStackInSlot(0, new ItemStack(ItemInit.FILLED_TIARA_MOLD.get(),
                        entity.itemHandler.getStackInSlot(8).getCount() + 1));
                entity.progress = 0;
            }
            //
             if (entity.itemHandler.getStackInSlot(5).getItem() == ItemInit.AMULET_MOLD.get() &&
                    entity.itemHandler.getStackInSlot(6).getItem() == Items.GOLD_INGOT
            && entity.itemHandler.getStackInSlot(7).getItem() == ItemInit.SAPPHIRE.get()) {
                entity.itemHandler.extractItem(5, 1, false);
                entity.itemHandler.extractItem(6, 1, false);
                entity.itemHandler.extractItem(7, 1, false);
                entity.itemHandler.setStackInSlot(5, new ItemStack(ItemInit.FILLED_SAPPHIRE_AMULET_MOLD.get(),
                        entity.itemHandler.getStackInSlot(5).getCount() + 1));
                entity.progress = 0;
            }
            //
             if (entity.itemHandler.getStackInSlot(5).getItem() == ItemInit.AMULET_MOLD.get() &&
                    entity.itemHandler.getStackInSlot(6).getItem() == ItemInit.SILVER_BAR.get()
                    && entity.itemHandler.getStackInSlot(7).getItem() == ItemInit.OPAL.get()) {
                entity.itemHandler.extractItem(5, 1, false);
                entity.itemHandler.extractItem(6, 1, false);
                entity.itemHandler.extractItem(7, 1, false);
                entity.itemHandler.setStackInSlot(5, new ItemStack(ItemInit.FILLED_OPAL_AMULET_MOLD.get(),
                        entity.itemHandler.getStackInSlot(5).getCount() + 1));
                entity.progress = 0;
            }



    }
}
