package com.ladestitute.runicages.blocks.ore;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.mining.RunicAgesMiningCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.registry.BlockInit;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.registry.SpecialBlockInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.registries.ForgeRegistries;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;

import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Stream;

public class TinRockBlock extends HorizontalDirectionalBlock {

    public TinRockBlock(Properties properties) {
        super(properties);
        runCalculation(SHAPE.orElse(Shapes.block()));
    }

    protected void runCalculation(VoxelShape shape) {
        for (Direction direction : Direction.values())
            SHAPES.put(direction, RunicAgesMain.calculateShapes(direction, shape));
    }

    private static final Map<Direction, VoxelShape> SHAPES = new EnumMap<>(Direction.class);

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        super.createBlockStateDefinition(builder);
        builder.add(FACING);
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter level, BlockPos pos, CollisionContext context) {
        return SHAPES.get(state.getValue(FACING));
    }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
    }

    private static final Optional<VoxelShape> SHAPE = Stream
            .of(Block.box(11, 0, 4, 13, 2, 10),
                    Block.box(12, 2, 4, 13, 3, 10),
                    Block.box(11, 2, 4, 12, 3, 9),
                    Block.box(3, 0, 4, 11, 3, 12),
                    Block.box(3, 0, 3, 10, 3, 4),
                    Block.box(5, 4, 4, 8, 5, 5),
                    Block.box(5, 3, 4, 6, 4, 5),
                    Block.box(7, 5, 4, 8, 6, 5),
                    Block.box(4, 3, 4, 5, 6, 11),
                    Block.box(6, 3, 4, 7, 4, 11),
                    Block.box(7, 3, 4, 8, 4, 11),
                    Block.box(5, 5, 4, 6, 6, 11),
                    Block.box(6, 5, 4, 7, 6, 11),
                    Block.box(8, 3, 4, 9, 6, 11),
                    Block.box(9, 3, 4, 10, 6, 10),
                    Block.box(5, 6, 5, 8, 7, 10),
                    Block.box(8, 6, 5, 9, 7, 9),
                    Block.box(10, 3, 6, 12, 5, 8),
                    Block.box(4, 0, 12, 10, 2, 13),
                    Block.box(5, 2, 12, 10, 3, 13),
                    Block.box(11, 0, 10, 12, 1, 11),
                    Block.box(11, 1, 10, 12, 2, 11),
                    Block.box(9, 3, 10, 10, 4, 11),
                    Block.box(9, 4, 10, 10, 5, 11),
                    Block.box(9, 5, 10, 10, 6, 11),
                    Block.box(4, 2, 12, 5, 3, 13),
                    Block.box(5, 3, 10, 6, 4, 12),
                    Block.box(5, 4, 10, 6, 5, 11),
                    Block.box(6, 4, 10, 7, 5, 11),
                    Block.box(7, 4, 10, 8, 5, 11),
                    Block.box(8, 6, 9, 9, 7, 10),
                    Block.box(7, 5, 10, 8, 6, 11),
                    Block.box(10, 3, 5, 11, 4, 6),
                    Block.box(10, 5, 6, 11, 6, 7),
                    Block.box(10, 0, 3, 11, 1, 4),
                    Block.box(10, 1, 3, 11, 2, 4),
                    Block.box(11, 2, 9, 12, 3, 10)
            )
            .reduce((v1, v2) -> Shapes.join(v1, v2, BooleanOp.OR));

    @Override
    public void appendHoverText(ItemStack p_49816_, @Nullable BlockGetter p_49817_, List<Component> p_49818_, TooltipFlag p_49819_) {
        if(RunicAgesConfig.modernrs.get()) {
            p_49818_.add(Component.literal("This rock contains tin."));
        }
        else p_49818_.add(Component.literal("A rocky outcrop."));
        super.appendHoverText(p_49816_, p_49817_, p_49818_, p_49819_);
    }

    protected boolean mayPlaceOn(BlockState p_51042_, BlockGetter p_51043_, BlockPos p_51044_) {
        return p_51042_.is(Blocks.SAND) || p_51042_.is(Blocks.RED_SAND) ||
                p_51042_.is(Blocks.DIRT) || p_51042_.is(Blocks.GRASS_BLOCK) ||
                p_51042_.is(Blocks.STONE) || p_51042_.is(Blocks.TERRACOTTA);
    }

    @Override
    public boolean canSurvive(BlockState p_51028_, LevelReader p_51029_, BlockPos p_51030_) {
        BlockPos blockpos = p_51030_.below();
        return this.mayPlaceOn(p_51029_.getBlockState(blockpos), p_51029_, blockpos);
    }

    public static final TagKey<Item> GEM_DROPS = ItemTags.create(new ResourceLocation(RunicAgesMain.MODID, "gem_drops"));
    public int gemchance;

    @Override
    public InteractionResult use(BlockState state, Level world, BlockPos pos, Player player, InteractionHand p_60507_, BlockHitResult p_60508_) {
        ItemStack pickaxestack = player.getItemBySlot(EquipmentSlot.MAINHAND);
        if (player.isCrouching()) {
            player.sendSystemMessage(Component.literal("This rock contains tin."));
        }
        player.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            player.getCapability(RunicAgesMiningCapability.Provider.MINING_LEVEL).ifPresent(h ->
            {
                if (!world.isClientSide && h.getMiningLevel() >= 1) {
                    if (player.getInventory().getFreeSlot() == -1) {
                        player.sendSystemMessage(Component.literal("Your inventory is too full to mine any more ore."));
                    } else if (h.getMiningLevel() > 1 && pickaxestack.getItem() == ItemInit.BRONZE_PICKAXE.get() ||
                            h.getMiningLevel() > 10 && pickaxestack.getItem() == Items.IRON_PICKAXE) {
                        //Gem drop stuff
                        Random rand = new Random();
                        ItemStack ring_of_luck =
                                CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_LUCK.get(), player).map(
                                        ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                        if(!ring_of_luck.isEmpty()) {
                            gemchance = rand.nextInt(86);
                        }
                        else gemchance = rand.nextInt(128);
                        ItemStack GEM =
                                new ItemStack(ForgeRegistries.ITEMS.tags().getTag(GEM_DROPS).getRandomElement(player.level().getRandom()).get());
                        ItemEntity gemdrop = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(),
                                GEM);
                        if (gemchance == 0) {
                            world.addFreshEntity(gemdrop);
                        }
                        //Gem drop stuff

                        //Rolling for drops
                        int dropchance = Math.round(rand.nextInt(101));
                        int bonus = (int) Math.round(h.getMiningLevel() * 0.7852);

                        int roll = rand.nextInt(101);
                        if (h.getMiningLevel() <= 3 && dropchance > roll) {
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.TIN_ORE.get().getDefaultInstance());
                            if(RunicAgesConfig.modernrs.get())
                            {
                                for (ItemStack cost : player.getInventory().items)
                                    if (cost.getItem() == ItemInit.TIN_STONE_SPIRIT.get() && cost.getCount() >= 1)
                                    {
                                        ItemHandlerHelper.giveItemToPlayer(player, ItemInit.TIN_ORE.get().getDefaultInstance());
                                        cost.shrink(1);
                                        break;
                                    }
                            }
                            h.addMiningXP(player, 17);
                            ed.addxptotalxp(17);
                            RunicAgesMiningCapability.levelClientUpdate(player);
                            player.displayClientMessage(Component.literal("You have gained XP: " + h.getMiningXP()), false);
                            int deplete = rand.nextInt(4);
                            if (deplete > 1 && !RunicAgesConfig.modernrs.get()) {
                                world.setBlock(pos, SpecialBlockInit.DEPLETED_TIN_ROCK.get().defaultBlockState().setValue(FACING, state.getValue(FACING)), 2);
                                player.sendSystemMessage(Component.literal("The rock you were mining has depleted."));
                            }
                        }
                        if (h.getMiningLevel() >= 4 && dropchance > roll + bonus) {
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.TIN_ORE.get().getDefaultInstance());
                            if(RunicAgesConfig.modernrs.get())
                            {
                                for (ItemStack cost : player.getInventory().items)
                                    if (cost.getItem() == ItemInit.TIN_STONE_SPIRIT.get() && cost.getCount() >= 1)
                                    {
                                        ItemHandlerHelper.giveItemToPlayer(player, ItemInit.TIN_ORE.get().getDefaultInstance());
                                        cost.shrink(1);
                                        break;
                                    }
                            }
                            h.addMiningXP(player, 17);
                            ed.addxptotalxp(17);
                            RunicAgesMiningCapability.levelClientUpdate(player);
                            player.displayClientMessage(Component.literal("You have gained XP: " + h.getMiningXP()), false);
                            int deplete = rand.nextInt(4);
                            if (deplete > 1 && !RunicAgesConfig.modernrs.get()) {
                                world.setBlock(pos, SpecialBlockInit.DEPLETED_TIN_ROCK.get().defaultBlockState().setValue(FACING, state.getValue(FACING)), 2);
                                player.sendSystemMessage(Component.literal("The rock you were mining has depleted."));
                            }
                        }
                        //Rolling for drops
                    }
                }
            });
        });
        return super.use(state, world, pos, player, p_60507_, p_60508_);
    }
}

