package com.ladestitute.runicages.blocks.ore.cave;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.mining.RunicAgesMiningCapability;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.FluidState;
import net.minecraftforge.registries.ForgeRegistries;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;

import javax.annotation.Nullable;
import java.util.*;

public class MithrilOreBlock extends Block {

    public MithrilOreBlock(Properties properties) {
        super(properties);
    }

    @Override
    public void appendHoverText(ItemStack p_49816_, @Nullable BlockGetter p_49817_, List<Component> p_49818_, TooltipFlag p_49819_) {
        if(RunicAgesConfig.modernrs.get()) {
            p_49818_.add(Component.literal("This ore contains mithril."));
        }
        else p_49818_.add(Component.literal("A rocky outcrop."));
        super.appendHoverText(p_49816_, p_49817_, p_49818_, p_49819_);
    }

    public static final TagKey<Item> GEM_DROPS = ItemTags.create(new ResourceLocation(RunicAgesMain.MODID, "gem_drops"));
    public int gemchance;

    @Override
    public boolean onDestroyedByPlayer(BlockState state, Level level, BlockPos pos, Player player, boolean willHarvest, FluidState fluid) {
        ItemStack pickaxestack = player.getItemBySlot(EquipmentSlot.MAINHAND);
        player.getCapability(RunicAgesMiningCapability.Provider.MINING_LEVEL).ifPresent(h ->
        {
            if(h.getMiningLevel() >= 30) {
                if (pickaxestack.getItem() == ItemInit.BRONZE_PICKAXE.get()||
                        pickaxestack.getItem() == Items.IRON_PICKAXE) {
                    //Gem drop stuff
                    Random rand = new Random();
                    ItemStack ring_of_luck =
                            CuriosApi.getCuriosHelper().findEquippedCurio(ItemInit.RING_OF_LUCK.get(), player).map(
                                    ImmutableTriple::getRight).orElse(ItemStack.EMPTY);
                    if(!ring_of_luck.isEmpty()) {
                        gemchance = rand.nextInt(86);
                    }
                    else gemchance = rand.nextInt(128);
                    ItemStack GEM =
                            new ItemStack(ForgeRegistries.ITEMS.tags().getTag(GEM_DROPS).getRandomElement(player.level().getRandom()).get());
                    ItemEntity gemdrop = new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(),
                            GEM);
                    if (gemchance == 0) {
                        level.addFreshEntity(gemdrop);
                    }
                    //Gem drop stuff

                    int chance = rand.nextInt(4) + 2;
                    ItemStack orestack = new ItemStack(ItemInit.MITHRIL_ORE.get());
                    ItemEntity ore1 = new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(),
                            orestack);
                    ItemEntity ore2 = new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(),
                            orestack);
                    ItemEntity ore3 = new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(),
                            orestack);
                    ItemEntity ore4 = new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(),
                            orestack);
                    ItemEntity ore5 = new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(),
                            orestack);
                    if (chance == 2) {
                        level.addFreshEntity(ore1);
                        level.addFreshEntity(ore2);
                    }
                    if (chance == 3) {
                        level.addFreshEntity(ore1);
                        level.addFreshEntity(ore2);
                        level.addFreshEntity(ore3);
                    }
                    if (chance == 4) {
                        level.addFreshEntity(ore1);
                        level.addFreshEntity(ore2);
                        level.addFreshEntity(ore3);
                        level.addFreshEntity(ore4);
                    }
                    if (chance == 5) {
                        level.addFreshEntity(ore1);
                        level.addFreshEntity(ore2);
                        level.addFreshEntity(ore3);
                        level.addFreshEntity(ore4);
                        level.addFreshEntity(ore5);
                    }
                    h.addMiningXP(player, 80);
                    RunicAgesMiningCapability.levelClientUpdate(player);
                }
            }
        });
        return super.onDestroyedByPlayer(state, level, pos, player, willHarvest, fluid);
    }
}


