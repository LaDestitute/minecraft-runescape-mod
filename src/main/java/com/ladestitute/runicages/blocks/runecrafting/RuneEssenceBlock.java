package com.ladestitute.runicages.blocks.runecrafting;

import com.ladestitute.runicages.capability.mining.RunicAgesMiningCapability;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.registry.SpecialBlockInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.Random;

public class RuneEssenceBlock extends Block {

    public RuneEssenceBlock(Properties properties) {
        super(properties);
    }

    @Override
    public InteractionResult use(BlockState p_60503_, Level level, BlockPos pos, Player player, InteractionHand p_60507_, BlockHitResult p_60508_) {
        ItemStack pickaxestack = player.getItemBySlot(EquipmentSlot.MAINHAND);
        player.getCapability(RunicAgesMiningCapability.Provider.MINING_LEVEL).ifPresent(h ->
        {
            if (!level.isClientSide && h.getMiningLevel() >= 1) {
                if(player.getInventory().getFreeSlot() == -1)
                {
                    player.sendSystemMessage(Component.literal("Your inventory is too full to mine any more ore."));
                }
                else if (pickaxestack.getItem() == ItemInit.BRONZE_PICKAXE.get() ||
                        pickaxestack.getItem() == Items.IRON_PICKAXE) {
                    //Gem drop stuff
                    Random rand = new Random();

                    //Rolling for drops
                    int dropchance = Math.round(rand.nextInt(101));
                    int bonus = (int) Math.round(h.getMiningLevel() * 0.7852);

                    int roll = rand.nextInt(101);
                    if (h.getMiningLevel() <= 3 && dropchance > roll) {
                        if(h.getMiningLevel() < 30 && RunicAgesConfig.modernrs.get()) {
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.RUNE_ESSENCE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.RUNE_ESSENCE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.RUNE_ESSENCE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.RUNE_ESSENCE.get().getDefaultInstance());
                        }
                        if(h.getMiningLevel() < 30 && !RunicAgesConfig.modernrs.get()) {
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.RUNE_ESSENCE.get().getDefaultInstance());
                        }
                        if(h.getMiningLevel() >= 30 && RunicAgesConfig.modernrs.get()) {
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.PURE_ESSENCE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.PURE_ESSENCE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.PURE_ESSENCE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.PURE_ESSENCE.get().getDefaultInstance());
                        }
                        if(h.getMiningLevel() >= 30 && !RunicAgesConfig.modernrs.get()) {
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.PURE_ESSENCE.get().getDefaultInstance());
                        }
                        h.addMiningXP(player, 5);
                        RunicAgesMiningCapability.levelClientUpdate(player);
                        player.displayClientMessage(Component.literal("You have gained XP: " + h.getMiningXP()), false);

                    }
                    if (h.getMiningLevel() >= 4 && dropchance > roll + bonus) {
                        if(h.getMiningLevel() < 30 && RunicAgesConfig.modernrs.get()) {
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.RUNE_ESSENCE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.RUNE_ESSENCE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.RUNE_ESSENCE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.RUNE_ESSENCE.get().getDefaultInstance());
                        }
                        if(h.getMiningLevel() < 30 && !RunicAgesConfig.modernrs.get()) {
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.RUNE_ESSENCE.get().getDefaultInstance());
                        }
                        if(h.getMiningLevel() >= 30 && RunicAgesConfig.modernrs.get()) {
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.PURE_ESSENCE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.PURE_ESSENCE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.PURE_ESSENCE.get().getDefaultInstance());
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.PURE_ESSENCE.get().getDefaultInstance());
                        }
                        if(h.getMiningLevel() >= 30 && !RunicAgesConfig.modernrs.get()) {
                            ItemHandlerHelper.giveItemToPlayer(player, ItemInit.PURE_ESSENCE.get().getDefaultInstance());
                        }
                        h.addMiningXP(player, 5);
                        RunicAgesMiningCapability.levelClientUpdate(player);
                        player.displayClientMessage(Component.literal("You have gained XP: " + h.getMiningXP()), false);

                    }
                    //Rolling for drops
                }
            }
        });
        return super.use(p_60503_, level, pos, player, p_60507_, p_60508_);
    }


}

