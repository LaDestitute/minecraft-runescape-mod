package com.ladestitute.runicages.blocks.runecrafting;

import com.ladestitute.runicages.capability.mining.RunicAgesMiningCapability;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.FluidState;

public class RuneEssenceOreBlock extends Block {

    public RuneEssenceOreBlock(Properties properties) {
        super(properties);
    }

    @Override
    public boolean onDestroyedByPlayer(BlockState state, Level level, BlockPos pos, Player player, boolean willHarvest, FluidState fluid) {
        ItemStack pickaxestack = player.getItemBySlot(EquipmentSlot.MAINHAND);
        if(pickaxestack.getItem() == ItemInit.BRONZE_PICKAXE.get()||
                pickaxestack.getItem() == Items.IRON_PICKAXE) {
            player.getCapability(RunicAgesMiningCapability.Provider.MINING_LEVEL).ifPresent(h ->
            {
            if(h.getMiningLevel() < 30 && RunicAgesConfig.modernrs.get()) {
                ItemStack orestack = new ItemStack(ItemInit.RUNE_ESSENCE.get());
                ItemEntity ore1 = new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(),
                        orestack);
                ItemEntity ore2 = new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(),
                        orestack);
                ItemEntity ore3 = new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(),
                        orestack);
                ItemEntity ore4 = new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(),
                        orestack);
                level.addFreshEntity(ore1);
                level.addFreshEntity(ore2);
                level.addFreshEntity(ore3);
                level.addFreshEntity(ore4);
            }
                if(h.getMiningLevel() < 30 && !RunicAgesConfig.modernrs.get()) {
                    ItemStack orestack = new ItemStack(ItemInit.RUNE_ESSENCE.get());
                    ItemEntity ore1 = new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(),
                            orestack);
                    level.addFreshEntity(ore1);
                }
                if(h.getMiningLevel() >= 30 && RunicAgesConfig.modernrs.get()) {
                    ItemStack orestack = new ItemStack(ItemInit.PURE_ESSENCE.get());
                    ItemEntity ore1 = new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(),
                            orestack);
                    ItemEntity ore2 = new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(),
                            orestack);
                    ItemEntity ore3 = new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(),
                            orestack);
                    ItemEntity ore4 = new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(),
                            orestack);
                    level.addFreshEntity(ore1);
                    level.addFreshEntity(ore2);
                    level.addFreshEntity(ore3);
                    level.addFreshEntity(ore4);
                }
                if(h.getMiningLevel() >= 30 && !RunicAgesConfig.modernrs.get()) {
                    ItemStack orestack = new ItemStack(ItemInit.PURE_ESSENCE.get());
                    ItemEntity ore1 = new ItemEntity(level, pos.getX(), pos.getY(), pos.getZ(),
                            orestack);
                    level.addFreshEntity(ore1);
                }
                h.addMiningXP(player, 5);
                RunicAgesMiningCapability.levelClientUpdate(player);
            });
        }
        return super.onDestroyedByPlayer(state, level, pos, player, willHarvest, fluid);
    }
}


