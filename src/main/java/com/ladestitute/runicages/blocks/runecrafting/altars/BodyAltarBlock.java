package com.ladestitute.runicages.blocks.runecrafting.altars;

import com.ladestitute.runicages.capability.runecrafting.RunicAgesRunecraftingCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.registry.ItemInit;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.items.ItemHandlerHelper;

public class BodyAltarBlock extends Block {

    public BodyAltarBlock(Properties properties) {
        super(properties);
    }

    @Override
    public InteractionResult use(BlockState p_60503_, Level p_60504_, BlockPos p_60505_, Player player, InteractionHand p_60507_, BlockHitResult p_60508_) {
        ItemStack talisman = ItemInit.BODY_TALISMAN.get().getDefaultInstance();
        ItemStack essence = ItemInit.RUNE_ESSENCE.get().getDefaultInstance();
        ItemStack pure = ItemInit.PURE_ESSENCE.get().getDefaultInstance();
        ItemStack rune = ItemInit.BODY_RUNE.get().getDefaultInstance();
        ItemStack tiara = ItemInit.TIARA.get().getDefaultInstance();
        ItemStack handstack = player.getItemBySlot(EquipmentSlot.MAINHAND);
        ItemStack headstack = player.getItemBySlot(EquipmentSlot.HEAD);
        int levelrequirement = 20;
        int xp = 7;

        player.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            player.getCapability(RunicAgesRunecraftingCapability.Provider.RUNECRAFTING_LEVEL).ifPresent(h ->
            {
                if (!player.level().isClientSide() && handstack.getItem() == ItemInit.TIARA.get() && player.getInventory().contains(talisman)
                        && player.getInventory().contains(tiara)) {
                    talisman.shrink(1);
                    tiara.shrink(1);
                    ItemHandlerHelper.giveItemToPlayer(player, ItemInit.BODY_TIARA.get().getDefaultInstance());
                    h.addRunecraftingXP(player, 37);
                    ed.addxptotalxp(37);
                }
                if (!player.level().isClientSide() && h.getRunecraftingLevel() >= levelrequirement && handstack != talisman) {
                    if (!player.getInventory().contains(talisman) && !player.getInventory().contains(essence)) {
                        player.displayClientMessage(Component.literal("You do not have an air talisman or any rune essence to bind."), false);
                    }
                    if (player.getInventory().contains(talisman) && !player.getInventory().contains(essence)) {
                        player.displayClientMessage(Component.literal("You do not have any rune essence to bind."), false);
                    }
                    if (!player.getInventory().contains(talisman) && player.getInventory().contains(essence)) {
                        player.displayClientMessage(Component.literal("You do not have an air talisman to bind with."), false);
                    }
                    if (player.getInventory().contains(talisman) && player.getInventory().contains(pure) ||
                            headstack.getItem() == ItemInit.BODY_TIARA.get() && player.getInventory().contains(pure)) {
                        for (ItemStack essenceconvert : player.getInventory().items) {
                            if (essenceconvert.getItem() == ItemInit.PURE_ESSENCE.get()) {
                                if (h.getRunecraftingLevel() < 46 && h.getRunecraftingLevel() >= 1) {
                                    rune.setCount(essenceconvert.getCount());
                                }
                                if (h.getRunecraftingLevel() >= 46 && h.getRunecraftingLevel() <= 91) {
                                    rune.setCount(essenceconvert.getCount() * 2);
                                }
                                ItemHandlerHelper.giveItemToPlayer(player, rune);
                                h.addRunecraftingXP(player, xp * essenceconvert.getCount());
                                ed.addxptotalxp(xp * essenceconvert.getCount());
                                essenceconvert.setCount(0);
                            }
                        }
                    } else if (player.getInventory().contains(talisman) && player.getInventory().contains(essence) ||
                            headstack.getItem() == ItemInit.BODY_TIARA.get() && player.getInventory().contains(essence)) {
                        for (ItemStack essenceconvert : player.getInventory().items) {
                            if (essenceconvert.getItem() == ItemInit.RUNE_ESSENCE.get()) {
                                if (h.getRunecraftingLevel() < 14 && h.getRunecraftingLevel() >= 1) {
                                    rune.setCount(essenceconvert.getCount());
                                }
                                if (h.getRunecraftingLevel() >= 14 && h.getRunecraftingLevel() <= 27) {
                                    rune.setCount(essenceconvert.getCount() * 2);
                                }
                                ItemHandlerHelper.giveItemToPlayer(player, rune);
                                h.addRunecraftingXP(player, xp * essenceconvert.getCount());
                                ed.addxptotalxp(xp * essenceconvert.getCount());
                                essenceconvert.setCount(0);
                            }
                        }
                    }
                }
            });
        });
        return super.use(p_60503_, p_60504_, p_60505_, player, p_60507_, p_60508_);
    }
}

