package com.ladestitute.runicages.util.bank;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.RandomSource;
import net.minecraft.world.item.ItemStack;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;

public class BankUtil {
    //Code based on Flanks255's backpacks
    public static boolean curiosLoaded = false;

    @SuppressWarnings("ConstantConditions")
    @Nonnull
    public static Optional<UUID> getUUID(@Nonnull ItemStack stack) {
        if (stack.hasTag() && stack.getTag().contains("UUID"))
            return Optional.of(stack.getTag().getUUID("UUID"));
        else
            return Optional.empty();
    }

    @SuppressWarnings("ConstantConditions")
    public static Optional<CompoundTag> getTag(ItemStack stack) {
        if (stack.hasTag())
            return Optional.of(stack.getTag());
        return Optional.empty();
    }

    public record Confirmation(String code, UUID player, UUID backpack){}

    public static String generateCode(RandomSource random) {
        return "%08x".formatted(random.nextInt(Integer.MAX_VALUE));
    }

    private static final HashMap<String, Confirmation> confirmationMap = new HashMap<>();

    public static void addConfirmation(String code, UUID player, UUID backpack) {
        confirmationMap.put(code, new Confirmation(code, player, backpack));
    }

    public static void removeConfirmation(String code) {
        confirmationMap.remove(code);
    }

    public static Optional<Confirmation> getConfirmation(String code) {
        return Optional.ofNullable(confirmationMap.get(code));
    }
}
