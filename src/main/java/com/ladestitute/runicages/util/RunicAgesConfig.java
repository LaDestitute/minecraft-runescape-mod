package com.ladestitute.runicages.util;

import net.minecraftforge.common.ForgeConfigSpec;

public class RunicAgesConfig {
    public static final ForgeConfigSpec.Builder BUILDER = new ForgeConfigSpec.Builder();
    public static final ForgeConfigSpec SPEC;

    public static final ForgeConfigSpec.ConfigValue<Boolean> modernrs;
    public static final ForgeConfigSpec.ConfigValue<Boolean> qualityoflifechanges;
    public static final ForgeConfigSpec.ConfigValue<Boolean> restrictrecipes;
    public static final ForgeConfigSpec.ConfigValue<Boolean> restrictmining;
    public static final ForgeConfigSpec.ConfigValue<Boolean> restrictchopping;
    public static final ForgeConfigSpec.ConfigValue<Boolean> restricthealing;
    public static final ForgeConfigSpec.ConfigValue<Boolean> receiveattackxp;
    public static final ForgeConfigSpec.ConfigValue<Boolean> receivestrengthxp;
    public static final ForgeConfigSpec.ConfigValue<Boolean> receivemeleedefensexp;
    public static final ForgeConfigSpec.ConfigValue<Boolean> receiverangedxp;
    public static final ForgeConfigSpec.ConfigValue<Boolean> receiverangeddefensexp;
    public static final ForgeConfigSpec.ConfigValue<Boolean> receivemagicxp;
    public static final ForgeConfigSpec.ConfigValue<Boolean> receivemagicdefensexp;

    static {
        BUILDER.push("Config");

        modernrs = BUILDER.comment("Whether the systems and tier-progression will behave like RS3, false will make it behave like OSRS instead")
                .define("modernrs", true);
        qualityoflifechanges = BUILDER.comment("Whether to enable minor to medium quality of life changes; for now, this is just toolbelt items")
                .define("qualityoflifechanges", true);
        restrictrecipes = BUILDER.comment("Whether to restrict vanilla recipes in Survival, most being unlocked or replaced with mod-versions")
                .define("restrictrecipes", true);
        restrictmining = BUILDER.comment("Whether to restrict mining ore blocks in Survival based on the player's mining level")
                .define("restrictmining", true);
        restrictchopping = BUILDER.comment("Whether to restrict mining tree-log blocks in Survival based on the player's woodcutting level")
                .define("restrictchopping", true);
        restricthealing = BUILDER.comment("Whether to restrict health regeneration in Survival, requiring the player to eat food or to stand near Banker villagers to heal")
                .define("restricthealing", true);
        receiveattackxp = BUILDER.comment("Whether the player wants to receive Attack XP when using melee weapons")
                .define("receiveattackxp", true);
        receivestrengthxp = BUILDER.comment("Whether the player wants to receive Strength XP when using melee weapons")
                .define("receivestrengthxp", true);
        receivemeleedefensexp = BUILDER.comment("Whether the player wants to receive Defense XP when using melee weapons")
                .define("receivemeleedefensexp", true);
        receiverangedxp = BUILDER.comment("Whether the player wants to receive Ranged XP when using ranged weapons")
                .define("receiverangedxp", true);
        receiverangeddefensexp = BUILDER.comment("Whether the player wants to receive Defense XP when using ranged weapons")
                .define("receiverangeddefensexp", true);
        receivemagicxp = BUILDER.comment("Whether the player wants to receive Magic XP when using magic weapons")
                .define("receivemagicxp", true);
        receivemagicdefensexp = BUILDER.comment("Whether the player wants to receive Defense XP when using magic weapons")
                .define("receivemagicdefensexp", true);

        BUILDER.pop();
        SPEC = BUILDER.build();
    }
}