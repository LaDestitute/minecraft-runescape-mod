package com.ladestitute.runicages.util.datagen.glm;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.registry.ItemInit;
import com.ladestitute.runicages.util.RunicAgesConfig;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.ai.gossip.GossipType;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.npc.Villager;
import net.minecraft.world.entity.npc.VillagerProfession;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraftforge.common.loot.IGlobalLootModifier;
import net.minecraftforge.common.loot.LootModifier;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nonnull;
import java.util.Random;

public class VillagerLootModifier extends LootModifier {
    public static final Codec<VillagerLootModifier> CODEC = RecordCodecBuilder.create(inst -> LootModifier.codecStart(inst).apply(inst, VillagerLootModifier::new));

    public static final TagKey<Item> rs3_villager_drops = TagKey.create(Registries.ITEM,
            new ResourceLocation(RunicAgesMain.MODID, "rs3_villager_drops"));
    public static final TagKey<Item> osrs_villager_drops = TagKey.create(Registries.ITEM,
            new ResourceLocation(RunicAgesMain.MODID, "osrs_villager_drops"));

    protected VillagerLootModifier(LootItemCondition[] conditionsIn) {
        super(conditionsIn);
    }

    @Nonnull
    @Override
    protected ObjectArrayList<ItemStack> doApply(ObjectArrayList<ItemStack> generatedLoot, LootContext context) {
        generatedLoot.clear();
        Villager villager = (Villager) context.getParam(LootContextParams.THIS_ENTITY);
        villager.getGossips().remove(GossipType.MAJOR_NEGATIVE);
        villager.getGossips().remove(GossipType.MINOR_NEGATIVE);
        if(villager.getVillagerData().getProfession().equals(VillagerProfession.FARMER))
        {

        }
        else {
            Random rand = new Random();
            int airstaffroll = rand.nextInt(116);
            ItemStack airstaff = new ItemStack(ItemInit.STAFF_OF_AIR.get());

            ItemStack rs3droptable = new ItemStack(ForgeRegistries.ITEMS.tags().getTag(rs3_villager_drops).getRandomElement(villager.getRandom()).get());
            ItemStack osrsdroptable = new ItemStack(ForgeRegistries.ITEMS.tags().getTag(osrs_villager_drops).getRandomElement(villager.getRandom()).get());

            ItemStack arrowstack = new ItemStack(ItemInit.BRONZE_ARROW.get());

            ItemStack mindrunestack = new ItemStack(ItemInit.MIND_RUNE.get());

            ItemStack earthrunestack = new ItemStack(ItemInit.EARTH_RUNE.get());

            ItemStack firerunestack = new ItemStack(ItemInit.FIRE_RUNE.get());

            ItemStack chaosrunestack = new ItemStack(ItemInit.CHAOS_RUNE.get());

            int droproll = rand.nextInt(16);
            int decidedrop = rand.nextInt(158);
            if(RunicAgesConfig.modernrs.get() && airstaffroll == 0)
            {
                generatedLoot.add(airstaff);
            }
            else
            if(droproll >= 1)
            {
                if(decidedrop <= 1)
                {
                    arrowstack.setCount(7);
                    generatedLoot.add(arrowstack);
                }
                if(decidedrop >= 2 && decidedrop < 7)
                {
                    mindrunestack.setCount(9);
                    generatedLoot.add(mindrunestack);
                }
                if(decidedrop >= 7 && decidedrop < 9)
                {
                    earthrunestack.setCount(4);
                    generatedLoot.add(earthrunestack);
                }
                if(decidedrop >= 9 && decidedrop < 11)
                {
                    firerunestack.setCount(6);
                    generatedLoot.add(firerunestack);
                }
                if(decidedrop == 11)
                {
                    chaosrunestack.setCount(2);
                    generatedLoot.add(chaosrunestack);
                }
                if(decidedrop >= 12 && decidedrop <= 44)
                {
                    arrowstack.setCount(rand.nextInt(11)+2);
                    generatedLoot.add(arrowstack);
                }
                if(decidedrop >= 44) {
                    if (RunicAgesConfig.modernrs.get()) {
                        generatedLoot.add(rs3droptable);
                    } else
                        generatedLoot.add(osrsdroptable);
                }
            }

            int dropchance = rand.nextInt(85);
            ItemStack item1 = new ItemStack(ItemInit.EYE_OF_NEWT.get());
            if(dropchance == 0)
            {
                generatedLoot.add(item1);
            }
        }
        return generatedLoot;
    }

    @Override
    public Codec<? extends IGlobalLootModifier> codec() {
        return VillagerLootModifier.CODEC;
    }
}
