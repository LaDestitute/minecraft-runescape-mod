package com.ladestitute.runicages.util;

import com.ladestitute.runicages.RunicAgesMain;
import com.mojang.blaze3d.platform.InputConstants;
import net.minecraft.client.KeyMapping;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.RegisterKeyMappingsEvent;
import net.minecraftforge.client.settings.KeyConflictContext;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.lwjgl.glfw.GLFW;

@SuppressWarnings("NoTranslation")
@Mod.EventBusSubscriber(modid = RunicAgesMain.MODID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class RunicAgesKeyboardUtil {

    public static final String KEY_CATEGORY_RUNICAGES = "key.category.runicages";
    public static final String KEY_OPEN_MAGIC_BOOK = "key.runicages.open_magic_book";
    public static final String KEY_SPELL_1 = "key.runicages.spell_one";

    public static final KeyMapping OPENING_MAGIC_BOOK = new KeyMapping(KEY_OPEN_MAGIC_BOOK, KeyConflictContext.IN_GAME,
            InputConstants.Type.KEYSYM, GLFW.GLFW_KEY_B, KEY_CATEGORY_RUNICAGES);

    public static KeyMapping closing_magic_book =
            new KeyMapping("closing_magic_book", GLFW.GLFW_KEY_ESCAPE, "key.category.runicages");

    public static KeyMapping spell_1 =
            new KeyMapping("spell_1", GLFW.GLFW_KEY_KP_1, "key.category.runicages");

    public static KeyMapping spell_2 =
            new KeyMapping("spell_2", GLFW.GLFW_KEY_KP_2, "key.category.runicages");

    public static KeyMapping spell_3 =
            new KeyMapping("spell_3", GLFW.GLFW_KEY_KP_3, "key.category.runicages");

    public static KeyMapping spell_4 =
            new KeyMapping("spell_4", GLFW.GLFW_KEY_KP_4, "key.category.runicages");

    public static KeyMapping spell_5 =
            new KeyMapping("spell_5", GLFW.GLFW_KEY_KP_5, "key.category.runicages");

    public static KeyMapping enter =
            new KeyMapping("enter", GLFW.GLFW_KEY_KP_ENTER, "key.category.runicages");

    public static KeyMapping spell_6 =
            new KeyMapping("spell_6", GLFW.GLFW_KEY_KP_6, "key.category.runicages");

    public static KeyMapping spell_7 =
            new KeyMapping("spell_7", GLFW.GLFW_KEY_KP_7, "key.category.runicages");

    public static KeyMapping spell_8 =
            new KeyMapping("spell_8", GLFW.GLFW_KEY_KP_8, "key.category.runicages");

    public static KeyMapping spell_9 =
            new KeyMapping("spell_9", GLFW.GLFW_KEY_KP_9, "key.category.runicages");

    public static KeyMapping open_summary =
            new KeyMapping("open_summary", GLFW.GLFW_KEY_H, "key.category.runicages");

    public static KeyMapping open_split_xp =
            new KeyMapping("open_split", GLFW.GLFW_KEY_Z, "key.category.runicages");


}
