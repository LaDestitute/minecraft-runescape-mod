package com.ladestitute.runicages.client.model;

import com.ladestitute.runicages.entities.mobs.DeadlyRedSpiderEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;

public class DeadlyRedSpiderModel extends EntityModel<DeadlyRedSpiderEntity> {
    public static final String BODY = "body";
    // This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
    public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("runicages", "deadly_red_spider"), BODY);
    private final ModelPart body0;
    private final ModelPart head;
    private final ModelPart body1;
    private final ModelPart leg0;
    private final ModelPart leg1;
    private final ModelPart leg2;
    private final ModelPart leg3;
    private final ModelPart leg4;
    private final ModelPart leg5;
    private final ModelPart leg6;
    private final ModelPart leg7;

    public DeadlyRedSpiderModel(ModelPart root) {
        this.body0 = root.getChild("body0");
        this.head = root.getChild("head");
        this.body1 = root.getChild("body1");
        this.leg0 = root.getChild("leg0");
        this.leg1 = root.getChild("leg1");
        this.leg2 = root.getChild("leg2");
        this.leg3 = root.getChild("leg3");
        this.leg4 = root.getChild("leg4");
        this.leg5 = root.getChild("leg5");
        this.leg6 = root.getChild("leg6");
        this.leg7 = root.getChild("leg7");
    }

    public static LayerDefinition createBodyLayer() {
        MeshDefinition meshdefinition = new MeshDefinition();
        PartDefinition partdefinition = meshdefinition.getRoot();
        int i = 15;
        partdefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(32, 4).addBox(-4.0F, -4.0F, -8.0F, 8.0F, 8.0F, 8.0F), PartPose.offset(0.0F, 15.0F, -3.0F));
        partdefinition.addOrReplaceChild("body0", CubeListBuilder.create().texOffs(0, 0).addBox(-3.0F, -3.0F, -3.0F, 6.0F, 6.0F, 6.0F), PartPose.offset(0.0F, 15.0F, 0.0F));
        partdefinition.addOrReplaceChild("body1", CubeListBuilder.create().texOffs(0, 12).addBox(-5.0F, -4.0F, -6.0F, 10.0F, 8.0F, 12.0F), PartPose.offset(0.0F, 15.0F, 9.0F));
        CubeListBuilder cubelistbuilder = CubeListBuilder.create().texOffs(18, 0).addBox(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F);
        CubeListBuilder cubelistbuilder1 = CubeListBuilder.create().texOffs(18, 0).mirror().addBox(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F);
        partdefinition.addOrReplaceChild("leg1", cubelistbuilder, PartPose.offset(-4.0F, 15.0F, 2.0F));
        partdefinition.addOrReplaceChild("leg0", cubelistbuilder1, PartPose.offset(4.0F, 15.0F, 2.0F));
        partdefinition.addOrReplaceChild("leg3", cubelistbuilder, PartPose.offset(-4.0F, 15.0F, 1.0F));
        partdefinition.addOrReplaceChild("leg2", cubelistbuilder1, PartPose.offset(4.0F, 15.0F, 1.0F));
        partdefinition.addOrReplaceChild("leg5", cubelistbuilder, PartPose.offset(-4.0F, 15.0F, 0.0F));
        partdefinition.addOrReplaceChild("leg4", cubelistbuilder1, PartPose.offset(4.0F, 15.0F, 0.0F));
        partdefinition.addOrReplaceChild("leg7", cubelistbuilder, PartPose.offset(-4.0F, 15.0F, -1.0F));
        partdefinition.addOrReplaceChild("leg6", cubelistbuilder1, PartPose.offset(4.0F, 15.0F, -1.0F));
        return LayerDefinition.create(meshdefinition, 64, 32);
    }

    @Override
    public void setupAnim(DeadlyRedSpiderEntity p_103509_, float p_103510_, float p_103511_, float p_103512_, float p_103513_, float p_103514_) {
        this.head.yRot = p_103513_ * ((float)Math.PI / 180F);
        this.head.xRot = p_103514_ * ((float)Math.PI / 180F);
        float f = ((float)Math.PI / 4F);
        this.leg1.zRot = (-(float)Math.PI / 4F);
        this.leg0.zRot = ((float)Math.PI / 4F);
        this.leg3.zRot = -0.58119464F;
        this.leg2.zRot = 0.58119464F;
        this.leg5.zRot = -0.58119464F;
        this.leg4.zRot = 0.58119464F;
        this.leg7.zRot = (-(float)Math.PI / 4F);
        this.leg6.zRot = ((float)Math.PI / 4F);
        float f1 = -0.0F;
        float f2 = ((float)Math.PI / 8F);
        this.leg1.yRot = ((float)Math.PI / 4F);
        this.leg0.yRot = (-(float)Math.PI / 4F);
        this.leg3.yRot = ((float)Math.PI / 8F);
        this.leg2.yRot = (-(float)Math.PI / 8F);
        this.leg5.yRot = (-(float)Math.PI / 8F);
        this.leg4.yRot = ((float)Math.PI / 8F);
        this.leg7.yRot = (-(float)Math.PI / 4F);
        this.leg6.yRot = ((float)Math.PI / 4F);
        float f3 = -(Mth.cos(p_103510_ * 0.6662F * 2.0F + 0.0F) * 0.4F) * p_103511_;
        float f4 = -(Mth.cos(p_103510_ * 0.6662F * 2.0F + (float)Math.PI) * 0.4F) * p_103511_;
        float f5 = -(Mth.cos(p_103510_ * 0.6662F * 2.0F + ((float)Math.PI / 2F)) * 0.4F) * p_103511_;
        float f6 = -(Mth.cos(p_103510_ * 0.6662F * 2.0F + ((float)Math.PI * 1.5F)) * 0.4F) * p_103511_;
        float f7 = Math.abs(Mth.sin(p_103510_ * 0.6662F + 0.0F) * 0.4F) * p_103511_;
        float f8 = Math.abs(Mth.sin(p_103510_ * 0.6662F + (float)Math.PI) * 0.4F) * p_103511_;
        float f9 = Math.abs(Mth.sin(p_103510_ * 0.6662F + ((float)Math.PI / 2F)) * 0.4F) * p_103511_;
        float f10 = Math.abs(Mth.sin(p_103510_ * 0.6662F + ((float)Math.PI * 1.5F)) * 0.4F) * p_103511_;
        this.leg1.yRot += f3;
        this.leg0.yRot += -f3;
        this.leg3.yRot += f4;
        this.leg2.yRot += -f4;
        this.leg5.yRot += f5;
        this.leg4.yRot += -f5;
        this.leg7.yRot += f6;
        this.leg6.yRot += -f6;
        this.leg1.zRot += f7;
        this.leg0.zRot += -f7;
        this.leg3.zRot += f8;
        this.leg2.zRot += -f8;
        this.leg5.zRot += f9;
        this.leg4.zRot += -f9;
        this.leg7.zRot += f10;
        this.leg6.zRot += -f10;
    }

    @Override
    public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
        body0.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        head.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        body1.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        leg0.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        leg1.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        leg2.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        leg3.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        leg4.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        leg5.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        leg6.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        leg7.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
    }
}
