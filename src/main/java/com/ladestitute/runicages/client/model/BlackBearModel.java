package com.ladestitute.runicages.client.model;

import com.ladestitute.runicages.entities.mobs.BlackBearEntity;
import com.ladestitute.runicages.entities.mobs.GrizzlyBearEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;

public class BlackBearModel extends EntityModel<BlackBearEntity> {
    public static final String BODY = "body";
    // This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
    public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("runicages", "black_bear"), BODY);
    private final ModelPart head;
    private final ModelPart right_front_leg;
    private final ModelPart left_front_leg;
    private final ModelPart right_hind_leg;
    private final ModelPart left_hind_leg;
    private final ModelPart body;

    public BlackBearModel(ModelPart root) {
        this.head = root.getChild("head");
        this.right_front_leg = root.getChild("right_front_leg");
        this.left_front_leg = root.getChild("left_front_leg");
        this.right_hind_leg = root.getChild("right_hind_leg");
        this.left_hind_leg = root.getChild("left_hind_leg");
        this.body = root.getChild("body");
    }

    public static LayerDefinition createBodyLayer() {
        //Invisible model part issues, using the createBodyLayer from PolarBearModel fixes this
        MeshDefinition meshdefinition = new MeshDefinition();
        PartDefinition partdefinition = meshdefinition.getRoot();
        partdefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-3.5F, -3.0F, -3.0F, 7.0F, 7.0F, 7.0F).texOffs(0, 44).addBox("mouth", -2.5F, 1.0F, -6.0F, 5.0F, 3.0F, 3.0F).texOffs(26, 0).addBox("right_ear", -4.5F, -4.0F, -1.0F, 2.0F, 2.0F, 1.0F).texOffs(26, 0).mirror().addBox("left_ear", 2.5F, -4.0F, -1.0F, 2.0F, 2.0F, 1.0F), PartPose.offset(0.0F, 10.0F, -16.0F));
        partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(0, 19).addBox(-5.0F, -13.0F, -7.0F, 14.0F, 14.0F, 11.0F).texOffs(39, 0).addBox(-4.0F, -25.0F, -7.0F, 12.0F, 12.0F, 10.0F), PartPose.offsetAndRotation(-2.0F, 9.0F, 12.0F, ((float)Math.PI / 2F), 0.0F, 0.0F));
        int i = 10;
        CubeListBuilder cubelistbuilder = CubeListBuilder.create().texOffs(50, 22).addBox(-2.0F, 0.0F, -2.0F, 4.0F, 10.0F, 8.0F);
        partdefinition.addOrReplaceChild("right_hind_leg", cubelistbuilder, PartPose.offset(-4.5F, 14.0F, 6.0F));
        partdefinition.addOrReplaceChild("left_hind_leg", cubelistbuilder, PartPose.offset(4.5F, 14.0F, 6.0F));
        CubeListBuilder cubelistbuilder1 = CubeListBuilder.create().texOffs(50, 40).addBox(-2.0F, 0.0F, -2.0F, 4.0F, 10.0F, 6.0F);
        partdefinition.addOrReplaceChild("right_front_leg", cubelistbuilder1, PartPose.offset(-3.5F, 14.0F, -8.0F));
        partdefinition.addOrReplaceChild("left_front_leg", cubelistbuilder1, PartPose.offset(3.5F, 14.0F, -8.0F));
        return LayerDefinition.create(meshdefinition, 128, 64);
    }

    @Override
    public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
        head.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        right_front_leg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        left_front_leg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        right_hind_leg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        left_hind_leg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        body.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
    }

    @Override
    public void setupAnim(BlackBearEntity p_103509_, float p_103510_, float p_103511_, float p_103512_, float p_103513_, float p_103514_) {
        //Code taken from QuadrupedModel
        this.head.xRot = p_103514_ * ((float)Math.PI / 180F);
        this.head.yRot = p_103513_ * ((float)Math.PI / 180F);
        this.right_hind_leg.xRot = Mth.cos(p_103510_ * 0.6662F) * 1.4F * p_103511_;
        this.left_hind_leg.xRot = Mth.cos(p_103510_ * 0.6662F + (float)Math.PI) * 1.4F * p_103511_;
        this.right_front_leg.xRot = Mth.cos(p_103510_ * 0.6662F + (float)Math.PI) * 1.4F * p_103511_;
        this.left_front_leg.xRot = Mth.cos(p_103510_ * 0.6662F) * 1.4F * p_103511_;

        //Code taken from PolarBearModel
        float f = p_103512_ - (float)p_103509_.tickCount;
        float f1 = p_103509_.getStandingAnimationScale(f);
        f1 *= f1;
        float f2 = 1.0F - f1;
        this.body.xRot = ((float)Math.PI / 2F) - f1 * (float)Math.PI * 0.35F;
        this.body.y = 9.0F * f2 + 11.0F * f1;
        this.right_front_leg.y = 14.0F * f2 - 6.0F * f1;
        this.right_front_leg.z = -8.0F * f2 - 4.0F * f1;
        this.right_front_leg.xRot -= f1 * (float)Math.PI * 0.45F;
        this.left_front_leg.y = this.right_front_leg.y;
        this.left_front_leg.z = this.right_front_leg.z;
        this.left_front_leg.xRot -= f1 * (float)Math.PI * 0.45F;
        if (this.young) {
            this.head.y = 10.0F * f2 - 9.0F * f1;
            this.head.z = -16.0F * f2 - 7.0F * f1;
        } else {
            this.head.y = 10.0F * f2 - 14.0F * f1;
            this.head.z = -16.0F * f2 - 3.0F * f1;
        }

        this.head.xRot += f1 * (float)Math.PI * 0.15F;
    }

}
