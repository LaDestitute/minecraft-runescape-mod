package com.ladestitute.runicages.client.model;

import com.ladestitute.runicages.entities.mobs.ImpEntity;
import com.ladestitute.runicages.entities.mobs.UnicornEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;

public class UnicornModel extends EntityModel<UnicornEntity>  {
    public static final String BODY = "body";
    // This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
    public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("runicages", "unicorn"), BODY);
    private final ModelPart LegBL;
    private final ModelPart LegFL;
    private final ModelPart LegFR;
    private final ModelPart Neck;
    private final ModelPart LegBR;
    private final ModelPart Tail;
    private final ModelPart Body;
    private final ModelPart Horn;

    public UnicornModel(ModelPart root) {
        this.LegBL = root.getChild("LegBL");
        this.LegFL = root.getChild("LegFL");
        this.LegFR = root.getChild("LegFR");
        this.Neck = root.getChild("Neck");
        this.LegBR = root.getChild("LegBR");
        this.Tail = root.getChild("Tail");
        this.Body = root.getChild("Body");
        this.Horn = root.getChild("Horn");
    }

    public static LayerDefinition createBodyLayer() {
        MeshDefinition meshdefinition = new MeshDefinition();
        PartDefinition partdefinition = meshdefinition.getRoot();

        PartDefinition LegBL = partdefinition.addOrReplaceChild("LegBL", CubeListBuilder.create().texOffs(48, 21).mirror().addBox(-2.0F, 0.0F, -2.0F, 4.0F, 11.0F, 4.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(3.0F, 13.0F, 9.0F));

        PartDefinition LegFL = partdefinition.addOrReplaceChild("LegFL", CubeListBuilder.create().texOffs(48, 21).mirror().addBox(-2.0F, 0.0F, -2.0F, 4.0F, 11.0F, 4.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(3.0F, 13.0F, -9.0F));

        PartDefinition LegFR = partdefinition.addOrReplaceChild("LegFR", CubeListBuilder.create().texOffs(48, 21).addBox(-2.0F, 0.0F, -2.0F, 4.0F, 11.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(-3.0F, 13.0F, -9.0F));

        PartDefinition Neck = partdefinition.addOrReplaceChild("Neck", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 7.0F, -8.0F, 0.1745F, 0.0F, 0.0F));

        PartDefinition Neck_r1 = Neck.addOrReplaceChild("Neck_r1", CubeListBuilder.create().texOffs(0, 35).addBox(-2.0F, -7.0F, -5.5F, 4.0F, 12.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -5.0F, 0.5F, 0.5236F, 0.0F, 0.0F));

        PartDefinition Head = Neck.addOrReplaceChild("Head", CubeListBuilder.create(), PartPose.offset(0.0F, -11.0F, -3.0F));

        PartDefinition Head_r1 = Head.addOrReplaceChild("Head_r1", CubeListBuilder.create().texOffs(0, 13).addBox(-3.0F, -4.5F, -9.5F, 6.0F, 5.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -2.5F, 3.5F, 0.5236F, 0.0F, 0.0F));

        PartDefinition Horn_r1 = Head.addOrReplaceChild("Horn_r1", CubeListBuilder.create().texOffs(56, 0).addBox(-0.6401F, -7.0F, 0.0F, 2.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.1401F, -5.4469F, -6.6121F, 0.5236F, -0.422F, -0.2325F));

        PartDefinition Muzzle = Head.addOrReplaceChild("Muzzle", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

        PartDefinition Muzzle_r1 = Muzzle.addOrReplaceChild("Muzzle_r1", CubeListBuilder.create().texOffs(0, 25).addBox(-2.0F, -1.5F, -9.0F, 4.0F, 5.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -2.5F, -2.5F, 0.5236F, 0.0F, 0.0F));

        PartDefinition EarL = Head.addOrReplaceChild("EarL", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 11.0F, 3.0F, 0.0F, 0.0F, 0.0873F));

        PartDefinition EarL_r1 = EarL.addOrReplaceChild("EarL_r1", CubeListBuilder.create().texOffs(19, 16).mirror().addBox(-5.0F, -5.5F, -7.5F, 2.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offsetAndRotation(0.5F, -16.5F, 3.49F, 0.5219F, 0.0436F, -0.0756F));

        PartDefinition EarR = Head.addOrReplaceChild("EarR", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 11.0F, 3.0F, 0.0F, 0.0F, -0.0873F));

        PartDefinition EarR_r1 = EarR.addOrReplaceChild("EarR_r1", CubeListBuilder.create().texOffs(19, 16).addBox(3.0F, -5.5F, -7.5F, 2.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-0.5F, -16.5F, 3.49F, 0.5219F, -0.0436F, 0.0756F));

        PartDefinition Mane = Neck.addOrReplaceChild("Mane", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 0.0F));

        PartDefinition Mane_r1 = Mane.addOrReplaceChild("Mane_r1", CubeListBuilder.create().texOffs(56, 36).addBox(-1.0F, -8.5F, -4.55F, 2.0F, 11.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -8.5F, 5.05F, 0.5236F, 0.0F, 0.0F));

        PartDefinition LegBR = partdefinition.addOrReplaceChild("LegBR", CubeListBuilder.create().texOffs(48, 21).addBox(-2.0F, 0.0F, -2.0F, 4.0F, 11.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(-3.0F, 13.0F, 9.0F));

        PartDefinition Tail = partdefinition.addOrReplaceChild("Tail", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 4.0F, 11.0F, 0.4363F, 0.0F, 0.0F));

        PartDefinition Tail_r1 = Tail.addOrReplaceChild("Tail_r1", CubeListBuilder.create().texOffs(42, 36).addBox(-1.5F, -6.0F, -2.0F, 3.0F, 14.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 6.0F, 0.0F, 0.2182F, 0.0F, 0.0F));

        PartDefinition Body = partdefinition.addOrReplaceChild("Body", CubeListBuilder.create().texOffs(0, 32).addBox(-5.0F, -8.0F, -20.0F, 10.0F, 10.0F, 22.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 11.0F, 9.0F));

        PartDefinition Horn = partdefinition.addOrReplaceChild("Horn", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

        return LayerDefinition.create(meshdefinition, 64, 64);
    }

    @Override
    public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
        LegBL.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        LegFL.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        LegFR.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        Neck.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        LegBR.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        Tail.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        Body.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        Horn.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
    }

    @Override
    public void setupAnim(UnicornEntity p_103509_, float p_103510_, float p_103511_, float p_103512_, float p_103513_, float p_103514_) {
        //Code taken from QuadrupedModel
        this.Neck.getChild("Head").xRot = p_103514_ * ((float)Math.PI / 180F);
        this.Neck.getChild("Head").yRot = p_103513_ * ((float)Math.PI / 180F);
     //   this.Neck.xRot = p_103514_ * ((float)Math.PI / 180F);
     //   this.Neck.yRot = p_103513_ * ((float)Math.PI / 180F);
        this.LegBR.xRot = Mth.cos(p_103510_ * 0.6662F) * 1.4F * p_103511_;
        this.LegBL.xRot = Mth.cos(p_103510_ * 0.6662F + (float)Math.PI) * 1.4F * p_103511_;
        this.LegFR.xRot = Mth.cos(p_103510_ * 0.6662F + (float)Math.PI) * 1.4F * p_103511_;
        this.LegFL.xRot = Mth.cos(p_103510_ * 0.6662F) * 1.4F * p_103511_;
        this.Body.y = 11.0F;
    }
}
