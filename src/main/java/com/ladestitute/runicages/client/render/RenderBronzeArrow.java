package com.ladestitute.runicages.client.render;

import com.ladestitute.runicages.entities.projectiles.arrows.BronzeArrowEntity;
import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

public class RenderBronzeArrow extends ArrowRenderer<BronzeArrowEntity> {

    public static final ResourceLocation ARROW_TEXTURE = new ResourceLocation("runicages:textures/entity/arrows/bronze_arrow.png");

    public RenderBronzeArrow(EntityRendererProvider.Context context) {
        super(context);
    }

    @Override @Nonnull @ParametersAreNonnullByDefault
    public ResourceLocation getTextureLocation(BronzeArrowEntity entity) {
        return ARROW_TEXTURE;
    }
}
