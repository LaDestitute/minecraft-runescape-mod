package com.ladestitute.runicages.client.render;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.client.model.GrizzlyBearModel;
import com.ladestitute.runicages.client.model.ImpModel;
import com.ladestitute.runicages.client.model.UnicornModel;
import com.ladestitute.runicages.entities.mobs.GrizzlyBearEntity;
import com.ladestitute.runicages.entities.mobs.ImpEntity;
import com.ladestitute.runicages.entities.mobs.UnicornEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;

import javax.annotation.Nullable;

public class RenderUnicorn extends MobRenderer<UnicornEntity, UnicornModel> {

    private static final ResourceLocation TEXTURE = new ResourceLocation(RunicAgesMain.MODID, "textures/entity/unicorn.png");

    //In +1.18, we now pass a LAYER_LOCATION (see the explanation in the entityModel) and bake it in
    //using the renderer's EntityRendererProvider.Context in the entity's renderer and pass it through to the constructor
    public RenderUnicorn(EntityRendererProvider.Context context) {
        super(context, new UnicornModel(context.getModelSet().bakeLayer(UnicornModel.LAYER_LOCATION)), 0.60f);
    }

    @Nullable
    @Override
    public ResourceLocation getTextureLocation(UnicornEntity entity) {
        return TEXTURE;
    }

}
