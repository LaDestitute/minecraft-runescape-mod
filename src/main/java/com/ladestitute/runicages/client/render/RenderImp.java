package com.ladestitute.runicages.client.render;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.client.model.ImpModel;
import com.ladestitute.runicages.entities.mobs.ImpEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;

import javax.annotation.Nullable;

public class RenderImp extends MobRenderer<ImpEntity, ImpModel> {

    private static final ResourceLocation TEXTURE = new ResourceLocation(RunicAgesMain.MODID, "textures/entity/imp.png");

    //In +1.18, we now pass a LAYER_LOCATION (see the explanation in the entityModel) and bake it in
    //using the renderer's EntityRendererProvider.Context in the entity's renderer and pass it through to the constructor
    public RenderImp(EntityRendererProvider.Context context) {
        super(context, new ImpModel(context.getModelSet().bakeLayer(ImpModel.LAYER_LOCATION)), 0.20f);
    }

    @Nullable
    @Override
    public ResourceLocation getTextureLocation(ImpEntity entity) {
        return TEXTURE;
    }

    @Override
    protected void scale(ImpEntity p_115314_, PoseStack p_115315_, float p_115316_) {
        p_115315_.scale(0.5f, 0.5f, 0.5f);
        super.scale(p_115314_, p_115315_, p_115316_);
    }

}
