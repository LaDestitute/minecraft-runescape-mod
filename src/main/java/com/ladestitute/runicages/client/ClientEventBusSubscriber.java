package com.ladestitute.runicages.client;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.blocks.entities.RuneCraftWoodTypes;
import com.ladestitute.runicages.client.model.*;
import com.ladestitute.runicages.client.render.*;
import com.ladestitute.runicages.items.herblore.potions.AttackPotionItem;
import com.ladestitute.runicages.registry.*;
import com.ladestitute.runicages.util.RunicAgesKeyboardUtil;
import net.minecraft.client.renderer.ItemBlockRenderTypes;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.ThrownItemRenderer;
import net.minecraft.client.renderer.item.ItemProperties;
import net.minecraft.client.renderer.item.ItemPropertyFunction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.ShieldItem;
import net.minecraft.world.level.block.state.properties.WoodType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.client.event.RegisterKeyMappingsEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.registries.RegistryObject;

@Mod.EventBusSubscriber(modid = RunicAgesMain.MODID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ClientEventBusSubscriber {

    public static final ResourceLocation DOSES = new ResourceLocation(RunicAgesMain.MODID, "doses");

    @SubscribeEvent
    public static void onStaticClientSetup(FMLClientSetupEvent event) {
        event.setPhase(EventPriority.HIGH);
       // WoodType.register(RuneCraftWoodTypes.NORMAL_TREE);
        ItemBlockRenderTypes.setRenderLayer(BlockInit.SPINNING_WHEEL.get(), RenderType.translucent());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.SEWING_HOOP.get(), RenderType.translucent());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.ALLOTMENT.get(), RenderType.translucent());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.HOPS_PATCH.get(), RenderType.translucent());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.HERB_PATCH.get(), RenderType.translucent());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.BUSH_PATCH.get(), RenderType.translucent());
        ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.ALLOTMENT_POTATO_CROP.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.BARLEY_CROP.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.ONION_CROP.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.CABBAGE_CROP.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.GUAM_CROP.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.REDBERRY_CROP.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.FLAX.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.YOUNG_FLAX.get(), RenderType.cutout());
        event.enqueueWork(() -> {
        ItemProperties.register(ItemInit.BARLEY_SEED.get(),
                new ResourceLocation("count"), (itemStack, clientWorld, livingEntity, seed) -> itemStack.getCount());
            ItemProperties.register(ItemInit.ONION_SEED.get(),
                    new ResourceLocation("count"), (itemStack, clientWorld, livingEntity, seed) -> itemStack.getCount());
            ItemProperties.register(ItemInit.CABBAGE_SEED.get(),
                    new ResourceLocation("count"), (itemStack, clientWorld, livingEntity, seed) -> itemStack.getCount());
            ItemProperties.register(ItemInit.GUAM_SEED.get(),
                    new ResourceLocation("count"), (itemStack, clientWorld, livingEntity, seed) -> itemStack.getCount());
            ItemProperties.register(ItemInit.REDBERRY_SEED.get(),
                    new ResourceLocation("count"), (itemStack, clientWorld, livingEntity, seed) -> itemStack.getCount());
            ItemProperties.register(ItemInit.BRONZE_ARROW.get(),
                    new ResourceLocation("count"), (itemStack, clientWorld, livingEntity, seed) -> itemStack.getCount());
            initShields();
        });
    }

    private static void initShields() {
        ItemPropertyFunction blockFn = (stack, world, entity, seed) -> entity != null && entity.isUsingItem() && entity.getUseItem() == stack ? 1.0F : 0.0F;
            ItemProperties.register(ItemInit.BRONZE_SQ_SHIELD.get(), new ResourceLocation("minecraft:blocking"), blockFn);
    }

    @Mod.EventBusSubscriber(modid = RunicAgesMain.MODID, value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD)
    public static class ClientModBusEvents {
        @SubscribeEvent
        public static void onKeyRegister(RegisterKeyMappingsEvent event) {
         //   event.register(RunicAgesKeyboardUtil.OPENING_MAGIC_BOOK);
         //   event.register(RunicAgesKeyboardUtil.closing_magic_book);
         //   event.register(RunicAgesKeyboardUtil.spell_1);
            event.register(RunicAgesKeyboardUtil.enter);
        //    event.register(RunicAgesKeyboardUtil.spell_2);
         //   event.register(RunicAgesKeyboardUtil.spell_3);
         //   event.register(RunicAgesKeyboardUtil.spell_4);
         //   event.register(RunicAgesKeyboardUtil.spell_5);
         //   event.register(RunicAgesKeyboardUtil.spell_6);
         //   event.register(RunicAgesKeyboardUtil.spell_7);
         //   event.register(RunicAgesKeyboardUtil.spell_8);
          //  event.register(RunicAgesKeyboardUtil.spell_9);
            event.register(RunicAgesKeyboardUtil.open_summary);
            event.register(RunicAgesKeyboardUtil.open_split_xp);
        }
    }

    @SubscribeEvent
    public static void initRenders(EntityRenderersEvent.RegisterRenderers event) {
        event.registerEntityRenderer(EntityTypeInit.AIR_STRIKE.get(),
                ThrownItemRenderer::new);
        event.registerEntityRenderer(EntityTypeInit.WEAKEN.get(),
                ThrownItemRenderer::new);
        event.registerEntityRenderer(EntityTypeInit.WATER_STRIKE.get(),
                ThrownItemRenderer::new);
        event.registerEntityRenderer(EntityTypeInit.EARTH_STRIKE.get(),
                ThrownItemRenderer::new);
        event.registerEntityRenderer(EntityTypeInit.CONFUSE.get(),
                ThrownItemRenderer::new);
        event.registerEntityRenderer(EntityTypeInit.FIRE_STRIKE.get(),
                ThrownItemRenderer::new);
        event.registerEntityRenderer(EntityTypeInit.IMP.get(), RenderImp::new);
        event.registerEntityRenderer(EntityTypeInit.GRIZZLY_BEAR.get(), RenderGrizzlyBear::new);
        event.registerEntityRenderer(EntityTypeInit.BLACK_BEAR.get(), RenderBlackBear::new);
        event.registerEntityRenderer(EntityTypeInit.UNICORN.get(), RenderUnicorn::new);
        event.registerEntityRenderer(EntityTypeInit.DEADLY_RED_SPIDER.get(), RenderDeadlyRedSpider::new);
        event.registerEntityRenderer(EntityTypeInit.BRONZE_ARROW.get(), RenderBronzeArrow::new);
    }

    @SubscribeEvent
    public static void layerDefinitions(EntityRenderersEvent.RegisterLayerDefinitions event) {
        event.registerLayerDefinition(ImpModel.LAYER_LOCATION, ImpModel::createBodyLayer);
        event.registerLayerDefinition(GrizzlyBearModel.LAYER_LOCATION, GrizzlyBearModel::createBodyLayer);
        event.registerLayerDefinition(BlackBearModel.LAYER_LOCATION, BlackBearModel::createBodyLayer);
        event.registerLayerDefinition(UnicornModel.LAYER_LOCATION, UnicornModel::createBodyLayer);
        event.registerLayerDefinition(DeadlyRedSpiderModel.LAYER_LOCATION, DeadlyRedSpiderModel::createBodyLayer);
    }

    }


