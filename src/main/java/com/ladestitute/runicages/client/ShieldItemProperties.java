package com.ladestitute.runicages.client;

import com.ladestitute.runicages.registry.ItemInit;
import net.minecraft.client.renderer.item.ItemProperties;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;

public class ShieldItemProperties {
    public static void addCustomItemProperties() {

        makeShield(ItemInit.BRONZE_SQ_SHIELD.get());
    }

    /**
     *makeShield is for shields or makeBow for bows ... ect
     * Look at Kaupenjoes custom bow tutorial 1.18.2 (YouTube) for more information, Yes it still works on 1.19.4
     * Think beyond the tutorial to add more custom items
     * Look at ItemProperties class for more information/examples
     * Example mod
     * https://legacy.curseforge.com/minecraft/mc-mods/unexpected-shields
     * __SK__
     */

    private static void makeShield(Item item) {

        ItemProperties.register(item, new ResourceLocation("blocking"), (p_174590_, p_174591_, p_174592_, p_174593_) -> {
            return p_174592_ != null && p_174592_.isUsingItem() && p_174592_.getUseItem() == p_174590_ ? 1.0F : 0.0F;
        });
    }
}
