package com.ladestitute.runicages.client.screen;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.client.menu.BankMenu;
import com.ladestitute.runicages.util.bank.BankStorageEnum;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

import javax.annotation.Nonnull;

public class BankScreen extends AbstractContainerScreen<BankMenu> {
    //Code based on Flanks255's backpacks
    private final ResourceLocation GUI = new ResourceLocation(RunicAgesMain.MODID,
            "textures/gui/bank_gui.png");

    public BankScreen(BankMenu container, Inventory playerInventory, Component name) {
        super(container, playerInventory, name);

        BankStorageEnum tier = container.getTier();

        this.imageWidth = tier.xSize;
        this.imageHeight = tier.ySize;
    }

    @Override
    protected void init() {
        super.init();
    }

    @Override
    protected void renderBg(@Nonnull GuiGraphics gg, float partialTicks, int x, int y) {
        gg.blit(GUI, this.leftPos, this.topPos, 0,0, this.imageWidth, this.imageHeight, this.imageWidth, this.imageHeight);
    }

    @Override
    protected void renderLabels(@Nonnull GuiGraphics gg, int x, int y) {
        gg.drawString(font, this.title.getString(), 7,6,0x404040, false);
    }

    @Override
    public void render(@Nonnull GuiGraphics gg, int pMouseX, int pMouseY, float pPartialTicks) {
        this.renderBackground(gg);
        super.render(gg,pMouseX, pMouseY, pPartialTicks);
        this.renderTooltip(gg, pMouseX, pMouseY);
    }
}
