package com.ladestitute.runicages.client.screen.player;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.util.RunicAgesConfig;
import com.mojang.blaze3d.platform.InputConstants;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;

import javax.annotation.Nullable;

public class RangedSplitScreen extends Screen {

    private final static ResourceLocation PLAYER_SUMMARY_RS3 = new ResourceLocation(RunicAgesMain.MODID,
            "textures/gui/blank1.png");
    private final static ResourceLocation PLAYER_SUMMARY_OSRS = new ResourceLocation(RunicAgesMain.MODID,
            "textures/gui/blank2.png");

    private final @Nullable Screen backScreen;
    private final Player player;

    private static final int MENU_PADDING_FULL = 50;
    private static final int PADDING = 4;

    private static final int BUTTON_WIDTH_HALF = 125;
    private static final int BUTTON_HEIGHT = 20;

    public RangedSplitScreen(@Nullable Screen backScreen, Player player) {
        super(Component.empty());
        this.backScreen = backScreen;
        this.player = player;
    }

    @Override
    public final void init() {
        int BUTTON_START_X_LEFT = (this.width/2) - (BUTTON_WIDTH_HALF+PADDING);
        int BUTTON_START_Y = MENU_PADDING_FULL;
        int y_OFFSET = BUTTON_HEIGHT + PADDING;

        CustomBooleanButton<Boolean> receiverangedxp = CustomBooleanButton.onOffBuilder(RunicAgesConfig.receiverangedxp.get())
                .create(BUTTON_START_X_LEFT+67, (BUTTON_START_Y + (y_OFFSET) - -15), BUTTON_WIDTH_HALF, BUTTON_HEIGHT,
                        Component.literal("Ranged XP"),
                        (b, Off) -> RunicAgesConfig.receiverangedxp.set(Off));
        CustomBooleanButton<Boolean> receivedefensexp = CustomBooleanButton.onOffBuilder(RunicAgesConfig.receiverangeddefensexp.get())
                .create(BUTTON_START_X_LEFT+67, (BUTTON_START_Y + (y_OFFSET) - -65), BUTTON_WIDTH_HALF, BUTTON_HEIGHT,
                        Component.literal("Defense XP"),
                        (b, Off) -> RunicAgesConfig.receiverangeddefensexp.set(Off));

        addRenderableWidget(receiverangedxp);
        addRenderableWidget(receivedefensexp);
    }

    @Override
    public void render(GuiGraphics gui, int mouseX, int mouseY, float partialTicks) {
        PoseStack poseStack = gui.pose();
        renderBg(gui);

        poseStack.pushPose();
        poseStack.translate(width / 1.85, height / 1.72, 0);
        poseStack.scale((float) ((width / 400) / 1.4), (float) ((height / 240) / 1.4), 1);

        if(RunicAgesConfig.modernrs.get()) {
            gui.drawString(font, Component.literal("Ranged XP Splits").withStyle(ChatFormatting.BOLD), -70, -107, 16747520);
        }
        if(!RunicAgesConfig.modernrs.get()) {
           gui.drawString(font, Component.literal("Ranged XP Splits").withStyle(ChatFormatting.BOLD), -70
                    , -107, 16750623);
        }

        poseStack.popPose();

        poseStack.pushPose();
        poseStack.translate(width / 1.85, height / 4.2, 0);
        poseStack.scale((float) ((width / 400) / 2.2), (float) ((height / 240) / 2.2), 1);

        poseStack.popPose();

        poseStack.pushPose();
        poseStack.translate(width / 1.635, height / 1.72, 0);
        poseStack.scale((float) ((width / 400) / 1.8), (float) ((height / 240) / 1.8), 1);

        poseStack.popPose();

        poseStack.pushPose();
        poseStack.translate(width / 2.8, height / 1.43, 0);
        poseStack.scale((float) ((width / 400) / 2.2), (float) ((height / 240) / 2.2), 1);

        poseStack.popPose();

        poseStack.pushPose();
        poseStack.translate(width / 2.38, height / 4.5, 0);
        poseStack.scale((float) ((width / 400) / 1.8), (float) ((height / 240) / 1.8), 1);

        poseStack.popPose();

        super.render(gui, mouseX, mouseY, partialTicks);
    }

    public void renderBg(GuiGraphics gui) {
        PoseStack poseStack = gui.pose();
        Minecraft.getInstance().getProfiler().push("player_render");

        if(RunicAgesConfig.modernrs.get()) {
            RenderSystem.setShaderTexture(0, PLAYER_SUMMARY_RS3);
        }
        else RenderSystem.setShaderTexture(0, PLAYER_SUMMARY_OSRS);

        if(RunicAgesConfig.modernrs.get()) {
            gui.blit(PLAYER_SUMMARY_RS3, width / 2 - (width / 6), height / 2 - (height / 3),
                    0, 0,
                    width / 3, (int) (height / 1.5),
                    width / 3, (int) (height / 1.5));
        }
        else gui.blit(PLAYER_SUMMARY_OSRS, width / 2 - (width / 6), height / 2 - (height / 3),
                0, 0,
                width / 3, (int) (height / 1.5),
                width / 3, (int) (height / 1.5));

        Minecraft.getInstance().getProfiler().pop();

    }

    @Override
    public boolean keyPressed(int keyCode, int scanCode, int Modifiers) {
        switch (keyCode) {
            case InputConstants.KEY_ESCAPE -> onClose();
             case InputConstants.KEY_LEFT -> openMelee();
            case InputConstants.KEY_RIGHT -> openMagic();
            default -> { return false; }
        }
        return true;
    }

    private void openMelee() {
        final Minecraft mc = Minecraft.getInstance();
        mc.setScreen(new MeleeSplitScreen(mc.screen, mc.player));
    }
    @Override
    public boolean shouldCloseOnEsc() {
        return true;
    }

    private void openMagic() {
        final Minecraft mc = Minecraft.getInstance();
        mc.setScreen(new MagicSplitScreen(mc.screen, mc.player));
    }

    @Override
    public boolean isPauseScreen() {
        return false;
    }

    @Override
    public void onClose() {
        this.minecraft.popGuiLayer();
    }

}