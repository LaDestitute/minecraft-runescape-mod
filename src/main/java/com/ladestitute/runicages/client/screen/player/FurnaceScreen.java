//package com.ladestitute.runicages.client.screen.player;
//
//import com.ladestitute.runicages.RunicAgesMain;
//import com.ladestitute.runicages.capability.agility.RunicAgesAgilityCapability;
//import com.ladestitute.runicages.capability.attack.RunicAgesAttackCapability;
//import com.ladestitute.runicages.capability.crafting.RunicAgesCraftingCapability;
//import com.ladestitute.runicages.capability.defense.RunicAgesDefenseCapability;
//import com.ladestitute.runicages.capability.farming.RunicAgesFarmingCapability;
//import com.ladestitute.runicages.capability.herblore.RunicAgesHerbloreCapability;
//import com.ladestitute.runicages.capability.magic.RunicAgesMagicCapability;
//import com.ladestitute.runicages.capability.mining.RunicAgesMiningCapability;
//import com.ladestitute.runicages.capability.ranged.RunicAgesRangedCapability;
//import com.ladestitute.runicages.capability.runecrafting.RunicAgesRunecraftingCapability;
//import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
//import com.ladestitute.runicages.capability.smithing.RunicAgesSmithingCapability;
//import com.ladestitute.runicages.capability.strength.RunicAgesStrengthCapability;
//import com.ladestitute.runicages.capability.thieving.RunicAgesThievingCapability;
//import com.ladestitute.runicages.capability.woodcutting.RunicAgesWoodcuttingCapability;
//import com.ladestitute.runicages.client.menu.SmithingFurnaceMenu;
//import com.ladestitute.runicages.util.RunicAgesConfig;
//import com.mojang.blaze3d.platform.InputConstants;
//import com.mojang.blaze3d.platform.Lighting;
//import com.mojang.blaze3d.systems.RenderSystem;
//import com.mojang.blaze3d.vertex.PoseStack;
//import com.mojang.math.Quaternion;
//import com.mojang.math.Vector3f;
//import net.minecraft.ChatFormatting;
//import net.minecraft.client.Minecraft;
//import net.minecraft.client.gui.components.Button;
//import net.minecraft.client.gui.components.EditBox;
//import net.minecraft.client.gui.components.ImageButton;
//import net.minecraft.client.gui.screens.Screen;
//import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
//import net.minecraft.client.renderer.GameRenderer;
//import net.minecraft.client.renderer.MultiBufferSource;
//import net.minecraft.client.renderer.entity.EntityRenderDispatcher;
//import net.minecraft.network.chat.Component;
//import net.minecraft.network.protocol.game.ServerboundRenameItemPacket;
//import net.minecraft.resources.ResourceLocation;
//import net.minecraft.world.entity.LivingEntity;
//import net.minecraft.world.entity.ai.attributes.Attributes;
//import net.minecraft.world.entity.player.Inventory;
//import net.minecraft.world.entity.player.Player;
//import net.minecraft.world.inventory.Slot;
//
//import javax.annotation.Nullable;
//
//public class FurnaceScreen extends AbstractContainerScreen<SmithingFurnaceMenu> {
//
//    //Credit goes to Zanckor for the screen code
//
//    private final static ResourceLocation FURNACE_RS3 = new ResourceLocation(RunicAgesMain.MODID,
//            "textures/gui/player_summary1.png");
//    private final static ResourceLocation FURNACE_OSRS = new ResourceLocation(RunicAgesMain.MODID,
//            "textures/gui/smelting.png");
//    //For buttons, wip
//    private final static ResourceLocation EMPTY_TEXTURE = new ResourceLocation(RunicAgesMain.MODID,
//            "textures/gui/blank_button.png");
//    private final static ResourceLocation ONE_TEXTURE = new ResourceLocation(RunicAgesMain.MODID,
//            "textures/gui/one_button.png");
//    private final static ResourceLocation FIVE_TEXTURE = new ResourceLocation(RunicAgesMain.MODID,
//            "textures/gui/five_button.png");
//    private final static ResourceLocation TEN_TEXTURE = new ResourceLocation(RunicAgesMain.MODID,
//            "textures/gui/ten_button.png");
//    private final static ResourceLocation X_TEXTURE = new ResourceLocation(RunicAgesMain.MODID,
//            "textures/gui/x_button.png");
//    private final static ResourceLocation ALL_TEXTURE = new ResourceLocation(RunicAgesMain.MODID,
//            "textures/gui/all_button.png");
//
//
//    public EditBox makexfield;
//
//    public FurnaceScreen(SmithingFurnaceMenu pMenu, Inventory pPlayerInventory, Component pTitle) {
//        super(pMenu, pPlayerInventory, pTitle);
//    }
//
//    @Override
//    protected void renderLabels(PoseStack p_97808_, int p_97809_, int p_97810_) {
//        super.renderLabels(p_97808_, p_97809_, p_97810_);
//    }
//
//    public int updateScreenPosition(int p_181402_, int p_181403_) {
//        int i;
//            i = (p_181402_ - p_181403_) / 2;
//
//
//        return i;
//    }
//
//    @Override
//    protected void renderBg(PoseStack pPoseStack, float pPartialTick, int pMouseX, int pMouseY) {
//        RenderSystem.setShader(GameRenderer::getPositionTexShader);
//        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
//        RenderSystem.setShaderTexture(0, FURNACE_OSRS);
//       // width=400;
//      //  height=240;
//        int x = 400;
//        int y = 240;
//        inventoryLabelX=2000;
//        inventoryLabelY=2000;
//
//        this.blit(pPoseStack, x, y, 0, 0, 400, 240);
//
//    }
//
//
//
//    //Test for buttons, will be using this for opening an options menu that lets the player choose how to split their combat XP
//    @Override
//    public final void init() {
//        addRenderableWidget(new ImageButton(
//                (int) (width / 1.85 + -78),
//                (int) (height / 1.72 - 59),
//                26,
//                37,
//                0, 0, 38,
//                EMPTY_TEXTURE,
//                this::test));
//        addRenderableWidget(new ImageButton(
//                (int) (width / 1.85 + -61),
//                (int) (height / 1.72 - 17),
//                26,
//                37,
//                0, 0, 38,
//                EMPTY_TEXTURE,
//                this::test));
//        addRenderableWidget(new ImageButton(
//                (int) (width / 1.85 + -61),
//                (int) (height / 1.72 + 24),
//                17,
//                16,
//                0, 0, 17,
//                ONE_TEXTURE,
//                this::makecount_one));
//        addRenderableWidget(new ImageButton(
//                (int) (width / 1.85 + -43),
//                (int) (height / 1.72 + 24),
//                17,
//                16,
//                0, 0, 17,
//                FIVE_TEXTURE,
//                this::makecount_five));
//        addRenderableWidget(new ImageButton(
//                (int) (width / 1.85 + -25),
//                (int) (height / 1.72 + 24),
//                17,
//                16,
//                0, 0, 17,
//                TEN_TEXTURE,
//                this::makecount_ten));
//        addRenderableWidget(new ImageButton(
//                (int) (width / 1.85 + -7),
//                (int) (height / 1.72 + 24),
//                17,
//                16,
//                0, 0, 17,
//                X_TEXTURE,
//                this::makecount_x));
//        addRenderableWidget(new ImageButton(
//                (int) (width / 1.85 + 11),
//                (int) (height / 1.72 + 24),
//                17,
//                16,
//                0, 0, 17,
//                ALL_TEXTURE,
//                this::makecount_all));
//
//        makexfield = new EditBox(this.font, (int) (width / 1.85 + -27),
//                (int) (height / 1.72 + 42),
//                20,
//                12,
//                Component.literal(""));
//        makexfield.setMaxLength(2);
//       // makexfield.setResponder((message) -> RunicAgesSmithingCapability.setMakeCountButton(Integer.parseInt(message)));
//        makexfield.setValue("");
//
//        this.addRenderableWidget(makexfield);
//    }
//
//
// //   this.leftPos + 5, this.height / 2 - 49, 20, 18, 0, 0, 19
//    // this.addRenderableWidget(new ImageButton(this.width / 2 + 104, l + 72 + 12, 20, 20, 0, 0, 20
//
//    protected void test(Button b) {
//        assert Minecraft.getInstance().player != null;
//        RunicAgesSmithingCapability.smeltBronzeBar(Minecraft.getInstance().player);
//    }
//
//    protected void makecount_one(Button b) {
//        assert Minecraft.getInstance().player != null;
//        Minecraft.getInstance().player.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(s ->
//        {
//            s.setMakingAll(false);
//            s.setMakeCount(1);
//            RunicAgesSmithingCapability.levelClientUpdate(Minecraft.getInstance().player);
//        });
//    }
//
//    protected void makecount_five(Button b) {
//        assert Minecraft.getInstance().player != null;
//        Minecraft.getInstance().player.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(s ->
//        {
//            s.setMakingAll(false);
//            s.setMakeCount(5);
//            RunicAgesSmithingCapability.levelClientUpdate(Minecraft.getInstance().player);
//        });
//    }
//
//    protected void makecount_ten(Button b) {
//        assert Minecraft.getInstance().player != null;
//        Minecraft.getInstance().player.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(s ->
//        {
//            s.setMakingAll(false);
//            s.setMakeCount(10);
//            RunicAgesSmithingCapability.levelClientUpdate(Minecraft.getInstance().player);
//        });
//    }
//
//    protected void makecount_x(Button b) {
//        assert Minecraft.getInstance().player != null;
//        Minecraft.getInstance().player.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(s ->
//        {
//            s.setMakingAll(false);
//            s.setMakeCount(Integer.parseInt(makexfield.getValue()));
//            RunicAgesSmithingCapability.levelClientUpdate(Minecraft.getInstance().player);
//        });
//    }
//
//    protected void makecount_all(Button b) {
//        assert Minecraft.getInstance().player != null;
//        Minecraft.getInstance().player.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(s ->
//        {
//            s.setMakingAll(true);
//            RunicAgesSmithingCapability.levelClientUpdate(Minecraft.getInstance().player);
//        });
//    }
//
//    @Override
//    public boolean keyPressed(int keyCode, int scanCode, int Modifiers) {
//        switch (keyCode) {
//            case InputConstants.KEY_ESCAPE -> onClose();
//            case InputConstants.KEY_SPACE -> bronzeHotkey();
//            default -> { return false; }
//        }
//        return true;
//    }
//
//    @Override
//    public void onClose() {
//        assert Minecraft.getInstance().player != null;
//        Minecraft.getInstance().player.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(s ->
//        {
//            s.setUsingFurnace(0);
//            RunicAgesSmithingCapability.levelClientUpdate(Minecraft.getInstance().player);
//        });
//        this.minecraft.popGuiLayer();
//    }
//
//    private void bronzeHotkey() {
//        assert Minecraft.getInstance().player != null;
//        RunicAgesSmithingCapability.smeltBronzeBar(Minecraft.getInstance().player);
//    }
//
//    @Override
//    public void render(PoseStack poseStack, int mouseX, int mouseY, float partialTicks) {
//        renderBg(poseStack);
//
//        poseStack.pushPose();
//        poseStack.translate(width / 1.85, height / 1.72, 0);
//        poseStack.scale((float) ((width / 400) / 1.8), (float) ((height / 240) / 1.8), 1);
//
//        //   Minecraft.getInstance().font.draw(poseStack, "Strength XP",  0, 0, 3613069);
//
//        poseStack.popPose();
//
//
//        poseStack.pushPose();
//        poseStack.translate(width / 1.85, height / 4.2, 0);
//        poseStack.scale((float) ((width / 400) / 2.2), (float) ((height / 240) / 2.2), 1);
//
//
//
//        poseStack.popPose();
//
//        poseStack.pushPose();
//        poseStack.translate(width / 1.635, height / 1.72, 0);
//        poseStack.scale((float) ((width / 400) / 1.8), (float) ((height / 240) / 1.8), 1);
//
//        poseStack.popPose();
//
//        poseStack.pushPose();
//        poseStack.translate(width / 2.8, height / 1.43, 0);
//        poseStack.scale((float) ((width / 400) / 2.2), (float) ((height / 240) / 2.2), 1);
//
//        if(RunicAgesConfig.modernrs.get()) {
//
//        }
//        if(!RunicAgesConfig.modernrs.get()) {
//        }
//
//        poseStack.popPose();
//
//        poseStack.pushPose();
//        poseStack.translate(width / 2.38, height / 4.5, 0);
//        poseStack.scale((float) ((width / 400) / 1.8), (float) ((height / 240) / 1.8), 1);
//
//        if(RunicAgesConfig.modernrs.get()) {
//
//        }
//        if(!RunicAgesConfig.modernrs.get()) {
//
//        }
//
//        poseStack.popPose();
//
//        super.render(poseStack, mouseX, mouseY, partialTicks);
//    }
//
//    public void renderBg(PoseStack poseStack) {
//        Minecraft.getInstance().getProfiler().push("player_render");
//
//        if(RunicAgesConfig.modernrs.get()) {
//            RenderSystem.setShaderTexture(0, FURNACE_OSRS);
//        }
//        else RenderSystem.setShaderTexture(0, FURNACE_OSRS);
//
//        blit(poseStack, width / 2 - (width / 6), height / 2 - (height / 3),
//                0, 0,
//                width / 3, (int) (height / 1.5),
//                width / 3, (int) (height / 1.5));
//
//        Minecraft.getInstance().getProfiler().pop();
//    }
//
//    @Override
//    public boolean isPauseScreen() {
//        return false;
//    }
//}