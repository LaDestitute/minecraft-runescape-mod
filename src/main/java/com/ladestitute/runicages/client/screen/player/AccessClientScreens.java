package com.ladestitute.runicages.client.screen.player;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.agility.RunicAgesAgilityCapability;
import com.ladestitute.runicages.capability.attack.RunicAgesAttackCapability;
import com.ladestitute.runicages.capability.crafting.RunicAgesCraftingCapability;
import com.ladestitute.runicages.capability.defense.RunicAgesDefenseCapability;
import com.ladestitute.runicages.capability.farming.RunicAgesFarmingCapability;
import com.ladestitute.runicages.capability.herblore.RunicAgesHerbloreCapability;
import com.ladestitute.runicages.capability.magic.RunicAgesMagicCapability;
import com.ladestitute.runicages.capability.mining.RunicAgesMiningCapability;
import com.ladestitute.runicages.capability.ranged.RunicAgesRangedCapability;
import com.ladestitute.runicages.capability.runecrafting.RunicAgesRunecraftingCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.capability.smithing.RunicAgesSmithingCapability;
import com.ladestitute.runicages.capability.strength.RunicAgesStrengthCapability;
import com.ladestitute.runicages.capability.thieving.RunicAgesThievingCapability;
import com.ladestitute.runicages.capability.woodcutting.RunicAgesWoodcuttingCapability;
import com.ladestitute.runicages.util.RunicAgesKeyboardUtil;
import net.minecraft.client.Minecraft;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = RunicAgesMain.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE) //, value = Dist.CLIENT
public class AccessClientScreens
{


    @SubscribeEvent
    public static void onClientTickEvent(TickEvent.ClientTickEvent event) {

        final Minecraft mc = Minecraft.getInstance();

        if(RunicAgesKeyboardUtil.open_summary.consumeClick()) {
            RunicAgesCraftingCapability.levelClientUpdate(mc.player);
            RunicAgesMagicCapability.levelClientUpdate(mc.player);
            RunicAgesMiningCapability.levelClientUpdate(mc.player);
            RunicAgesRunecraftingCapability.levelClientUpdate(mc.player);
            RunicAgesSmithingCapability.levelClientUpdate(mc.player);
            RunicAgesWoodcuttingCapability.levelClientUpdate(mc.player);
            RunicAgesExtraDataCapability.levelClientUpdate(mc.player);
            RunicAgesAttackCapability.levelClientUpdate(mc.player);
            RunicAgesStrengthCapability.levelClientUpdate(mc.player);
            RunicAgesDefenseCapability.levelClientUpdate(mc.player);
            RunicAgesRangedCapability.levelClientUpdate(mc.player);
            RunicAgesHerbloreCapability.levelClientUpdate(mc.player);
            RunicAgesFarmingCapability.levelClientUpdate(mc.player);
            RunicAgesThievingCapability.levelClientUpdate(mc.player);
            RunicAgesAgilityCapability.levelClientUpdate(mc.player);
            mc.setScreen(new SummaryScreen(mc.screen, mc.player));
            return;
        }

        if(RunicAgesKeyboardUtil.open_split_xp.consumeClick()) {
            mc.setScreen(new MeleeSplitScreen(mc.screen, mc.player));
            return;
        }

        if(RunicAgesKeyboardUtil.OPENING_MAGIC_BOOK.consumeClick()) {
            mc.setScreen(new NewMagicBookScreen(mc.screen, mc.player));
            return;
        }

    }
}
