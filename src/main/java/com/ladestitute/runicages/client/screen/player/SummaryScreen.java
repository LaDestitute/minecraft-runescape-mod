package com.ladestitute.runicages.client.screen.player;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.agility.RunicAgesAgilityCapability;
import com.ladestitute.runicages.capability.attack.RunicAgesAttackCapability;
import com.ladestitute.runicages.capability.crafting.RunicAgesCraftingCapability;
import com.ladestitute.runicages.capability.defense.RunicAgesDefenseCapability;
import com.ladestitute.runicages.capability.farming.RunicAgesFarmingCapability;
import com.ladestitute.runicages.capability.herblore.RunicAgesHerbloreCapability;
import com.ladestitute.runicages.capability.magic.RunicAgesMagicCapability;
import com.ladestitute.runicages.capability.mining.RunicAgesMiningCapability;
import com.ladestitute.runicages.capability.ranged.RunicAgesRangedCapability;
import com.ladestitute.runicages.capability.runecrafting.RunicAgesRunecraftingCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.capability.smithing.RunicAgesSmithingCapability;
import com.ladestitute.runicages.capability.strength.RunicAgesStrengthCapability;
import com.ladestitute.runicages.capability.thieving.RunicAgesThievingCapability;
import com.ladestitute.runicages.capability.woodcutting.RunicAgesWoodcuttingCapability;
import com.ladestitute.runicages.util.RunicAgesConfig;
import com.mojang.blaze3d.platform.Lighting;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderDispatcher;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import org.joml.Quaternionf;

import javax.annotation.Nullable;

public class SummaryScreen extends Screen {

    //Credit goes to Zanckor for the screen code
    private int mining;
    private int smithing;
    private int herblore;
    private int crafting;
    private int magic;
    private int woodcutting;
    private int runecrafting;
    private int attack;
    private int strength;
    private int defense;
    private int ranged;
    private int farming;
    private int thieving;
    private int agility;
    private int totalxp;
    private int attackboost;
    private int rangedboost;
    private int defenseboost;
    private int magicboost;

    private final static ResourceLocation PLAYER_SUMMARY_RS3 = new ResourceLocation(RunicAgesMain.MODID,
            "textures/gui/player_summary1.png");
    private final static ResourceLocation PLAYER_SUMMARY_OSRS = new ResourceLocation(RunicAgesMain.MODID,
            "textures/gui/player_summary2.png");
    //For buttons, wip
    private final static ResourceLocation EMPTY_TEXTURE = new ResourceLocation(RunicAgesMain.MODID,
            "textures/gui/blank_button1.png");

    private final @Nullable Screen backScreen;
    private final Player player;

    public SummaryScreen(@Nullable Screen backScreen, Player player) {
        super(Component.empty());
        this.backScreen = backScreen;
        this.player = player;
    }

    //Test for buttons, will be using this for opening an options menu that lets the player choose how to split their combat XP
//    @Override
//    public final void init() {
//        addRenderableWidget(new ImageButton(
//                (int) (width / 1.85),
//                (int) (height / 1.72),
//                width / 18,
//                height / 38,
//                0, 0,
//                EMPTY_TEXTURE,
//                this::test));
//    }

    protected void test(Button b) {
        assert Minecraft.getInstance().player != null;
        player.getCapability(RunicAgesMiningCapability.Provider.MINING_LEVEL).ifPresent(h ->
        {
            h.addMiningLevel(Minecraft.getInstance().player, 10);
        });

        RunicAgesMiningCapability.levelClientUpdate(Minecraft.getInstance().player);
    }

    @Override
    public void render(GuiGraphics gui, int mouseX, int mouseY, float partialTicks) {
        PoseStack poseStack = gui.pose();
        renderBg(gui);

        poseStack.pushPose();
        poseStack.translate(width / 1.85, height / 1.72, 0);
        poseStack.scale((float) ((width / 400) / 1.8), (float) ((height / 240) / 1.8), 1);

     //   gui.drawString(font, "Strength XP",  0, 0, 3613069);

        poseStack.popPose();


        poseStack.pushPose();
        poseStack.translate(width / 1.85, height / 4.2, 0);
        poseStack.scale((float) ((width / 400) / 2.2), (float) ((height / 240) / 2.2), 1);

        assert player != null;
        player.getCapability(RunicAgesWoodcuttingCapability.Provider.WOODCUTTING_LEVEL).ifPresent(h ->
        {
            woodcutting = h.getWoodcuttingLevel();
        });
        player.getCapability(RunicAgesMiningCapability.Provider.MINING_LEVEL).ifPresent(h ->
        {
            mining = h.getMiningLevel();
        });
        player.getCapability(RunicAgesSmithingCapability.Provider.SMITHING_LEVEL).ifPresent(h ->
        {
            smithing = h.getSmithingLevel();
        });
        player.getCapability(RunicAgesDefenseCapability.Provider.DEFENSE_LEVEL).ifPresent(h ->
        {
            defense = h.getDefenseLevel();
            defenseboost = h.getDefenseBoost();
        });
        //implementation needed for herblore, for now; assuming herblore is 1 in calcs
        player.getCapability(RunicAgesHerbloreCapability.Provider.HERBLORE_LEVEL).ifPresent(h ->
        {
            herblore = h.getHerbloreLevel();
        });
        player.getCapability(RunicAgesRangedCapability.Provider.RANGED_LEVEL).ifPresent(h ->
        {
            ranged = h.getRangedLevel();
            rangedboost = h.getRangedBoost();
        });
        player.getCapability(RunicAgesCraftingCapability.Provider.CRAFTING_LEVEL).ifPresent(h ->
        {
            crafting = h.getCraftingLevel();
        });
        player.getCapability(RunicAgesMagicCapability.Provider.MAGIC_LEVEL).ifPresent(h ->
        {
            magic = h.getMagicLevel();
            magicboost = h.getMagicBoost();
        });
        player.getCapability(RunicAgesRunecraftingCapability.Provider.RUNECRAFTING_LEVEL).ifPresent(h ->
        {
            runecrafting = h.getRunecraftingLevel();
        });
        player.getCapability(RunicAgesAttackCapability.Provider.ATTACK_LEVEL).ifPresent(h ->
        {
            attack = h.getAttackLevel();
            attackboost = h.getAttackBoost();
        });
        player.getCapability(RunicAgesStrengthCapability.Provider.STRENGTH_LEVEL).ifPresent(h ->
        {
            strength = h.getStrengthLevel();
        });
        player.getCapability(RunicAgesFarmingCapability.Provider.FARMING_LEVEL).ifPresent(h ->
        {
            farming = h.getFarmingLevel();
        });
        player.getCapability(RunicAgesThievingCapability.Provider.THIEVING_LEVEL).ifPresent(h ->
        {
            thieving = h.getThievingLevel();
        });
        player.getCapability(RunicAgesAgilityCapability.Provider.AGILITY_LEVEL).ifPresent(h ->
        {
            agility = h.getAgilityLevel();
        });
        player.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(h ->
        {
            totalxp = h.getTotalXP();
        });

        if(RunicAgesConfig.modernrs.get()) {
            gui.drawString(font,
                    Component.literal("Skills")
                            .withStyle(ChatFormatting.BOLD),
                    30, 0, 16747520);
                gui.drawString(font,
                        Component.literal("Attack: " + (attack+attackboost) + "/" + attack),
                        -15, 14, 16747520);
            gui.drawString(font,
                    Component.literal("Mining: " + mining + "/" + mining),
                    -15, 24, 16747520);
            gui.drawString(font,
                    Component.literal("Strength: " + strength + "/" + strength),
                    -15, 34, 16747520);
            gui.drawString(font,
                    Component.literal("Agility: " + agility + "/" + agility),
                    -15, 44, 16747520);
            gui.drawString(font,
                    Component.literal("Smithing: " + smithing + "/" + smithing),
                    -15, 54, 16747520);
            gui.drawString(font,
                    Component.literal("Defense: " + (defense+defenseboost) + "/" + defense),
                    -15, 64, 16747520);
            gui.drawString(font,
                    Component.literal("Herblore: " + herblore + "/" + herblore),
                    -15, 74, 16747520);
            gui.drawString(font,
                    Component.literal("Ranged: " + (ranged+rangedboost) + "/" + ranged),
                    -15, 84, 16747520);
            gui.drawString(font,
                    Component.literal("Thieving: " + thieving + "/" + thieving),
                    -15, 94, 16747520);
            gui.drawString(font,
                    Component.literal("Crafting: " + crafting + "/" + crafting),
                    -15, 104, 16747520);
            gui.drawString(font,
                    Component.literal("Magic: " + (magic+magicboost) + "/" + magic),
                    -15, 114, 16747520);
            gui.drawString(font,
                    Component.literal("Woodcutting: " + woodcutting + "/" + woodcutting),
                    -15, 124, 16747520);
            gui.drawString(font,
                    Component.literal("Runecrafting: " + runecrafting + "/" + runecrafting),
                    -15, 134, 16747520);
            gui.drawString(font,
                    Component.literal("Farming: " + farming + "/" + farming),
                    -15, 144, 16747520);
        }
        if(!RunicAgesConfig.modernrs.get()) {
            gui.drawString(font,
                    Component.literal("Skills")
                            .withStyle(ChatFormatting.BOLD),
                    30, 0, 16750623);
                gui.drawString(font,
                        Component.literal("Attack: " + (attack+attackboost) + "/" + attack),
                        -15, 14, 16776960);
            gui.drawString(font,
                    Component.literal("Mining: " + mining + "/" + mining),
                    -15, 24, 16776960);
            gui.drawString(font,
                    Component.literal("Strength: " + strength + "/" + strength),
                    -15, 34, 16776960);
            gui.drawString(font,
                    Component.literal("Agility: " + agility + "/" + agility),
                    -15, 44, 16776960);
            gui.drawString(font,
                    Component.literal("Smithing: " + smithing + "/" + smithing),
                    -15, 54, 16776960);
            gui.drawString(font,
                    Component.literal("Defense: " + (defense+defenseboost) + "/" + defense),
                    -15, 64, 16776960);
            gui.drawString(font,
                    Component.literal("Herblore: " + herblore + "/" + herblore),
                    -15, 74, 16776960);
            gui.drawString(font,
                    Component.literal("Ranged: " + (ranged+rangedboost) + "/" + ranged),
                    -15, 84, 16776960);
            gui.drawString(font,
                    Component.literal("Thieving: " + thieving + "/" + thieving),
                    -15, 94, 16776960);
            gui.drawString(font,
                    Component.literal("Crafting: " + crafting + "/" + crafting),
                    -15, 104, 16776960);
            gui.drawString(font,
                    Component.literal("Magic: " + (magic+magicboost) + "/" + magic),
                    -15, 114, 16776960);
            gui.drawString(font,
                    Component.literal("Woodcutting: " + woodcutting + "/" + woodcutting),
                    -15, 124, 16776960);
            gui.drawString(font,
                    Component.literal("Runecraft: " + runecrafting + "/" + runecrafting),
                    -15, 134, 16776960);
            gui.drawString(font,
                    Component.literal("Farming: " + farming + "/" + farming),
                    -15, 144, 16776960);
        }

        poseStack.popPose();

        poseStack.pushPose();
        poseStack.translate(width / 1.635, height / 1.72, 0);
        poseStack.scale((float) ((width / 400) / 1.8), (float) ((height / 240) / 1.8), 1);

        poseStack.popPose();

        poseStack.pushPose();
        poseStack.translate(width / 2.8, height / 1.43, 0);
        poseStack.scale((float) ((width / 400) / 2.2), (float) ((height / 240) / 2.2), 1);

        if(RunicAgesConfig.modernrs.get()) {
            gui.drawString(font, Component.literal("Total Skill Level: "
                    + (attack + ranged + defense + strength + farming + thieving + agility + mining + smithing + herblore + crafting + magic + woodcutting + runecrafting)), 0, 3, 14869475);
            gui.drawString(font, Component.literal("Combat Level: 3"),
                    0, 15, 14869475);
            gui.drawString(font, Component.literal("Total XP: " + totalxp),
                    0, 27, 14869475);
        }
        if(!RunicAgesConfig.modernrs.get()) {
            gui.drawString(font, Component.literal("Total Skill Level: "
                    + (attack + defense + ranged + strength + mining + thieving + agility + farming + smithing + herblore + crafting + magic + woodcutting + runecrafting)), 0, 3, 16750899);
            gui.drawString(font, Component.literal("Combat Level: 3"),
                    0, 15, 16750899);
            gui.drawString(font, Component.literal("Total XP: " + totalxp),
                    0, 27, 16750899);
        }

        poseStack.popPose();

        poseStack.pushPose();
        poseStack.translate(width / 2.38, height / 4.5, 0);
        poseStack.scale((float) ((width / 400) / 1.8), (float) ((height / 240) / 1.8), 1);

        if(RunicAgesConfig.modernrs.get()) {
            gui.drawCenteredString(font,
                    Component.literal(player.getName().getString())
                            .withStyle(ChatFormatting.BOLD),
                    -6, 0, 16747520);
        }
        if(!RunicAgesConfig.modernrs.get()) {
            gui.drawCenteredString(font,
                    Component.literal(player.getName().getString())
                            .withStyle(ChatFormatting.BOLD),
                    -6, 0, 16750623);
        }

        poseStack.popPose();

        super.render(gui, mouseX, mouseY, partialTicks);
    }

    public void renderBg(GuiGraphics gui) {
        Minecraft.getInstance().getProfiler().push("player_render");

        if(RunicAgesConfig.modernrs.get()) {
            RenderSystem.setShaderTexture(0, PLAYER_SUMMARY_RS3);
        }
        else RenderSystem.setShaderTexture(0, PLAYER_SUMMARY_OSRS);

        if(RunicAgesConfig.modernrs.get()) {
            gui.blit(PLAYER_SUMMARY_RS3, width / 2 - (width / 6), height / 2 - (height / 3),
                    0, 0,
                    width / 3, (int) (height / 1.5),
                    width / 3, (int) (height / 1.5));
        }
        else gui.blit(PLAYER_SUMMARY_OSRS, width / 2 - (width / 6), height / 2 - (height / 3),
                0, 0,
                width / 3, (int) (height / 1.5),
                width / 3, (int) (height / 1.5));

        Minecraft.getInstance().getProfiler().pop();

        renderEntityInInventory((int) (width / 2.37), (int) (height / 1.5), height / 5, height / 4, 0, player);
    }

    public static void renderEntityInInventory(int xPos, int yPos, int size, float xRot, float yRot, LivingEntity entity) {
        float f = (float)Math.atan((double)(xRot / 40.0F));
        float f1 = (float)Math.atan((double)(yRot / 40.0F));
        PoseStack posestack = RenderSystem.getModelViewStack();
        posestack.pushPose();
        posestack.translate((double)xPos, (double)yPos, 1050.0D);
        posestack.scale(1.0F, 1.0F, -1.0F);
        RenderSystem.applyModelViewMatrix();
        PoseStack posestack1 = new PoseStack();
        posestack1.translate(0.0D, 0.0D, 1000.0D);
        posestack1.scale((float)size, (float)size, (float)size);
        Quaternionf quaternion = Axis.ZP.rotationDegrees(180.0F);
        Quaternionf quaternion1 = Axis.XP.rotationDegrees(f1 * 20.0F);
        quaternion.mul(quaternion1);
        posestack1.mulPose(quaternion);
        float f2 = entity.yBodyRot;
        float f3 = entity.getYRot();
        float f4 = entity.getXRot();
        float f5 = entity.yHeadRotO;
        float f6 = entity.yHeadRot;
        entity.yBodyRot = 180.0F + f * 20.0F;
        entity.setYRot(180.0F + f * 40.0F);
        entity.setXRot(-f1 * 20.0F);
        entity.yHeadRot = entity.getYRot();
        entity.yHeadRotO = entity.getYRot();
        Lighting.setupForEntityInInventory();
        EntityRenderDispatcher entityrenderdispatcher = Minecraft.getInstance().getEntityRenderDispatcher();
        quaternion1.conjugate();
        entityrenderdispatcher.overrideCameraOrientation(quaternion1);
        entityrenderdispatcher.setRenderShadow(false);
        MultiBufferSource.BufferSource multibuffersource$buffersource = Minecraft.getInstance().renderBuffers().bufferSource();
        RenderSystem.runAsFancy(() -> {
            entityrenderdispatcher.render(entity, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F, posestack1, multibuffersource$buffersource, 15728880);
        });
        multibuffersource$buffersource.endBatch();
        entityrenderdispatcher.setRenderShadow(true);
        entity.yBodyRot = f2;
        entity.setYRot(f3);
        entity.setXRot(f4);
        entity.yHeadRotO = f5;
        entity.yHeadRot = f6;
        posestack.popPose();
        RenderSystem.applyModelViewMatrix();
        Lighting.setupFor3DItems();
    }

    @Override
    public boolean isPauseScreen() {
        return false;
    }

    @Override
    public void onClose() {
        if (minecraft != null)
            minecraft.setScreen(backScreen);
    }
}