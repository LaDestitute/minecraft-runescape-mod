package com.ladestitute.runicages.client.screen;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.client.menu.SteelAlloyFurnaceMenu;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

// !!!
public class SteelAlloyFurnaceScreen extends AbstractContainerScreen<SteelAlloyFurnaceMenu> {
    private static final ResourceLocation TEXTURE =
            new ResourceLocation(RunicAgesMain.MODID, "textures/gui/alloy_furnace_gui.png");

    // !!!
    public SteelAlloyFurnaceScreen(SteelAlloyFurnaceMenu pMenu, Inventory pPlayerInventory, Component pTitle) {
        super(pMenu, pPlayerInventory, pTitle);
    }

    @Override
    protected void init() {
        super.init();
    }

    @Override
    protected void renderBg(GuiGraphics guigraphics, float pPartialTicks, int pMouseX, int pMouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.setShaderTexture(0, TEXTURE);
        int x = (width - imageWidth) / 2;
        int y = (height - imageHeight) / 2;

        guigraphics.blit(TEXTURE, x, y, 0, 0, imageWidth, imageHeight);

        // Add to the renderBg
        if(menu.isCrafting()) {
            guigraphics.blit(TEXTURE, x + 84, y + 22, 176, 14, menu.getScaledProgress(), 36);
        }

        if(menu.hasFuel()) {
            guigraphics.blit(TEXTURE, x + 18, y + 33 + 14 - menu.getScaledFuelProgress(), 176,
                    14 - menu.getScaledFuelProgress(), 14, menu.getScaledFuelProgress());
        }
    }

    @Override
    public void render(GuiGraphics guigraphics, int mouseX, int mouseY, float delta) {
        renderBackground(guigraphics);
        super.render(guigraphics, mouseX, mouseY, delta);
        renderTooltip(guigraphics, mouseX, mouseY);
    }
}


