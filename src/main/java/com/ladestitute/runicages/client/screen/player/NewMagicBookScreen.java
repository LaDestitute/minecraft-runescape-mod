package com.ladestitute.runicages.client.screen.player;

import com.ladestitute.runicages.RunicAgesMain;
import com.ladestitute.runicages.capability.magic.RunicAgesMagicCapability;
import com.ladestitute.runicages.capability.mining.RunicAgesMiningCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.ladestitute.runicages.util.RunicAgesConfig;
import com.mojang.blaze3d.platform.InputConstants;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.ImageButton;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;

import javax.annotation.Nullable;

public class NewMagicBookScreen extends Screen {

    private final static ResourceLocation PLAYER_SUMMARY_RS3 = new ResourceLocation(RunicAgesMain.MODID,
            "textures/gui/blank1.png");
    private final static ResourceLocation PLAYER_SUMMARY_OSRS = new ResourceLocation(RunicAgesMain.MODID,
            "textures/gui/blank2.png");
    private final static ResourceLocation HOME_SPELL = new ResourceLocation(RunicAgesMain.MODID,
            "textures/gui/home_button.png");
    private final static ResourceLocation AIR_STRIKE_SPELL = new ResourceLocation(RunicAgesMain.MODID,
            "textures/gui/air_strike_button.png");
    private final static ResourceLocation ENCHANT_LEVEL_ONE_SPELL = new ResourceLocation(RunicAgesMain.MODID,
            "textures/gui/enchant_blue_button.png");


    private final @Nullable
    Screen backScreen;
    private final Player player;

    private static final int MENU_PADDING_FULL = 50;
    private static final int PADDING = 4;

    private static final int BUTTON_WIDTH_HALF = 125;
    private static final int BUTTON_HEIGHT = 20;

    public NewMagicBookScreen(@Nullable Screen backScreen, Player player) {
        super(Component.empty());
        this.backScreen = backScreen;
        this.player = player;
    }

    @Override
    public final void init() {
        int BUTTON_START_X_LEFT = (this.width/2) - (BUTTON_WIDTH_HALF+PADDING);
        int BUTTON_START_Y = MENU_PADDING_FULL;
        int y_OFFSET = BUTTON_HEIGHT + PADDING;

                addRenderableWidget(new ImageButton(
                        (int) (width / 1.85 + -78),
                (int) (height / 1.72 - 59),
                15,
                14,
                0, 0, 15,
                HOME_SPELL,
                this::home));
        addRenderableWidget(new ImageButton(
                (int) (width / 1.85 + -63),
                (int) (height / 1.72 - 59),
                15,
                14,
                0, 0, 15,
                AIR_STRIKE_SPELL,
                this::air_strike));
        addRenderableWidget(new ImageButton(
                (int) (width / 1.85 + -48),
                (int) (height / 1.72 - 59),
                15,
                14,
                0, 0, 15,
                ENCHANT_LEVEL_ONE_SPELL,
                this::enchant_level_one));
    }

    protected void home(Button b) {
        this.onClose();
        assert Minecraft.getInstance().player != null;
        player.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
        {
            player.teleportTo(ed.getX(), ed.getY(), ed.getZ());
            player.displayClientMessage(Component.literal("You teleported home"), false);
        });
    }

    protected void air_strike(Button b) {
        assert Minecraft.getInstance().player != null;
        player.getCapability(RunicAgesMagicCapability.Provider.MAGIC_LEVEL).ifPresent(h ->
        {
            h.setMainhandSpell(1);
            Minecraft.getInstance().player.displayClientMessage(Component.literal("You have set your mainhand spell to Air Strike."), false);
            RunicAgesMagicCapability.levelClientUpdate(Minecraft.getInstance().player);
            System.out.println("Mainhand is: " + h.getMainhandSpell());
        });
    }

    protected void enchant_level_one(Button b) {
        assert Minecraft.getInstance().player != null;
        player.getCapability(RunicAgesMagicCapability.Provider.MAGIC_LEVEL).ifPresent(h ->
        {
            h.setGuiButtonPressed(3);
            RunicAgesMagicCapability.levelClientUpdate(Minecraft.getInstance().player);
            RunicAgesExtraDataCapability.levelClientUpdate(Minecraft.getInstance().player);
            this.onClose();
            System.out.println("GUI BUTTON: " + h.getGuiButtonPressed());
        });
    }

    @Override
    public void render(GuiGraphics gui, int mouseX, int mouseY, float partialTicks) {
        PoseStack poseStack = gui.pose();
        renderBg(gui);

        poseStack.pushPose();
        poseStack.translate(width / 1.85, height / 1.72, 0);
        poseStack.scale((float) ((width / 400) / 1.4), (float) ((height / 240) / 1.4), 1);

        if(RunicAgesConfig.modernrs.get()) {
            gui.drawString(font, Component.literal("Magic Book").withStyle(ChatFormatting.BOLD), -58, -107, 16747520);
        }
        if(!RunicAgesConfig.modernrs.get()) {
            gui.drawString(font, Component.literal("Magic Book").withStyle(ChatFormatting.BOLD), -58
                    , -107, 16750623);
        }

        poseStack.popPose();

        poseStack.pushPose();
        poseStack.translate(width / 1.85, height / 4.2, 0);
        poseStack.scale((float) ((width / 400) / 2.2), (float) ((height / 240) / 2.2), 1);

        poseStack.popPose();

        poseStack.pushPose();
        poseStack.translate(width / 1.635, height / 1.72, 0);
        poseStack.scale((float) ((width / 400) / 1.8), (float) ((height / 240) / 1.8), 1);

        poseStack.popPose();

        poseStack.pushPose();
        poseStack.translate(width / 2.8, height / 1.43, 0);
        poseStack.scale((float) ((width / 400) / 2.2), (float) ((height / 240) / 2.2), 1);

        poseStack.popPose();

        poseStack.pushPose();
        poseStack.translate(width / 2.38, height / 4.5, 0);
        poseStack.scale((float) ((width / 400) / 1.8), (float) ((height / 240) / 1.8), 1);

        poseStack.popPose();

        super.render(gui, mouseX, mouseY, partialTicks);
    }

    public void renderBg(GuiGraphics gui) {
        PoseStack poseStack = gui.pose();
        Minecraft.getInstance().getProfiler().push("player_render");

        if(RunicAgesConfig.modernrs.get()) {
            RenderSystem.setShaderTexture(0, PLAYER_SUMMARY_RS3);
        }
        else RenderSystem.setShaderTexture(0, PLAYER_SUMMARY_OSRS);

        if(RunicAgesConfig.modernrs.get()) {
            gui.blit(PLAYER_SUMMARY_RS3, width / 2 - (width / 6), height / 2 - (height / 3),
                    0, 0,
                    width / 3, (int) (height / 1.5),
                    width / 3, (int) (height / 1.5));
        }
        else gui.blit(PLAYER_SUMMARY_OSRS, width / 2 - (width / 6), height / 2 - (height / 3),
                0, 0,
                width / 3, (int) (height / 1.5),
                width / 3, (int) (height / 1.5));

        Minecraft.getInstance().getProfiler().pop();

    }

    @Override
    public boolean keyPressed(int keyCode, int scanCode, int Modifiers) {
        switch (keyCode) {
            case InputConstants.KEY_ESCAPE -> onClose();
            case InputConstants.KEY_NUMPAD0-> Home();
            case InputConstants.KEY_NUMPAD1-> Air();
            case InputConstants.KEY_NUMPAD2-> Enchant_Blue();
            default -> { return false; }
        }
        return true;
    }

    private void Home()
    {
        Minecraft.getInstance().player.getCapability(RunicAgesMagicCapability.Provider.MAGIC_LEVEL).ifPresent(h ->
        {
            h.setGuiButtonPressed(1);
            RunicAgesMagicCapability.levelClientUpdate(Minecraft.getInstance().player);
            this.onClose();
        });
    }

    private void Air()
    {
        Minecraft.getInstance().player.getCapability(RunicAgesMagicCapability.Provider.MAGIC_LEVEL).ifPresent(h ->
        {
            h.setMainhandSpell(1);
            Minecraft.getInstance().player.displayClientMessage(Component.literal("You have set your mainhand spell to Air Strike."), false);
            RunicAgesMagicCapability.levelClientUpdate(Minecraft.getInstance().player);
        });
    }

    private void Enchant_Blue()
    {
        Minecraft.getInstance().player.getCapability(RunicAgesMagicCapability.Provider.MAGIC_LEVEL).ifPresent(h ->
        {
            h.setGuiButtonPressed(3);
            RunicAgesMagicCapability.levelClientUpdate(Minecraft.getInstance().player);
        });
    }

    @Override
    public boolean isPauseScreen() {
        return false;
    }

    @Override
    public boolean shouldCloseOnEsc() {
        return true;
    }

    @Override
    public void onClose() {
        this.minecraft.popGuiLayer();
    }
}
