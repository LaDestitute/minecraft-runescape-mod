package com.ladestitute.runicages.commands;

import com.ladestitute.runicages.capability.crafting.RunicAgesCraftingCapability;
import com.ladestitute.runicages.capability.mining.RunicAgesMiningCapability;
import com.ladestitute.runicages.capability.runicextradata.RunicAgesExtraDataCapability;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Player;

public class SetHomeCommand {
    public static void register(CommandDispatcher<CommandSourceStack> dispatcher){
        dispatcher.register(Commands.literal("sethome").executes((command) -> {
            return execute(command);
        }));
    }

    private static int execute(CommandContext<CommandSourceStack> command){
        if(command.getSource().getEntity() instanceof Player player){
            player.getCapability(RunicAgesExtraDataCapability.Provider.RA_EXTRADATA).ifPresent(ed ->
            {
                //Setting home

                        ed.setx(player.blockPosition().getX());
                        ed.sety(player.blockPosition().getY());
                        ed.setz(player.blockPosition().getZ());
                        ed.sethashome(1);
                        RunicAgesExtraDataCapability.levelClientUpdate(player);
                        player.displayClientMessage(Component.literal("You have set your Home Teleport location."), false);
            });
        }
        return Command.SINGLE_SUCCESS;
    }
}