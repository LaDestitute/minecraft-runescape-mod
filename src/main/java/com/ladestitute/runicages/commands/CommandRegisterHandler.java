package com.ladestitute.runicages.commands;

import com.ladestitute.runicages.RunicAgesMain;
import net.minecraftforge.event.RegisterCommandsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = RunicAgesMain.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class CommandRegisterHandler {

    @SubscribeEvent
    public static void registerCommands(RegisterCommandsEvent event){
        MiningLevelCommand.register(event.getDispatcher());
        SmithingLevelCommand.register(event.getDispatcher());
        WoodcuttingLevelCommand.register(event.getDispatcher());
        RunecraftingLevelCommand.register(event.getDispatcher());
        MagicLevelCommand.register(event.getDispatcher());
        CraftingLevelCommand.register(event.getDispatcher());
        SetHomeCommand.register(event.getDispatcher());
        TeleHomeCommand.register(event.getDispatcher());
    }
}