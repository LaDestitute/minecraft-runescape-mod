package com.ladestitute.runicages.commands;

import com.ladestitute.runicages.capability.mining.RunicAgesMiningCapability;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Player;

public class MiningLevelCommand {
    public static void register(CommandDispatcher<CommandSourceStack> dispatcher){
        dispatcher.register(Commands.literal("mininglevel").executes((command) -> {
            return execute(command);
        }));
    }

    private static int execute(CommandContext<CommandSourceStack> command){
        if(command.getSource().getEntity() instanceof Player player){
            player.getCapability(RunicAgesMiningCapability.Provider.MINING_LEVEL).ifPresent(h ->
            {
                player.sendSystemMessage(Component.literal("Your Mining level is: " + h.getMiningLevel() + "/99"));
            });
        }
        return Command.SINGLE_SUCCESS;
    }
}
