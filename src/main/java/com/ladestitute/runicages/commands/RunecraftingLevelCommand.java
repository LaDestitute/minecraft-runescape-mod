package com.ladestitute.runicages.commands;

import com.ladestitute.runicages.capability.runecrafting.RunicAgesRunecraftingCapability;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Player;

public class RunecraftingLevelCommand {
    public static void register(CommandDispatcher<CommandSourceStack> dispatcher){
        dispatcher.register(Commands.literal("runecraftinglevel").executes((command) -> {
            return execute(command);
        }));
    }

    private static int execute(CommandContext<CommandSourceStack> command){
        if(command.getSource().getEntity() instanceof Player player){
            player.getCapability(RunicAgesRunecraftingCapability.Provider.RUNECRAFTING_LEVEL).ifPresent(h ->
            {
                player.sendSystemMessage(Component.literal("Your Runecrafting level is: " + h.getRunecraftingLevel() + "/99"));
            });
        }
        return Command.SINGLE_SUCCESS;
    }
}

