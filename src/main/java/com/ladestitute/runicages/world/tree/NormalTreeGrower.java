package com.ladestitute.runicages.world.tree;

import com.ladestitute.runicages.registry.ConfiguredFeatureInit;
import net.minecraft.resources.ResourceKey;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.block.grower.AbstractTreeGrower;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;

public class NormalTreeGrower extends AbstractTreeGrower
{
    @Override
    protected ResourceKey<ConfiguredFeature<?, ?>> getConfiguredFeature(RandomSource p_225546_1_, boolean p_225546_2_)
    {
        return ConfiguredFeatureInit.NORMAL_TREE;
    }
}
